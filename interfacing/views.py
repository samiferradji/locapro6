import json

from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError
from django.core.serializers.json import DjangoJSONEncoder
from django.db import transaction
from django.shortcuts import render

from common.models import Error
from interfacing import helpers


@login_required(login_url='/login/')
def upload_interfacing_files(request):
    if request.method == 'POST':
        helpers.import_purchase_data(request)
        return render(request, 'interfacing/upload_multiple_files_form.html', {'uploaded': True})
    return render(request, 'interfacing/upload_multiple_files_form.html')


@login_required(login_url='/login/')
def upload_external_transfer(request):
    user = request.user
    if request.method == 'POST' and request.FILES['file']:
        try:
            with transaction.atomic():
                new_obj_id = helpers.import_external_transfer_data(request)
                return render(request, 'upload_form_json.html', {'uploaded': new_obj_id})
        except ValidationError as e:
            Error.objects.create(
                created_by=user,
                error=str(e),
                data=dict(request.POST)
            )
            return render(request, 'upload_form_json.html', {'error_msg': e})
    else:
        return render(request, 'upload_form_json.html')

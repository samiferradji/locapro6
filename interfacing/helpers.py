import json
import time
from dbfread import DBF
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType

from django.core.exceptions import ObjectDoesNotExist, ValidationError, PermissionDenied
from django.db import transaction

from common.models import CompanyProfile, UserProfile
from locapro6.settings import LOG_ROOT
from common import parameters as common_params
from wms import parameters as wms_params
from interfacing import parameters as interfacing_params
from common.models import ArticleCategory
from wms.models import PharmaceuticalSupplier, PharmaceuticalInvoice, Purchase, Product, Tva, Laboratory, \
    PharmaceuticalForm, Dci, Emplacement, Batch, Inventory, ExternalTransfer, ExternalTransferDetails, \
    ProductStatus, PurchaseDetail


def import_purchase_data(request):
    """
    Import purchase data from Pharmnet files
    """

    user = request.user
    current_company = UserProfile.objects.get(user=user).company
    company_profile = CompanyProfile.objects.get(company=current_company)
    default_article_category = ArticleCategory.objects.filter(company=current_company).order_by('created_date').first()
    receipt_content_type = ContentType.objects.get_for_model(Purchase)
    receipt_details_content_type = ContentType.objects.get_for_model(PurchaseDetail)
    default_product_status_id = wms_params.get_default_product_status_id(request.user)
    global_start_time = time.time()
    log_list = []
    try:
        for file in request.FILES.getlist('files'):
            if file.name == "IEACH00.DBF":
                start_time = time.time()
                with open(LOG_ROOT + '/' + file.name, 'wb+') as headers:
                    for chunk in file.chunks():
                        headers.write(chunk)
                log_dict = {'text': 'File IEACH00.DBF copied', 'execution_time': (time.time() - start_time) * 1000}
                log_list.append(log_dict)
            elif file.name == "ILACH00.DBF":
                start_time = time.time()
                with open(LOG_ROOT + '/' + file.name, 'wb+') as details:
                    for chunk in file.chunks():
                        details.write(chunk)
                log_dict = {'text': 'File ILACH00.DBF copied', 'execution_time': (time.time() - start_time) * 1000}
                log_list.append(log_dict)
        start_time = time.time()
        purchase_headers = DBF(LOG_ROOT + '/' + "IEACH00.DBF", encoding='CP850')
        purchase_details = DBF(LOG_ROOT + '/' + "ILACH00.DBF", encoding='CP850')
        log_dict = {'text': 'converting DBF files', 'execution_time': (time.time() - start_time) * 1000}
        log_list.append(log_dict)
        with transaction.atomic():
            if current_company.group_mode and current_company.master_company_id != current_company.id:
                # In this case reference tables are supposed to be already created by replication or supposed
                # to already exist if a unique server is used for multiple companies
                for data in purchase_headers:
                    new_supplier = PharmaceuticalSupplier.objects.get(code__iexact=data['ECFRS'],
                                                                      company=current_company.master_company)
                    try:
                        start_time = time.time()
                        Purchase.objects.get(
                            receipt_number__iexact='{}X{:0>5}/{}'.format(current_company.prefix, data['ENBR'],
                                                                         common_params.CURRENT_YEAR_ABBREVIATION),
                            receipt_date=data['EDBR'],
                        )
                        log_dict = {'text': 'Purchase exists, GET', 'execution_time': (time.time() - start_time) * 1000}
                        log_list.append(log_dict)

                    except ObjectDoesNotExist:
                        try:
                            start_time = time.time()
                            new_invoice = PharmaceuticalInvoice.objects.get(
                                supplier=new_supplier,
                                invoice_number__iexact=data['ENFACT'],
                                invoice_date=data['EDFACT'],
                            )
                            log_dict = {'text': 'Invoice exists, GET',
                                        'execution_time': (time.time() - start_time) * 1000}
                            log_list.append(log_dict)
                        except ObjectDoesNotExist:
                            start_time = time.time()
                            new_invoice = PharmaceuticalInvoice(
                                supplier=new_supplier,
                                invoice_number=data['ENFACT'],
                                invoice_date=data['EDFACT'],
                                value_ht=data['ETOTHT'],
                                discount_on_invoice=0,
                                value_ht_net=data['ETOTHT'],
                                value_tva=data['ETOTTVA'],
                                value_ttc=data['ETOTTTC'],
                                payment_date=interfacing_params.IMPORT_DEFAULT_INVOICE_PAYMENT_DATE,
                                document_status_id=interfacing_params.IMPORT_DEFAULT_INVOICE_STATUS,
                                created_by=request.user)
                            new_invoice.save()
                            log_dict = {'text': 'New invoice created',
                                        'execution_time': (time.time() - start_time) * 1000}
                            log_list.append(log_dict)
                        start_time = time.time()
                        new_receipt = Purchase(
                            invoice=new_invoice,
                            receipt_number='{}X{:0>5}/{}'.format(current_company.prefix, data['ENBR'],
                                                                 common_params.CURRENT_YEAR_ABBREVIATION),
                            receipt_date=data['EDBR'],
                            document_status_id=wms_params.DOCUMENT_CREATED_ID,
                            observation=interfacing_params.IMPORT_DEFAULT_RECEIPT_OBSERVATION,
                            created_by=request.user,
                            flew_type_id=interfacing_params.IMPORT_PURCHASE_FLOW_TYPE_ID
                        )
                        new_receipt.save()
                        log_dict = {'text': 'New purchase created', 'execution_time': (time.time() - start_time) * 1000}
                        log_list.append(log_dict)

                        for line in purchase_details:
                            if line['LNBR'] == data['ENBR']:
                                # TVA
                                # !!! TVA IDs must be the same with PHARMNET TVA's IDs, or a mapping table must be used
                                new_tva = Tva.objects.get(id=interfacing_params.IMPORT_TVA_MAPPING[line['LCODTVA']])
                                new_product = Product.objects.get(code__iexact=line['LCPROD'],
                                                                  company=current_company.master_company)
                                new_batch = Batch(
                                    content_type=receipt_content_type,
                                    object_id=new_receipt.id,
                                    product=new_product,
                                    supplier=new_supplier,
                                    batch_number=line['LNLOT'],
                                    expiration_date=line['LPEREMP'],
                                    packaging=line['LCOLISA'],
                                    purchase_pu_ht=line['LPVU1'],
                                    purchase_pu_ht_net=line['LPVU2'],
                                    unit_discount_percentage=0,
                                    unit_discount_value=0,
                                    tva=new_tva,
                                    selling_pu_ht=line['LPVU3'],
                                    selling_pu_ttc=line['LPVU3'],
                                    ppa_ht=line['LPVU4A'],
                                    shp=line['LSHP4'],
                                    created_by=request.user
                                )
                                new_batch.save()
                                log_dict = {'text': 'New Batch {} created'.format(new_batch),
                                            'execution_time': (time.time() - start_time) * 1000}
                                log_list.append(log_dict)
                                new_purchase_line = PurchaseDetail(
                                    batch=new_batch,
                                    parent=new_receipt,
                                    quantity=line['LQTE'],
                                    emplacement=company_profile.pharmaceutical_purchase_reception_emplacement,
                                    created_by=request.user
                                )
                                new_purchase_line.save()
                                log_dict = {'text': 'New purchase_line {} created'.format(new_purchase_line),
                                            'execution_time': (time.time() - start_time) * 1000}
                                log_list.append(log_dict)
                                # new_inventory_line = Inventory(
                                #     content_type=receipt_details_content_type,
                                #     object_id=new_purchase_line.id,
                                #     created_by=request.user,
                                #     batch=new_batch,
                                #     transaction='Achat Importé',
                                #     quantity=new_purchase_line.quantity,
                                #     product_status_id=default_product_status_id,
                                #     emplacement=new_purchase_line.emplacement,
                                #     sent=True
                                # )
                                # new_inventory_line.save()
                                log_list.append(log_dict)
                        new_receipt.document_status_id = interfacing_params.IMPORT_DEFAULT_RECEIPT_STATUS
                        new_receipt.save()
            else:
                for data in purchase_headers:
                    try:
                        start_time = time.time()
                        new_supplier = PharmaceuticalSupplier.objects.get(code__iexact=data['ECFRS'],
                                                                          company=current_company)
                        log_dict = {'text': 'Supplier exists, GET', 'execution_time': (time.time() - start_time) * 1000}
                        log_list.append(log_dict)
                    except ObjectDoesNotExist:
                        start_time = time.time()
                        new_supplier = PharmaceuticalSupplier(code=data['ECFRS'], supplier=data['ELFRS'],
                                                              created_by=request.user)
                        new_supplier.save()
                        log_dict = {'text': 'New supplier created', 'execution_time': (time.time() - start_time) * 1000}
                        log_list.append(log_dict)
                    try:
                        start_time = time.time()
                        Purchase.objects.get(
                            receipt_number__iexact='{}X{:0>5}/{}'.format(current_company.prefix, data['ENBR'],
                                                                         common_params.CURRENT_YEAR_ABBREVIATION),
                            receipt_date=data['EDBR'],
                        )
                        log_dict = {'text': 'Purchase exists, GET', 'execution_time': (time.time() - start_time) * 1000}
                        log_list.append(log_dict)

                    except ObjectDoesNotExist:
                        try:
                            start_time = time.time()
                            new_invoice = PharmaceuticalInvoice.objects.get(
                                supplier=new_supplier,
                                invoice_number__iexact=data['ENFACT'],
                                invoice_date=data['EDFACT'],
                            )
                            log_dict = {'text': 'Invoice exists, GET',
                                        'execution_time': (time.time() - start_time) * 1000}
                            log_list.append(log_dict)
                        except ObjectDoesNotExist:
                            start_time = time.time()
                            new_invoice = PharmaceuticalInvoice(
                                supplier=new_supplier,
                                invoice_number=data['ENFACT'],
                                invoice_date=data['EDFACT'],
                                value_ht=data['ETOTHT'],
                                discount_on_invoice=0,
                                value_ht_net=data['ETOTHT'],
                                value_tva=data['ETOTTVA'],
                                value_ttc=data['ETOTTTC'],
                                payment_date=interfacing_params.IMPORT_DEFAULT_INVOICE_PAYMENT_DATE,
                                document_status_id=interfacing_params.IMPORT_DEFAULT_INVOICE_STATUS,
                                created_by=request.user)
                            new_invoice.save()
                            log_dict = {'text': 'New invoice created',
                                        'execution_time': (time.time() - start_time) * 1000}
                            log_list.append(log_dict)
                        start_time = time.time()
                        new_receipt = Purchase(
                            invoice=new_invoice,
                            receipt_number='{}X{:0>5}/{}'.format(current_company.prefix, data['ENBR'],
                                                                 common_params.CURRENT_YEAR_ABBREVIATION),
                            receipt_date=data['EDBR'],
                            document_status_id=wms_params.DOCUMENT_CREATED_ID,
                            observation=interfacing_params.IMPORT_DEFAULT_RECEIPT_OBSERVATION,
                            created_by=request.user,
                            flew_type_id=interfacing_params.IMPORT_PURCHASE_FLOW_TYPE_ID
                        )
                        new_receipt.save()
                        log_dict = {'text': 'New purchase created', 'execution_time': (time.time() - start_time) * 1000}
                        log_list.append(log_dict)

                        for line in purchase_details:

                            if line['LNBR'] == data['ENBR']:
                                # TVA
                                # !!! TVA IDs must be the same with PHARMNET TVA's IDs, or a mapping table must be used
                                try:
                                    start_time = time.time()
                                    new_tva = Tva.objects.get(id=interfacing_params.IMPORT_TVA_MAPPING[line['LCODTVA']])
                                    log_dict = {'text': 'TVA exists, GET',
                                                'execution_time': (time.time() - start_time) * 1000}
                                    log_list.append(log_dict)
                                except ObjectDoesNotExist as e:
                                    raise e
                                try:
                                    start_time = time.time()
                                    new_product = Product.objects.get(code__iexact=line['LCPROD'],
                                                                      company=current_company)
                                    log_dict = {'text': 'Product exists, GET',
                                                'execution_time': (time.time() - start_time) * 1000}
                                    log_list.append(log_dict)
                                except ObjectDoesNotExist:
                                    # DCIs
                                    try:
                                        start_time = time.time()
                                        new_dci = Dci.objects.get(code__iexact=line['LCDCI'], company=current_company)
                                        log_dict = {'text': 'DCI exists, GET',
                                                    'execution_time': (time.time() - start_time) * 1000}
                                        log_list.append(log_dict)
                                    except ObjectDoesNotExist:
                                        start_time = time.time()
                                        new_dci = Dci(code=line['LCDCI'], designation=line['LDCI'],
                                                      created_by=request.user)
                                        new_dci.save()
                                        log_dict = {'text': 'New DCI created',
                                                    'execution_time': (time.time() - start_time) * 1000}
                                        log_list.append(log_dict)

                                    # Pharmaceutical forms
                                    try:
                                        start_time = time.time()
                                        new_pharmaceutical_form = PharmaceuticalForm.objects.get(
                                            designation=line['LFORME'], company=current_company)
                                        log_dict = {'text': 'Pharmaceutical Form exists, GET',
                                                    'execution_time': (time.time() - start_time) * 1000}
                                        log_list.append(log_dict)
                                    except ObjectDoesNotExist:
                                        start_time = time.time()
                                        new_pharmaceutical_form = PharmaceuticalForm(designation=line['LFORME'],
                                                                                     short_designation=line['LFORME'][
                                                                                                       0:5],
                                                                                     created_by=request.user)
                                        new_pharmaceutical_form.save()
                                        log_dict = {'text': 'New Pharmaceutical Form created',
                                                    'execution_time': (time.time() - start_time) * 1000}
                                        log_list.append(log_dict)

                                    # Laboratories
                                    try:
                                        start_time = time.time()
                                        new_laboratory = Laboratory.objects.get(code__iexact=line['LFRS1'],
                                                                                company=current_company)
                                        log_dict = {'text': 'Laboratory {} exists, GET'.format(new_laboratory),
                                                    'execution_time': (time.time() - start_time) * 1000}
                                        log_list.append(log_dict)
                                    except ObjectDoesNotExist:
                                        start_time = time.time()
                                        new_laboratory = Laboratory(designation=line['LLIBFRS1'], code=line['LFRS1'],
                                                                    created_by=request.user)
                                        new_laboratory.save()
                                        log_dict = {'text': 'New Laboratory {} created'.format(new_laboratory),
                                                    'execution_time': (time.time() - start_time) * 1000}
                                        log_list.append(log_dict)
                                    # Picking emplacement
                                    try:
                                        start_time = time.time()
                                        new_emplacement = Emplacement.objects.get(
                                            emplacement__iexact="{}.{}".format(line['LDEPOT'], line['LSDEPOT']),
                                            company=current_company)
                                        log_dict = {'text': 'Emplacament {} exists, GET'.format(new_emplacement),
                                                    'execution_time': (time.time() - start_time) * 1000}
                                        log_list.append(log_dict)
                                    except ObjectDoesNotExist:
                                        start_time = time.time()
                                        picking_store = company_profile.picking_store
                                        new_emplacement = Emplacement(
                                            emplacement="{}.{}".format(line['LDEPOT'], line['LSDEPOT']),
                                            store=picking_store,
                                            storage_type=picking_store.storage_type,
                                            created_by=request.user)
                                        new_emplacement.save()
                                        log_dict = {'text': 'New Emplacement {} created'.format(new_emplacement),
                                                    'execution_time': (time.time() - start_time) * 1000}
                                        log_list.append(log_dict)
                                    start_time = time.time()
                                    new_product = Product(
                                        code=line['LCPROD'],
                                        product=line['LCMRC'],
                                        category=default_article_category,
                                        dci=new_dci,
                                        pharmaceutical_form=new_pharmaceutical_form,
                                        dosage=line['LDOSA'],
                                        laboratory=new_laboratory,
                                        conditioning='',
                                        tva=new_tva,
                                        picking_emplacement=new_emplacement,
                                        created_by=request.user

                                    )
                                    new_product.save()
                                    log_dict = {'text': 'New Product {} created'.format(new_product),
                                                'execution_time': (time.time() - start_time) * 1000}
                                    log_list.append(log_dict)
                                start_time = time.time()
                                new_batch = Batch(
                                    content_type=receipt_content_type,
                                    object_id=new_receipt.id,
                                    product=new_product,
                                    supplier=new_supplier,
                                    batch_number=line['LNLOT'],
                                    expiration_date=line['LPEREMP'],
                                    packaging=line['LCOLISA'],
                                    purchase_pu_ht=line['LPVU1'],
                                    purchase_pu_ht_net=line['LPVU2'],
                                    unit_discount_percentage=0,
                                    unit_discount_value=0,
                                    tva=new_tva,
                                    selling_pu_ht=line['LPVU3'],
                                    selling_pu_ttc=line['LPVU3'],
                                    ppa_ht=line['LPVU4A'],
                                    shp=line['LSHP4'],
                                    created_by=request.user
                                )
                                new_batch.save()
                                log_dict = {'text': 'New Batch {} created'.format(new_batch),
                                            'execution_time': (time.time() - start_time) * 1000}
                                log_list.append(log_dict)
                                new_purchase_line = PurchaseDetail(
                                    batch=new_batch,
                                    parent=new_receipt,
                                    quantity=line['LQTE'],
                                    emplacement=company_profile.pharmaceutical_purchase_reception_emplacement,
                                    created_by=request.user
                                )
                                new_purchase_line.save()
                                log_dict = {'text': 'New purchase_line {} created'.format(new_purchase_line),
                                            'execution_time': (time.time() - start_time) * 1000}
                                log_list.append(log_dict)
                                # new_inventory_line = Inventory(
                                #     content_type=receipt_details_content_type,
                                #     object_id=new_purchase_line.id,
                                #     created_by=request.user,
                                #     batch=new_batch,
                                #     transaction='Achat Importé',
                                #     quantity=new_purchase_line.quantity,
                                #     product_status_id=default_product_status_id,
                                #     emplacement=new_purchase_line.emplacement,
                                #     sent=True
                                # )
                                # new_inventory_line.save()
                                # log_dict = {'text': 'New inventory_line {} created'.format(new_inventory_line),
                                #             'execution_time': (time.time() - start_time) * 1000}
                                # log_list.append(log_dict)
                        new_receipt.document_status_id = interfacing_params.IMPORT_DEFAULT_RECEIPT_STATUS
                        new_receipt.save()
    except Exception as e:
        raise e
    finally:
        with open(LOG_ROOT + '/' + "import_log.txt", "w+") as log_file:
            for log in log_list:
                log_file.write(log['text'] + '..........' + str(log['execution_time']) + ' ms')
                log_file.write('\n')
            log_file.write('Total import time ..........' + str((time.time() - global_start_time) * 1000))


def import_external_transfer_data(request):
    user = request.user
    current_company = UserProfile.objects.get(user=user).company
    company_profile = CompanyProfile.objects.get(company=current_company)
    external_transfer_content_type = ContentType.objects.get_for_model(ExternalTransfer)
    file = request.FILES['file']
    with open(LOG_ROOT + '/' + file.name, 'wb+') as headers:
        for chunk in file.chunks():
            headers.write(chunk)
    with open(LOG_ROOT + '/' + file.name, 'r', encoding='utf-8') as _file_data:
        _data_dict = json.load(_file_data)

    _header = _data_dict['header'][0]
    _body = _data_dict['body']
    if _header['document_status_id'] != 3:
        raise ValidationError(
            "Le transfert {} n'est pas en statut 'Expédié' par la filiale d'origine".format(_header['id']))

    elif _header['to_company_id'] != current_company.id:
        raise ValidationError('Le destinataire de ce bon n\'est pas {}'.format(current_company))

    elif ExternalTransfer.objects.filter(id=_header['id']).exists():
        raise ValidationError('Le transfert entre filiales {} est déja importé'.format(_header['id']))

    else:
        new_external_transfert = ExternalTransfer(
            created_by=user,
            id=_header['id'],
            flew_type_id=_header['flew_type_id'],
            from_company_id=_header['from_company_id'],
            to_company_id=_header['to_company_id'],
            document_status_id=_header['document_status_id'],
            observation=_header['observation']
        )
        new_external_transfert.save()
        if current_company.group_mode:
            replicator_user = User.objects.get(username='replicator')
            for line in _body:
                if not ProductStatus.objects.filter(id=line['status_id']).exists():
                    new_product_status = ProductStatus(
                        id=line['status_id'],
                        designation=line['status__designation'],
                        created_by=replicator_user
                    )
                    new_product_status.save()

                if not PharmaceuticalSupplier.objects.filter(id=line['batch__supplier_id']).exists():
                    new_supplier = PharmaceuticalSupplier(
                        id=line['batch__supplier_id'],
                        designation=line['batch__supplier__supplier'],
                        created_by=replicator_user
                    )
                    new_supplier.save()

                if not ArticleCategory.objects.filter(id=line['batch__product__category_id']).exists():
                    new_article_category = ArticleCategory(
                        id=line['batch__product__category_id'],
                        code=line['batch__product__category__code'],
                        designation=line['batch__product__category__designation'],
                        created_by=replicator_user
                    )
                    new_article_category.save()

                if not PharmaceuticalForm.objects.filter(id=line['batch__product__pharmaceutical_form_id']).exists():
                    new_pharmaceutical_forme = PharmaceuticalForm(
                        created_by=replicator_user,
                        id=line['batch__product__pharmaceutical_form_id'],
                        short_designation=line['batch__product__pharmaceutical_form__short_designation'],
                        designation=line['batch__product__pharmaceutical_form__designation'],
                    )
                    new_pharmaceutical_forme.save()

                if not Laboratory.objects.filter(id=line['batch__product__laboratory_id']).exists():
                    new_laboratory = Laboratory(
                        created_by=replicator_user,
                        id=line['batch__product__laboratory_id'],
                        code=line['batch__product__laboratory__code'],
                        designation=line['batch__product__laboratory__designation'],
                    )
                    new_laboratory.save()

                if not Dci.objects.filter(id=line['batch__product__dci_id']).exists():
                    new_dci = Dci(
                        created_by=replicator_user,
                        id=line['batch__product__dci_id'],
                        code=line['batch__product__dci__code'],
                        designation=line['batch__product__dci__designation'],
                    )
                    new_dci.save()

                if not Tva.objects.filter(id=line['batch__product__tva_id']).exists():
                    new_tva = Tva(
                        created_by=replicator_user,
                        id=line['batch__product__tva_id'],
                        tva_rate=line['batch__product__tva__tva_rate'],
                    )
                    new_tva.save()

                if not Product.objects.filter(id=line['batch__product_id']).exists():
                    new_product = Product(
                        created_by=replicator_user,
                        id=line['batch__product_id'],
                        code=line['batch__product__code'],
                        product=line['batch__product__product'],
                        dosage=line['batch__product__dosage'],
                        conditioning=line['batch__product__conditioning'],
                        weight=line['batch__product__weight'],
                        volume=line['batch__product__volume'],
                        package_weight=line['batch__product__package_weight'],
                        thermal_sensibility=line['batch__product__thermal_sensibility'],
                        psychotropic=line['batch__product__psychotropic'],
                        active=line['batch__product__active']
                    )
                    new_product.save()

                if not Batch.objects.filter(id=line['batch_id']).exists():
                    new_batch = Batch(
                        id=line['batch_id'],
                        created_by=user,
                        content_type=external_transfer_content_type,
                        object_id=_header['id'],
                        product_id=line['batch__product_id'],
                        batch_number=line['batch__batch_number'],
                        expiration_date=line['batch__expiration_date'],
                        packaging=line['batch__packaging'],
                        purchase_pu_ht=line['batch__purchase_pu_ht'],
                        unit_discount_percentage=line['batch__unit_discount_percentage'],
                        unit_discount_value=line['batch__unit_discount_value'],
                        purchase_pu_ht_net=line['batch__purchase_pu_ht_net'],
                        selling_pu_ht=line['batch__selling_pu_ht'],
                        selling_pu_ttc=line['batch__selling_pu_ttc'],
                        shp=line['batch__shp'],
                        ppa_ht=line['batch__ppa_ht'],
                        tva_id=line['batch__tva_id'],
                        unit_weight=line['batch__unit_weight'],
                        unit_volume=line['batch__unit_volume'],
                        packaged_weight=line['batch__packaged_weight'],
                        supplier_id=line['batch__supplier_id']
                    )
                    new_batch.save()

                new_line = ExternalTransferDetails(
                    created_by=user,
                    id=line['id'],
                    batch_id=line['batch_id'],
                    emplacement=company_profile.external_transfer_reception_emplacement,
                    status_id=line['status_id'],
                    parent_id=line['parent_id'],
                    quantity=line['quantity'],
                )
                new_line.save()
            return _header['id']
        else:
            for line in _body:
                if not ProductStatus.objects.filter(id=line['status_id']).exists():
                    new_product_status = ProductStatus(
                        designation=line['status__designation'],
                        created_by=user
                    )
                    new_product_status.save()

                if not PharmaceuticalSupplier.objects.filter(id=line['batch__supplier_id']).exists():
                    new_supplier = PharmaceuticalSupplier(
                        designation=line['batch__supplier__supplier'],
                        created_by=user
                    )
                    new_supplier.save()

                if not ArticleCategory.objects.filter(id=line['batch__product__category_id']).exists() and \
                        line['batch__product__category__code'] is not None:
                    new_article_category = ArticleCategory(
                        code=line['batch__product__category__code'],
                        designation=line['batch__product__category__designation'],
                        created_by=user
                    )
                    new_article_category.save()

                if not PharmaceuticalForm.objects.filter(
                        id=line['batch__product__pharmaceutical_form_id']
                ).exists() and line['batch__product__pharmaceutical_form_id'] is not None:
                    new_pharmaceutical_forme = PharmaceuticalForm(
                        created_by=user,
                        short_designation=line['batch__product__pharmaceutical_form__short_designation'],
                        designation=line['batch__product__pharmaceutical_form__designation'],
                    )
                    new_pharmaceutical_forme.save()

                if not Laboratory.objects.filter(id=line['batch__product__laboratory_id']).exists() and \
                        line['batch__product__laboratory_id'] is not None:
                    new_laboratory = Laboratory(
                        created_by=user,
                        code=line['batch__product__laboratory__code'],
                        designation=line['batch__product__laboratory__designation'],
                    )
                    new_laboratory.save()

                if not Dci.objects.filter(id=line['batch__product__dci_id']).exists() and \
                        line['batch__product__dci_id'] is not None:
                    new_dci = Dci(
                        created_by=user,
                        code=line['batch__product__dci__code'],
                        designation=line['batch__product__dci__designation'],
                    )
                    new_dci.save()

                if not Tva.objects.filter(id=line['batch__product__tva_id']).exists():
                    new_tva = Tva(
                        created_by=user,
                        id=line['batch__product__tva_id'],
                        tva_rate=line['batch__product__tva__tva_rate'],
                    )
                    new_tva.save()

                if not Product.objects.filter(id=line['batch__product_id']).exists():
                    new_product = Product(
                        created_by=user,
                        code=line['batch__product__code'],
                        product=line['batch__product__product'],
                        dosage=line['batch__product__dosage'],
                        conditioning=line['batch__product__conditioning'],
                        weight=line['batch__product__weight'],
                        volume=line['batch__product__volume'],
                        package_weight=line['batch__product__package_weight'],
                        thermal_sensibility=line['batch__product__thermal_sensibility'],
                        psychotropic=line['batch__product__psychotropic'],
                        active=line['batch__product__active'],
                    )
                    new_product.save()

                new_batch = Batch(
                    created_by=user,
                    content_type=external_transfer_content_type,
                    object_id=_header['id'],
                    product_id=line['batch__product_id'],
                    batch_number=line['batch__batch_number'],
                    expiration_date=line['batch__expiration_date'],
                    packaging=line['batch__packaging'],
                    purchase_pu_ht=line['batch__purchase_pu_ht'],
                    unit_discount_percentage=line['batch__unit_discount_percentage'],
                    unit_discount_value=line['batch__unit_discount_value'],
                    purchase_pu_ht_net=line['batch__purchase_pu_ht_net'],
                    selling_pu_ht=line['batch__selling_pu_ht'],
                    selling_pu_ttc=line['batch__selling_pu_ttc'],
                    shp=line['batch__shp'],
                    ppa_ht=line['batch__ppa_ht'],
                    tva_id=line['batch__tva_id'],
                    unit_weight=line['batch__unit_weight'],
                    unit_volume=line['batch__unit_volume'],
                    packaged_weight=line['batch__packaged_weight'],
                    supplier_id=line['batch__supplier_id']
                )
                new_batch.save()

                new_line = ExternalTransferDetails(
                    created_by=user,
                    batch=new_batch,
                    emplacement=company_profile.external_transfer_reception_emplacement,
                    status_id=line['status_id'],
                    parent_id=line['parent_id'],
                    quantity=line['quantity'],
                )
                new_line.save()
            return _header['id']

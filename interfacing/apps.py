from django.apps import AppConfig


class InterfacingConfig(AppConfig):
    name = 'interfacing'

from django.contrib import admin
from .models import Article, ArticleStatus, AffectationArticleToAffiliate, AffectationArticleToEmployee, \
    AffectationArticleToStatus, InvestmentJournal, Acquisition, DetailsAcquisition, Invoice, Reform, ReformDetails, \
    ExternalCession, ExternalCessionDetails
from common.admin import AdminBase


class ArticleAdmin(AdminBase):
    list_display = [
        'id',
        'article_code',
        'designation',
        'category',
        'accounting_code',
        'created_by',
        'created_date',
        'modified_date'
    ]
    search_fields = [
        'id',
        'article_code',
        'designation',
        'category__designation',
        'accounting_code__designation'
    ]
    list_select_related = ['category', 'created_by', 'accounting_code']


class InvoiceAdmin(AdminBase):
    list_display = ['id',
                    'supplier',
                    'company',
                    'invoice_number',
                    'invoice_date',
                    'value_ht',
                    'discount_on_invoice',
                    'value_tva',
                    'value_ttc',
                    'created_by',
                    'created_date',
                    ]
    search_fields = ['id',
                     'supplier__supplier',
                     'company__filiale',
                     'invoice_number',
                     'invoice_date',
                     'value_ht',
                     'discount_on_invoice',
                     'value_tva',
                     'value_ttc',
                     ]
    list_select_related = ['supplier', 'created_by']


class AcquisitionAdmin(AdminBase):
    list_display = ['id',
                    'invoice',
                    'operating_date',
                    'observation',
                    'created_by',
                    'created_date',
                    'modified_date'
                    ]
    search_fields = ['id',
                     'invoice__invoice_number',
                     'invoice__invoice_date',
                     'invoice__supplier__supplier',
                     'operating_date',
                     'observation',
                     ]
    list_select_related = ['created_by', 'invoice']


class DetailsAcquisitionAdmin(AdminBase):
    fieldsets = (
        (None, {
            'fields': [
                'parent',
                'article',
                'marque',
                'type',
                'serial_number',
                'code_barre_id',

            ]
        }),
        ('Informations financières', {
            'classes': (),
            'fields': ('pu_ht',
                       'unit_discount_perc',
                       'unit_discount_value',
                       'pu_ht_net',
                       'tva',
                       'pu_ttc',),
        }),
    )
    list_display = ['id',
                    'article',
                    'marque',
                    'type',
                    'serial_number',
                    'code_barre_id',
                    'pu_ht',
                    'unit_discount_perc',
                    'unit_discount_value',
                    'pu_ht_net',
                    'tva',
                    'pu_ttc',
                    ]
    readonly_fields = ['pu_ht_net', 'pu_ttc', 'unit_discount_value']
    search_fields = ['id',
                     'article__article_code',
                     'article__designation',
                     'article__category__designation',
                     'marque',
                     'type',
                     'serial_number',
                     'code_barre_id',
                     'pu_ht',
                     'unit_discount_perc',
                     'pu_ht_net',
                     'tva__tva_rate',
                     'pu_ttc',
                     ]
    list_select_related = ['created_by', 'article', 'article__category']


class ReformAdmin(AdminBase):
    list_display = ['id',
                    'operating_date',
                    'observation',
                    'created_by',
                    'created_date',
                    'modified_date'
                    ]
    search_fields = ['id',
                     'operating_date',
                     'observation',
                     ]
    list_select_related = ['created_by']


class DetailsReformAdmin(AdminBase):
    list_display = ['id',
                    'parent',
                    'investment'
                    ]
    search_fields = ['id',
                     'parent__id',
                     'investment__article__article_code',
                     'investment__article__designation',
                     'investment__article__category__designation',
                     'investment__type',
                     'investment__marque',
                     'investment__code_barre_id',
                     ]
    list_select_related = ['investment', 'parent', 'investment__article', 'investment__article__category']


class ExternalCessionAdmin(AdminBase):
    list_display = ['id',
                    'third_party',
                    'operating_date',
                    'observation',
                    'created_by',
                    'created_date',
                    'modified_date'
                    ]
    search_fields = ['id',
                     'operating_date',
                     'observation',
                     ]
    list_select_related = ['created_by']


class ExternalCessionDetailsAdmin(AdminBase):
    list_display = ['id',
                    'parent',
                    'investment',
                    ]
    search_fields = ['id',
                     'parent__id',
                     'investment__article__article_code',
                     'investment__article__designation',
                     'investment__article__category__designation',
                     'investment__type',
                     'investment__marque',
                     'investment__code_barre_id',
                     ]
    list_select_related = ['parent', 'investment', 'investment__article', 'investment__article__category']


class ArticleToEmployeeAdmin(AdminBase):
    list_display = ['id',
                    'article',
                    'employee',
                    'effect_date',
                    'observation',
                    'created_by',
                    'terminated',
                    'terminated_date',
                    'terminated_by']
    search_fields = ['id',
                     'article__article__designation',
                     'employee__name',
                     'date_effet',
                     'observation',
                     'terminated',
                     'terminated_date',
                     'terminated_by__username', ]
    list_select_related = ['article', 'article__article', 'employee', 'terminated_by', 'created_by']


class ArticleToStatusAdmin(AdminBase):
    list_display = ['id',
                    'article',
                    'status',
                    'effect_date',
                    'observation',
                    'terminated',
                    'terminated_date',
                    'terminated_by', ]
    search_fields = ['id',
                     'article__article__designation',
                     'status__status',
                     'effect_date',
                     'observation',
                     'terminated',
                     'terminated_date',
                     'terminated_by__username', ]
    list_select_related = ['article', 'article__article', 'status', 'terminated_by', 'created_by']


class ArticleToAffiliateAdmin(AdminBase):
    list_display = ['id',
                    'article',
                    'user_company',
                    'effect_date',
                    'observation',
                    'terminated',
                    'terminated_date',
                    'terminated_by', ]

    search_fields = ['id',
                     'article__article__designation',
                     'user_company__designation',
                     'effect_date',
                     'observation',
                     'terminated',
                     'terminated_date',
                     'terminated_by__username', ]
    list_select_related = ['article', 'article__article', 'user_company', 'terminated_by', 'created_by']


class ArticleStatusAdmin(AdminBase):
    search_fields = ['status']


admin.site.register(Article, ArticleAdmin)
admin.site.register(Invoice, InvoiceAdmin)
admin.site.register(Acquisition, AcquisitionAdmin)
admin.site.register(DetailsAcquisition, DetailsAcquisitionAdmin)
admin.site.register(Reform, ReformAdmin)
admin.site.register(ReformDetails, DetailsReformAdmin)
admin.site.register(ExternalCession, ExternalCessionAdmin)
admin.site.register(ExternalCessionDetails, ExternalCessionDetailsAdmin)
admin.site.register(ArticleStatus, ArticleStatusAdmin)
admin.site.register(AffectationArticleToAffiliate, ArticleToAffiliateAdmin)
admin.site.register(AffectationArticleToStatus, ArticleToStatusAdmin)
admin.site.register(AffectationArticleToEmployee, ArticleToEmployeeAdmin)

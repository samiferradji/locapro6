from django.apps import AppConfig


class ImmobilisationConfig(AppConfig):
    name = 'immobilisation'

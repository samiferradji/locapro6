def get_default_reception_emplacement_id_for_immobilisations_purchase(user):
    """
    This function returns the default emplacement when creating a new articles acquisition
    """
    from common.models import CompanyProfile
    from common.models import UserProfile
    current_company = UserProfile.objects.get(user=user).company
    return CompanyProfile.objects.get(company=current_company).investment_reception_emplacement_id

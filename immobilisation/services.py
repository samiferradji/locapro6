import csv

from django.db.models import OuterRef, Subquery, Q
from django.utils import timezone
from django.http import HttpResponse
from django.views.generic import View

from common.models import UserProfile
from common.services import StandardTable
from .models import Article, AffectationArticleToEmployee, AffectationArticleToAffiliate, AffectationArticleToStatus, \
    ArticleStatus, InvestmentJournal, Invoice, DetailsAcquisitionTempo, DetailsAcquisition, Acquisition, \
    ReformDetailsTempo, ReformDetails, Reform, ExternalCessionDetailsTempo, ExternalCessionDetails, ExternalCession, \
    MgxEmplacement, AffectationArticleToMgxEmplacement


class ListOfArticles(StandardTable):
    """ Produce a serialized JSON list of Articles """
    model = Article
    model_data = {'enumeration': False, 'sort': ['designation'],
                  'reverse_sort': False, 'text_field': ['article_code', 'designation', 'category__designation']}
    fields = [
        {'name': 'article_code', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'designation', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'category__designation', 'title': 'Famille', 'width': 6, 'column_order': 3, 'search': True,
         'search_type': '__icontains'},
    ]


class ListOfMgxEmplacements(StandardTable):
    """ Produce a serialized JSON list of MGX emplacements """
    model = MgxEmplacement
    model_data = {'enumeration': True, 'sort': ['designation'],
                  'reverse_sort': False, 'text_field': ['code', 'designation']}
    fields = [
        {'name': 'code', 'width': 3, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'designation', 'width': 6, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
    ]


class ListOfInvestment(StandardTable):
    """ Produce a serialized JSON list of Investments """
    model = InvestmentJournal
    model_filter = {'ceded': False}
    model_data = {'enumeration': True, 'sort': ['article_id', 'id'],
                  'reverse_sort': False,
                  'text_field': ['article__designation', 'type', 'marque', 'code_barre_id']}
    fields = [
        {'name': 'article__article_code', 'title': 'Code', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'article__designation', 'title': 'Designation', 'width': 5, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'type', 'title': 'Type', 'width': 3, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'marque', 'width': 3, 'column_order': 3, 'search': True, 'search_type': '__icontains'},
        {'name': 'code_barre_id', 'width': 3, 'column_order': 3, 'search': True, 'search_type': '__iexact'},
        {'name': 'reformed', 'format': 'boolean', 'width': 3, 'column_order': 3, 'search': True,
         'search_type': '__icontains'},
    ]


class ListOfArticlesStatus(StandardTable):
    """ Produce a serialized JSON list of Articles """

    model = ArticleStatus
    model_data = {'enumeration': True, 'sort': ['id'],
                  'reverse_sort': False,
                  'text_field': ['id', 'status']}
    fields = [
        {'name': 'status', 'width': 4, 'column_order': 1, 'search': True,
         'search_type': '__icontains'}
    ]


# Invoices Services
class HistoryOfInvoices(StandardTable):
    """ Produce a serialized JSON list """
    model = Invoice
    model_data = {'enumeration': False, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['supplier__supplier', 'invoice_number', 'invoice_date']}
    fields = [
        {'name': 'id', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'supplier__supplier', 'format': 'text', 'title': 'Fournisseur',
         'width': 7, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'invoice_number', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'invoice_date', 'format': 'date', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'value_ht', 'format': 'money', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'discount_on_invoice', 'format': 'money', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'value_ht_net', 'format': 'money', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'value_tva', 'format': 'money', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'value_ttc', 'format': 'money', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'company__designation', 'title': 'Entrepise', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'created_date', 'format': 'datetime', 'width': False, 'column_order': 1, 'search': False,
         'search_type': '__icontains'},
        {'name': 'created_by__username', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'document_scan', 'format': 'media', 'width': False, 'column_order': 1, 'search': False,
         'search_type': '__icontains'},
    ]


class ListOfInvoices(StandardTable):
    """ Produce a serialized JSON list """
    model = Invoice
    model_data = {'enumeration': False, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['supplier__supplier', 'invoice_number', 'invoice_date']}
    fields = [
        {'name': 'id', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'supplier__supplier', 'format': 'text', 'title': 'Fournisseur',
         'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'invoice_number', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'invoice_date', 'format': 'date', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'value_ht', 'format': 'money', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'discount_on_invoice', 'format': 'money', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'value_ht_net', 'format': 'money', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'value_tva', 'format': 'money', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'value_ttc', 'format': 'money', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
    ]


# Acquisition Services
class ListOfAcquisitionDetailsTempo(StandardTable):
    """ Produce a serialized JSON list """
    model = DetailsAcquisitionTempo
    model_data = {'enumeration': True, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['created_date', 'created_by__username']}
    fields = [

        {'name': 'article__article_code', 'title': 'Code', 'format': 'text', 'width': False, 'column_order': 1,
         'search': True,
         'search_type': '__icontains'},
        {'name': 'article__designation', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'article__category__designation', 'title': 'Famille', 'format': 'text', 'width': False,
         'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'code_barre_id', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__iexact'},
        {'name': 'marque', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'serial_number', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'type', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'pu_ht', 'format': 'money', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'unit_discount_perc', 'format': 'percent', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'pu_ht_net', 'format': 'money', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'tva__tva_rate', 'format': 'percent', 'title': 'TVA (%)', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'pu_ttc', 'format': 'money', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'emplacement__designation', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'employee_affectation__name', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
    ]


class ListOfAcquisitionDetails(StandardTable):
    """ Produce a serialized JSON list """
    model = DetailsAcquisition
    model_data = {'enumeration': True, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['created_date', 'created_by__username']}
    fields = [
        {'name': 'article__article_code', 'title': 'Code', 'format': 'text', 'width': False, 'column_order': 1,
         'search': True,
         'search_type': '__icontains'},
        {'name': 'article__designation', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'article__category__designation', 'title': 'Famille', 'format': 'text', 'width': False,
         'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'code_barre_id', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__iexact'},
        {'name': 'marque', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'serial_number', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'type', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'pu_ht', 'format': 'money', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'unit_discount_perc', 'format': 'percent', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'pu_ht_net', 'format': 'money', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'tva__tva_rate', 'format': 'percent', 'title': 'TVA (%)', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'pu_ttc', 'format': 'money', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
    ]


class ListOfAcquisition(StandardTable):
    """ Produce a serialized JSON list """
    model = Acquisition
    model_data = {'enumeration': False, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['id', 'operating_date']}
    fields = [
        {'name': 'id', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'invoice__invoice_number', 'format': 'text', 'title': 'N° Facture',
         'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'invoice__invoice_date', 'format': 'text', 'title': 'Date Facture',
         'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'invoice__supplier__supplier', 'format': 'text', 'title': 'Fournisseur',
         'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'operating_date', 'format': 'date', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'observation', 'format': 'text', 'width': 6, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'created_date', 'format': 'datetime', 'width': False, 'column_order': 1, 'search': False,
         'search_type': '__icontains'},
        {'name': 'created_by__username', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'document_scan', 'format': 'media', 'width': False, 'column_order': 1, 'search': False,
         'search_type': '__icontains'},
    ]


# Affectation services

class ArticleAffectationsReport(StandardTable):
    def get_queryset(self):
        employee = AffectationArticleToEmployee.objects.filter(
            article_id=OuterRef('id')).filter(terminated=False).order_by('-created_date')
        status = AffectationArticleToStatus.objects.filter(
            article_id=OuterRef('id')).filter(terminated=False).order_by('-created_date')
        user_company = AffectationArticleToAffiliate.objects.filter(
            article_id=OuterRef('id')).filter(terminated=False).order_by('-created_date')
        mgx_emplacement = AffectationArticleToMgxEmplacement.objects.filter(
            article_id=OuterRef('id')).filter(terminated=False).order_by('-created_date')
        queryset = self.model.objects.all().annotate(
            employee=Subquery(employee.values('employee__name')[:1]),
            code_employee=Subquery(employee.values('employee__code')[:1]),
            current_status=Subquery(status.values('status__status')[:1]),
            user_company=Subquery(user_company.values('user_company__designation')[:1]),
            current_emplacement=Subquery(mgx_emplacement.values('mgx_emplacement__designation')[:1])
        ).filter(ceded=False)
        return queryset

    def apply_domain_filter(self, queryset, request):
        user_profile = UserProfile.objects.get(user=request.user)
        user_company_name = user_profile.company.designation
        queryset = queryset.filter(
            Q(company=user_profile.company) | Q(user_company=user_company_name))

        return queryset

    model = InvestmentJournal
    model_data = {'enumeration': True, 'sort': ['article__designation', 'code_barre_id'],
                  'reverse_sort': False, 'text_field': ['article__designation', 'code_barre_id']}
    fields = [
        {
            'search': True,
            'column_order': 1,
            'width': False,
            'search_type': '__icontains',
            'name': 'article__article_code',
            'title': 'Code article',
            'format': 'text',
        },
        {
            'search': True,
            'column_order': 2,
            'width': False,
            'search_type': '__icontains',
            'name': 'article__designation',
            'title': 'Article'
        },
        {
            'search': True,
            'column_order': 3,
            'width': False,
            'search_type': '__icontains',
            'name': 'article__category__designation',
            'title': 'Famille'
        },
        {
            'search': True,
            'column_order': 4,
            'width': False,
            'search_type': '__icontains',
            'name': 'type',
        },
        {
            'search': True,
            'column_order': 5,
            'width': False,
            'search_type': '__icontains',
            'name': 'marque',
        },
        {
            'search': True,
            'column_order': 6,
            'width': False,
            'search_type': '__iexact',
            'name': 'serial_number',
        },
        {
            'search': True,
            'column_order': 7,
            'width': False,
            'search_type': '__icontains',
            'name': 'supplier__supplier',
            'title': 'Fournisseur'},
        {
            'search': True,
            'column_order': 8,
            'width': False,
            'search_type': '__iexact',
            'name': 'code_barre_id',

        },
        {
            'search': True,
            'column_order': 9,
            'width': False,
            'search_type': '__icontains',
            'name': 'current_emplacement',
            'title': 'Emplacement'},
        {
            'search': True,
            'column_order': 9,
            'width': False,
            'search_type': '__icontains',
            'name': 'code_employee',
            'title': 'Code Employé'},
        {
            'search': True,
            'column_order': 9,
            'width': False,
            'search_type': '__icontains',
            'name': 'employee',
            'title': 'Employé'},
        {
            'search': True,
            'column_order': 10,
            'width': False,
            'search_type': '__icontains',
            'name': 'current_status',
            'title': 'Statut'
        },
        {
            'search': True,
            'column_order': 11,
            'width': False,
            'search_type': '__icontains',
            'name': 'user_company',
            'title': 'Utilisé par'
        },
        {
            'search': True,
            'column_order': 12,
            'width': False,
            'search_type': '__icontains',
            'name': 'company__designation',
            'title': 'Propriétaire'
        },
        {
            'search': True,
            'column_order': 13,
            'width': False,
            'search_type': '__icontains',
            'name': 'reformed',
            'format': 'text',
            'title': 'Réformé ?'
        },
    ]


class HistoryArticleToEmployeeAffectation(StandardTable):
    """ Produce a serialized JSON list """
    model = AffectationArticleToEmployee
    model_data = {'enumeration': False, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['article', 'employee']}
    fields = [
        {'name': 'id', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'article__article__designation', 'format': 'text', 'title': 'Article',
         'width': 6, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'article__code_barre_id', 'title': 'Code barre', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__iexact'},
        {'name': 'employee__name', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'employee__structure', 'title': 'Structure', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'employee__direction', 'title': 'Direction', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'effect_date', 'format': 'date', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'observation', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'terminated_date', 'format': 'date', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'terminated_by__username', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'created_date', 'format': 'datetime', 'width': False, 'column_order': 1, 'search': False,
         'search_type': '__icontains'},
        {'name': 'created_by__username', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'document_scan', 'format': 'media', 'width': False, 'column_order': 1, 'search': False,
         'search_type': '__icontains'},
    ]


class HistoryArticleToStatusAffectation(StandardTable):
    """ Produce a serialized JSON list """
    model = AffectationArticleToStatus
    model_data = {'enumeration': False, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['article', 'article']}
    fields = [
        {'name': 'id', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'article__article__designation', 'format': 'text', 'title': 'Article',
         'width': 8, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'article__code_barre_id', 'title': 'Code barre', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__iexact'},
        {'name': 'status__status', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'effect_date', 'format': 'datetime', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'observation', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'created_date', 'format': 'datetime', 'width': False, 'column_order': 1, 'search': False,
         'search_type': '__icontains'},
        {'name': 'created_by__username', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
    ]


class HistoryArticleToCompanyAffectation(StandardTable):
    """ Produce a serialized JSON list """
    model = AffectationArticleToAffiliate
    model_data = {'enumeration': False, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['article', 'article']}
    fields = [
        {'name': 'id', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'article__article__designation', 'format': 'text', 'title': 'Article',
         'width': 8, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'article__code_barre_id', 'title': 'Code barre', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__iexact'},
        {'name': 'user_company__designation', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'effect_date', 'format': 'datetime', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'observation', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'terminated_date', 'format': 'datetime', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'terminated_by__username', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'created_date', 'format': 'datetime', 'width': False, 'column_order': 1, 'search': False,
         'search_type': '__icontains'},
        {'name': 'created_by__username', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'document_scan', 'format': 'media', 'width': False, 'column_order': 1, 'search': False,
         'search_type': '__icontains'},
    ]


class HistoryArticleToCompanyMgxEmplacementAffectation(StandardTable):
    """ Produce a serialized JSON list """
    model = AffectationArticleToMgxEmplacement
    model_data = {'enumeration': False, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['article', 'article']}
    fields = [
        {'name': 'id', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'article__article__designation', 'format': 'text', 'title': 'Article',
         'width': 8, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'article__code_barre_id', 'title': 'Code barre', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__iexact'},
        {'name': 'mgx_emplacement__designation', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'effect_date', 'format': 'datetime', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'observation', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'created_date', 'format': 'datetime', 'width': False, 'column_order': 1, 'search': False,
         'search_type': '__icontains'},
        {'name': 'created_by__username', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},

    ]


# Reform services

class ListOfInvestmentToReform(StandardTable):
    """ Produce a serialized JSON list of Investments """

    def get_queryset(self):
        """
        Get the model data as a default queryset
        """

        queryset = self.model.objects.exclude(
            code_barre_id__in=[c[0] for c in ReformDetailsTempo.objects.values_list('investment__code_barre_id')])
        return queryset

    model = InvestmentJournal
    model_filter = {'ceded': False, 'reformed': False}
    model_data = {'enumeration': True, 'sort': ['article_id', 'id'],
                  'reverse_sort': False,
                  'text_field': ['article__designation', 'type', 'marque', 'code_barre_id']}
    fields = [
        {'name': 'article__article_code', 'title': 'Code', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'article__designation', 'title': 'Designation', 'width': 5, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'type', 'title': 'Type', 'width': 3, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'marque', 'width': 3, 'column_order': 3, 'search': True, 'search_type': '__icontains'},
        {'name': 'code_barre_id', 'width': 3, 'column_order': 3, 'search': True, 'search_type': '__iexact'},

    ]


class ListOfReformDetailsTempo(StandardTable):
    """ Produce a serialized JSON list """
    model = ReformDetailsTempo
    model_data = {'enumeration': True, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['id', 'investment']}
    fields = [

        {'name': 'investment__article__article_code', 'title': 'Code', 'format': 'text', 'width': False,
         'column_order': 1,
         'search': True,
         'search_type': '__icontains'},
        {'name': 'investment__article__designation', 'format': 'text', 'title': 'Designation', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'investment__article__category__designation', 'title': 'Famille', 'format': 'text', 'width': False,
         'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'investment__code_barre_id', 'format': 'text', 'title': 'Code Barre', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__iexact'},
        {'name': 'investment__marque', 'format': 'text', 'title': 'Marque', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'investment__serial_number', 'format': 'text', 'title': 'Numéro de série', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'investment__type', 'format': 'text', 'title': 'Type', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'investment__pu_ht_net', 'format': 'money', 'title': 'PU HT Net', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'investment__tva__tva_rate', 'format': 'percent', 'title': 'TVA', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'investment__pu_ttc', 'format': 'money', 'title': 'PU TTC ', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
    ]


class ListOfReformDetails(StandardTable):
    """ Produce a serialized JSON list """
    model = ReformDetails
    model_data = {'enumeration': True, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['id', 'investment']}
    fields = [

        {'name': 'investment__article__article_code', 'title': 'Code', 'format': 'text', 'width': False,
         'column_order': 1,
         'search': True,
         'search_type': '__icontains'},
        {'name': 'investment__article__designation', 'format': 'text', 'title': 'Designation', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'investment__article__category__designation', 'title': 'Famille', 'format': 'text', 'width': False,
         'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'investment__code_barre_id', 'format': 'text', 'title': 'Code Barre', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__iexact'},
        {'name': 'investment__marque', 'format': 'text', 'title': 'Marque', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'investment__serial_number', 'format': 'text', 'title': 'Numéro de série', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'investment__type', 'format': 'text', 'title': 'Type', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'investment__pu_ht_net', 'format': 'money', 'title': 'PU HT Net', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'investment__tva__tva_rate', 'format': 'percent', 'title': 'TVA', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'investment__pu_ttc', 'format': 'money', 'title': 'PU TTC ', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
    ]


class ListOfReforms(StandardTable):
    """ Produce a serialized JSON list """

    def get_queryset(self):
        queryset = self.model.objects.all()
        return queryset

    model = Reform
    model_data = {'enumeration': False, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['id', 'reference']}
    fields = [
        {'name': 'id', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'reference', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'operating_date', 'format': 'date', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'observation', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'created_date', 'format': 'datetime', 'width': False, 'column_order': 1, 'search': False,
         'search_type': '__icontains'},
        {'name': 'created_by__username', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'document_scan', 'format': 'media', 'width': False, 'column_order': 1, 'search': False,
         'search_type': '__icontains'},

    ]


# External Cessions

class ListOfInvestmentToCede(StandardTable):
    """ Produce a serialized JSON list of Investments """

    def get_queryset(self):
        """
        Get the model data as a default queryset
        """

        queryset = self.model.objects.all().exclude(
            code_barre_id__in=[c[0] for c in
                               ExternalCessionDetailsTempo.objects.values_list('investment__code_barre_id')])
        return queryset

    model = InvestmentJournal
    model_filter = {'ceded': False}
    model_data = {'enumeration': True, 'sort': ['article_id', 'id'],
                  'reverse_sort': False,
                  'text_field': ['article__designation', 'type', 'marque', 'code_barre_id']}
    fields = [
        {'name': 'article__article_code', 'title': 'Code', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'article__designation', 'title': 'Designation', 'width': 5, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'type', 'title': 'Type', 'width': 3, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'marque', 'width': 3, 'column_order': 3, 'search': True, 'search_type': '__icontains'},
        {'name': 'code_barre_id', 'width': 3, 'column_order': 3, 'search': True, 'search_type': '__iexact'},
        {'name': 'reformed', 'width': 3, 'type': 'boolean', 'column_order': 3, 'search': True,
         'search_type': '__icontains'},

    ]


class ListOfExternalCessionDetailsTempo(StandardTable):
    """ Produce a serialized JSON list """
    model = ExternalCessionDetailsTempo
    model_data = {'enumeration': True, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['id', 'investment']}
    fields = [

        {'name': 'investment__article__article_code', 'title': 'Code', 'format': 'text', 'width': False,
         'column_order': 1,
         'search': True,
         'search_type': '__icontains'},
        {'name': 'investment__article__designation', 'format': 'text', 'title': 'Designation', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'investment__article__category__designation', 'title': 'Famille', 'format': 'text', 'width': False,
         'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'investment__code_barre_id', 'format': 'text', 'title': 'Code Barre', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__iexact'},
        {'name': 'investment__marque', 'format': 'text', 'title': 'Marque', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'investment__serial_number', 'format': 'text', 'title': 'Numéro de série', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'investment__type', 'format': 'text', 'title': 'Type', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'investment__pu_ht_net', 'format': 'money', 'title': 'PU HT Net', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'investment__tva__tva_rate', 'format': 'percent', 'title': 'TVA', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'investment__pu_ttc', 'format': 'money', 'title': 'PU TTC ', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
    ]


class ListOfExternalCessionDetails(StandardTable):
    """ Produce a serialized JSON list """
    model = ExternalCessionDetails
    model_data = {'enumeration': True, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['id', 'investment']}
    fields = [

        {'name': 'investment__article__article_code', 'title': 'Code', 'format': 'text', 'width': False,
         'column_order': 1,
         'search': True,
         'search_type': '__icontains'},
        {'name': 'investment__article__designation', 'format': 'text', 'title': 'Designation', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'investment__article__category__designation', 'title': 'Famille', 'format': 'text', 'width': False,
         'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'investment__code_barre_id', 'format': 'text', 'title': 'Code Barre', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__iexact'},
        {'name': 'investment__marque', 'format': 'text', 'title': 'Marque', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'investment__serial_number', 'format': 'text', 'title': 'Numéro de série', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'investment__type', 'format': 'text', 'title': 'Type', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'investment__pu_ht_net', 'format': 'money', 'title': 'PU HT Net', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
        {'name': 'investment__tva__tva_rate', 'format': 'percent', 'title': 'TVA', 'width': False,
         'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'investment__pu_ttc', 'format': 'money', 'title': 'PU TTC ', 'width': False, 'column_order': 1,
         'search': True, 'search_type': '__icontains'},
    ]


class ListOfExternalCessions(StandardTable):
    """ Produce a serialized JSON list """
    model = ExternalCession
    model_data = {'enumeration': False, 'sort': ['created_date'],
                  'reverse_sort': True, 'text_field': ['id', 'third_party']}
    fields = [
        {'name': 'id', 'width': False, 'column_order': 1, 'search': True, 'search_type': '__icontains'},
        {'name': 'third_party', 'title': 'Bénéficaire', 'format': 'text', 'width': False, 'column_order': 1,
         'search': True,
         'search_type': '__icontains'},
        {'name': 'operating_date', 'format': 'date', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'observation', 'format': 'text', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'created_date', 'format': 'datetime', 'width': False, 'column_order': 1, 'search': False,
         'search_type': '__icontains'},
        {'name': 'created_by__username', 'width': False, 'column_order': 1, 'search': True,
         'search_type': '__icontains'},
        {'name': 'document_scan', 'format': 'media', 'width': False, 'column_order': 1, 'search': False,
         'search_type': '__icontains'},
    ]


# Exporting data as backup

class ExportGlobalCsv(View):
    """
    Export a csv file to use as backup
    """

    def get(self, request):
        employee = AffectationArticleToEmployee.objects.filter(
            article_id=OuterRef('id')).filter(terminated=False).order_by('-created_date')
        status = AffectationArticleToStatus.objects.filter(
            article_id=OuterRef('id')).filter(terminated=False).order_by('-created_date')
        user_company = AffectationArticleToAffiliate.objects.filter(
            article_id=OuterRef('id')).filter(terminated=False).order_by('-created_date')
        mgx_emplacement = AffectationArticleToMgxEmplacement.objects.filter(
            article_id=OuterRef('id')).filter(terminated=False).order_by('-created_date')
        _data = InvestmentJournal.objects.select_related().annotate(
            employee=Subquery(employee.values('employee__name')[:1]),
            code_employee=Subquery(employee.values('employee__code')[:1]),
            function_employee=Subquery(employee.values('employee__function')[:1]),
            structure_employee=Subquery(employee.values('employee__structure')[:1]),
            direction_employee=Subquery(employee.values('employee__direction')[:1]),
            current_status=Subquery(status.values('status__status')[:1]),
            emplacement_code=Subquery(mgx_emplacement.values('mgx_emplacement__code')[:1]),
            emplacement=Subquery(mgx_emplacement.values('mgx_emplacement__designation')[:1]),
            user_company=Subquery(user_company.values('user_company__designation')[:1])
        )
        response = HttpResponse(content_type='text/csv')
        filename = 'backup du ' + str(timezone.now())
        response['Content-Disposition'] = 'attachment; filename="' + filename + '.csv"'
        writer = csv.writer(response, delimiter=';')
        writer.writerow([
            'Code article',
            'Désignation',
            'Famille',
            'Code Famille',
            'Usage commun',
            'Type',
            'Marque',
            'N° de série',
            'Code Barre',
            'PU achat',
            'PU achat Net',
            'TVA',
            'PU TTC',
            'Facture',
            'Date Facture',
            'Fournisseur',
            'Code Fournisseur',
            'Code Emplacement',
            'Emplacement',
            'Entreprise Propriétaire',
            'Filiale Bénéficiaire',
            'Employé Bénéficiaire',
            'Code Employé',
            'Poste',
            'Structure',
            'Direction',
            'Statut',
            'Réformé',
            'Cédé'
        ])
        for item in _data:
            row = [item.article.article_code,
                   item.article.designation,
                   item.article.category.designation,
                   item.article.category.code,
                   item.article.common_use,
                   item.type,
                   item.marque,
                   item.serial_number,
                   item.code_barre_id,
                   item.pu_ht,
                   item.pu_ht_net,
                   item.tva,
                   item.pu_ht_net * (1 + (item.tva.tva_rate / 100)),
                   item.invoice.invoice_number,
                   item.invoice.invoice_date,
                   item.invoice.supplier.supplier,
                   item.invoice.supplier.code,
                   item.emplacement_code,
                   item.emplacement,
                   item.company.designation,
                   item.user_company,
                   item.employee,
                   item.code_employee,
                   item.function_employee,
                   item.structure_employee,
                   item.direction_employee,
                   item.current_status,
                   item.reformed, item.ceded]
            writer.writerow(row)

        return response

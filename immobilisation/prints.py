from django.http import HttpResponse
from django.shortcuts import render
from .models import AffectationArticleToAffiliate, AffectationArticleToStatus, AffectationArticleToEmployee, \
    Acquisition, DetailsAcquisition, Reform, ReformDetails, ExternalCession, ExternalCessionDetails, \
    AffectationArticleToMgxEmplacement


def print_articles_affectation(request):
    """
    Produice printable affectation document
    :param request: HTTP request
    :return: HTML document
    """
    id_parent = request.GET.get('parent_id', None)
    try:
        if request.GET.get('history', None) == 'AffectationArticleToEmployee':
            header = AffectationArticleToEmployee.objects.select_related().get(id=id_parent)
            return render(request, 'immobilisations/prints/print_affectation_article_to_employee.html',
                          {'header': header})
        elif request.GET.get('history', None) == 'AffectationArticleToStatus':
            header = AffectationArticleToStatus.objects.select_related().get(id=id_parent)
            return render(request, 'immobilisations/prints/print_affectation_article_to_status.html',
                          {'header': header})
        elif request.GET.get('history', None) == 'AffectationArticleToAffiliate':
            header = AffectationArticleToAffiliate.objects.select_related().get(id=id_parent)
            return render(request, 'immobilisations/prints/print_affectation_article_to_emplacement.html',
                          {'header': header})
        elif request.GET.get('history', None) == 'AffectationArticleToMgxEmplacement':
            header = AffectationArticleToMgxEmplacement.objects.select_related().get(id=id_parent)
            return render(request, 'immobilisations/prints/print_affectation_article_to_emplacement.html',
                          {'header': header})
    except:
        return HttpResponse('Aucune transaction séléctionnée pour l\'impression')


def print_articles_acquisition(request):
    """
    Produice printable affectation document
    :param request: HTTP request
    :return: HTML document
    """
    id_parent = request.GET.get('parent_id', None)
    try:
        header = Acquisition.objects.select_related().get(id=id_parent)
        details = DetailsAcquisition.objects.filter(parent=header)
        return render(request, 'immobilisations/prints/print_article_acquisition.html',
                      {'header': header, 'details': details})
    except:
        return HttpResponse('Aucune transaction séléctionnée pour l\'impression')


def print_articles_reform(request):
    """
    Produice printable reform document
    :param request: HTTP request
    :return: HTML document
    """
    id_parent = request.GET.get('parent_id', None)
    try:
        header = Reform.objects.select_related().get(id=id_parent)
        details = ReformDetails.objects.filter(parent=header)
        return render(request, 'immobilisations/prints/print_article_reform.html',
                      {'header': header, 'details': details})
    except:
        return HttpResponse('Aucune transaction séléctionnée pour l\'impression')


def print_articles_external_cession(request):
    """
    Produice printable reform document
    :param request: HTTP request
    :return: HTML document
    """
    id_parent = request.GET.get('parent_id', None)
    try:
        header = ExternalCession.objects.select_related().get(id=id_parent)
        details = ExternalCessionDetails.objects.filter(parent=header)
        return render(request, 'immobilisations/prints/print_article_external_cession.html',
                      {'header': header, 'details': details})
    except:
        return HttpResponse('Aucune transaction séléctionnée pour l\'impression')

from django.urls import path
from immobilisation import views, services, prints
from locapro6 import settings

if not settings.MIGRATION_MODE:
    from immobilisation.menu import menu_list

urlpatterns = []


# common
urlpatterns += [
    path('list_of_articles/', services.ListOfArticles.as_view()),
    path('list_of_mgx_emplacements/', services.ListOfMgxEmplacements.as_view()),
    path('list_of_investments/', services.ListOfInvestment.as_view()),
    path('list_of_articles_status/', services.ListOfArticlesStatus.as_view()),
    path('export_investments_global/', services.ExportGlobalCsv.as_view()),
    path('upload_file_form/', views.upload_file),
]
# Invoices management
urlpatterns += [
    path('list_of_invoices/', services.HistoryOfInvoices.as_view()),
    path('list_of_invoices_combo/', services.ListOfInvoices.as_view()),
    path('list_of_article_acquisition_invoices/', views.articles_acquisition_invoices),
]

# Acquisitions management
urlpatterns += [
    path('articles_acquisition_transactions/', views.articles_acquisition_transactions),
    path('list_of_acquisitions_details_tempo/', services.ListOfAcquisitionDetailsTempo.as_view()),
    path('list_of_acquisitions_details/', services.ListOfAcquisitionDetails.as_view()),
    path('list_of_acquisitions/', services.ListOfAcquisition.as_view()),
    path('history_of_articles_acquisitions/', views.history_of_article_acquisitions),
    path('print_article_acquisition/', prints.print_articles_acquisition)
]

# Affectations management

urlpatterns += [
    path('articles_affectations_manager/', views.articles_affectations_manager),
    path('articles_affectations_report/', services.ArticleAffectationsReport.as_view()),
    path('history_of_articles_affectations/', views.history_of_article_affectations),
    path('history_of_articles_affectations_to_emplyee/', services.HistoryArticleToEmployeeAffectation.as_view()),
    path('history_of_articles_affectations_to_status/', services.HistoryArticleToStatusAffectation.as_view()),
    path('history_of_articles_affectations_to_affiliate/', services.HistoryArticleToCompanyAffectation.as_view()),
    path('history_of_articles_affectations_to_emplacement/',
         services.HistoryArticleToCompanyMgxEmplacementAffectation.as_view()),
    path('print_article_affectation/', prints.print_articles_affectation),
]

# Reform management
urlpatterns += [
    path('list_of_investments_to_reform/', services.ListOfInvestmentToReform.as_view()),
    path('articles_reform_transactions/', views.articles_reform_transactions),
    path('list_of_reforms_details_tempo/', services.ListOfReformDetailsTempo.as_view()),
    path('list_of_reforms_details/', services.ListOfReformDetails.as_view()),
    path('list_of_reforms/', services.ListOfReforms.as_view()),
    path('history_of_acticles_reforms/', views.history_of_articles_reform),
    path('print_article_reforms/', prints.print_articles_reform)
]

# External Cession management
urlpatterns += [
    path('list_of_investments_to_cede/', services.ListOfInvestmentToCede.as_view()),
    path('articles_external_cession_transactions/', views.articles_external_cession_transactions),
    path('list_of_external_cessions_details_tempo/', services.ListOfExternalCessionDetailsTempo.as_view()),
    path('list_of_external_cessions_details/', services.ListOfExternalCessionDetails.as_view()),
    path('list_of_external_cessions/', services.ListOfExternalCessions.as_view()),
    path('history_of_articles_external_cessions/', views.history_of_articles_external_cessions),
    path('print_article_external_cessions/', prints.print_articles_external_cession)
]
if not settings.MIGRATION_MODE:
    urlpatterns += [path(menu['service']._url, menu['service'].as_view()) for menu in menu_list if menu['service']]
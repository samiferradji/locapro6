# -*- coding: utf-8 -*-
import json
import traceback
from datetime import date
from decimal import Decimal

from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.core.serializers.json import DjangoJSONEncoder
from django.forms import model_to_dict
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.http import HttpResponse
from django.db.models import Sum, F, DecimalField
from django.apps import apps

from common.models import Error
from common.helpers import get_uuid
from .models import AffectationArticleToAffiliate, AffectationArticleToEmployee, AffectationArticleToStatus, \
    DetailsAcquisitionTempo, AcquisitionTempo, Invoice, ReformTempo, ReformDetailsTempo, ExternalCessionTempo, \
    ExternalCessionDetailsTempo, AffectationArticleToMgxEmplacement
from .helpers import terminate_current_article_to_affiliate_affectation, \
    terminate_current_article_to_employee_affectation
from .validators import validate_unique_barre_code_id
from immobilisation import parameters as immobilisations_params


@login_required(login_url='/login/')
def articles_affectations_manager(request):
    """
        this function manage actions for articles affectations (owner, Status, Affiliate)
        :param request:with parameters :action, article_id ...other parameters
        :return: dictionary {'status':status, 'status_massage':message}
        """
    if request.method == 'POST':
        user = request.user
        if request.POST['action'] == 'add_article_to_employee_affectation':
            try:
                with transaction.atomic():
                    article_id = request.POST['article_id']
                    effect_date = request.POST.get('effect_date')
                    employee_id = request.POST['affectation_subject_id']
                    observation = request.POST['observation'].strip()
                    new_obj = AffectationArticleToEmployee(
                        effect_date=effect_date,
                        article_id=article_id,
                        employee_id=employee_id,
                        observation=observation,
                        created_by=user
                    )
                    new_obj.full_clean(exclude=['company', 'id'])
                    new_obj.save()
                    status = 'Done'
                    statut_massage = 'Affectation ajoutée sous le numéro %s' % new_obj.pk
                    response = {'status': status, 'status_message': statut_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')

            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'add_article_status_affectation':
            try:
                with transaction.atomic():
                    article_id = request.POST['article_id']
                    effect_date = request.POST.get('effect_date')
                    status_id = request.POST['affectation_subject_id']
                    observation = request.POST['observation'].strip()
                    new_obj = AffectationArticleToStatus(
                        effect_date=effect_date,
                        article_id=article_id,
                        status_id=status_id,
                        observation=observation,
                        created_by=user
                    )
                    new_obj.full_clean(exclude=['company', 'id'])
                    new_obj.save()
                    status = 'Done'
                    statut_massage = 'Nouveau statut ajouté sous le numéro %s' % new_obj.pk
                    response = {'status': status, 'status_message': statut_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'add_article_to_emplacement_affectation':
            try:
                with transaction.atomic():
                    article_id = request.POST['article_id']
                    effect_date = request.POST.get('effect_date')
                    mgx_emplacement_id = request.POST['affectation_subject_id']
                    observation = request.POST['observation'].strip()
                    new_obj = AffectationArticleToMgxEmplacement(
                        effect_date=effect_date,
                        article_id=article_id,
                        mgx_emplacement_id=mgx_emplacement_id,
                        observation=observation,
                        created_by=user
                    )
                    new_obj.full_clean(exclude=['company', 'id'])
                    new_obj.save()
                    status = 'Done'
                    statut_massage = 'Nouvelle affectation vers emplacemnt ajoutée sous le numéro %s' % new_obj.pk
                    response = {'status': status, 'status_message': statut_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'add_article_to_affiliate_affectation':
            try:
                with transaction.atomic():
                    article_id = request.POST['article_id']
                    effect_date = request.POST.get('effect_date')
                    user_company_id = request.POST['affectation_subject_id']
                    observation = request.POST['observation'].strip()
                    new_obj = AffectationArticleToAffiliate(
                        effect_date=effect_date,
                        article_id=article_id,
                        user_company_id=user_company_id,
                        observation=observation,
                        created_by=user
                    )
                    new_obj.full_clean(exclude=['company', 'id'])
                    new_obj.save()
                    status = 'Done'
                    statut_massage = 'Affectation ajoutée sous le numéro %s' % new_obj.pk
                    response = {'status': status, 'status_message': statut_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'terminate_employee_affectation':
            article_id = request.POST['article_id']
            effect_date = request.POST.get('effect_date', None)
            response = terminate_current_article_to_employee_affectation(article_id, effect_date, user.id)
            return HttpResponse(json.dumps(response), content_type='application/json')
        elif request.POST['action'] == 'terminate_affiliate_affectation':
            article_id = request.POST['article_id']
            effect_date = request.POST.get('effect_date')
            response = terminate_current_article_to_affiliate_affectation(article_id, effect_date, user.id)
            return HttpResponse(json.dumps(response), content_type='application/json')

    else:
        _uuid = get_uuid()
        today = date.today().strftime('%d/%m/%Y')
        return render(request, 'immobilisations/article_affectations_manager.html', {'uuid': _uuid, 'today': today})


@login_required(login_url='/login/')
def history_of_article_affectations(request):
    """
        this function returns articles affectations history.
        :param request:with parameters :history, article_id
        :return: dictionary
        """
    _uuid = get_uuid()
    history = request.GET.get('history', None)
    url = None
    app_label = 'immobilisation'
    model_name = None
    if history == 'article_to_employee':
        url = '/immobilisations/history_of_articles_affectations_to_emplyee/'
        model_name = 'affectationarticletoemployee'
    elif history == 'article_to_article_status':
        url = '/immobilisations/history_of_articles_affectations_to_status/'
        model_name = 'affectationarticletostatus'
    elif history == 'article_to_affiliate':
        url = '/immobilisations/history_of_articles_affectations_to_affiliate/'
        model_name = 'affectationarticletoaffiliate'
    elif history == 'article_to_emplacement':
        url = '/immobilisations/history_of_articles_affectations_to_emplacement/'
        model_name = 'affectationarticletomgxemplacement'
    return render(request, 'immobilisations/history_articles_affectations.html',
                  {'uuid': _uuid,
                   'history': history,
                   'url': url,
                   'app_label': app_label,
                   'model_name': model_name
                   }
                  )


@login_required(login_url='/login/')
def articles_acquisition_invoices(request):
    """
    History and creation of articles acquisition invoices
    :param request:dict
    :return: dict {'status':status, 'status_massage':message}
    """
    if request.method == 'POST':
        user = request.user
        if request.POST['action'] == 'create_invoice':
            try:
                with transaction.atomic():
                    supplier = request.POST.get('supplier')
                    invoice_number = request.POST.get('invoice_number').strip()
                    invoice_date = request.POST.get('invoice_date')
                    invoice_date = invoice_date if invoice_date != '' else None
                    value_ht = request.POST.get('value_ht')
                    value_ht = Decimal(value_ht.replace(" ", "")) if value_ht != '' else None
                    discount_on_invoice = request.POST.get('discount_on_invoice')
                    discount_on_invoice = Decimal(
                        discount_on_invoice.replace(" ", "")) if discount_on_invoice != '' else None
                    value_ht_net = request.POST.get('value_ht_net')
                    value_ht_net = Decimal(value_ht_net.replace(" ", "")) if value_ht != '' else None
                    value_tva = request.POST.get('value_tva')
                    value_tva = Decimal(value_tva.replace(" ", "")) if value_tva != '' else None
                    value_ttc = request.POST.get('value_ttc')
                    value_ttc = Decimal(value_ttc.replace(" ", "")) if value_ttc != '' else None
                    new_obj = Invoice(
                        created_by=user,
                        supplier_id=supplier,
                        invoice_number=invoice_number,
                        invoice_date=invoice_date,
                        value_ht=value_ht,
                        value_ht_net=value_ht_net,
                        value_ttc=value_ttc,
                        discount_on_invoice=discount_on_invoice,
                        value_tva=value_tva)
                    new_obj.full_clean(exclude=['company'])
                    new_obj.save()
                    status = 'Done'
                    status_massage = 'Nouvelle facture créée sous le numéro %s' % str(new_obj.pk)
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')

            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')

            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
    else:
        _uuid = get_uuid()
        return render(request, 'immobilisations/invoices.html', {'uuid': _uuid})


@login_required(login_url='/login/')
def articles_acquisition_transactions(request):
    """
    this function manage crud actions for investments
    :param request:dict
    :return: dict {'status':status, 'status_massage':message}
    """
    if request.method == 'POST':
        user = request.user
        if request.POST['action'] == 'add_line':
            try:
                with transaction.atomic():
                    parent_id = request.POST.get('parent_id')
                    article_id = request.POST.get('article_id')
                    code_barre_id = request.POST.get('code_barre_id').strip()
                    marque = request.POST.get('marque').strip()
                    serial_number = request.POST.get('serial_number').strip()
                    article_type = request.POST.get('type').strip()
                    pu_ht = (Decimal(
                        request.POST.get('pu_ht', '0.00').replace(' ', '').replace('DZD', '')) if request.POST.get(
                        'pu_ht') != '' else None)

                    unit_discount_perc = (
                        Decimal(request.POST.get('unit_discount_perc', '0.00').replace(' ', '')) if request.POST.get(
                            'unit_discount_perc') != '' else None)
                    unit_discount_value = (
                        Decimal(request.POST.get('unit_discount_value', '0.00').replace(' ', '')) if request.POST.get(
                            'unit_discount_value') != '' else None)
                    tva_id = request.POST.get('tva_id')
                    mgx_emplacement_id = request.POST.get('mgx_emplacement_id')
                    mgx_emplacement_id = mgx_emplacement_id if mgx_emplacement_id != '' else \
                        immobilisations_params.get_default_reception_emplacement_id_for_immobilisations_purchase(
                            user)
                    affected_to_employee_id = request.POST.get('affected_to_employee_id')
                    affected_to_employee_id = None if affected_to_employee_id == '' else affected_to_employee_id
                    new_obj = DetailsAcquisitionTempo(
                        article_id=article_id,
                        code_barre_id=code_barre_id,
                        marque=marque,
                        serial_number=serial_number,
                        type=article_type,
                        pu_ht=pu_ht,
                        unit_discount_value=unit_discount_value,
                        unit_discount_perc=unit_discount_perc,
                        tva_id=tva_id,
                        employee_affectation_id=affected_to_employee_id,
                        emplacement_id=mgx_emplacement_id,
                        parent_id=parent_id,
                        created_by=user,
                    )
                    new_obj.full_clean(exclude=['company'])
                    validate_unique_barre_code_id(code_barre_id=new_obj.code_barre_id, user=request.user)
                    new_obj.save()
                    status = 'Done'
                    status_massage = 'Acquisition ajoutée'
                    response = {'status': status, 'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')

            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'create_parent':
            try:
                with transaction.atomic():
                    parent_id = request.POST.get('parent_id')
                    invoice_id = request.POST.get('invoice_id')
                    operating_date = request.POST.get('operating_date')
                    observation = request.POST.get('observation').strip()

                    new_obj = AcquisitionTempo(
                        created_by=user,
                        id=parent_id,
                        invoice_id=invoice_id,
                        operating_date=operating_date,
                        observation=observation
                    )
                    new_obj.full_clean(exclude=['company'])
                    new_obj.save()
                    status = 'Done'
                    status_massage = 'Entete créé'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')

            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')

            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'apply_global_discount':
            try:
                with transaction.atomic():
                    parent_id = request.POST.get('parent_id')
                    global_discount = Decimal(request.POST.get('global_discount'))
                    new_obj = AcquisitionTempo.objects.get(id=parent_id)
                    new_obj.dispatch_global_discount(global_discount)
                    new_obj.full_clean(exclude=['company'])
                    new_obj.save()
                    status = 'Done'
                    status_massage = 'Une remise de %s dispatché' % '{:20,.2f}'.format(global_discount)
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')

            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except ObjectDoesNotExist:
                status = 'Error'
                status_massage = {
                    'Integrité des données': "Créer l'entête du document avant d'appliquer la remise !!"}
                status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                Error.objects.create(
                    created_by=user,
                    error='IntegrityError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                response = {'status': status, 'status_message': status_massage}
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'get_totals':
            parent_id = request.POST.get('parent_id')
            obj_details = DetailsAcquisitionTempo.objects.filter(parent_id=parent_id).aggregate(
                Sum('pu_ht'),
                Sum('unit_discount_value'),
                Sum('pu_ht_net'),
                Sum('pu_ttc'),
                tva__sum=Sum(F('pu_ht_net') * F('tva__tva_rate') / 100, output_field=DecimalField())
            )

            response = {}
            response['total_pu_ht'] = obj_details['pu_ht__sum']
            response['total_discount_value'] = obj_details['unit_discount_value__sum']
            response['total_pu_ht_net'] = obj_details['pu_ht_net__sum']
            response['total_tva'] = obj_details['tva__sum']
            response['total_pu_ttc'] = obj_details['pu_ttc__sum']
            return HttpResponse(json.dumps(response, cls=DjangoJSONEncoder), content_type='application/json')
        if request.POST['action'] == 'commit':
            try:
                with transaction.atomic():
                    parent_id = request.POST['parent_id']
                    obj = AcquisitionTempo.objects.get(id=parent_id)
                    new_obj = obj.commit_transaction()
                    status = 'Done'
                    status_massage = 'Réception enregistrée sous le numéro %s' % str(new_obj)
                    response = {'status': status,
                                'status_message': status_massage,
                                'data': {'new_id': new_obj}
                                }
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ObjectDoesNotExist:
                status = 'Error'
                status_massage = {
                    'Integrité des données': "Aucun document à enregistrer !!"}
                status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                Error.objects.create(
                    created_by=user,
                    error='IntegrityError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                response = {'status': status, 'status_message': status_massage}
                return HttpResponse(json.dumps(response), content_type='application/json')

            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')

            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'cancel':
            try:
                with transaction.atomic():
                    parent_id = request.POST['parent_id']
                    obj = AcquisitionTempo.objects.get(id=parent_id)
                    obj.cancel_transaction()
                    status = 'Done'
                    status_massage = 'Réception annulée'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ObjectDoesNotExist:
                status = 'Error'
                status_massage = {
                    'Integrité des données': "Aucun document à annuler !!"}
                status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                Error.objects.create(
                    created_by=user,
                    error='IntegrityError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                response = {'status': status, 'status_message': status_massage}
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'delete_line':
            try:
                line_id = request.POST.get('line_id')
                if line_id == 'undefined' or not line_id:
                    status = 'Error'
                    status_massage = {
                        'Ligne à supprimer': "Merci de sélectionner une ligne"}
                    status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                    Error.objects.create(
                        created_by=user,
                        error='IntegrityError : ' + str(status_massage),
                        data=dict(request.POST)
                    )
                    response = {'status': status, 'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
                else:
                    with transaction.atomic():
                        obj = DetailsAcquisitionTempo.objects.get(id=line_id)
                        obj.delete()
                        status = 'OK'
                        status_massage = 'Ligne supprimée'
                        response = {'status': status,
                                    'status_message': status_massage,
                                    }
                    return HttpResponse(json.dumps(response, cls=DjangoJSONEncoder), content_type='application/json')
            except ObjectDoesNotExist:
                status = 'Error'
                status_massage = {
                    'Integrité des données': "Aucune ligne à supprimer !!"}
                status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                Error.objects.create(
                    created_by=user,
                    error='IntegrityError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                response = {'status': status, 'status_message': status_massage}
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'edit_line':
            try:
                with transaction.atomic():
                    line_id = request.POST.get('line_id')
                    parent_id = request.POST.get('parent_id')
                    article_id = request.POST.get('article_id')
                    code_barre_id = request.POST.get('code_barre_id').strip()
                    marque = request.POST.get('marque').strip()
                    serial_number = request.POST.get('serial_number').strip()
                    article_type = request.POST.get('type').strip()
                    pu_ht = (Decimal(
                        request.POST.get('pu_ht', '0.00').replace(' ', '').replace('DZD', '')) if request.POST.get(
                        'pu_ht') != '' else None)

                    unit_discount_perc = (
                        Decimal(request.POST.get('unit_discount_perc', '0.00').replace(' ', '')) if request.POST.get(
                            'unit_discount_perc') != '' else None)
                    unit_discount_value = (
                        Decimal(request.POST.get('unit_discount_value', '0.00').replace(' ', '')) if request.POST.get(
                            'unit_discount_value') != '' else None)
                    tva_id = request.POST.get('tva_id')
                    mgx_emplacement_id = request.POST.get('mgx_emplacement_id')
                    mgx_emplacement_id = mgx_emplacement_id if mgx_emplacement_id != '' else \
                        immobilisations_params.get_default_reception_emplacement_id_for_immobilisations_purchase(
                            user)
                    affected_to_employee_id = request.POST.get('affected_to_employee_id')
                    affected_to_employee_id = None if affected_to_employee_id == '' else affected_to_employee_id

                    curr_obj = DetailsAcquisitionTempo.objects.get(id=line_id)
                    if curr_obj.code_barre_id != code_barre_id:
                        validate_unique_barre_code_id(code_barre_id=code_barre_id, user=request.user)
                    curr_obj.created_by = user
                    curr_obj.parent_id = parent_id
                    curr_obj.article_id = article_id
                    curr_obj.code_barre_id = code_barre_id
                    curr_obj.marque = marque
                    curr_obj.serial_number = serial_number
                    curr_obj.type = article_type
                    curr_obj.pu_ht = pu_ht
                    curr_obj.unit_discount_perc = unit_discount_perc
                    curr_obj.unit_discount_value = unit_discount_value
                    curr_obj.tva_id = tva_id
                    curr_obj.emplacement_id = mgx_emplacement_id
                    curr_obj.employee_affectation_id = affected_to_employee_id
                    curr_obj.full_clean(exclude=['company'])
                    curr_obj.save()
                    status = 'Done'
                    status_massage = 'Ligne modifliée'
                    response = {'status': status, 'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')

            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'get_line':
            try:
                line_id = request.POST.get('line_id')
                if line_id == 'undefined' or not line_id:
                    status = 'Error'
                    status_massage = {
                        'Erreur Interne!': "Merci de sélectionner une ligne"}
                    status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                    Error.objects.create(
                        created_by=user,
                        error='Javascript validation error : ' + str(status_massage),
                        data=dict(request.POST)
                    )
                    response = {'status': status, 'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
                else:
                    with transaction.atomic():
                        obj = DetailsAcquisitionTempo.objects.get(id=line_id)
                        item_data = model_to_dict(obj)
                        status = 'OK'
                        status_massage = 'Ligne chargée'
                        status_data = json.dumps(item_data, cls=DjangoJSONEncoder)
                        response = {'status': status,
                                    'status_message': status_massage,
                                    'status_data': status_data}
                    return HttpResponse(json.dumps(response, cls=DjangoJSONEncoder), content_type='application/json')
            except ObjectDoesNotExist:
                status = 'Error'
                status_massage = {
                    'Integrité des données': "Aucune ligne trouvée !!"}
                status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                Error.objects.create(
                    created_by=user,
                    error='IntegrityError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                response = {'status': status, 'status_message': status_massage}
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
    else:
        _uuid = get_uuid()
        return render(request, 'immobilisations/acquisiton_immoblisations.html', {'uuid': _uuid})


@login_required(login_url='/login/')
def history_of_article_acquisitions(request):
    """
        this function returns articles acquisitions history.
    """
    _uuid = get_uuid()
    return render(request, 'immobilisations/history_articles_acquisitions.html',
                  {'uuid': _uuid})


@login_required(login_url='/login/')
def articles_reform_transactions(request):
    """
    this function manage crud actions for articles reform
    :param request:dict
    :return: dict {'status':status, 'status_massage':message}
    """
    if request.method == 'POST':
        user = request.user
        if request.POST['action'] == 'create_parent':
            try:
                with transaction.atomic():
                    parent_id = request.POST.get('parent_id')
                    reference = request.POST.get('reference').strip()
                    operating_date = request.POST.get('operating_date')
                    observation = request.POST.get('observation').strip()

                    new_obj = ReformTempo(
                        created_by=user,
                        id=parent_id,
                        reference=reference,
                        operating_date=operating_date,
                        observation=observation
                    )
                    new_obj.full_clean(exclude=['company'])
                    new_obj.save()
                    status = 'Done'
                    status_massage = 'Entete créé'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')

            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')

            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'add_line':
            try:
                with transaction.atomic():
                    parent_id = request.POST.get('parent_id')
                    investment_id = request.POST.get('investment_id')
                    new_obj = ReformDetailsTempo(
                        investment_id=investment_id,
                        parent_id=parent_id,
                        created_by=user,
                    )
                    new_obj.full_clean(exclude=['company'])
                    new_obj.save()
                    status = 'Done'
                    status_massage = 'Article ajouté'
                    response = {'status': status, 'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')

            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'commit':
            try:
                with transaction.atomic():
                    parent_id = request.POST['parent_id']
                    obj = ReformTempo.objects.get(id=parent_id)
                    new_obj = obj.commit_transaction()
                    status = 'Done'
                    status_massage = 'Réforme enregistrée sous le numéro %s' % str(new_obj)
                    response = {'status': status,
                                'status_message': status_massage,
                                'data': {'new_id': new_obj}
                                }
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ObjectDoesNotExist:
                status = 'Error'
                status_massage = {
                    'Integrité des données': "Aucun document à enregistrer !!"}
                status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                Error.objects.create(
                    created_by=user,
                    error='IntegrityError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                response = {'status': status, 'status_message': status_massage}
                return HttpResponse(json.dumps(response), content_type='application/json')

            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')

            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'cancel':
            try:
                with transaction.atomic():
                    parent_id = request.POST['parent_id']
                    obj = ReformTempo.objects.get(id=parent_id)
                    obj.cancel_transaction()
                    status = 'Done'
                    status_massage = 'Réforme annulée'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ObjectDoesNotExist:
                status = 'Error'
                status_massage = {
                    'Integrité des données': "Aucun document à annuler !!"}
                status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                Error.objects.create(
                    created_by=user,
                    error='IntegrityError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                response = {'status': status, 'status_message': status_massage}
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'delete_line':
            try:
                line_id = request.POST.get('line_id')
                if line_id == 'undefined' or not line_id:
                    status = 'Error'
                    status_massage = {
                        'Ligne à supprimer': "Merci de sélectionner une ligne"}
                    status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                    Error.objects.create(
                        created_by=user,
                        error='IntegrityError : ' + str(status_massage),
                        data=dict(request.POST)
                    )
                    response = {'status': status, 'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
                else:
                    with transaction.atomic():
                        ReformDetailsTempo.objects.get(id=line_id).delete()
                        status = 'OK'
                        status_massage = 'Ligne supprimée'
                        response = {'status': status,
                                    'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ObjectDoesNotExist:
                status = 'Error'
                status_massage = {
                    'Integrité des données': "Aucune ligne à supprimer !!"}
                status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                Error.objects.create(
                    created_by=user,
                    error='IntegrityError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                response = {'status': status, 'status_message': status_massage}
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
    else:
        _uuid = get_uuid()
        return render(request, 'immobilisations/reform_immoblisations.html', {'uuid': _uuid})


@login_required(login_url='/login/')
def history_of_articles_reform(request):
    """
        this function returns articles reform history.
    """
    _uuid = get_uuid()
    return render(request, 'immobilisations/history_articles_reform.html',
                  {'uuid': _uuid})


@login_required(login_url='/login/')
def articles_external_cession_transactions(request):
    """
    this function manage crud actions for articles external cession
    :param request:dict
    :return: dict {'status':status, 'status_massage':message}
    """
    if request.method == 'POST':
        user = request.user
        if request.POST['action'] == 'create_parent':
            try:
                with transaction.atomic():
                    parent_id = request.POST.get('parent_id')
                    third_party = request.POST.get('third_party').strip()
                    operating_date = request.POST.get('operating_date')
                    observation = request.POST.get('observation').strip()

                    new_obj = ExternalCessionTempo(
                        created_by=user,
                        id=parent_id,
                        third_party=third_party,
                        operating_date=operating_date,
                        observation=observation
                    )
                    new_obj.full_clean(exclude=['company'])
                    new_obj.save()
                    status = 'Done'
                    status_massage = 'Entete créé'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')

            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')

            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'add_line':
            try:
                with transaction.atomic():
                    parent_id = request.POST.get('parent_id')
                    investment_id = request.POST.get('investment_id')
                    new_obj = ExternalCessionDetailsTempo(
                        investment_id=investment_id,
                        parent_id=parent_id,
                        created_by=user,
                    )
                    new_obj.full_clean(exclude=['company'])
                    new_obj.save()
                    status = 'Done'
                    status_massage = 'Article ajouté'
                    response = {'status': status, 'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')

            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'commit':
            try:
                with transaction.atomic():
                    parent_id = request.POST['parent_id']
                    obj = ExternalCessionTempo.objects.get(id=parent_id)
                    new_obj = obj.commit_transaction()
                    status = 'Done'
                    status_massage = 'Cession vers tiers enregistrée  sous le numéro %s' % str(new_obj)
                    response = {'status': status,
                                'status_message': status_massage,
                                'data': {'new_id': new_obj}
                                }
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ObjectDoesNotExist:
                status = 'Error'
                status_massage = {
                    'Integrité des données': "Aucun document à enregistrer !!"}
                status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                Error.objects.create(
                    created_by=user,
                    error='IntegrityError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                response = {'status': status, 'status_message': status_massage}
                return HttpResponse(json.dumps(response), content_type='application/json')

            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')

            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'cancel':
            try:
                with transaction.atomic():
                    parent_id = request.POST['parent_id']
                    obj = ExternalCessionTempo.objects.get(id=parent_id)
                    obj.cancel_transaction()
                    status = 'Done'
                    status_massage = 'Cession vers tiers annulée'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ObjectDoesNotExist:
                status = 'Error'
                status_massage = {
                    'Integrité des données': "Aucun document à annuler !!"}
                status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                Error.objects.create(
                    created_by=user,
                    error='IntegrityError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                response = {'status': status, 'status_message': status_massage}
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'delete_line':
            try:
                line_id = request.POST.get('line_id')
                if line_id == 'undefined' or not line_id:
                    status = 'Error'
                    status_massage = {
                        'Ligne à supprimer': "Merci de sélectionner une ligne"}
                    status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                    Error.objects.create(
                        created_by=user,
                        error='IntegrityError : ' + str(status_massage),
                        data=dict(request.POST)
                    )
                    response = {'status': status, 'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
                else:
                    with transaction.atomic():
                        ExternalCessionDetailsTempo.objects.get(id=line_id).delete()
                        status = 'OK'
                        status_massage = 'Ligne supprimée'
                        response = {'status': status,
                                    'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ObjectDoesNotExist:
                status = 'Error'
                status_massage = {
                    'Integrité des données': "Aucune ligne à supprimer !!"}
                status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                Error.objects.create(
                    created_by=user,
                    error='IntegrityError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                response = {'status': status, 'status_message': status_massage}
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
    else:
        _uuid = get_uuid()
        return render(request, 'immobilisations/external_cession_immoblisations.html', {'uuid': _uuid})


@login_required(login_url='/login/')
def history_of_articles_external_cessions(request):
    """
        this function returns articles external cession history.
    """
    _uuid = get_uuid()
    return render(request, 'immobilisations/history_articles_external_cession.html',
                  {'uuid': _uuid})


@login_required(login_url='/login/')
def upload_file(request):
    if request.method == 'POST' and request.FILES['file']:
        with transaction.atomic():
            file = request.FILES['file']
            model_name = request.POST['model_name']
            app_label = request.POST['app_label']
            document_id = request.POST['document_id']
            current_model = apps.get_model(app_label=app_label, model_name=model_name)
            current_object = current_model.objects.get(id=document_id)
            if not current_object.document_scan:
                current_object.document_scan = file
                current_object.save()
                return render(request, 'upload_form.html', {'uploaded': True})
            else:
                error_msg = 'Un Scan pour ce document exist déja.' \
                            ' Si vous voulez le changer merci de contacter votre administrateur'
                return render(request, 'upload_form.html', {'error_msg': error_msg})
    else:
        document_id = request.GET.get('document_id')
        app_label = request.GET.get('app_label')
        model_name = request.GET.get('model_name')
        return render(request, 'upload_form.html',
                      {'id': document_id, 'app_label': app_label, 'model_name': model_name})

import json
from datetime import timedelta, datetime

from django.core.serializers.json import DjangoJSONEncoder
from django.db import transaction
from django.utils import timezone
from django.core.exceptions import MultipleObjectsReturned

from .models import AffectationArticleToEmployee, AffectationArticleToAffiliate, ArticleStatus
from common.models import UserProfile


def terminate_current_article_to_employee_affectation(article_id, date, user_id):
    """
    Terminate de current affectation of an article to employee
    :param article_id: The id of the vehicle
    :param date: The date at witch we want to terminate the affectation
    :param user_id: integer
    :return:  dict or error
    """
    try:
        with transaction.atomic():
            obj = AffectationArticleToEmployee.objects.filter(
                article_id=article_id).order_by('created_date').last()
            if obj:
                if obj.terminated:
                    status = 'Error'
                    status_message = {'Article séléctioné': 'Aucune affecation à terminer !'}
                    status_message = json.dumps(dict(status_message), cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_message}
                    return response
                else:
                    obj.terminated = True
                    obj.terminated_by_id = user_id
                    obj.terminated_date = date
                    obj.save()
                    status = 'Done'
                    status_massage = 'Affectation terminée'
                    response = {'status': status, 'status_message': status_massage}
                    return response
            else:
                status = 'Error'
                status_message = {'Article séléctioné': 'Aucune affecation en cours !'}
                status_message = json.dumps(dict(status_message), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_message}
                return response
    except Exception as e:
        raise e


def terminate_last_article_to_employee_affectation(investment_id):
    """
    Terminate de laste affectation of an article to employee,auto activated if an article if affected
    whithout terminating the las affectation
    """

    affectations_queryset = AffectationArticleToEmployee.objects.filter(article_id=investment_id, terminated=False)
    if not affectations_queryset.exists():
        pass
    elif affectations_queryset.count() > 1:
        raise MultipleObjectsReturned
    else:
        last_affectations = affectations_queryset.order_by('-created_date')[0]
        last_affectations.terminated = True
        last_affectations.terminated_by_id = 1
        last_affectations.terminated_date = timezone.now()
        last_affectations.observation += ' (Restitution auto)'
        last_affectations.save()


def terminate_last_article_to_affiliate_affectation(investment_id):
    """
    Terminate de laste affectation of an article to affiliate,auto activated if an article if affected
    whithout terminating the las affectation
    """
    affectations_queryset = AffectationArticleToAffiliate.objects.filter(article_id=investment_id, terminated=False)
    if not affectations_queryset.exists():
        pass
    elif affectations_queryset.count() > 1:
        raise MultipleObjectsReturned
    else:
        last_affectations = affectations_queryset.order_by('-created_date')[0]
        last_affectations.terminated = True
        last_affectations.terminated_by_id = 1
        last_affectations.terminated_date = timezone.now()
        last_affectations.observation += ' (Restitution auto)'
        last_affectations.save()
        pass


def terminate_current_article_to_affiliate_affectation(article_id, date, user_id):
    """
    Terminate de current affectation of an article to affiliate
    :param article_id: The id of the vehicle
    :param date: The date at witch we want to terminate the affectation
    :param user_id: integer
    :return: dict or error
    """
    try:
        with transaction.atomic():
            obj = AffectationArticleToAffiliate.objects.filter(
                article_id=article_id).order_by('created_date').last()
            if obj:
                if obj.terminated:
                    status = 'Error'
                    status_message = {'Article séléctioné': 'Aucune affecation à terminer !'}
                    status_message = json.dumps(dict(status_message), cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_message}
                    return response
                else:
                    obj.terminated = True
                    obj.terminated_by_id = user_id
                    obj.terminated_date = date
                    obj.save()
                    status = 'Done'
                    status_message = 'Affectation terminée'
                    response = {'status': status, 'status_message': status_message}
                    return response
            else:
                status = 'Error'
                status_message = {'Article séléctioné': 'Aucune affecation en cours !'}
                status_message = json.dumps(dict(status_message), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_message}
                return response
    except Exception as e:
        raise e


def default_article_status():
    """
    Returns the defaut status of investment at entry
    :return: Status Object instance
    """
    obj = ArticleStatus.objects.order_by('id').first()
    return obj


def default_article_company(user_obj):
    """
    Returns the user's company
    :param user_obj: User Object instance
    :return: Company Object instance
    """
    obj = UserProfile.objects.get(user_id=user_obj).company
    return obj

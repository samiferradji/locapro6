from django.core.exceptions import ValidationError

from common.models import UserProfile
from .models import InvestmentJournal, DetailsAcquisitionTempo


def validate_unique_barre_code_id(user, code_barre_id):
    current_affiliate = UserProfile.objects.get(user=user).company
    if InvestmentJournal.objects.filter(company=current_affiliate, code_barre_id=code_barre_id).exists():
        raise ValidationError({'Code Barre': 'Ce code barre est déja utilisé'})
    if DetailsAcquisitionTempo.objects.filter(company=current_affiliate, code_barre_id=code_barre_id).exists():
        raise ValidationError({'Code Barre': 'Ce code barre exist dans le bon encours'})
    else:
        return True


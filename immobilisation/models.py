from django.contrib.auth.models import User
from django.db import models, transaction
from django.db.models import Sum
from django.utils import timezone

from common.models import SupplierMGX, Employee, Company, ArticleCategory, AccountingCode, Tva, \
    DomainBaseModel


class MgxEmplacement(DomainBaseModel):
    reference_table = False
    code = models.CharField(max_length=20, verbose_name='Code')
    designation = models.CharField(max_length=150, verbose_name='Désignation')

    def __str__(self):
        return ' | '.join((self.code, self.designation))

    class Meta:
        verbose_name = 'Emplacement MGX'
        verbose_name_plural = 'Emplacements MGX'
        unique_together = (('code', 'company', 'deleted_at'), ('designation', 'company', 'deleted_at'))
        permissions = (
            ('view_mgxemplacement', 'Can View Mgx Emplacement'),
            ('export_mgxemplacement', 'Can export Mgx Emplacement'),
        )


class Article(DomainBaseModel):
    reference_table = True
    article_code = models.CharField(max_length=50, verbose_name='Code')
    designation = models.CharField(max_length=150, verbose_name='Désignation')
    category = models.ForeignKey(ArticleCategory, verbose_name='Famille', on_delete=models.CASCADE)
    accounting_code = models.ForeignKey(AccountingCode, verbose_name='Code Comptable', on_delete=models.CASCADE)
    common_use = models.BooleanField(verbose_name='Article à usage commun')

    def __str__(self):
        return ' | '.join((self.article_code, self.designation, str(self.category.designation)))

    class Meta:
        verbose_name = 'Article'
        unique_together = (('article_code', 'company', 'deleted_at'), ('designation', 'company', 'deleted_at'))
        permissions = (
            ('view_article', 'Can View Article'),
            ('export_article', 'Can export Article'),
        )


class ArticleStatus(DomainBaseModel):
    reference_table = True
    custom_id_generator = True
    status = models.CharField(max_length=50, verbose_name='Statut Article')

    def __str__(self):
        return str(self.status)

    class Meta:
        verbose_name = 'Statut d\'article'
        verbose_name_plural = 'Statuts d\'articles'
        unique_together = ('status', 'company', 'deleted_at')
        permissions = (
            ('view_articlestatus', 'Can View Article Status'),
            ('export_articlestatus', 'Can export Article Status'),
        )


#  Acquisition management

class Invoice(DomainBaseModel):
    supplier = models.ForeignKey(SupplierMGX, on_delete=models.PROTECT, verbose_name='Fournisseur')
    invoice_number = models.CharField(max_length=50, verbose_name='N° Facture')
    invoice_date = models.DateField(verbose_name='Date facture')
    value_ht = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Montant HT')
    discount_on_invoice = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Remise total')
    value_ht_net = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Montant HT Net')
    value_tva = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Montant TVA')
    value_ttc = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Montant TTC')

    class Meta:
        verbose_name = 'Facture d\'acquisition'
        verbose_name_plural = 'Factures d\'acquisition'
        unique_together = ('supplier', 'invoice_number', 'invoice_date', 'company', 'deleted_at')
        permissions = (
            ('view_invoice', 'Can view Invoice'),
            ('export_invoice', 'Can export Invoice'),
        )

    def __str__(self):
        return ' | '.join((str(self.invoice_number), str(self.invoice_date), str(self.supplier), str(self.value_ttc)))


class InvestmentBase(DomainBaseModel):
    parent = None
    invoice = models.ForeignKey(Invoice, on_delete=models.PROTECT, verbose_name='Facture', blank=True, null=True)
    article = models.ForeignKey(Article, verbose_name='Article', on_delete=models.PROTECT)
    code_barre_id = models.CharField(max_length=50, verbose_name='Code à barre')
    marque = models.CharField(max_length=100, verbose_name='Marque')
    serial_number = models.CharField(max_length=50, verbose_name='Numéro de série', blank=True)
    type = models.CharField(max_length=150, verbose_name='Type')
    pu_ht = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='PU HT')
    unit_discount_value = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Remise Article (Valeur)',
                                              default=0)
    unit_discount_perc = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Remise Article (%)',
                                             default=0)
    pu_ht_net = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='PU HT NET', default=0)
    tva = models.ForeignKey(Tva, on_delete=models.PROTECT, verbose_name='TVA')
    pu_ttc = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='PU TTC', default=0)

    def calculate_net_costs(self):
        """"
        calculate the net price of return for the article
        """
        self.pu_ht_net = self.pu_ht - self.unit_discount_value
        self.pu_ttc = self.pu_ht_net + ((self.tva.tva_rate * self.pu_ht_net) / 100)

    def calculate_discount(self):
        self.unit_discount_value = (self.pu_ht * self.unit_discount_perc) / 100

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        with transaction.atomic():
            self.calculate_discount()
            self.calculate_net_costs()
            super(InvestmentBase, self).save()

    class Meta:
        verbose_name = 'Investissement'
        verbose_name_plural = 'Journal des investissements'
        abstract = True


class AcquisitionBase(DomainBaseModel):
    invoice = models.ForeignKey(Invoice, on_delete=models.PROTECT, verbose_name='Facture d\'acquisition')
    operating_date = models.DateField(verbose_name='Date de réception', default=timezone.now)
    observation = models.CharField(max_length=200, blank=True)

    class Meta:
        verbose_name = 'Acquisition'
        abstract = True

    def __str__(self):
        return ' | '.join((str(self.id), str(self.invoice)))


class AcquisitionTempo(AcquisitionBase):
    id = models.CharField(max_length=36, primary_key=True)

    def commit_transaction(self):
        """
        Transferts the inputed data from temporary tables to final tables
        :param self: the current object
        :returns: String 'Done' if executed without exceptions
        :raises: returns the exception
        """
        try:
            with transaction.atomic():
                from .helpers import default_article_company, default_article_status
                self.new_obj = Acquisition(
                    invoice=self.invoice,
                    created_by=self.created_by,
                    operating_date=self.operating_date,
                    observation=self.observation,
                    created_date=timezone.now()
                )
                self.new_obj.save()
                self.temporary_details_objs = DetailsAcquisitionTempo.objects.filter(
                    parent=self
                )
                for obj in self.temporary_details_objs:
                    DetailsAcquisition.objects.create(
                        created_by=obj.created_by,
                        parent=self.new_obj,
                        article=obj.article,
                        code_barre_id=obj.code_barre_id,
                        marque=obj.marque,
                        serial_number=obj.serial_number,
                        type=obj.type,
                        pu_ht=obj.pu_ht,
                        unit_discount_perc=obj.unit_discount_perc,
                        tva=obj.tva,
                        pu_ttc=obj.pu_ttc,
                    )
                    new_acquisition_elm = InvestmentJournal(
                        created_by=obj.created_by,
                        invoice=self.invoice,
                        article=obj.article,
                        code_barre_id=obj.code_barre_id,
                        marque=obj.marque,
                        serial_number=obj.serial_number,
                        type=obj.type,
                        pu_ht=obj.pu_ht,
                        pu_ttc=obj.pu_ttc,
                        unit_discount_perc=obj.unit_discount_perc,
                        tva=obj.tva,
                        company=self.new_obj.invoice.company,
                        reformed=False,
                        supplier=self.new_obj.invoice.supplier
                    )
                    new_acquisition_elm.save()
                    AffectationArticleToStatus.objects.create(
                        created_by=obj.created_by,
                        article=new_acquisition_elm,
                        status=default_article_status(),
                        observation='Auto created status',
                        effect_date=obj.created_date
                    )
                    AffectationArticleToAffiliate.objects.create(
                        created_by=obj.created_by,
                        article=new_acquisition_elm,
                        user_company=default_article_company(obj.created_by),
                        observation='Auto created affectation',
                        effect_date=obj.created_date
                    )
                    AffectationArticleToMgxEmplacement.objects.create(
                        created_by=obj.created_by,
                        article=new_acquisition_elm,
                        mgx_emplacement=obj.emplacement,
                        observation='',
                        effect_date=obj.created_date
                    )
                    if obj.employee_affectation:
                        AffectationArticleToEmployee.objects.create(
                            created_by=obj.created_by,
                            article=new_acquisition_elm,
                            employee=obj.employee_affectation,
                            observation='',
                            effect_date=obj.created_date
                        )

                self.temporary_details_objs.delete()
                self.delete()
                return self.new_obj.id

        except Exception as e:
            raise e

    def cancel_transaction(self):
        """
        Cancel the temporary transaction

        :param self: the current object
        :returns: String 'Done' if executed without exceptions
        :raises: returns the exception
        """
        try:
            with transaction.atomic():
                self.temporary_details_objs = DetailsAcquisitionTempo.objects.filter(
                    parent=self
                )
                self.temporary_details_objs.delete()
                self.delete()
                return self
        except Exception as e:
            raise e

    def dispatch_global_discount(self, global_discount):
        obj_details = DetailsAcquisitionTempo.objects.filter(parent=self)
        obj_details_sum = DetailsAcquisitionTempo.objects.filter(parent=self).aggregate(
            Sum('pu_ht_net'))
        with transaction.atomic():
            if obj_details.exists():
                for obj in obj_details:
                    obj.unit_discount_perc = 0
                    obj.unit_discount_value += obj.pu_ht_net / obj_details_sum['pu_ht_net__sum'] * global_discount
                    obj.save()


class Acquisition(AcquisitionBase):
    class Meta:
        permissions = (
            ('view_acquisition', 'Can view Acquisition'),
            ('export_acquisition', 'Can export Acquisition'),
        )


class DetailsAcquisitionTempo(InvestmentBase):
    invoice = None
    parent = models.ForeignKey(AcquisitionTempo, on_delete=models.PROTECT)
    employee_affectation = models.ForeignKey(Employee, on_delete=models.CASCADE, verbose_name='Affectation', null=True,
                                             blank=True)
    emplacement = models.ForeignKey(MgxEmplacement, on_delete=models.CASCADE, verbose_name='Emplacement')


class DetailsAcquisition(InvestmentBase):
    invoice = None
    parent = models.ForeignKey(Acquisition, on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Détails d\'acquisition'
        verbose_name_plural = 'Détails des acquisitions'

        permissions = (
            ('view_detailsacquisition', 'Can view Details Acquisition'),
            ('export_detailsacquisition', 'Can export Details Acquisition'),
        )


class InvestmentJournal(InvestmentBase):
    supplier = models.ForeignKey(SupplierMGX, on_delete=models.PROTECT, verbose_name='Fournisseur')
    reformed = models.BooleanField(default=False, verbose_name='Réformé ?')
    ceded = models.BooleanField(default=False, verbose_name='Cédé ?')

    class Meta:
        verbose_name = 'Journal des investissements'
        unique_together = ('company', 'code_barre_id', 'deleted_at')
        permissions = (
            ('view_investmentjournal', 'Can view Investment Journal'),
            ('export_investmentjournal', 'Can export Investment Journal'),
        )

    def __str__(self):
        return ' | '.join((str(self.article), self.type, self.marque, self.code_barre_id))


#  Affectations management

class ArticleAffectationBase(DomainBaseModel):
    article = models.ForeignKey(InvestmentJournal, verbose_name='Article', on_delete=models.PROTECT)
    effect_date = models.DateTimeField(verbose_name="Date d'effet", default=timezone.now)
    observation = models.TextField(max_length=250, verbose_name='Observations', blank=True)
    terminated = models.BooleanField(verbose_name='Affectation terminée', default=False)
    terminated_date = models.DateTimeField(verbose_name="Fin de l'affectation", blank=True, null=True)
    terminated_by = models.ForeignKey(User, related_name='article_affectation_terminated_by', on_delete=models.PROTECT,
                                      null=True, blank=True)

    def __str__(self):
        return str(self.id)

    class Meta:
        abstract = True


class AffectationArticleToEmployee(ArticleAffectationBase):
    employee = models.ForeignKey(Employee, verbose_name='Employé', on_delete=models.PROTECT)
    terminated_by = models.ForeignKey(User, related_name='article_employee_terminated_by', on_delete=models.PROTECT,
                                      null=True, blank=True)

    class Meta:
        verbose_name = 'Affectation employé'
        verbose_name_plural = 'Affectations des articles aux employés'
        permissions = (
            ("view_affectationarticletoemployee", "Can view Affectation Article To Employee"),
            ("export_affectationarticletoemployee", "Can Export Affectation Article To Employee"),
            ("manage_article_affectations", "Can manage affectations"),  # permission for articles management
            ("view_all_article_affectations", "Can view all articles affectations"),
            ("export_all_article_affectations", "Can Export all articles affectations"),
        )

    def save(self, *args, **kwargs):
        from .helpers import terminate_last_article_to_employee_affectation
        with transaction.atomic():
            if not self.id:
                terminate_last_article_to_employee_affectation(self.article_id)
                super(AffectationArticleToEmployee, self).save(*args, **kwargs)
            else:
                super(AffectationArticleToEmployee, self).save(*args, **kwargs)


class AffectationArticleToStatus(ArticleAffectationBase):
    status = models.ForeignKey(ArticleStatus, verbose_name='Statut de l\'arcticle', on_delete=models.PROTECT)
    terminated_by = models.ForeignKey(User, related_name='article_status_terminated_by', on_delete=models.PROTECT,
                                      null=True, blank=True)

    class Meta:
        verbose_name = 'Statuts Article'
        verbose_name_plural = 'Changements des statuts des articles'
        permissions = (
            ("view_affectationarticletostatus", "Can view Affectation Article To Status"),
            ("export_affectationarticletostatus", "Can export Affectation Article To Status"))

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super(AffectationArticleToStatus, self).save(*args, **kwargs)


class AffectationArticleToAffiliate(ArticleAffectationBase):
    class Meta:
        verbose_name = 'Affectation filiale'
        verbose_name_plural = 'Affectations des articles aux filiales'
        permissions = (
            ("view_affectationarticletoaffiliate", "Can view Affectation Article To Affiliate"),
            ("export_affectationarticletoaffiliate", "Can export Affectation Article To Affiliate"),
        )

    user_company = models.ForeignKey(Company, verbose_name='Entreprise utilisatrice', on_delete=models.PROTECT,
                                     related_name='used_by_company')
    terminated_by = models.ForeignKey(User, related_name='article_company_terminated_by', on_delete=models.PROTECT,
                                      null=True, blank=True)

    def save(self, *args, **kwargs):
        from .helpers import terminate_last_article_to_affiliate_affectation
        with transaction.atomic():
            if not self.id:
                terminate_last_article_to_affiliate_affectation(self.article_id)
                super(AffectationArticleToAffiliate, self).save(*args, **kwargs)
            else:
                super(AffectationArticleToAffiliate, self).save(*args, **kwargs)


class AffectationArticleToMgxEmplacement(ArticleAffectationBase):
    class Meta:
        verbose_name = 'Affectation Emplacement MGX'
        verbose_name_plural = 'Affectations des articles aux Emplacements Mgx'
        permissions = (
            ("view_affectationarticletomgxemplacement", "Can view Affectation Article To Mgx Emplacement"),
            ("export_affectationarticletomgxemplacement", "Can export Affectation Article To Mgx Emplacement"),
        )

    mgx_emplacement = models.ForeignKey(MgxEmplacement, verbose_name='Emplacement MGX', on_delete=models.PROTECT)
    terminated_by = models.ForeignKey(User, related_name='article_emplacement_terminated_by', on_delete=models.PROTECT,
                                      null=True, blank=True)

    def save(self, *args, **kwargs):
        with transaction.atomic():
            super(AffectationArticleToMgxEmplacement, self).save(*args, **kwargs)


#  Reform management


class ReformBase(DomainBaseModel):
    reference = models.CharField(max_length=200, verbose_name='Réference du PV')
    operating_date = models.DateField(verbose_name='Date de la réforme', default=timezone.now)
    observation = models.CharField(max_length=200, blank=True)

    class Meta:
        verbose_name = 'Réforme'
        abstract = True

    def __str__(self):
        return self.reference


class ReformDetailsBase(DomainBaseModel):
    parent = None
    investment = models.ForeignKey(InvestmentJournal, verbose_name='Article', on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Détails Réfome'
        verbose_name_plural = 'Détails des Réfomes'
        abstract = True


class ReformTempo(ReformBase):
    id = models.CharField(max_length=36, primary_key=True)

    def commit_transaction(self):
        """
        Transferts the inputed data from temporary tables to final tables
        :param self: the current object
        :returns: String 'Done' if executed without exceptions
        :raises: returns the exception
        """
        try:
            with transaction.atomic():
                self.new_obj = Reform.objects.create(
                    reference=self.reference,
                    created_by=self.created_by,
                    operating_date=self.operating_date,
                    observation=self.observation,
                )
                self.temporary_details_objs = ReformDetailsTempo.objects.filter(
                    parent=self
                )
                for obj in self.temporary_details_objs:
                    ReformDetails.objects.create(
                        created_by=obj.created_by,
                        parent=self.new_obj,
                        investment=obj.investment,
                    )
                    reform_elm = InvestmentJournal.objects.get(id=obj.investment.id)
                    reform_elm.reformed = True
                    reform_elm.save()
                self.temporary_details_objs.delete()
                self.delete()
                return self.new_obj.id
        except Exception as e:
            raise e

    def cancel_transaction(self):
        """
        Cancel the temporary transaction

        :param self: the current object
        :returns: String 'Done' if executed without exceptions
        :raises: returns the exception
        """
        try:
            with transaction.atomic():
                temporary_details_objs = ReformDetailsTempo.objects.filter(
                    parent=self
                )
                temporary_details_objs.delete()
                self.delete()
                return self.id
        except Exception as e:
            raise e


class ReformDetailsTempo(ReformDetailsBase):
    parent = models.ForeignKey(ReformTempo, on_delete=models.PROTECT, verbose_name='N° Réfome')


class Reform(ReformBase):
    class Meta:
        verbose_name = 'Réforme'
        unique_together = ('reference', 'company', 'deleted_at')
        permissions = (
            ("view_reform", "Can view Reform"),
            ("export_reform", "Can export Reform"))


class ReformDetails(ReformDetailsBase):
    parent = models.ForeignKey(Reform, on_delete=models.PROTECT, verbose_name='N° Réfome')

    class Meta:
        verbose_name = 'Détail Réforme'
        permissions = (
            ("view_reformdetails", "Can view Reform Details"),
            ("export_reformdetails", "Can export Reform Details"))


#  External Cessions management


class ExternalCessionBase(DomainBaseModel):
    third_party = models.CharField(max_length=200, verbose_name='Cession pour')
    operating_date = models.DateField(verbose_name='Date de la cession', default=timezone.now)
    observation = models.CharField(max_length=200, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        return str(self.pk)


class ExternalCessionDetailsBase(DomainBaseModel):
    parent = None
    investment = models.ForeignKey(InvestmentJournal, verbose_name='Article', on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Cession pour tier'
        verbose_name_plural = 'Cessions pour tiers'
        abstract = True


class ExternalCessionTempo(ExternalCessionBase):
    id = models.CharField(max_length=36, primary_key=True)

    def commit_transaction(self):
        """
        Transferts the inputed data from temporary tables to final tables
        :param self: the current object
        :returns: String 'Done' if executed without exceptions
        :raises: returns the exception
        """
        try:
            with transaction.atomic():
                self.new_obj = ExternalCession.objects.create(
                    third_party=self.third_party,
                    created_by=self.created_by,
                    operating_date=self.operating_date,
                    observation=self.observation,
                )
                self.temporary_details_objs = ExternalCessionDetailsTempo.objects.filter(
                    parent=self
                )
                for obj in self.temporary_details_objs:
                    ExternalCessionDetails.objects.create(
                        created_by=obj.created_by,
                        parent=self.new_obj,
                        investment=obj.investment,
                    )
                    reform_elm = InvestmentJournal.objects.get(id=obj.investment.id)
                    reform_elm.ceded = True
                    reform_elm.save()
                self.temporary_details_objs.delete()
                self.delete()
                return self.new_obj.id
        except Exception as e:
            raise e

    def cancel_transaction(self):
        """
        Cancel the temporary transaction

        :param self: the current object
        :returns: String 'Done' if executed without exceptions
        :raises: returns the exception
        """
        try:
            with transaction.atomic():
                self.temporary_details_objs = ExternalCessionDetailsTempo.objects.filter(
                    parent=self
                )
                self.temporary_details_objs.delete()
                self.delete()
                return self.id
        except Exception as e:
            raise e


class ExternalCessionDetailsTempo(ExternalCessionDetailsBase):
    parent = models.ForeignKey(ExternalCessionTempo, on_delete=models.PROTECT, verbose_name='N° Cession Externe')


class ExternalCession(ExternalCessionBase):
    id = models.CharField(max_length=15, verbose_name='id', primary_key=True)

    class Meta:
        verbose_name = 'Cession externe'
        verbose_name_plural = 'Cessions externes'
        permissions = (
            ("view_externalcession", "Can view External Cession"),
            ("export_externalcession", "Can Export External Cession"))


class ExternalCessionDetails(ExternalCessionDetailsBase):
    parent = models.ForeignKey(ExternalCession, on_delete=models.PROTECT, verbose_name='N° Cession Externe')

    class Meta:
        verbose_name = 'Détail Cession externe'
        verbose_name_plural = 'Détails Cessions externes'
        permissions = (
            ("view_externalcessiondetails", "Can view External Cession Details"),
            ("export_externalcessiondetails", "Can Export External Cession Details"))

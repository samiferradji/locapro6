[
  {
    "code": "1U032",
    "designation": "MDS-02220-309-16"
  },
  {
    "code": "1U044",
    "designation": "MDS-09391-308-16"
  },
  {
    "code": "1U048",
    "designation": "MDS-02217-309-16"
  },
  {
    "code": "1U067",
    "designation": "MDS-01297-312-16"
  },
  {
    "code": "1U072",
    "designation": "MDS-000177-317-16"
  },
  {
    "code": "1U108",
    "designation": "MDS-001677-316-16"
  },
  {
    "code": "1U109",
    "designation": "MDS-001676-316-16"
  },
  {
    "code": "1U133",
    "designation": "MDS-000532-317-16"
  },
  {
    "code": "1U013",
    "designation": "NISS-10187-308-16"
  },
  {
    "code": "1U014",
    "designation": "NISS-03241-309-16"
  },
  {
    "code": "1U015",
    "designation": "NISS-03534-309-16"
  },
  {
    "code": "1U016",
    "designation": "NISS-03506-309-16"
  },
  {
    "code": "1U017",
    "designation": "NISS-04044-309-16"
  },
  {
    "code": "1U018",
    "designation": "NISS-04213-309-16"
  },
  {
    "code": "1U019",
    "designation": "NISS-04532-309-16"
  },
  {
    "code": "1U021",
    "designation": "NISS-05456-309-16"
  },
  {
    "code": "1U023",
    "designation": "NISS-06956-309-16"
  },
  {
    "code": "1U024",
    "designation": "NISS-07095-309-16"
  },
  {
    "code": "1U025",
    "designation": "NISS-07711-309-16"
  },
  {
    "code": "1U026",
    "designation": "NISS-10215-308-16"
  },
  {
    "code": "1U029",
    "designation": "NISS-07222-309-16"
  },
  {
    "code": "1U035",
    "designation": "NISS-07344-309-16"
  },
  {
    "code": "1U047",
    "designation": "NISS-01761-309-16"
  },
  {
    "code": "1U054",
    "designation": "H1-08210-310-16"
  },
  {
    "code": "1U058",
    "designation": "NISS-04886-309-16"
  },
  {
    "code": "1U059",
    "designation": "NISS-04888-309-16"
  },
  {
    "code": "1U069",
    "designation": "MDS-024478-312-16"
  },
  {
    "code": "1U071",
    "designation": "MDS-024982-312-16"
  },
  {
    "code": "1U073",
    "designation": "TOY-008856-313-16"
  },
  {
    "code": "1U074",
    "designation": "TOY-008693-313-16"
  },
  {
    "code": "1U075",
    "designation": "TOY-008841-313-16"
  },
  {
    "code": "1U076",
    "designation": "TOY-008772-313-16"
  },
  {
    "code": "1U077",
    "designation": "MDS-000176-317-16"
  },
  {
    "code": "1U082",
    "designation": "MDS-005847-314-16"
  },
  {
    "code": "1U084",
    "designation": "MDS-07989-312-16"
  },
  {
    "code": "1U085",
    "designation": "MDS-005577-314-16"
  },
  {
    "code": "1U086",
    "designation": "MDS-005536-314-16"
  },
  {
    "code": "1U087",
    "designation": "MDS-005522-314-16"
  },
  {
    "code": "1U088",
    "designation": "TOY-022387-312-16"
  },
  {
    "code": "1U089",
    "designation": "MDS-005877-314-16"
  },
  {
    "code": "1U090",
    "designation": "TOY-008848-313-16"
  },
  {
    "code": "1U091",
    "designation": "H1-08209-310-16"
  },
  {
    "code": "1U093",
    "designation": "NISS-012403-315-16"
  },
  {
    "code": "1U094",
    "designation": "NISS-012278-315-16"
  },
  {
    "code": "1U095",
    "designation": "NISS-012340-315-16"
  },
  {
    "code": "1U096",
    "designation": "NISS-012341-315-16"
  },
  {
    "code": "1U097",
    "designation": "NISS-012319-315-16"
  },
  {
    "code": "1U098",
    "designation": "NISS-012345-315-16"
  },
  {
    "code": "1U099",
    "designation": "NISS-012407-315-16"
  },
  {
    "code": "1U100",
    "designation": "NISS-012346-315-16"
  },
  {
    "code": "1U101",
    "designation": "NISS-012339-315-16"
  },
  {
    "code": "1U102",
    "designation": "MDS-007868-315-16"
  },
  {
    "code": "1U103",
    "designation": "MDS-010935-315-16"
  },
  {
    "code": "1U104",
    "designation": "MDS-010936-315-16"
  },
  {
    "code": "1U115",
    "designation": "MDS-000179-317-16"
  },
  {
    "code": "1U120",
    "designation": "MDS-000445-317-16"
  },
  {
    "code": "1U121",
    "designation": "MDS-000443-317-16"
  },
  {
    "code": "1U122",
    "designation": "MDS-000446-317-16"
  },
  {
    "code": "1U123",
    "designation": "MDS-000444-317-16"
  },
  {
    "code": "1U124",
    "designation": "MDS-000447-317-16"
  },
  {
    "code": "1U128",
    "designation": "MDS-000536-317-16"
  },
  {
    "code": "1U129",
    "designation": "MDS-000442-317-16"
  },
  {
    "code": "1U134",
    "designation": "MDS-000534-317-16"
  },
  {
    "code": "1U140",
    "designation": "MDS-000178-317-16"
  },
  {
    "code": "8U005",
    "designation": "H1-05722-311-16"
  },
  {
    "code": "1U079",
    "designation": "NISS-004217-316-16"
  },
  {
    "code": "1U078",
    "designation": "NISS-004218-316-16"
  },
  {
    "code": "1U080",
    "designation": "NISS-12364-309-15"
  },
  {
    "code": "1U135",
    "designation": "NISS-13255-309-15"
  },
  {
    "code": "1U137",
    "designation": "NISS-13301-309-15"
  },
  {
    "code": "1U105",
    "designation": "NISS-014136-309-16"
  },
  {
    "code": "1U012",
    "designation": "NISS-10182-308-16"
  },
  {
    "code": "1U007",
    "designation": "NISS-03552-306-16"
  },
  {
    "code": "1U106",
    "designation": "MDS-001683-316-16"
  },
  {
    "code": "1U107",
    "designation": "MDS-001678-316-16"
  },
  {
    "code": "1U130",
    "designation": "MDS-000450-317-16"
  },
  {
    "code": "1U126",
    "designation": "MDS-000453-317-16"
  },
  {
    "code": "1T001",
    "designation": "Q7-50047-107-16"
  },
  {
    "code": "1T002",
    "designation": "X1-31892-110-16"
  },
  {
    "code": "1T003",
    "designation": "X3-35900-110-16"
  },
  {
    "code": "1T008",
    "designation": "TUCSON-62296-110-16"
  },
  {
    "code": "1T012",
    "designation": "TIIDA-23082-106-16"
  },
  {
    "code": "1T013",
    "designation": "TIIDA-67950-108-16"
  },
  {
    "code": "1T014",
    "designation": "X-TRAIL-20758-108-16"
  },
  {
    "code": "1T019",
    "designation": "YARIS-027412-116-16"
  },
  {
    "code": "1T020",
    "designation": "YARIS-40564-110-16"
  },
  {
    "code": "1T027",
    "designation": "GOLF-08132-110-16"
  },
  {
    "code": "1T029",
    "designation": "JETTA-03887-108-16"
  },
  {
    "code": "1T032",
    "designation": "COR-67023-111-16"
  },
  {
    "code": "1T033",
    "designation": "COR-63741-111-16"
  },
  {
    "code": "1T035",
    "designation": "COR-66433-111-16"
  },
  {
    "code": "1T036",
    "designation": "A4-85034-112-16"
  },
  {
    "code": "1T037",
    "designation": "MINI-22910-112-16"
  },
  {
    "code": "1T040",
    "designation": "X5-66188-112-16"
  },
  {
    "code": "1T042",
    "designation": "YARIS-092741-113-16"
  },
  {
    "code": "1T045",
    "designation": "RAV4-098371-113-16"
  },
  {
    "code": "1T046",
    "designation": "YETI-078088-113-16"
  },
  {
    "code": "1T048",
    "designation": "COR-019023-114-16"
  },
  {
    "code": "1T049",
    "designation": "COR-015998-114-16"
  },
  {
    "code": "1T051",
    "designation": "COR-021058-114-16"
  },
  {
    "code": "1T052",
    "designation": "CRUISER-011165-114-16"
  },
  {
    "code": "1T053",
    "designation": "YARIS-007145-117-16"
  },
  {
    "code": "1T054",
    "designation": "YARIS-006818-117-16"
  },
  {
    "code": "1T055",
    "designation": "YARIS-006819-117-16"
  },
  {
    "code": "1T057",
    "designation": "PICANTO-023355-115-16"
  },
  {
    "code": "1T058",
    "designation": "PICANTO-020153-115-16"
  },
  {
    "code": "1T059",
    "designation": "YARIS-021348-115-16"
  },
  {
    "code": "1T060",
    "designation": "YARIS-021293-115-16"
  },
  {
    "code": "1T061",
    "designation": "COR-075994-115-16"
  },
  {
    "code": "1T062",
    "designation": "COR-068896-115-16"
  },
  {
    "code": "1T064",
    "designation": "AMG-071366-115-16"
  },
  {
    "code": "1T065",
    "designation": "TIGUAN-076618-115-16"
  },
  {
    "code": "1T068",
    "designation": "YARIS-033844-116-16"
  },
  {
    "code": "1T072",
    "designation": "COR-75241-111-16"
  },
  {
    "code": "1T074",
    "designation": "YARIS-031964-116-16"
  },
  {
    "code": "4T005",
    "designation": "YARIS-71990-111-16"
  },
  {
    "code": "1T073",
    "designation": "COR-080627-114-16"
  },
  {
    "code": "0L003",
    "designation": "SITE HAMADI"
  },
  {
    "code": "0L017",
    "designation": "SITE BENI MESSOUS BT-C"
  },
  {
    "code": "0L018",
    "designation": "DEPOT KHEMIS MELIANA"
  },
  {
    "code": "0L004",
    "designation": "TERRAIN OR"
  },
  {
    "code": "1V001",
    "designation": "GROUPE ÉLÉCTROGÈNE OS"
  },
  {
    "code": "1V002",
    "designation": "GROUPE ÉLÉCTROGÈNE BM"
  },
  {
    "code": "1V004",
    "designation": "MONTE CHARGE OS"
  },
  {
    "code": "1V006",
    "designation": "CHAMBRES FROIDES OS"
  },
  {
    "code": "1V008",
    "designation": "CHARIOT ELEVATEUR CST"
  },
  {
    "code": "1V007",
    "designation": "GROUPE ÉLÉCTROGÈNE CST"
  },
  {
    "code": "1L001",
    "designation": "Villa BM"
  },
  {
    "code": "1L002",
    "designation": "Bloc A BM"
  },
  {
    "code": "1L003",
    "designation": "Bloc B BM"
  },
  {
    "code": "1L004",
    "designation": "Dépôt Principal OS"
  },
  {
    "code": "1L005",
    "designation": "Dépôt Principal Anderson OS"
  },
  {
    "code": "1L006",
    "designation": "Bloc administratif OS"
  },
  {
    "code": "1L007",
    "designation": "Dépôt Cst"
  }
]
[
  {
    "Code": "A010003",
    "Designation": "ARMOIRE BASSE EN BOIS",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010004",
    "Designation": "ARMOIRE HAUTE EN BOIS",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010005",
    "Designation": "ARMOIRE HAUTE METALIQUE",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010006",
    "Designation": "ARMOIRE VESTIAIRE",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010008",
    "Designation": "BAHUT EN BOIS",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010009",
    "Designation": "BUREAU EN BOIS",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 0
  },
  {
    "Code": "A010014",
    "Designation": "CAISSON EN BOIS",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 0
  },
  {
    "Code": "A010016",
    "Designation": "CHAISE OPERATEUR",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 0
  },
  {
    "Code": "A010017",
    "Designation": "CHAISE VISITEUR",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010020",
    "Designation": "FAUTEUIL DE BUREAU",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 0
  },
  {
    "Code": "A010021",
    "Designation": "PORTE MONTEAU",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010023",
    "Designation": "TABLE BASSE",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010025",
    "Designation": "TABLE DE REUNION",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010030",
    "Designation": "TABLE RONDE EN BOIS",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010031",
    "Designation": "COMPTOIR DE RECEPTION",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010036",
    "Designation": "BAND VISITEUR",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010051",
    "Designation": "ARMOIRE PORTE CLEFS",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010060",
    "Designation": "ARMOIRE METALIQUE DOSSIERS SUSPENDUS",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010074",
    "Designation": "TABLE DE PRÉPARATION",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A010076",
    "Designation": "CHAISE HAUTE",
    "Category": "A01",
    "Accounting": 218.21,
    "Comon_use": 0
  },
  {
    "Code": "A020004",
    "Designation": "COMPTEUSE DE BILLETS",
    "Category": "A02",
    "Accounting": 218.22,
    "Comon_use": 1
  },
  {
    "Code": "A020009",
    "Designation": "COFFRE FORT GM",
    "Category": "A02",
    "Accounting": 218.22,
    "Comon_use": 1
  },
  {
    "Code": "A020010",
    "Designation": "COFFRE FORT PM",
    "Category": "A02",
    "Accounting": 218.22,
    "Comon_use": 0
  },
  {
    "Code": "A020011",
    "Designation": "DECHICTEUSE DE PAPIER",
    "Category": "A02",
    "Accounting": 218.22,
    "Comon_use": 1
  },
  {
    "Code": "A020020",
    "Designation": "CLIMATISEUR DE BUREAU (-30 BTU)",
    "Category": "A02",
    "Accounting": 213.4,
    "Comon_use": 1
  },
  {
    "Code": "A030001",
    "Designation": "REFREGIRATEUR DE BUREAU",
    "Category": "A03",
    "Accounting": 218.4,
    "Comon_use": 0
  },
  {
    "Code": "A030006",
    "Designation": "CUMULUS",
    "Category": "A03",
    "Accounting": 213.4,
    "Comon_use": 1
  },
  {
    "Code": "A030009",
    "Designation": "FONTAINE FRAICHE",
    "Category": "A03",
    "Accounting": 218.4,
    "Comon_use": 1
  },
  {
    "Code": "A030010",
    "Designation": "MACHINE A CAFE",
    "Category": "A03",
    "Accounting": 218.4,
    "Comon_use": 0
  },
  {
    "Code": "A030011",
    "Designation": "MICRO ONDE",
    "Category": "A03",
    "Accounting": 218.4,
    "Comon_use": 1
  },
  {
    "Code": "A030012",
    "Designation": "TELEVISEUR",
    "Category": "A03",
    "Accounting": 218.4,
    "Comon_use": 1
  },
  {
    "Code": "A030014",
    "Designation": "TABLE DE CUISINE",
    "Category": "A03",
    "Accounting": 218.4,
    "Comon_use": 1
  },
  {
    "Code": "A030015",
    "Designation": "CHAISE DE CUISINE",
    "Category": "A03",
    "Accounting": 218.4,
    "Comon_use": 1
  },
  {
    "Code": "A030016",
    "Designation": "REFREGIRATEUR DE CUISINE",
    "Category": "A03",
    "Accounting": 218.4,
    "Comon_use": 1
  },
  {
    "Code": "A030020",
    "Designation": "Article de sport",
    "Category": "A03",
    "Accounting": 218.4,
    "Comon_use": 1
  },
  {
    "Code": "A040002",
    "Designation": "DISQUE DUR EXTERNE",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 0
  },
  {
    "Code": "A040003",
    "Designation": "ECRAN",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 0
  },
  {
    "Code": "A040004",
    "Designation": "POINTEUR LASER",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 0
  },
  {
    "Code": "A040010",
    "Designation": "IMPRIMANTE MATRICIELLE",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040011",
    "Designation": "IMPRIMANTE JET D'ENCRE",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040012",
    "Designation": "IMPRIMANTE LASER",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040014",
    "Designation": "MICRO PORTABLE",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 0
  },
  {
    "Code": "A040016",
    "Designation": "ONDULEUR",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 0
  },
  {
    "Code": "A040017",
    "Designation": "ONDULEUR MULTIPRISE",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040019",
    "Designation": "SWITCH",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040020",
    "Designation": "ROUTEUR",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 0
  },
  {
    "Code": "A040021",
    "Designation": "UNITE CENTRALE",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 0
  },
  {
    "Code": "A040022",
    "Designation": "UNITE  CENTRALE SERVEUR",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040023",
    "Designation": "POINT D'ACCES WIFI",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040026",
    "Designation": "LECTEUR CODE BARRES",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040037",
    "Designation": "ECRAN DE PROJECTION MURAL",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040038",
    "Designation": "STATION D'ACCUEIL POUR PC PORTABLE",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 0
  },
  {
    "Code": "A040041",
    "Designation": "SWITCH KVM",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040042",
    "Designation": "ECRAN CONSOLE RACKABLE",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040043",
    "Designation": "MULTIFONCTION IMPRIMANTE",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040049",
    "Designation": "ONDULEUR RACKABLE",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040101",
    "Designation": "DATA SHOW",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040200",
    "Designation": "IMPRIMANTE ETIQUETTE",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040300",
    "Designation": "SERVEUR NAS",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040301",
    "Designation": "HDD-NAS",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040302",
    "Designation": "MODEM",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A040303",
    "Designation": "DOCKING STATION",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 0
  },
  {
    "Code": "A040400",
    "Designation": "MATERIEL SONORISATION",
    "Category": "A04",
    "Accounting": 218.3,
    "Comon_use": 1
  },
  {
    "Code": "A050008",
    "Designation": "NISSAN URVAN 3.0",
    "Category": "A05",
    "Accounting": 218.11,
    "Comon_use": 1
  },
  {
    "Code": "A050017",
    "Designation": "MERCEDES SPRINT 311 CDI CH. MOYEN",
    "Category": "A05",
    "Accounting": 218.11,
    "Comon_use": 1
  },
  {
    "Code": "A050018",
    "Designation": "MERCEDES SPRINT 515  CDI CH . LONG",
    "Category": "A05",
    "Accounting": 218.11,
    "Comon_use": 1
  },
  {
    "Code": "A0600100",
    "Designation": "CRETA",
    "Category": "A06",
    "Accounting": 218.13,
    "Comon_use": 0
  },
  {
    "Code": "A0600101",
    "Designation": "SANTAFE",
    "Category": "A06",
    "Accounting": 218.13,
    "Comon_use": 0
  },
  {
    "Code": "A070001",
    "Designation": "ECHELLE",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A070002",
    "Designation": "POSTE A SOUDER",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A070006",
    "Designation": "ESCABEAU",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A070007",
    "Designation": "KARCHER",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A070008",
    "Designation": "PERCEUSE/Visseuse",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A070012",
    "Designation": "CAISSE A OUTILS INFORMATIQUE",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A070013",
    "Designation": "CRIC HYDRAULIQUE",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A070028",
    "Designation": "CAISSE A OUTILS MECANIQUE",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A070029",
    "Designation": "CHARIOT DE MENAGE",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A070030",
    "Designation": "CAISSE A OUTILS ELECTRIQUE",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A070041",
    "Designation": "porte poids étalons",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A070094",
    "Designation": "LASER METRE",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A070095",
    "Designation": "DéBROUSSAILLEUSE",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A079001",
    "Designation": "Table en inox",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A079002",
    "Designation": "Chariot en inox",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A079003",
    "Designation": "Armoire en inox",
    "Category": "A07",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A079004",
    "Designation": "Armoire pour emmagasinage inflammables",
    "Category": "A07",
    "Accounting": 218.21,
    "Comon_use": 1
  },
  {
    "Code": "A079005",
    "Designation": "Etagère en inox",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A079006",
    "Designation": "Plonge",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A079007",
    "Designation": "BIN",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A079008",
    "Designation": "pièce format",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A079009",
    "Designation": "Poinçon",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A079010",
    "Designation": "Canne",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A079011",
    "Designation": "Enrouleur",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A079012",
    "Designation": "Fut à couvercle",
    "Category": "A07",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A080001",
    "Designation": "EXTINCTEUR EAU",
    "Category": "A08",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A080002",
    "Designation": "EXTINCTEUR POUDRE",
    "Category": "A08",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A080003",
    "Designation": "EXTINCTEUR CO2",
    "Category": "A08",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A080004",
    "Designation": "R I A",
    "Category": "A08",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A080008",
    "Designation": "EXTINCTEUR 50 KG",
    "Category": "A08",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A080010",
    "Designation": "EXTINCTEUR VHL",
    "Category": "A08",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A080026",
    "Designation": "ASPIRATEUR INDUSTRIEL",
    "Category": "A08",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A080039",
    "Designation": "CAMERA DE SECURITE",
    "Category": "A08",
    "Accounting": 215.1,
    "Comon_use": 1
  },
  {
    "Code": "A080042",
    "Designation": "BAC A SABLE",
    "Category": "A08",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A080043",
    "Designation": "LAMPE TORCHE",
    "Category": "A08",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A080044",
    "Designation": "NVR",
    "Category": "A08",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A090002",
    "Designation": "TRANSPALETTE MANUELLE",
    "Category": "A09",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A090003",
    "Designation": "CONVOYEUR A TAPIS",
    "Category": "A09",
    "Accounting": 215.1,
    "Comon_use": 1
  },
  {
    "Code": "A090022",
    "Designation": "CHARIOT MANUEL",
    "Category": "A09",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A090025",
    "Designation": "CHARIOT ELEVATEUR",
    "Category": "A09",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A090026",
    "Designation": "RAYONNAGE LEGER",
    "Category": "A09",
    "Accounting": 215.1,
    "Comon_use": 1
  },
  {
    "Code": "A090027",
    "Designation": "RAYONNAGE SEMI-LOURD",
    "Category": "A09",
    "Accounting": 215.1,
    "Comon_use": 1
  },
  {
    "Code": "A090028",
    "Designation": "GERBEUR ELECTRIQUE",
    "Category": "A09",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A090031",
    "Designation": "TRANSPALETTE ELECTRIQUE",
    "Category": "A09",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A090032",
    "Designation": "CHARIOT INOX",
    "Category": "A09",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A090033",
    "Designation": "ÉCHAFAUDAGE",
    "Category": "A09",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A100001",
    "Designation": "CONGELATEUR",
    "Category": "A10",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A100002",
    "Designation": "REFREGIRATEUR GM",
    "Category": "A10",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A100006",
    "Designation": "CHAMBRE FROIDE",
    "Category": "A10",
    "Accounting": 215.1,
    "Comon_use": 1
  },
  {
    "Code": "A100007",
    "Designation": "CHAMBRE FROIDE 75 M3",
    "Category": "A10",
    "Accounting": 215.1,
    "Comon_use": 1
  },
  {
    "Code": "A100011",
    "Designation": "MONTE CHARGE",
    "Category": "A10",
    "Accounting": 215.1,
    "Comon_use": 1
  },
  {
    "Code": "A100018",
    "Designation": "POMPE IMMERGEE",
    "Category": "A10",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A100021",
    "Designation": "SURPRESSEUR",
    "Category": "A10",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A100028",
    "Designation": "ENREGISTREUR DE TEMPERATURE",
    "Category": "A10",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A100029",
    "Designation": "BALANCE ELECTRONIQUE",
    "Category": "A10",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A100034",
    "Designation": "ARMOIRE FRIGORIFIQUE",
    "Category": "A10",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A100055",
    "Designation": "GROUPE ELECTROGENE",
    "Category": "A10",
    "Accounting": 215.2,
    "Comon_use": 1
  },
  {
    "Code": "A100060",
    "Designation": "CLIMATISEUR ARMOIRE (+30 BTU)",
    "Category": "A10",
    "Accounting": 213.4,
    "Comon_use": 1
  },
  {
    "Code": "A110002",
    "Designation": "TELEPHONE FIXE",
    "Category": "A11",
    "Accounting": 218.23,
    "Comon_use": 1
  },
  {
    "Code": "A110003",
    "Designation": "TELEPHONE MOBILE SIMPLE",
    "Category": "A11",
    "Accounting": 218.23,
    "Comon_use": 0
  },
  {
    "Code": "A110014",
    "Designation": "SMARTPHONE SIMPLE",
    "Category": "A11",
    "Accounting": 218.23,
    "Comon_use": 0
  },
  {
    "Code": "A110015",
    "Designation": "SMARTPHONE LUXE",
    "Category": "A11",
    "Accounting": 218.23,
    "Comon_use": 0
  },
  {
    "Code": "A120011",
    "Designation": "INSTALLATION ERP AX 9000",
    "Category": "A12",
    "Accounting": 204,
    "Comon_use": 1
  },
  {
    "Code": "A120016",
    "Designation": "MS WINDOWS CLIENT",
    "Category": "A12",
    "Accounting": 204,
    "Comon_use": 1
  },
  {
    "Code": "A120017",
    "Designation": "MS OFFICE",
    "Category": "A12",
    "Accounting": 204,
    "Comon_use": 1
  },
  {
    "Code": "A120020",
    "Designation": "Logiciel Audit infrastructure IT",
    "Category": "A12",
    "Accounting": 204,
    "Comon_use": 1
  }
]
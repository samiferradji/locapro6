[
  {
    "code": 218.21,
    "designation": "Mobilier de bureau"
  },
  {
    "code": 218.22,
    "designation": "Matériel de bureau"
  },
  {
    "code": 213.4,
    "designation": "Agencement et aménagement sur bâtiment d'autrui"
  },
  {
    "code": 218.4,
    "designation": "Equipements sociaux"
  },
  {
    "code": 218.3,
    "designation": "Matériel informatique"
  },
  {
    "code": 218.11,
    "designation": "Véhicules utilitaires appartenant à l’entreprise"
  },
  {
    "code": 218.13,
    "designation": "Véhicules de tourisme appartenant à l’entreprise"
  },
  {
    "code": 215.2,
    "designation": "Matériel et outillages"
  },
  {
    "code": 215.1,
    "designation": "Installations techniques"
  },
  {
    "code": 218.23,
    "designation": "Téléphonie"
  },
  {
    "code": 204,
    "designation": "Logiciels informatiques et assimilés"
  }
]
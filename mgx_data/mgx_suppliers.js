[
  {
    "code": "FL0114",
    "designation": "A. OUARTI",
    "adresse": "ROUTE DE L'AERODROME, BEJAIA",
    "telephone": "03420 20 99 / 0555070155"
  },
  {
    "code": "FL1988",
    "designation": "AASYS",
    "adresse": "3, AVENUE DES FRERES BOUADOU",
    "telephone": "023540307"
  },
  {
    "code": "FL2051",
    "designation": "ALGERIENNE DU MEUBLE EURL",
    "adresse": "N°10 LOTISSEMENT LE FUMEE BOUZAREAH",
    "telephone": ""
  },
  {
    "code": "FL2030",
    "designation": "ATLAS ACCESS SARL",
    "adresse": "Coopérative El Bina n°58, Dely Ibrahim",
    "telephone": "023-30-48-90"
  },
  {
    "code": "FL1887",
    "designation": "AVISYS TECHENOLOGIE",
    "adresse": "CITE DES FRERES ABBES LOT 10,N° 04 DAR EL BIEDA ALGER",
    "telephone": "0560436970"
  },
  {
    "code": "FL1282",
    "designation": "BAB PHONE SECURITE",
    "adresse": "29, Rue Mohamed Segihier Saddaoui BAB EL OUED",
    "telephone": "021 96 51 10"
  },
  {
    "code": "FL0678",
    "designation": "BNP PARIBAS EL DJAZAIR",
    "adresse": "8, rue de Cirta 16035 Hydra",
    "telephone": "21 48 04 40"
  },
  {
    "code": "FL1713",
    "designation": "BRANDT ALGERIE",
    "adresse": "01, Rue colonel Othmane EL SAKIA",
    "telephone": "048-54-70-57"
  },
  {
    "code": "FL1437",
    "designation": "CITROEN ALGERIE",
    "adresse": "06 BIS ROUTE DELY IBRAHIM CHERAGA",
    "telephone": "021 37 42 42"
  },
  {
    "code": "FL1093",
    "designation": "COFFRAME",
    "adresse": "136 rue mohamed zekkal belcourt - alger -",
    "telephone": "021.27.27.27 / 021.27.17.38"
  },
  {
    "code": "FL0959",
    "designation": "CONDOR ELECTRONICS SPA",
    "adresse": "LOTISSEMENT  426 LOTS N° 8 EL EULMA",
    "telephone": "036 87 54 34"
  },
  {
    "code": "FL1154",
    "designation": "DATATOOLS INFORMATIQUE EURL",
    "adresse": "LOT KAOUCH N° 08 ILOT 03 - DELY IBRAHIM -",
    "telephone": "021 36 92 73 / 021 36 96 45 / 021 36 31 04 / 021 36 25 00 / 021 36 23 92"
  },
  {
    "code": "FL1682",
    "designation": "EASY CONSULT SARL",
    "adresse": "Siége social: Rue Said Youyouz 19B -SIDI M'HAMED- ALGER",
    "telephone": ""
  },
  {
    "code": "FL1492",
    "designation": "EPE / ENIEM SPA",
    "adresse": "ZONE INDUSTRIELLE AISSAT IDIR BP 605 RP OUED-AISSI",
    "telephone": "026 22 54 60/026 22 53 90"
  },
  {
    "code": "FL1384",
    "designation": "GEO SCIENCE SARL",
    "adresse": "09 RUE KADDOUR BOUTALEB KOUBA",
    "telephone": "021 29 82 13 / 021 29 76 10"
  },
  {
    "code": "FL0277",
    "designation": "GMS MERCEDES-BENZ SARL",
    "adresse": "ORAN",
    "telephone": "/"
  },
  {
    "code": "FL1914",
    "designation": "GSAG TECH EURL",
    "adresse": "",
    "telephone": ""
  },
  {
    "code": "FL0125",
    "designation": "HAL HYDRA ASSISTANCE & LOGICIELS",
    "adresse": "LOT 775 N°99 - BIRTOUTA ALGER",
    "telephone": "021 444 546 /"
  },
  {
    "code": "FL1868",
    "designation": "HANOUTCOUM PRODUCTION SARL",
    "adresse": "DOMAINE BEN HAMOUDA LOT 103 EX 94 N°1 BAB EZZOUAR -",
    "telephone": "021-24-28-28"
  },
  {
    "code": "FL0809",
    "designation": "HYDRA OFFICE SARL",
    "adresse": "04, RUE SIDI YAHIYA, BIR MOURAD RAIS, ALGER",
    "telephone": "023 57 33 54 / 023 57 37 29 / 0555 090 396"
  },
  {
    "code": "FL1289",
    "designation": "HYDROFREL EURL",
    "adresse": "Bd Colonel Amirouche BP 107 BOUSMAIL",
    "telephone": "021 39 02 09"
  },
  {
    "code": "FL0871",
    "designation": "HYUNDAI SOMM - SOCIETE OUEST MAINTENANCE MOTORS HYUNDAI",
    "adresse": "quelle adresse ?",
    "telephone": "Numéro de téléphone ?"
  },
  {
    "code": "FL1740",
    "designation": "IBCP(INDUSTRIAL AUSINESS COMPANY)",
    "adresse": "Centre Commercial et d'Affaires El-Qods BUREAU 24-08 NIV 4 CHERAGA",
    "telephone": "021 34 14 28"
  },
  {
    "code": "FL0097",
    "designation": "INDEFRIGO SARL",
    "adresse": "HAOUCHE R'MEL ROUTE DE AIN TAYA ROUIBA - ALGER, ALGER",
    "telephone": "021854 760 /"
  },
  {
    "code": "FL2023",
    "designation": "KB OFFICE EURL",
    "adresse": "siège social 47, rue des romains Birkhadem",
    "telephone": 23366430
  },
  {
    "code": "FL1828",
    "designation": "KEEP CALM",
    "adresse": "11 RUE DES FRERES OUGHLIS EL MOURADIA",
    "telephone": ""
  },
  {
    "code": "FL1471",
    "designation": "KIA MOTORS ALGERIE",
    "adresse": "GARE ROUTIERE DU CAROUBIER HUSSEIN DEY",
    "telephone": "021 49 74 19 A 22"
  },
  {
    "code": "FL1899",
    "designation": "KIT MEUBLE KIT BACHTARZI ABDELMALEK",
    "adresse": "cite amara boulangerie  industrielle lot 299 cheraga",
    "telephone": ""
  },
  {
    "code": "FL1653",
    "designation": "LE BUT ELECTROMENAGER EURL",
    "adresse": "CITE SONELGAZ 02 N° 09 ALGER",
    "telephone": 21837737
  },
  {
    "code": "FL1760",
    "designation": "M.SEHOUR MED EL TAHAR(Propriétaire site Constantine)",
    "adresse": "EL KHROUB-CONSTANTINE",
    "telephone": ""
  },
  {
    "code": "FL1943",
    "designation": "MGM INDUSTIEL",
    "adresse": "ZI MULTI SERVICE AIN EL BEY N 207 LOCAL D",
    "telephone": ""
  },
  {
    "code": "FL1322",
    "designation": "MICRO INFORMATIQUE DIFFUSION",
    "adresse": "04 RUE LOUIS GALIGO PANORAMA HUSSEIN DEY",
    "telephone": "021234633"
  },
  {
    "code": "FL1455",
    "designation": "NEXTSOFTWARE SARL",
    "adresse": "24 LA PETITE PROVENCE HYDRA",
    "telephone": "021 48 34 85 / 021 60 28 73"
  },
  {
    "code": "FL0011",
    "designation": "NISSAN ALGERIE SARL",
    "adresse": "CENTRE D'AFFAIRE RAFIK ROUTE DE AIN TAYA, ROUIBA ALGER",
    "telephone": "02181 78 98 /"
  },
  {
    "code": "FL0057",
    "designation": "ORASCOM TELECOM ALGERIE SPA",
    "adresse": "Rue Mouloud Feraoun,Lot n°8A Dar El Beida, ALGER",
    "telephone": "770850 000 /"
  },
  {
    "code": "FL2055",
    "designation": "R.DRIR",
    "adresse": "OUED ROMANE  STATION DE SERVICE EL ACHOUR",
    "telephone": ""
  },
  {
    "code": "FL0291",
    "designation": "REAL SECURITE",
    "adresse": "SBA",
    "telephone": "/"
  },
  {
    "code": "FL1363",
    "designation": "RENAULT ALGERIE SPA (Dely Brahim)",
    "adresse": "BP79 succursale des grands vents,dely Brahim",
    "telephone": "021 38 76 86/88"
  },
  {
    "code": "FL1865",
    "designation": "RETAIL BANKING SERVICES SARL",
    "adresse": "LOTISSEMENT CHARBONIER N°337",
    "telephone": "021 54 28 28"
  },
  {
    "code": "FL1407",
    "designation": "SAMEG KARCHER",
    "adresse": "27 AVENUE DE L’AN CARBOUIER LA GLACIERE",
    "telephone": "021 49 70 08"
  },
  {
    "code": "FL0499",
    "designation": "SOCIETE GENERALE ALGERIE",
    "adresse": "RESIDENCE EL KERMA- GUE DE CONSTANTINE, BP 55 ALGER",
    "telephone": "021 45 14 00 / 021 45 15 00 /"
  },
  {
    "code": "FL0049",
    "designation": "SOVAC SPA",
    "adresse": "ETABLISSEMENT GRAND VENT , ALGER",
    "telephone": ""
  },
  {
    "code": "FL0027",
    "designation": "STAR BLX EURL",
    "adresse": "rue des abatoires n10 cheraga , ALGER",
    "telephone": "02138 30 56 /"
  },
  {
    "code": "FL0091",
    "designation": "TOYOTA ALGERIE",
    "adresse": "LOT MUCHACHO N° 2 HAUT SITE HYDRA - BENAKNOUR, ALGER",
    "telephone": "021 98 30 00"
  },
  {
    "code": "FL0117",
    "designation": "AMIMER ENERGIE SPA",
    "adresse": "BOUHIA SEDDOUK , BEJAIA",
    "telephone": "03432 31 52 / 03432 31 53"
  },
  {
    "code": "FL1944",
    "designation": "AMS",
    "adresse": "Lot C 190B DRARIA",
    "telephone": ""
  },
  {
    "code": "FL1694",
    "designation": "ATLAS COPCO ALGERIE SPA",
    "adresse": "Route de Sidi Menif Tranche 3 Lot 119 Zeralda",
    "telephone": "023 32 06 34 38"
  },
  {
    "code": "FL1966",
    "designation": "B.I.O",
    "adresse": "",
    "telephone": ""
  },
  {
    "code": "FL0317",
    "designation": "BENHADJ MAAMAR M'HAMED(ELECTROMENAGER-HAIER)",
    "adresse": "KHEMIS MILIANA",
    "telephone": "027 66 48 04  /  0770 40 15 47 /"
  },
  {
    "code": "FL1859",
    "designation": "BT INDUSTRIE",
    "adresse": "13 Rue 1 Novembre Beni MERED",
    "telephone": ""
  },
  {
    "code": "FL2042",
    "designation": "CAME SYSTÈME EURL",
    "adresse": "EL HAMADIA BOUZAREAH",
    "telephone": "023 27 03 27"
  },
  {
    "code": "FL1243",
    "designation": "Celcius Ingénierie",
    "adresse": "Cité djama route de l'université Targa Ouzemour 06000",
    "telephone": "034 20 76 49"
  },
  {
    "code": "FL1807",
    "designation": "CELMED SARL",
    "adresse": "08 RUE Mohamed Douba, -H-DEY",
    "telephone": "021 47 94 08"
  },
  {
    "code": "FL1465",
    "designation": "CGF EURL",
    "adresse": "",
    "telephone": ""
  },
  {
    "code": "FL1862",
    "designation": "CUISINOX SARL",
    "adresse": "Local C lieu dit Thilkamine Flikki Azzazga",
    "telephone": ""
  },
  {
    "code": "FL2035",
    "designation": "DEVISION SERVICES CLIMATISATION SARL",
    "adresse": "108 MARTYRE M'MED IMER DRARIA",
    "telephone": ""
  },
  {
    "code": "FL1885",
    "designation": "DJURAWOOD",
    "adresse": "SUD OUEST",
    "telephone": ""
  },
  {
    "code": "FL1965",
    "designation": "FETTEE",
    "adresse": "",
    "telephone": ""
  },
  {
    "code": "FL1533",
    "designation": "GLOBAL INFO LAGA & CIE SNC",
    "adresse": "RN 12 TAJENANT TIRSATINE AZAZGA TIZI OUZOU",
    "telephone": ""
  },
  {
    "code": "FL1422",
    "designation": "IBLO EURL",
    "adresse": "Sidi Lakhdar Ain Defla",
    "telephone": ""
  },
  {
    "code": "FL1905",
    "designation": "KIFFANE CABINE ET CHAUDRONNERIE",
    "adresse": "HAI BEN MERABETLOT 03 COOP 154 BORDJ EL KIFFAN",
    "telephone": "0560939726"
  },
  {
    "code": "FL0283",
    "designation": "MAGHREB LEASING ALGERIE",
    "adresse": "31,Avenue Mohamed Belkacemi (ex Ravin de la Femme Sauvage), El Madania 16 075",
    "telephone": "21 77 12 12"
  },
  {
    "code": "FL1034",
    "designation": "VEP SARL",
    "adresse": "RUE AHMED OUAKED DELY IBRAHIM ALGER",
    "telephone": "021 33 61 47 / 0770 50 34 99"
  },
  {
    "code": "FL1479",
    "designation": "VIA MENA",
    "adresse": "lot N°201 Draria Alger",
    "telephone": "077 01 17 62"
  },
  {
    "code": "FL1789",
    "designation": "CPFC BOUDIAF EURL",
    "adresse": "",
    "telephone": ""
  },
  {
    "code": "FL0497",
    "designation": "ENALUX  EQUIPEMENT SARL",
    "adresse": "50 ROUTE DE MEFTAH Z I OUED SMAR",
    "telephone": "021.51.36.40 / 021.51.69.02"
  },
  {
    "code": "FL1978",
    "designation": "KMY ASCENSEURS",
    "adresse": "06 Loot Gourjon 16100 Ain Benian",
    "telephone": ""
  }
]
[
  {
    "code": "A01",
    "designation": "MOBILIER DE BUREAU"
  },
  {
    "code": "A02",
    "designation": "EQUIPEMENT DE BUREAU"
  },
  {
    "code": "A03",
    "designation": "EQUIPEMENT SOCIAUX"
  },
  {
    "code": "A04",
    "designation": "MATERIEL INFORMATIQUE"
  },
  {
    "code": "A05",
    "designation": "VEHICIULES UTILITAIRE"
  },
  {
    "code": "A06",
    "designation": "VEHICULE TOURISTIQUE"
  },
  {
    "code": "A07",
    "designation": "MATERIEL ET OUTILLAGE"
  },
  {
    "code": "A08",
    "designation": "EQUIPEMENT DE SECURITE"
  },
  {
    "code": "A09",
    "designation": "EQUIPEMENT ET MATERIEL DE STOCKAGE ET DE MANUTENTION"
  },
  {
    "code": "A10",
    "designation": "EQUIPEMENT TECHNIQUE"
  },
  {
    "code": "A11",
    "designation": "TELEPHONIE"
  },
  {
    "code": "A12",
    "designation": "INVESTISSEMENT INCORPOREL"
  },
  {
    "code": "A13",
    "designation": "EQUIPEMENT INDUSTRIEL"
  },
  {
    "code": "A14",
    "designation": "INSTRUMENT DE LABORATOIRE"
  }
]
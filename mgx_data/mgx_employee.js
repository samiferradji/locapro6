[
  {
    "code_rh": "00040",
    "nom_prenom": "BENTEBIBEL Noureddine",
    "structure": "Achat",
    "direction": "Opérationnel"
  },
  {
    "code_rh": "00543",
    "nom_prenom": "MAKHLOUF Massinissa",
    "structure": "Informatique",
    "direction": "Organisation et système d'informations"
  },
  {
    "code_rh": "00852",
    "nom_prenom": "ALILAT Faiza",
    "structure": "Ressources humaines",
    "direction": "Ressources humaines"
  },
  {
    "code_rh": "00948",
    "nom_prenom": "BOUYACOUB Hiba",
    "structure": "Achat non pharmaceutique",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "01053",
    "nom_prenom": "ABASSI Mohamed",
    "structure": "Moyens généraux",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "01143",
    "nom_prenom": "KATEB Meriem",
    "structure": "Ressources humaines",
    "direction": "Ressources humaines"
  },
  {
    "code_rh": "00002",
    "nom_prenom": "ABBAS TERKI  EL BACHIR",
    "structure": "Présidence",
    "direction": "Présidence"
  },
  {
    "code_rh": "01034",
    "nom_prenom": "AIT YOUCEF ANISS",
    "structure": "Contrôle de gestion",
    "direction": "Contrôle de gestion"
  },
  {
    "code_rh": "00556",
    "nom_prenom": "ALLOUAT FAIZA",
    "structure": "Ventes",
    "direction": "Opérationnel"
  },
  {
    "code_rh": "00427",
    "nom_prenom": "ANANE FARIDA",
    "structure": "Finance",
    "direction": "DFC"
  },
  {
    "code_rh": "01026",
    "nom_prenom": "ANNANI MOUHSSIN CHERIF",
    "structure": "Contrôle financier",
    "direction": "DFC"
  },
  {
    "code_rh": "00440",
    "nom_prenom": "ASMA Mohamed",
    "structure": "Présidence",
    "direction": "Présidence"
  },
  {
    "code_rh": "00926",
    "nom_prenom": "BARRIS IMENE",
    "structure": "Achat",
    "direction": "Opérationnel"
  },
  {
    "code_rh": "01061",
    "nom_prenom": "BEDDOU YASSINE",
    "structure": "Ventes",
    "direction": "Opérationnel"
  },
  {
    "code_rh": "01003",
    "nom_prenom": "BENAISSA FATHI",
    "structure": "Système d'informations",
    "direction": "Organisation et système d'informations"
  },
  {
    "code_rh": "01024",
    "nom_prenom": "BENTALEB IMENE",
    "structure": "Ressources humaines",
    "direction": "Ressources humaines"
  },
  {
    "code_rh": "01205",
    "nom_prenom": "BERBOUCHA Omar",
    "structure": "Juridique",
    "direction": "Secrétariat général"
  },
  {
    "code_rh": "00043",
    "nom_prenom": "BESSAM FADILA",
    "structure": "Intendance",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "01186",
    "nom_prenom": "BOUALLOUCHE Smail",
    "structure": "Communication",
    "direction": "Communication"
  },
  {
    "code_rh": "01261",
    "nom_prenom": "BOURKAIB Mohamed",
    "structure": "Organisation",
    "direction": "Organisation et système d'informations"
  },
  {
    "code_rh": "01086",
    "nom_prenom": "BOUTAA BAYA",
    "structure": "Intendance",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "01087",
    "nom_prenom": "BOUYOUCEF MOHAMED AMINE",
    "structure": "Organisation",
    "direction": "Organisation et système d'informations"
  },
  {
    "code_rh": "01062",
    "nom_prenom": "BRAZA FADILA",
    "structure": "Intendance",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "01553",
    "nom_prenom": "CHEKHAB DJAMEL",
    "structure": "Secrétariat général",
    "direction": "Secrétariat général"
  },
  {
    "code_rh": "01326",
    "nom_prenom": "DAOUD Rafik Brahim",
    "structure": "Informatique",
    "direction": "Organisation et système d'informations"
  },
  {
    "code_rh": "01253",
    "nom_prenom": "DEHIRI Abdelmadjid",
    "structure": "Travaux neufs",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "01332",
    "nom_prenom": "DIRACHE Meriem",
    "structure": "Comptabilité",
    "direction": "DFC"
  },
  {
    "code_rh": "00992",
    "nom_prenom": "DJEZAR AHMED",
    "structure": "Présidence",
    "direction": "Présidence"
  },
  {
    "code_rh": "01277",
    "nom_prenom": "EL HESSAR Mostefa",
    "structure": "Présidence",
    "direction": "Présidence"
  },
  {
    "code_rh": "00831",
    "nom_prenom": "FERRADJI SAMY",
    "structure": "Logistique",
    "direction": "Logistique"
  },
  {
    "code_rh": "01078",
    "nom_prenom": "FRANCIS MUSTAPHA",
    "structure": "Opérationnel",
    "direction": "Opérationnel"
  },
  {
    "code_rh": "00684",
    "nom_prenom": "FRANCIS Abed Djamel Eddine",
    "structure": "Achat",
    "direction": "Opérationnel"
  },
  {
    "code_rh": "01329",
    "nom_prenom": "GHETTAS Mohamed Salah",
    "structure": "Recouvrement",
    "direction": "Opérationnel"
  },
  {
    "code_rh": "00928",
    "nom_prenom": "GUELFI HOURIA",
    "structure": "Communication",
    "direction": "Communication"
  },
  {
    "code_rh": "00786",
    "nom_prenom": "HADDAD MERYEM",
    "structure": "Communication",
    "direction": "Communication"
  },
  {
    "code_rh": "01215",
    "nom_prenom": "HADDOUCHE Amel",
    "structure": "Finance",
    "direction": "DFC"
  },
  {
    "code_rh": "01140",
    "nom_prenom": "HADJERES TAYEB",
    "structure": "Secrétariat général",
    "direction": "Secrétariat général"
  },
  {
    "code_rh": "00689",
    "nom_prenom": "HAMDAD MONCEF",
    "structure": "Finances et comptabilité",
    "direction": "DFC"
  },
  {
    "code_rh": "01084",
    "nom_prenom": "HAMDI YASMINE SAMIA",
    "structure": "Evènementiel",
    "direction": "Evènementiel"
  },
  {
    "code_rh": "01027",
    "nom_prenom": "HAMMADI IBRAHIM",
    "structure": "Business intelligence",
    "direction": "Organisation et système d'informations"
  },
  {
    "code_rh": "01038",
    "nom_prenom": "KANOUN MOHAMED KAMEL",
    "structure": "Achat non pharmaceutique",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "00766",
    "nom_prenom": "KANOUN YASMINE",
    "structure": "Service client",
    "direction": "Contrôle de gestion"
  },
  {
    "code_rh": "00623",
    "nom_prenom": "KEDJIT FERHAT",
    "structure": "Intendance",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "00710",
    "nom_prenom": "KOUDACHE CHAFIK",
    "structure": "Juridique",
    "direction": "Secrétariat général"
  },
  {
    "code_rh": "01138",
    "nom_prenom": "LAKROUT LAMINE",
    "structure": "Intendance",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "01238",
    "nom_prenom": "LEKHDARI Said",
    "structure": "Présidence",
    "direction": "Présidence"
  },
  {
    "code_rh": "00607",
    "nom_prenom": "MEDJDOUBA ZACCHARI",
    "structure": "Finance",
    "direction": "DFC"
  },
  {
    "code_rh": "01045",
    "nom_prenom": "MERZKANE OTMANE",
    "structure": "Logistique",
    "direction": "Logistique"
  },
  {
    "code_rh": "00871",
    "nom_prenom": "MOBARKI KHALED",
    "structure": "Achat",
    "direction": "Opérationnel"
  },
  {
    "code_rh": "00797",
    "nom_prenom": "MOKRANI NASSIBA",
    "structure": "Finance",
    "direction": "DFC"
  },
  {
    "code_rh": "00378",
    "nom_prenom": "RADJI BAYA",
    "structure": "Opérationnel",
    "direction": "Opérationnel"
  },
  {
    "code_rh": "00146",
    "nom_prenom": "RADJI YACINE",
    "structure": "Logistique",
    "direction": "Logistique"
  },
  {
    "code_rh": "01293",
    "nom_prenom": "SENHADJI Bachir",
    "structure": "Présidence",
    "direction": "Présidence"
  },
  {
    "code_rh": "01239",
    "nom_prenom": "SID Walid",
    "structure": "Logistique",
    "direction": "Logistique"
  },
  {
    "code_rh": "01294",
    "nom_prenom": "TAHRAOUI Ahmed Amine",
    "structure": "Travaux neufs",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "00941",
    "nom_prenom": "TALEB HAYATSH",
    "structure": "Intendance",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "01227",
    "nom_prenom": "TOUATI Sara Meriem",
    "structure": "Ventes",
    "direction": "Opérationnel"
  },
  {
    "code_rh": "00394",
    "nom_prenom": "NENOUCHE Wafa",
    "structure": "Affaires réglementaires",
    "direction": "AR/DT"
  },
  {
    "code_rh": "00681",
    "nom_prenom": "YAGOUBI Yasser",
    "structure": "Affaires réglementaires",
    "direction": "AR/DT"
  },
  {
    "code_rh": "01085",
    "nom_prenom": "YOUSFI ZAHOUA",
    "structure": "Pool assistantes",
    "direction": "Secrétariat général"
  },
  {
    "code_rh": "00907",
    "nom_prenom": "ZERGAT MOHAMED AMINE",
    "structure": "Business intelligence",
    "direction": "Organisation et système d'informations"
  },
  {
    "code_rh": "01092",
    "nom_prenom": "ZIGHEMI KENZA",
    "structure": "Contrôle de gestion",
    "direction": "Contrôle de gestion"
  },
  {
    "code_rh": "01337",
    "nom_prenom": "AISSIOU Atmane",
    "structure": "Contrôle de gestion",
    "direction": "Contrôle de gestion"
  },
  {
    "code_rh": "01335",
    "nom_prenom": "BENMOUSSA Nabil",
    "structure": "Comptabilité",
    "direction": "DFC"
  },
  {
    "code_rh": "01334",
    "nom_prenom": "SAF Walid",
    "structure": "Communication",
    "direction": "Communication"
  },
  {
    "code_rh": "01338",
    "nom_prenom": "BRADAI Salim",
    "structure": "Système d'informations",
    "direction": "Organisation et système d'informations"
  },
  {
    "code_rh": "01341",
    "nom_prenom": "LOUNI Sarah",
    "structure": "Secrétariat général",
    "direction": "Secrétariat général"
  },
  {
    "code_rh": "01358",
    "nom_prenom": "OMARI Farid",
    "structure": "DOSI",
    "direction": "DOSI"
  },
  {
    "code_rh": "01360",
    "nom_prenom": "BERNAOUI Sihem",
    "structure": "Service client",
    "direction": "Contrôle de gestion"
  },
  {
    "code_rh": "01361",
    "nom_prenom": "HAMDAD Nazim",
    "structure": "Présidence",
    "direction": "Présidence"
  },
  {
    "code_rh": "01362",
    "nom_prenom": "DJABRI Samy",
    "structure": "Finance",
    "direction": "DFC"
  },
  {
    "code_rh": "01552",
    "nom_prenom": "BOUADDIS Sarra",
    "structure": "Affaires réglementaires",
    "direction": "AR/DT"
  },
  {
    "code_rh": "01391",
    "nom_prenom": "MIRA Ouafa Rania",
    "structure": "Ressources humaines",
    "direction": "Ressources humaines"
  },
  {
    "code_rh": "01390",
    "nom_prenom": "DOUZIDIA Saddek",
    "structure": "Ressources humaines",
    "direction": "Ressources humaines"
  },
  {
    "code_rh": "01370",
    "nom_prenom": "HALAZ Amina",
    "structure": "Pool assistantes",
    "direction": "Secrétariat général"
  },
  {
    "code_rh": "01364",
    "nom_prenom": "SAICHI Houcine",
    "structure": "Comptabilité",
    "direction": "DFC"
  },
  {
    "code_rh": "01397",
    "nom_prenom": "AIT ABDERRAHMANE Amar",
    "structure": "Audit",
    "direction": "Audit"
  },
  {
    "code_rh": "01402",
    "nom_prenom": "LATTAR Idir",
    "structure": "Intendance",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "01413",
    "nom_prenom": "LAKEHAL Mustapha",
    "structure": "Travaux neufs",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "01414",
    "nom_prenom": "TOUAIBIA Mohamed",
    "structure": "Présidence",
    "direction": "Présidence"
  },
  {
    "code_rh": "01415",
    "nom_prenom": "LOUNNAS Radia",
    "structure": "Affaires réglementaires",
    "direction": "AR/DT"
  },
  {
    "code_rh": "01429",
    "nom_prenom": "KOURTAA Yasmine",
    "structure": "Pool assistantes",
    "direction": "Secrétariat général"
  },
  {
    "code_rh": "01434",
    "nom_prenom": "KHELIFI Wissem",
    "structure": "Organisation",
    "direction": "Organisation et système d'informations"
  },
  {
    "code_rh": "01436",
    "nom_prenom": "CHAA Redha",
    "structure": "Intendance",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "01433",
    "nom_prenom": "ZOUBIR Abdelmalik Nedjmallah",
    "structure": "Informatique",
    "direction": "Organisation et système d'informations"
  },
  {
    "code_rh": "01486",
    "nom_prenom": "SERGOUA Feriel",
    "structure": "Pool assistantes",
    "direction": "Secrétariat général"
  },
  {
    "code_rh": "01487",
    "nom_prenom": "SEMMAR Neila",
    "structure": "Pool assistantes",
    "direction": "Secrétariat général"
  },
  {
    "code_rh": "01484",
    "nom_prenom": "DEBIH Fares",
    "structure": "Audit",
    "direction": "Audit"
  },
  {
    "code_rh": "01494",
    "nom_prenom": "MERAZGA Mohamed Mouloud Yacine",
    "structure": "Intendance",
    "direction": "Moyens généraux"
  },
  {
    "code_rh": "01498",
    "nom_prenom": "EDDOUIK Nesrine",
    "structure": "Pool assistantes",
    "direction": "Secrétariat général"
  },
  {
    "code_rh": "01544",
    "nom_prenom": "CHALAL Fayçal",
    "structure": "Développement",
    "direction": "Développement"
  },
  {
    "code_rh": "01506",
    "nom_prenom": "BELARIBI Radia Imène",
    "structure": "Pool assistantes",
    "direction": "Secrétariat général"
  },
  {
    "code_rh": "01522",
    "nom_prenom": "HAMMANA Abdelouahab",
    "structure": "Service client",
    "direction": "Contrôle de gestion"
  },
  {
    "code_rh": "01521",
    "nom_prenom": "BENMANSOUR Yacine",
    "structure": "Audit",
    "direction": "Audit"
  },
  {
    "code_rh": "01542",
    "nom_prenom": "MANSOURI Khaireddine",
    "structure": "Business intelligence",
    "direction": "Organisation et système d'informations"
  },
  {
    "code_rh": "01571",
    "nom_prenom": "KARCOUCHE Lamia",
    "structure": "Pool assistantes",
    "direction": "Secrétariat général"
  }
]
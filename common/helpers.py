import operator
import uuid
import traceback
from datetime import date, timedelta
import datetime as dt

from django.db import IntegrityError
from common import parameters


def get_uuid():
    """
    function to generate un global unique id (type 4)
    :return: varChar
    """
    _uuid = uuid.uuid4()
    return _uuid


def get_user_company(user):
    """
    Get the user's company.
    In case of multi-domain version , the object manager must contains .filter(company=user.userProfile.company)
    :param user:
    :return:
    """
    from common.models import UserProfile
    return UserProfile.objects.get(user=user).company


def create_new_custom_id(model_object, current_user):
    """
    Generate new customised identifier string (id) for the given model
    :return: str
    """
    from common.models import UserProfile
    new_number = None
    new_id = None
    try:
        from common import parameters as common_parameters
        current_company = UserProfile.objects.get(user=current_user).company
        prefix = current_company.prefix
        new_number = model_object.default_manager.filter(company=current_company,
                                                         created_date__year=common_parameters.CURRENT_YEAR).count() + 1
        new_id = '{}{:0>6}/{}'.format(prefix, new_number, common_parameters.CURRENT_YEAR_ABBREVIATION)
        return new_id
    except IntegrityError:
        from common.models import Error
        create_new_custom_id(model_object)
        Error.objects.create(
            created_by_id=1,
            error='Custom ID generation duplicate Error',
            data='New number count {}. New ID {}'.format(str(new_number), new_id)
        )
    except Exception as e:
        from common.models import Error
        Error.objects.create(
            created_by_id=1,
            error=traceback.format_exc(),
            data='New number count {}. New ID {}'.format(str(new_number), new_id)
        )
        raise e


def get_default_date_range():
    """
    Produice a date range "from_date, to_date. This range is used to limit reporting queries size
    :return: Dict
    """
    to_date = date.today()
    from_date = to_date - timedelta(days=90)
    return {'from_date': from_date, 'to_date': to_date}


def compare(inp, relate, cut):
    """
    Compare two inputs
    """
    ops = {'>': operator.gt,
           '<': operator.lt,
           '>=': operator.ge,
           '<=': operator.le,
           '=': operator.eq}
    return ops[relate](inp, cut)


def is_date(string_input):
    """
    Detect if an string input is a date
    :param string_input:
    :return:
    """
    _formats = (
        '%Y',
        '%m/%Y',
        '%m-%Y',
        '%d/%m/%Y',
        '%d/%m/%y',
        '%d-%m-%Y',
        '%d-%m-%y',
    )
    for fmt in _formats:
        try:
            t = dt.datetime.strptime(string_input, fmt)
            return t
        except ValueError as err:
            pass
    return False


def add_new_menus(menu_list):
    """
    Add new menu elements if detected in menu_list
    """
    try:
        from common.models import MenuElement
        current_menu = MenuElement.objects.all().order_by('id')
        for item in menu_list:
            if not current_menu.filter(id=item['id']).exists():
                if item['service']:
                    new_item = MenuElement(
                        id=item['id'],
                        text=item['service']._short_title,
                        title=item['service']._title,
                        sub_title=item['service']._model._meta.app_config.verbose_name,
                        url=item['service']._model._meta.app_label + '/' + item['service']._url,
                        parameters=item['service']._parameters,
                        icon=item['icon'],
                        permission=item['service']().get_view_permission(),
                        state=item['state'],
                        order=item['order'],
                        description=item['service']._description,
                        parent_id=item['parent_id']
                    )
                    new_item.save()
                else:
                    new_item = MenuElement(
                        id=item['id'],
                        text=item['text'],
                        title=item['text'],
                        sub_title=item['text'],
                        url='',
                        parameters='',
                        icon=item['icon'],
                        permission='',
                        state=item['state'],
                        order=item['order'],
                        description='',
                        parent_id=item['parent_id']
                    )
                    new_item.save()
    except Exception:
        pass


def update_menu(menu_list):
    """
    Update menu elements if from description in menu_list
    """
    try:
        from common.models import MenuElement
        for item in menu_list:
            if item['service']:
                current_menu_item = MenuElement.objects.get(id=item['id'])
                current_menu_item.text = item['service']._short_title
                current_menu_item.title = item['service']._title
                current_menu_item.sub_title = item['service']._model._meta.app_config.verbose_name
                current_menu_item.url = item['service']._model._meta.app_label + '/' + item['service']._url
                current_menu_item.parameters = item['service']._parameters
                current_menu_item.icon = item['icon']
                current_menu_item.permission = item['service']().get_view_permission()
                current_menu_item.state = item['state']
                current_menu_item.order = item['order']
                current_menu_item.description = item['service']._description
                current_menu_item.parent_id = item['parent_id']
                current_menu_item.save()
            else:
                current_menu_item = MenuElement.objects.get(id=item['id'])
                current_menu_item.text = item['text']
                current_menu_item.title = item['text']
                current_menu_item.sub_title = item['text']
                current_menu_item.icon = item['icon']
                current_menu_item.state = item['state']
                current_menu_item.order = item['order']
                current_menu_item.parent_id = item['parent_id']
                current_menu_item.save()
    except Exception:
        pass


def documents_storage_path(instance, filename):
    return '{0}/{1}/{2}/{3}/{4}'.format(
        parameters.CURRENT_YEAR,
        instance.origin._meta.app_label,
        instance.origin._meta.object_name,
        str(instance.object_id).replace('/', '_'),
        filename

    )

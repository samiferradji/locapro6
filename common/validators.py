from django.core.exceptions import ValidationError


def validate_file_size(max_file_size_kb):
    """
    Raise validation error if the file size is superior to a max_file_size_kb
    """

    def inner_function(file):
        limit = max_file_size_kb * 1024
        if file.size > limit:
            raise ValidationError(
                ('Téléchargement non authorisé. La taille du fichier ne doit pas dépasser {} kb').format(
                    max_file_size_kb))
    return inner_function

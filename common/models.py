import json
import os

from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.db import models, transaction
from django.db.models.deletion import Collector
from django.forms.models import model_to_dict
from django.utils import timezone

from common.helpers import create_new_custom_id
from locapro6 import settings
from .helpers import documents_storage_path
from .validators import validate_file_size


class CustomManager(models.Manager):
    def get_queryset(self):
        return super(CustomManager, self).get_queryset().exclude(deleted=True)


class DefaultManager(models.Manager):
    def get_queryset(self):
        return super(DefaultManager, self).get_queryset()


class BaseModel(models.Model):
    """
    System tables ---> model.Model like MenuElement
    Common tables ---> BaseModel like TVA
    Domain tables ---> DomainBaseModel like inventory
    """
    custom_id_generator = False
    reference_table = False
    all_domains_common_table = False
    id = models.AutoField(primary_key=True, verbose_name='id', editable=False)
    created_date = models.DateTimeField(auto_now=True, verbose_name='Créer le')
    modified_date = models.DateTimeField(auto_now=True, verbose_name='Modifier le')
    created_by = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Créer par', editable=False)
    deleted = models.BooleanField(verbose_name='Supprimé', default=False, editable=False)
    deleted_at = models.DateTimeField(verbose_name='Supprimé le', default=timezone.datetime(3000, 1, 1), editable=False)
    objects = CustomManager()
    default_manager = DefaultManager()

    def save(self, user=None, safe=True, editing=False, *args, **kwargs):
        if editing and safe:
            if user is None:
                raise ValidationError({'User': 'You ara making a safe data editing whit no User'})
            else:
                data_before = model_to_dict(self.__class__.default_manager.get(id=self.id))
                super(BaseModel, self).save(*args, **kwargs)
                DataChangeLog(origin=self,
                              user=user,
                              action_type='Edit',
                              data_before=data_before,
                              data_after=model_to_dict(self)
                              ).save()
        else:
            super(BaseModel, self).save(*args, **kwargs)

    def delete(self, safe=True, user=None, using_db='default', *args, **kwargs):
        if safe:
            if user is None:
                raise ValidationError({'User': 'You ara making a safe data deleting whit no User'})
            else:
                collector = Collector(using=using_db)
                collector.collect([self])
                if not collector.dependencies:
                    with transaction.atomic():
                        self.deleted = True
                        self.deleted_at = timezone.now()
                        self.save(user=user, safe=False)
                        DataChangeLog(origin=self,
                                      user=user,
                                      action_type='Delete',
                                      data_before=model_to_dict(self),
                                      data_after='').save()

        else:
            super(BaseModel, self).delete(*args, **kwargs)

    class Meta:
        abstract = True


class Company(BaseModel):
    reference_table = True
    all_domains_common_table = True
    id = models.IntegerField(primary_key=True)
    designation = models.CharField(max_length=80, verbose_name="Nom de l'entreprise")
    prefix = models.CharField(max_length=10, verbose_name='Préfix de codification')
    group_mode = models.BooleanField(verbose_name='Mode Groupe', default=False)
    master_company = models.ForeignKey('self', verbose_name='Entreprise de référence', null=True, blank=True,
                                       on_delete=models.PROTECT)
    separated_db = models.BooleanField(verbose_name='Base de données séparées', default=False)

    def full_clean(self, *args, **kwargs):

        if self.group_mode == 'True' and self.master_company_id == '':
            raise ValidationError(
                {'Entreprise de référence': 'Vous devez choisir une entreprise de référence pour le mode groupe'}
            )
        elif self.separated_db == 'True' and self.group_mode == 'False':
            raise ValidationError(
                {'Base de données séparées': 'Valable uniquement si le mode groupe est activé'}
            )
        else:
            super(Company, self).full_clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        if not CompanyProfile.objects.filter(company=self).exists():
            from wms.models import ProductStatus, Store, Emplacement
            from immobilisation.models import MgxEmplacement, ArticleStatus

            with transaction.atomic():
                base_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
                super(Company, self).save()
                if UserProfile.objects.filter(user=self.created_by).exists():
                    user_profile = UserProfile.objects.get(user=self.created_by)
                    user_initial_company = user_profile.company
                    user_profile.company = self
                    user_profile.save()
                else:
                    user_profile = UserProfile(user=self.created_by, company=self)
                    user_initial_company = self
                    user_profile.save()

                with open(os.path.join(base_dir, "locapro6/initial_data", 'product_status.json'), 'r',
                          encoding='utf-8') as f:
                    data_json = json.load(f)
                for item in data_json:
                    new_obj = ProductStatus(
                        **item,
                        created_by=self.created_by,
                        company=self
                    )
                    new_obj.save()

                with open(os.path.join(base_dir, "locapro6/initial_data", 'category.json'), 'r',
                          encoding='utf-8') as f:
                    data_json = json.load(f)
                for item in data_json:
                    new_obj = ArticleCategory(
                        **item,
                        created_by=self.created_by,
                        company=self
                    )
                    new_obj.save()

                with open(os.path.join(base_dir, "locapro6/initial_data", 'store.json'), 'r',
                          encoding='utf-8') as f:
                    data_json = json.load(f)
                for item in data_json:
                    new_obj = Store(
                        **item,
                        created_by=self.created_by,
                        company=self
                    )
                    new_obj.save()

                new_company_profile = CompanyProfile(
                    company=self,
                    picking_store=Store.objects.filter(company=self).order_by('created_date')[2],
                    pharmaceutical_purchase_reception_emplacement=Emplacement.objects.filter(
                        company=self).order_by(
                        'created_date').first(),
                    external_transfer_reception_emplacement=Emplacement.objects.filter(company=self).order_by(
                        'created_date').first(),
                    created_by=self.created_by
                )
                new_company_profile.save()

                with open(os.path.join(base_dir, "locapro6/initial_data", 'immobilisations_articles_status.json'),
                          'r', encoding='utf-8') as f:
                    data_json = json.load(f)
                for item in data_json:
                    new_obj = ArticleStatus(
                        **item,
                        created_by=self.created_by
                    )
                    new_obj.save()
                with open(os.path.join(base_dir, "locapro6/initial_data", 'immobilisations_emplacements.json'), 'r',
                          encoding='utf-8') as f:
                    data_json = json.load(f)
                for item in data_json:
                    new_obj = MgxEmplacement(
                        **item,
                        created_by=self.created_by,
                    )
                    new_obj.save()

                user_profile = UserProfile.objects.get(user=self.created_by)
                user_profile.company = user_initial_company
                user_profile.save()
        else:
            super(Company, self).save()

    def __str__(self):
        return self.designation

    class Meta:
        verbose_name = 'Entreprise'
        unique_together = (('designation', 'deleted_at'), ('prefix', 'deleted_at'))
        permissions = (
            ('view_company', 'Can View Companies'),
            ('export_company', 'Can export Companies'),
        )


class DomainBaseModel(BaseModel):
    custom_id_generator = True
    id = models.CharField(max_length=15, primary_key=True, editable=False)
    company = models.ForeignKey(Company, on_delete=models.PROTECT, verbose_name='Entreprise')

    def save(self, *args, **kwargs):
        user_profile = UserProfile.objects.get(user=self.created_by)
        current_company = user_profile.company
        if user_profile.company.group_mode and self.reference_table \
                and current_company != user_profile.company.master_company:
            raise PermissionDenied

        self.company = current_company
        if self.custom_id_generator and not self.id:
            self.id = create_new_custom_id(type(self), self.created_by)
        if user_profile.company.separated_db and self.reference_table:
            new_distribution = Distribution(
                company=self.company,
                action='save',
                db_table=self._meta.model_name,
                data=self.__dict__,
                created_by=self.created_by
            )
            new_distribution.save()
        super(DomainBaseModel, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        user_profile = UserProfile.objects.get(user=self.created_by)
        current_company = user_profile.company
        if user_profile.company.group_mode and self.reference_table \
                and current_company != user_profile.company.master_company:
            raise PermissionDenied
        else:
            super(DomainBaseModel, self).delete(*args, **kwargs)

    def validate_unique(self, exclude=None):
        self.company_id = UserProfile.objects.get(user=self.created_by).company
        if exclude and 'company' in exclude:
            exclude.remove('company')
        super(DomainBaseModel, self).validate_unique(exclude=exclude)

    class Meta:
        abstract = True


class MenuElement(models.Model):
    """
    Menu element
    """
    text = models.CharField(max_length=80, verbose_name='Texte')
    title = models.CharField(max_length=80, verbose_name='Titre', blank=True)
    sub_title = models.CharField(max_length=80, verbose_name='Sous Titre', blank=True)
    url = models.CharField(max_length=200, verbose_name='URL', blank=True)
    parameters = models.CharField(max_length=200, verbose_name='Paramètres', blank=True)
    icon = models.CharField(max_length=80, verbose_name='Font Awson icon text', blank=True)
    permission = models.CharField(max_length=200, verbose_name='Permission', blank=True)
    state = models.CharField(max_length=80, verbose_name='State', blank=True)
    order = models.IntegerField(verbose_name='Ordre dans le menu', null=True, blank=True)
    description = models.CharField(max_length=200, verbose_name='Description', blank=True)
    parent = models.ForeignKey('self', on_delete=models.PROTECT, verbose_name='Parent', null=True, blank=True)

    def __str__(self):
        return "|".join((self.text, str(self.sub_title), str(self.title)))


class QuickMenu(models.Model):
    """
    Quick menu settings
    """
    menu = models.ForeignKey(MenuElement, verbose_name='Menu', on_delete=models.CASCADE)
    user = models.ForeignKey(User, verbose_name='Utilisateur', on_delete=models.CASCADE)

    class Meta:
        unique_together = ('menu', 'user')

    def __str__(self):
        return "|".join((str(self.menu), str(self.user)))


class AccessLog(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    access_type = models.CharField(max_length=10)
    access_date = models.DateTimeField(auto_now_add=True, verbose_name='Créer le')


class DataChangeLog(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.CharField(max_length=15)
    origin = GenericForeignKey('content_type', 'object_id')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    action_type = models.CharField(max_length=10)
    data_before = models.CharField(max_length=3000)
    data_after = models.CharField(max_length=3000)
    access_date = models.DateTimeField(auto_now_add=True, verbose_name='Créer le')


class ArticleCategory(DomainBaseModel):
    reference_table = True
    code = models.CharField(max_length=50, verbose_name='Code')
    designation = models.CharField(max_length=150, verbose_name='Désignation')

    def __str__(self):
        return ' | '.join((self.code, self.designation))

    class Meta:
        verbose_name = 'Famille d\'articles'
        verbose_name_plural = 'Familles d\'articles'
        unique_together = (('code', 'company', 'deleted_at'), ('designation', 'company', 'deleted_at'))
        permissions = (
            ('admin_articlecategory', 'Can Admin Article Categories'),
            ('view_articlecategory', 'Can View Article Categories'),
            ('export_articlecategory', 'Can export Article Categories'),
        )


class AccountingCode(DomainBaseModel):
    reference_table = True
    code = models.CharField(max_length=50, verbose_name='Code')
    designation = models.CharField(max_length=150, verbose_name='Désignation')

    def __str__(self):
        return ' | '.join((self.code, self.designation))

    class Meta:
        verbose_name = 'Codes comptables'
        verbose_name_plural = 'Codes comptables'
        unique_together = (('code', 'company', 'deleted_at'), ('designation', 'company', 'deleted_at'))
        permissions = (
            ('admin_accountingcode', 'Can Admin Accounting Codes'),
            ('view_accountingcode', 'Can View Accounting Codes'),
            ('export_accountingcode', 'Can export Accounting Codes'),
        )


class Tva(BaseModel):
    all_domains_common_table = True
    reference_table = True
    tva_rate = models.DecimalField(decimal_places=0, max_digits=4, verbose_name='Taux de TVA')

    def __str__(self):
        return str(self.tva_rate) + '%'

    class Meta:
        verbose_name = 'TVA'
        verbose_name_plural = 'TVA'
        permissions = (
            ('admin_tva', 'Can Admin TVA'),
            ('view_tva', 'Can View TVA'),
            ('export_tva', 'Can export TVA'),
        )
        unique_together = ('tva_rate', 'deleted_at')


class DeliveryAxis(DomainBaseModel):
    reference_table = True
    code = models.CharField(max_length=20, verbose_name="Code")
    designation = models.CharField(max_length=80, verbose_name="Axe de livraison")
    description = models.TextField(max_length=400, verbose_name="Descrition de l'axe")

    def __str__(self):
        return self.designation

    class Meta:
        verbose_name = 'Axe de livraison'
        verbose_name_plural = 'Axes de livraison'
        unique_together = (('code', 'company', 'deleted_at'), ('designation', 'company', 'deleted_at'))
        permissions = (
            ('admin_deliveryaxis', 'Can Admin Delivery Axis'),
            ('view_deliveryaxis', 'Can View Delivery Axis'),
            ('export_deliveryaxis', 'Can export Delivery Axis'),
        )


class SupplierMGX(DomainBaseModel):
    reference_table = True
    code = models.CharField(max_length=20, verbose_name='Code')
    supplier = models.CharField(max_length=100, verbose_name='Fournisseur')
    RC = models.CharField(max_length=30, verbose_name='RC', blank=True)
    NIF = models.CharField(max_length=30, verbose_name='NIF', blank=True)
    contact = models.CharField(max_length=100, verbose_name='Contact', blank=True)
    telephone = models.CharField(max_length=100, verbose_name='Telephone', blank=True)
    address = models.CharField(max_length=150, verbose_name='Adresse', blank=True)
    email = models.EmailField(verbose_name='Email', blank=True)
    active = models.BooleanField(verbose_name='Actif ?', default=True)

    def __str__(self):
        return ' '.join((self.code, self.supplier))

    class Meta:
        verbose_name = 'Fournisseur MGX'
        verbose_name_plural = 'Fournisseurs MGX'
        unique_together = (('code', 'company', 'deleted_at'), ('code', 'supplier', 'deleted_at'))
        permissions = (
            ('admin_mgxsupplier', 'Can Admin MGX Supplier'),
            ('view_mgxsupplier', 'Can View MGX Supplier'),
            ('export_mgxsupplier', 'Can export MGX Supplier'),
        )


class Employee(DomainBaseModel):
    class Meta:
        ordering = ['name']
        unique_together = (('code', 'company', 'deleted_at'), ('identity_code', 'deleted_at'))
        verbose_name = 'Employé'
        permissions = (
            ('admin_employee', 'Can Admin Employees'),
            ('view_employee', 'Can view Employees'),
            ('export_employee', 'Can Export Employees'),
        )

    identity_code = models.CharField(max_length=20, verbose_name='Code d\'identité')
    code = models.CharField(max_length=20, verbose_name='Code RH')
    name = models.CharField(max_length=100, verbose_name='Nom et prénom')
    function = models.CharField(max_length=100, verbose_name='Fonction', blank=True)
    structure = models.CharField(max_length=100, verbose_name='Structure', blank=True)
    direction = models.CharField(max_length=100, verbose_name='Direction', blank=True)
    activity_based_costing_code = models.CharField(max_length=20, verbose_name='Code analytique par activité',
                                                   blank=True)
    full_costing_method_code = models.CharField(max_length=20, verbose_name='Code analytique par centre de coût',
                                                blank=True)

    def __str__(self):
        return ' '.join((self.code, ' - ', self.name))


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='user_parc')
    company = models.ForeignKey(Company, verbose_name='Entreprise', on_delete=models.CASCADE)
    code_employee = models.ForeignKey(Employee, verbose_name='Code Employé', on_delete=models.CASCADE, null=True,
                                      blank=True)

    def __str__(self):
        return self.user.__str__()

    class Meta:
        verbose_name = 'Profile utilisateur'
        permissions = (
            ('admin_userprofile', 'Can Admin User Profile'),
            ('view_userprofile', 'Can view User Profile'),
            ('export_userprofile', 'Can export User Profile'),
        )


class Error(DomainBaseModel):
    error_type = models.TextField(max_length=20, verbose_name='Error Type', default='')
    error_name = models.TextField(max_length=50, verbose_name='Error Type', default='')
    error = models.TextField(max_length=500, verbose_name='Error')
    data = models.TextField(max_length=500, verbose_name='Data')


class Wilaya(DomainBaseModel):
    reference_table = True
    code = models.PositiveSmallIntegerField(verbose_name='Code')
    wilaya = models.CharField(max_length=50, verbose_name='Nom de Wilaya')

    class Meta:
        unique_together = (('code', 'company', 'deleted_at'), ('wilaya', 'company', 'deleted_at'))
        permissions = (
            ('admin_wilaya', 'Can Admin Wilaya'),
            ('view_wilaya', 'Can view Wilaya'),
            ('export_wilaya', 'Can export Wilaya'),
        )

    def __str__(self):
        return ' '.join((self.code.__str__(), self.wilaya))


class Commune(DomainBaseModel):
    reference_table = True
    code = models.PositiveSmallIntegerField(verbose_name='Code')
    commune = models.CharField(max_length=50, verbose_name='Nom de la commune')
    wilaya = models.ForeignKey(Wilaya, verbose_name='Wilaya', on_delete=models.PROTECT)

    class Meta:
        unique_together = (('code', 'company', 'deleted_at'), ('commune', 'company', 'deleted_at'))
        permissions = (
            ('admin_commune', 'Can view Commune'),
            ('view_commune', 'Can view Commune'),
            ('export_commune', 'Can export Commune'),
        )

    def __str__(self):
        return self.commune


class Client(DomainBaseModel):
    reference_table = True
    code = models.CharField(max_length=20, verbose_name='Dossier Client')
    name = models.CharField(max_length=50, verbose_name='Nom du client')
    address = models.CharField(max_length=100, verbose_name='Adresse de livraion', blank=True)
    commune = models.ForeignKey(Commune, verbose_name='Commune', on_delete=models.PROTECT, null=True, blank=True)
    delivery_axis = models.ForeignKey(DeliveryAxis, verbose_name='Axe de livaion', on_delete=models.PROTECT, null=True,
                                      blank=True)
    telephone = models.CharField(max_length=30, verbose_name='Téléphone', blank=True)

    class Meta:
        unique_together = (('code', 'company', 'deleted_at'), ('name', 'company', 'deleted_at'))
        permissions = (
            ('admin_client', 'Can Admin Client'),
            ('view_client', 'Can view Client'),
            ('export_client', 'Can export Client'),
        )

    def __str__(self):
        return ' '.join((self.code, '-', self.name))


class DocumentStatus(BaseModel):
    reference_table = True
    status = models.CharField(max_length=30, verbose_name='Statut Document')

    def __str__(self):
        return self.status

    class Meta:
        verbose_name = 'Statut Document'
        verbose_name_plural = 'Statuts des Documents'
        unique_together = ('status', 'deleted_at')
        permissions = (
            ('admin_documentstatus', 'Can Admin Document Status'),
            ('view_documentstatus', 'Can view Document Status'),
            ('export_documentstatus', 'Can export Document Status'),
        )


class CompanyProfile(DomainBaseModel):
    from wms.models import Store, Emplacement
    from immobilisation.models import MgxEmplacement

    class Meta:
        verbose_name = 'Paramètres Entreprise'
        permissions = (
            ('admin_companyprofile', 'Can Admin Company Profile'),
            ('view_companyprofile', 'Can view Company Profile'),
            ('export_companyprofile', 'Can export Company Profile'),
        )

    company = models.OneToOneField(Company, on_delete=models.PROTECT, verbose_name='Entreprise')
    picking_store = models.ForeignKey(Store, on_delete=models.PROTECT, verbose_name='Magasin de picking', null=True,
                                      blank=True)
    pharmaceutical_purchase_reception_emplacement = models.ForeignKey(
        Emplacement, on_delete=models.PROTECT,
        related_name='purchase_reception_emplacement',
        verbose_name='Emplacement de récéption des achats',
        null=True,
        blank=True
    )
    external_transfer_reception_emplacement = models.ForeignKey(
        Emplacement, on_delete=models.PROTECT,
        related_name='external_transfer_reception_emplacement',
        verbose_name='Emplacement de récéption des transfert inter-filiales',
        null=True,
        blank=True)

    investment_reception_emplacement = models.ForeignKey(
        MgxEmplacement, on_delete=models.PROTECT,
        verbose_name='Emplacement par défault de récéption des investissement',
        null=True,
        blank=True)

    def __str__(self):
        return self.company.designation

    def save(self, *args, **kwargs):
        if self.custom_id_generator and not self.id:
            self.id = create_new_custom_id(type(self), self.created_by)
            super(CompanyProfile, self).save(*args, **kwargs)
        else:
            super(CompanyProfile, self).save(*args, **kwargs)


class Document(DomainBaseModel):
    max_file_size_kb = 512
    content_type = models.ForeignKey(ContentType, on_delete=models.PROTECT)
    object_id = models.CharField(max_length=15)
    origin = GenericForeignKey('content_type', 'object_id')
    title = models.CharField(max_length=120, verbose_name='Titre', blank=True)
    description = models.CharField(max_length=1000, verbose_name='Description', blank=True)
    file = models.FileField(
        validators=[#validate_file_size(max_file_size_kb=max_file_size_kb),
                    FileExtensionValidator(allowed_extensions=['PDF', 'PNG', 'JPG', 'JPEG'])],
        upload_to=documents_storage_path
    )

    class Meta:
        verbose_name = 'Documents archivés'
        permissions = (
            ('admin_document', 'Can Admin archived documents'),
            ('view_document', 'Can view archived documents'),
            ('export_document', 'Can export archived documents'),
        )

    def delete(self, *args, **kwargs):
        if kwargs.get('safe'):
            if kwargs.get('safe'):
                file_name = self.file.name.replace('/' + self.file.name.split('/')[-1], "")
                directory = settings.MEDIA_ROOT + '/deleted/' + file_name
                if not os.path.exists(directory):
                    os.makedirs(directory)
            os.rename(self.file.path,
                      settings.MEDIA_ROOT + '/deleted/' +
                      self.file.name)
        else:
            os.remove(self.file.path)
        super(Document, self).delete(*args, **kwargs)


class Distribution(BaseModel):
    company = models.ForeignKey(Company, on_delete=models.PROTECT, verbose_name='Entreprise')
    db_table = models.CharField(max_length=30, verbose_name='Table')
    action = models.CharField(max_length=15, verbose_name='Action')
    data = models.CharField(max_length=500, verbose_name='Data')

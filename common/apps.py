from django.apps import AppConfig


class ReferencesConfig(AppConfig):
    name = 'common'
    verbose_name = 'Références'


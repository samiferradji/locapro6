import datetime
import json, csv
import os
import decimal

from django.conf import settings
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render

from common.models import SupplierMGX, AccessLog, CompanyProfile, UserProfile
from immobilisation.models import Article, MgxEmplacement
from wms.models import LogisticFlowType, StorageType, PharmaceuticalForm, Laboratory, PharmaceuticalSupplier, Store, \
    Emplacement, Product, Batch, PharmaceuticalInvoice, Purchase, Workload, WorkloadExecution, Inventory, ProductStatus
from .models import MenuElement, DocumentStatus, Tva, Employee, ArticleCategory, AccountingCode, Wilaya, Commune, \
    Company
from .services import StandardAdminApi


@login_required(login_url='/login/')
def logout_view(request):
    new_access = AccessLog(user=request.user, access_type='Logout')
    new_access.save()
    logout(request)
    return HttpResponseRedirect('/login/')


@login_required(login_url='/login/')
def home(request):
    media_url = settings.MEDIA_URL
    new_access = AccessLog(
        user=request.user,
        access_type='Login'
    )
    new_access.save()
    current_verstion = '7.0.14'

    return render(request, 'base.html', {'media_url': media_url, 'version': current_verstion})


def schedule(request):
    return render(request, 'schedule.html')


@login_required(login_url='/login/')
def activate(request):
    with transaction.atomic():
        base_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

        with open(os.path.join(base_dir, "locapro6/initial_data", 'document_status.json'), 'r', encoding='utf-8') as f:
            data_json = json.load(f)
        for item in data_json:
            new_obj = DocumentStatus(
                **item,
                created_by=request.user
            )
            new_obj.save()

        with open(os.path.join(base_dir, "locapro6/initial_data", 'flew_type.json'), 'r', encoding='utf-8') as f:
            data_json = json.load(f)
        for item in data_json:
            new_obj = LogisticFlowType(
                **item,
                created_by=request.user
            )
            new_obj.save()

        with open(os.path.join(base_dir, "locapro6/initial_data", 'storage_type.json'), 'r', encoding='utf-8') as f:
            data_json = json.load(f)
        for item in data_json:
            new_obj = StorageType(
                **item,
                created_by=request.user
            )
            new_obj.save()

        with open(os.path.join(base_dir, "locapro6/initial_data", 'tva.json'), 'r', encoding='utf-8') as f:
            data_json = json.load(f)
        for item in data_json:
            new_obj = Tva(
                **item,
                created_by=request.user
            )
            new_obj.save()

        return HttpResponse('Activated')


@login_required(login_url='/login/')
def import_mgx_data(request):
    """
    Import initial data for immobilisations
    Parameters
    ----------
    request

    Returns
    -------

    """
    user = request.user
    base_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    with open(os.path.join(base_dir, "locapro6/initial_data/", 'wms_product.json'), 'r', encoding='utf-8') as f:
        data_json = json.load(f)
    with transaction.atomic():
        # i = {
        #     "created_date": "2018-12-29 00:00:27.151016",
        #     "modified_date": "2018-12-29 00:00:27.151044",
        #     "deleted": 1,
        #     "deleted_at": "3000-01-01 00:00:00.000000",
        #     "id": "HP007058/18",
        #     "code": "6924",
        #     "product": "CLAMOCLAV 8:1 AD SCHT B/12",
        #     "dosage": "",
        #     "conditioning": "",
        #     "weight": 0,
        #     "volume": 0,
        #     "package_weight": 0,
        #     "thermal_sensibility": 0,
        #     "psychotropic": 0,
        #     "picking_stock_min": 50,
        #     "picking_stock_max": 200,
        #     "principal_stock_min": 500,
        #     "principal_stock_max": 2000,
        #     "active": 1,
        #     "description": "",
        #     "category_id": "HC000001/19",
        #     "created_by_id": 1,
        #     "dci_id": "",
        #     "laboratory_id": "",
        #     "pharmaceutical_form_id": "",
        #     "picking_emplacement_id": "HC000001/19",
        #     "tva_id": 1,
        #     "company_id": 1
        # }
        for item in data_json:
            new_obj = Product(
                **item
            )
            new_obj.save()

    # with transaction.atomic():
    #     user = request.user
    #     base_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    #
    #     with open(os.path.join(base_dir, "locapro7/mgx_data", 'accounting_codes.js'), 'r', encoding='utf-8') as f:
    #         data_json = json.load(f)
    #     for item in data_json:
    #         new_obj = AccountingCode(
    #             code=item['code'],
    #             designation=item['designation'],
    #             created_by=user
    #         )
    #         new_obj.save()
    #
    #     with open(os.path.join(base_dir, "locapro7/mgx_data", 'categories.js'), 'r', encoding='utf-8') as f:
    #         data_json = json.load(f)
    #     for item in data_json:
    #         new_obj = ArticleCategory(
    #             code=item['code'],
    #             designation=item['designation'],
    #             created_by=user
    #
    #         )
    #         new_obj.save()
    #
    #     with open(os.path.join(base_dir, "locapro7/mgx_data", 'mgx_suppliers.js'), 'r', encoding='utf-8') as f:
    #         data_json = json.load(f)
    #     for item in data_json:
    #         new_obj = SupplierMGX(
    #             code=item['code'],
    #             supplier=item['designation'],
    #             address=item['adresse'],
    #             telephone=item['telephone'],
    #             created_by=user
    #         )
    #         new_obj.save()
    #
    #     with open(os.path.join(base_dir, "locapro7/mgx_data", 'articles.js'), 'r', encoding='utf-8') as f:
    #         data_json = json.load(f)
    #     for item in data_json:
    #         new_obj = Article(
    #             article_code=item['Code'],
    #             designation=item['Designation'],
    #             category=ArticleCategory.objects.get(code=item['Category']),
    #             accounting_code=AccountingCode.objects.get(code=item['Accounting']),
    #             common_use=item['Comon_use'],
    #             created_by=user
    #         )
    #         new_obj.save()
    #
    #     with open(os.path.join(base_dir, "locapro7/mgx_data", 'mgx_employee.js'), 'r', encoding='utf-8') as f:
    #         data_json = json.load(f)
    #     for item in data_json:
    #         new_obj = Employee(
    #             code=item['code_rh'],
    #             name=item['nom_prenom'],
    #             structure=item['structure'],
    #             direction=item['direction'],
    #             created_by=user
    #         )
    #         new_obj.save()
    #
    #     with open(os.path.join(base_dir, "locapro7/mgx_data", 'mgx_emplacements.js'), 'r', encoding='utf-8') as f:
    #         data_json = json.load(f)
    #     for item in data_json:
    #         new_obj = MgxEmplacement(
    #             code=item['code'],
    #             designation=item['designation'],
    #             created_by=user
    #         )
    #         new_obj.save()
    #     return HttpResponse('Activated')


@login_required(login_url='/login/')
def validate_all_inventory_transactions(request):
    """
    Import initial data for immobilisations
    Parameters
    ----------
    request

    Returns
    -------

    """
    with transaction.atomic():
        inventory_count = Inventory.objects.filter(received=False, sent=True).count()
        Inventory.objects.filter(received=False, sent=True).update(received=True)
        return HttpResponse(str(inventory_count) + 'lines confirmés')


@login_required(login_url='/login/')
def import_batch_data(request):
    """
    Import initial data for immobilisations
    Parameters
    ----------
    request

    Returns
    -------

    """
    user = request.user
    base_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    with open(os.path.join(base_dir, "locapro6/initial_data/", '_19NOMEN.csv'), 'r', encoding='UTF-8') as f:
        data_json = csv.reader(f, delimiter=';')
        with transaction.atomic():
            i = 1

            for item in data_json:
                # if not PharmaceuticalSupplier.objects.filter(code=item[6]).exists():
                #     PharmaceuticalSupplier(
                #         code=item[6],
                #         supplier=item[7],
                #         created_by_id=1
                #     ).save(item[7])
                #     print(item[7])
                # if not Product.objects.filter(code=item[0]).exists():
                #     Product(
                #         code=item[0],
                #         product=item[3],
                #         created_by_id=1
                #     ).save()
                print(str(i) + '-' + item[0])
                Batch(
                    content_type_id=1,
                    object_id=1,
                    product=Product.objects.get(code=item[0]),
                    batch_number=item[1],
                    expiration_date=datetime.datetime.strptime(item[2], "%d/%m/%y"),
                    supplier=PharmaceuticalSupplier.objects.get(code=item[6]),
                    packaging=int(item[8]),
                    purchase_pu_ht=0,
                    unit_discount_percentage=0,
                    unit_discount_value=0,
                    purchase_pu_ht_net=0,
                    tva_id=1,
                    selling_pu_ht=decimal.Decimal(item[4], ),
                    selling_pu_ttc=decimal.Decimal(item[4]),
                    shp=decimal.Decimal(item[5]),
                    ppa_ht=decimal.Decimal(item[4]),
                    unit_weight=0,
                    unit_volume=0,
                    packaged_weight=0,
                    created_by_id=1

                ).save()
                print(i)
                i += 1
            return HttpResponse('Lot Importés ')


@login_required(login_url='/login/')
def import_inventory_data(request):
    """
    Import initial data for immobilisations
    Parameters
    ----------
    request

    Returns
    -------

    """
    base_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    with open(os.path.join(base_dir, "locapro6/initial_data/", 'stock.csv'), 'r', encoding='CP850') as f:
        data_csv = csv.reader(f, delimiter=';')

        with transaction.atomic():
            index = 1
            default_content_type = ContentType.objects.get_for_model(Inventory)
            default_status = ProductStatus.objects.first()
            for item in data_csv:
                emplacement = Emplacement.objects.get(emplacement=item[11]) if Emplacement.objects.filter(
                    emplacement=item[11]).exists() else Emplacement(
                    emplacement=item[11],
                    store=Store.objects.get(designation=item[10]),
                    storage_type=Store.objects.get(designation=item[10]).storage_type,
                    created_by=request.user
                )
                emplacement.save()

                if Batch.objects.filter(batch_number=item[2], product__code=item[0]).exists():
                    new_inventory = Inventory(
                        content_type=default_content_type,
                        object_id=index,
                        batch=Batch.objects.get(batch_number=item[2], product__code=item[0]),
                        emplacement=emplacement,
                        transaction='Inventory',
                        received=True,
                        sent=True,
                        quantity=item[13],
                        product_status=default_status,
                        created_by=request.user
                    )
                    new_inventory.save()
                    index += 1
                else:
                    tva = Tva.objects.get(tva_rate=item[8])
                    if Product.objects.filter(code=item[0]).exists():
                        product = Product.objects.get(code=item[0])
                    else:
                        product = Product.objects.create(
                            code=item[0],
                            product=item[1],
                            created_by=request.user
                        )
                    if PharmaceuticalSupplier.objects.filter(supplier=item[4]).exists():
                        supplier = PharmaceuticalSupplier.objects.get(supplier=item[4])
                    else:
                        supplier = PharmaceuticalSupplier.objects.create(
                            code='AUTO' + str(index),
                            supplier=item[4],
                            created_by=request.user
                        )
                        print(supplier)
                    Batch(
                        content_type=default_content_type,
                        object_id='HP{:0>6}/50'.format(str(index)),
                        id='HP{:0>6}/50'.format(str(index)),
                        product=product,
                        batch_number=item[2],
                        expiration_date=item[3],
                        supplier_id=supplier.id,
                        packaging=item[12],
                        purchase_pu_ht=decimal.Decimal(item[5]),
                        purchase_pu_ht_net=decimal.Decimal(item[5]),
                        tva=tva,
                        selling_pu_ht=decimal.Decimal(item[6]),
                        selling_pu_ttc=decimal.Decimal(item[6]) * (1 + tva.tva_rate / 100),
                        shp=decimal.Decimal(item[9]),
                        ppa_ht=decimal.Decimal(item[7]),
                        created_by=request.user
                    ).save()

                    new_inventory = Inventory(
                        content_type=default_content_type,
                        object_id=index,
                        batch=Batch.objects.get(batch_number=item[2], product__code=item[0]),
                        emplacement=emplacement,
                        transaction='Inventory',
                        received=True,
                        sent=True,
                        quantity=item[13],
                        product_status=default_status,
                        created_by=request.user
                    )
                    new_inventory.save()
                    index += 1

    return HttpResponse('Stock Importé ')


def migrate_data_from_locapro6(request):
    pass


#     with transaction.atomic():
#         i = 0
#         users = User.objects.using('locapro6_db').values()
#         for obj in users:
#             ins = User(**obj)
#             ins.save()
#             i += 1
#
#         users_profile = UserProfile.objects.using('locapro6_db').values('id', 'user_id', 'company_id')
#         for obj in users_profile:
#             ins = UserProfile(**obj)
#             ins.save()
#             i += 1
#
#         employee = Employee.default_manager.using('locapro6_db').values(
#             'id',
#             'name',
#             'code',
#             'created_by_id',
#             'created_date',
#         )
#         for obj in employee:
#             inst = Employee(**obj, identity_code='{}{}'.format(obj['code'], str(i)))
#             inst.save()
#             i += 1
#
#         forms = PharmaceuticalForm.default_manager.using('locapro6_db').values(
#             'id',
#             'designation',
#             'short_designation',
#             'created_by_id',
#             'created_date',
#         )
#         for obj in forms:
#             inst = PharmaceuticalForm(**obj)
#             inst.save()
#             i += 1
#         labos = Laboratory.default_manager.using('locapro6_db').values(
#             'id',
#             'code',
#             'designation',
#             'created_by_id',
#             'created_date',
#         )
#         for obj in labos:
#             inst = Laboratory(**obj)
#             inst.save()
#             i += 1
#
#         pharma_suppliers = PharmaceuticalSupplier.default_manager.using('locapro6_db').values(
#             'id',
#             'code',
#             'supplier',
#             'created_by_id',
#             'created_date',
#         )
#         for obj in pharma_suppliers:
#             inst = PharmaceuticalSupplier(**obj)
#             inst.save()
#             i += 1
#
#         emplacement = Emplacement.default_manager.using('locapro6_db').values(
#             'id',
#             'emplacement',
#             'store_id',
#             'created_by_id',
#             'created_date',
#         )
#         for obj in emplacement:
#             inst = Emplacement(**obj)
#             inst.save()
#             i += 1
#
#         products = Product.default_manager.using('locapro6_db').values(
#             'id',
#             'product',
#             'code',
#             'laboratory_id',
#             'picking_emplacement_id',
#             'tva_id',
#             'created_by_id',
#             'created_date',
#         )
#         for obj in products:
#             inst = Product(**obj)
#             inst.save()
#             i += 1
#         batchs = Batch.default_manager.using('locapro6_db').values(
#             'id',
#             'product_id',
#             'supplier_id',
#             'batch_number',
#             'expiration_date',
#             'packaging',
#             'purchase_pu_ht',
#             'purchase_pu_ht_net',
#             'unit_discount_percentage',
#             'unit_discount_value',
#             'selling_pu_ht',
#             'selling_pu_ttc',
#             'ppa_ht',
#             'shp',
#             'object_id',
#             'content_type_id',
#             'tva_id',
#             'created_by_id',
#             'created_date',
#         )
#         for obj in batchs:
#             inst = Batch(**obj)
#             inst.save()
#             i += 1
#         invoices = PharmaceuticalInvoice.default_manager.using('locapro6_db').values(
#             'id',
#
#             'supplier_id',
#             'invoice_number',
#             'invoice_date',
#             'value_ht',
#             'value_ht_net',
#             'discount_on_invoice',
#             'value_tva',
#             'value_ttc',
#             'payment_date',
#             'document_status_id',
#
#             'created_by_id',
#             'created_date',
#
#         )
#         for obj in invoices:
#             inst = PharmaceuticalInvoice(**obj)
#             inst.save()
#             i += 1
#
#         purchases = Purchase.default_manager.using('locapro6_db').values(
#             'id',
#
#             'receipt_number',
#             'receipt_date',
#             'observation',
#             'invoice_id',
#             'flew_type_id',
#             'document_status_id',
#
#             'created_by_id',
#             'created_date',
#         )
#         for obj in purchases:
#             inst = Purchase(**obj)
#             inst.save()
#             i += 1
#
#         purchases_details = PurchaseDetail.default_manager.using('locapro6_db').values(
#             'id',
#
#             'parent_id',
#             'batch_id',
#             'quantity',
#             'emplacement_id',
#             'created_by_id',
#             'created_date',
#         )
#         for obj in purchases_details:
#             inst = PurchaseDetail(**obj)
#             inst.save()
#             i += 1
#         workloads = Workload.default_manager.using('locapro6_db').values(
#             'id',
#
#             'confirmed_by_id',
#             'confirmed_date',
#             'total_units',
#             'unpacked_units',
#             'lines',
#             'complete_packages',
#             'palletized_packages',
#             'flew_type_id',
#             'content_type_id',
#             'object_id',
#
#             'created_by_id',
#             'created_date',
#         )
#         for obj in workloads:
#             inst = Workload(**obj)
#             inst.save()
#             i += 1
#
#         workloads_executions = WorkloadExecution.default_manager.using('locapro6_db').values(
#             'id',
#
#             'workload_id',
#             'employee_id',
#             'group_workforce',
#
#             'created_by_id',
#             'created_date',
#         )
#         for obj in workloads_executions:
#             inst = WorkloadExecution(**obj)
#             inst.save()
#             i += 1
#
#
#         return HttpResponse(i)


class EmployeeAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Employee
    _default_actions = ['add', 'change', 'delete', 'export', 'attach']
    _model_data = {
        'enumeration': True,
        'sort': ['name'],
        'reverse_sort': False,
        'text_field': ['name'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _fields = {
        'identity_code':
            {
                'title': 'N° d\'identité',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('identity_code').blank,
                'tooltip': 'le nuémro d\'identité unique national',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 2,
                'edit': True,
                'required': not _model._meta.get_field('code').blank,
                'tooltip': 'le code interne de l\'employé',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'name':
            {
                'title': 'Nom et Prénom',
                'format': 'text',
                'width': False,
                'column_order': 3,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 3,
                'edit': True,
                'required': not _model._meta.get_field('name').blank,
                'tooltip': 'le nom et le prénom de l\'employé',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            }
    }
    _html_page = 'references/employee_admin_page.html'
    _url = 'employee_admin/'
    _parameters = ''
    _short_title = 'Employés'
    _title = 'Table des Employés'
    _description = 'La gestion de la table des employés'


class ArticleCategoryAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = ArticleCategory
    _model_data = {
        'enumeration': True,
        'sort': ['designation'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation ou famille d\'articles',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('code').blank,
                'tooltip': 'Code de la famille d\'articles',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _html_page = 'references/article_category_admin_page.html'
    _url = 'article_category_admin/'
    _parameters = ''
    _short_title = 'Familles d\'articles'
    _title = 'Familles d\'articles'
    _description = 'La gestion de la table des familles d\'articles'


class AccountingCodeAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = AccountingCode
    _model_data = {
        'enumeration': True,
        'sort': ['designation'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation ou famille d\'articles',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('code').blank,
                'tooltip': 'Code de la famille d\'articles',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _html_page = 'references/accounting_code_admin_page.html'
    _url = 'accounting_code_admin/'
    _parameters = ''
    _short_title = 'Codes comptables'
    _title = 'Codes comptables'
    _description = 'La gestion de la table des codes comptables'


class TvaAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Tva
    _model_data = {
        'enumeration': True,
        'sort': ['tva_rate'],
        'reverse_sort': False,
        'text_field': ['tva_rate'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete']
    _fields = {
        'tva_rate':
            {
                'title': 'Taux de TVA',
                'format': 'decimal',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('tva_rate').blank,
                'tooltip': 'Taux de TVA. Exemple : 19.00',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _html_page = 'references/tva_admin_page.html'
    _url = 'tva_admin/'
    _parameters = ''
    _short_title = 'TVA'
    _title = 'Table des TVA'
    _description = 'La gestion de la table des TVA'


class WilayaAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Wilaya
    _model_data = {
        'enumeration': True,
        'sort': ['wilaya'],
        'reverse_sort': False,
        'text_field': ['wilaya'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'wilaya':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': 5,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('wilaya').blank,
                'tooltip': 'Désignation de la Wilaya',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('code').blank,
                'tooltip': 'Code de la Wilaya',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _html_page = 'references/wilaya_admin_page.html'
    _url = 'wilaya_admin/'
    _parameters = ''
    _short_title = 'Wilaya'
    _title = 'Table des Wilaya'
    _description = 'La gestion de la table des codes Wilaya'


class CommuneAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Commune
    _model_data = {
        'enumeration': True,
        'sort': ['commune'],
        'reverse_sort': False,
        'text_field': ['commune'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'commune':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('commune').blank,
                'tooltip': 'Désignation de la commune',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('code').blank,
                'tooltip': 'Code de la commune',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'wilaya_id':
            {
                'title': 'Wilaya',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'common/wilaya_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('wilaya_id').blank,
                'tooltip': 'Choix de la wilaya',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'wilaya__wilaya':
            {
                'title': 'Wilaya',
                'format': 'text',
                'width': False,
                'column_order': 3,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('wilaya').blank,
                'tooltip': '',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },

    }
    _html_page = 'references/commune_admin_page.html'
    _url = 'commune_admin/'
    _parameters = ''
    _short_title = 'Communes'
    _title = 'Table des communes'
    _description = 'La gestion de la table des communes'


class CompanyAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Company
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export', 'attach']
    _fields = {
        'id':
            {
                'title': 'ID unique',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('id').blank,
                'tooltip': 'L\'identifient unique de l\'entreprise',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'designation':
            {
                'title': 'Nom de l\'entreprise',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Le nom de l\'entreprise ou raison sociale',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'prefix':
            {
                'title': 'Codification',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('prefix').blank,
                'tooltip': 'Le préfixe de codification des documents',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'group_mode':
            {
                'title': 'Mode Groupe',
                'format': 'boolean',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'checkbox',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('group_mode').blank,
                'tooltip': 'Les données de cette entreprise en mode groupe?',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'master_company_id':
            {
                'title': 'Entreprise Maître',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'common/companies_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 2,
                'edit': True,
                'required': not _model._meta.get_field('master_company').blank,
                'tooltip': 'Entreprise Maître ?',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'master_company__designation':
            {
                'title': 'Entreprise Maître',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': '',
                'input_autocomplete_url': '',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 2,
                'edit': False,
                'required': '',
                'tooltip': 'Entreprise Maître ?',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'separated_db':
            {
                'title': 'DB séparée',
                'format': 'boolean',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'checkbox',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 3,
                'edit': True,
                'required': not _model._meta.get_field('separated_db').blank,
                'tooltip': 'Bases de données séparées de celle des autres entreprises du groupe?',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },

    }
    _html_page = 'references/company_admin_page.html'
    _url = 'company_admin/'
    _parameters = ''
    _short_title = 'Entreprises'
    _title = 'Entreprises'
    _description = 'La gestion de la table des entreprises'


class CompanyProfileAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = CompanyProfile
    _default_actions = ['change']
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': False,
        'text_field': ['company'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _fields = {
        'company_id':
            {
                'title': 'Nom de l\'entreprise',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'common/companies_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('company_id').blank,
                'tooltip': 'Le nom de l\'entreprise ou raison sociale',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'picking_store__designation':
            {
                'title': 'Prélèvement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/store_list/',
                'input_validation_url': '',
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('picking_store').blank,
                'tooltip': 'Le préfixe de codification des documents',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'external_transfer_reception_emplacement__emplacement':
            {
                'title': 'Réc. Transfert Ext.',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/emplacement_list/',
                'input_validation_url': '',
                'input_group': 3,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('external_transfer_reception_emplacement').blank,
                'tooltip': 'Emplacement de réception des transfert externes',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'pharmaceutical_purchase_reception_emplacement__emplacement':
            {
                'title': 'Réc. Achats Pharma',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/emplacement_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 2,
                'edit': True,
                'required': not _model._meta.get_field('pharmaceutical_purchase_reception_emplacement').blank,
                'tooltip': 'Emplacement de réception des achats pharmaceutiques',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'investment_reception_emplacement__designation':
            {
                'title': 'Réc. Achats Immob.',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/emplacement_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 5,
                'input_order': 2,
                'edit': True,
                'required': not _model._meta.get_field('investment_reception_emplacement').blank,
                'tooltip': 'Emplacement de réception des achats des immobilisations',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },

    }
    _html_page = 'references/company_profile_admin_page.html'
    _url = 'company_profile_admin/'

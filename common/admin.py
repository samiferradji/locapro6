from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from immobilisation.models import MgxEmplacement
from .models import SupplierMGX, Company, Employee, AccountingCode, ArticleCategory, \
    Tva, DeliveryAxis, DocumentStatus, UserProfile, CompanyProfile, Document

admin.site.site_header = "LOCAPRO 6"
admin.site.site_title = "LOCAPRO 6"
admin.site.index_title = "Administration des données"


class AdminBase(admin.ModelAdmin):
    exclude = ['id', 'created_by', 'company']

    def get_queryset(self, request):
        queryset = super(AdminBase, self).get_queryset(request)
        user_profile = UserProfile.objects.get(user=request.user)
        if user_profile.company.group_mode and self.model.reference_table:
            queryset = queryset.filter(company=user_profile.company.master_company)
        else:
            queryset = queryset.filter(company=user_profile.company)
        return queryset

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()


class ProfileInline(admin.StackedInline):
    model = UserProfile
    can_delete = False
    verbose_name = 'Profile'
    fk_name = 'user'


class CustomUserAdmin(UserAdmin):
    inlines = (ProfileInline,)

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)


class EmployeeAdmin(AdminBase):
    list_display = ['id', 'code', 'name', 'structure', 'direction', 'created_by', 'created_date', 'modified_date']
    search_fields = ['id', 'code', 'name', 'structure', 'direction']
    list_select_related = ['created_by']


class SupplierMGXAdmin(AdminBase):
    list_display = ['id', 'code', 'supplier', 'RC', 'NIF', 'contact', 'telephone', 'address', 'email', 'active',
                    'created_by', 'created_date', 'modified_date']
    search_fields = ['id', 'code', 'supplier', 'RC', 'NIF', 'contact', 'telephone', 'address', 'email', 'active']


class MgxEmplacementAdmin(AdminBase):
    list_display = ['id', 'code', 'designation', 'created_by', 'created_date', 'modified_date']
    search_fields = ['id', 'code', 'designation']


class DeliveryAxisAdmin(AdminBase):
    list_display = ['id', 'code', 'designation', 'description', 'created_by', 'created_date', 'modified_date']
    search_fields = ['id', 'code', 'designation', 'description', ]


class CompanyProfileAdmin(AdminBase):
    pass


class CompanyAdmin(admin.ModelAdmin):
    list_display = ['id', 'designation', 'prefix', 'group_mode', 'master_company', 'created_by', 'created_date',
                    'modified_date']
    search_fields = ['id', 'designation', 'prefix']
    exclude = ['created_by']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()


class TvaAdmin(admin.ModelAdmin):
    exclude = ['created_by']
    readonly_fields = ['id']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()


class DocumentStatusAdmin(admin.ModelAdmin):
    exclude = ['created_by']
    readonly_fields = ['id']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()


class ArticleCategoryAdmin(AdminBase):
    list_display = ['id', 'code', 'designation']
    readonly_fields = ['id']
    search_fields = ['code', 'designation']


class AccountingCodeAdmin(AdminBase):
    list_display = ['code', 'designation']
    search_fields = ['code', 'designation']


class DocumentAdmin(admin.ModelAdmin):

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()


admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
admin.site.register(SupplierMGX, SupplierMGXAdmin)
admin.site.register(MgxEmplacement, MgxEmplacementAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(CompanyProfile, CompanyProfileAdmin)
admin.site.register(Employee, EmployeeAdmin)
admin.site.register(AccountingCode, AccountingCodeAdmin)
admin.site.register(ArticleCategory, ArticleCategoryAdmin)
admin.site.register(Tva, TvaAdmin)
admin.site.register(DocumentStatus, DocumentStatusAdmin)
admin.site.register(DeliveryAxis, DeliveryAxisAdmin)

admin.site.register(Document, DocumentAdmin)

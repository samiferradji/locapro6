from django.urls import path
from locapro6 import settings
from common import services
from common import views

if not settings.MIGRATION_MODE:
    from common.menu import menu_list
    from common.services import enabled_services

urlpatterns = [
    path('home/', views.home),
    path('schedule/', views.schedule),
    path('activate/', views.activate),
    path('migrate/', views.migrate_data_from_locapro6),
    path('logout/', views.logout_view, name='logout'),
    path('quick_menu/', services.GetQuickMenuElement.as_view()),
    path('main_menu/', services.GetMenuElements.as_view()),
    path('get_raw_data_from_server/', services.GetRawData.as_view()),
    path('valider_tout', views.validate_all_inventory_transactions),
    path('import_inventory_data', views.import_inventory_data),
]

# import mgx data
urlpatterns += [
    path('import_mgx_data/', views.import_mgx_data),
    path('import_batch_data/', views.import_batch_data)
]

if not settings.MIGRATION_MODE:
    urlpatterns += [path(menu['service']._url, menu['service'].as_view()) for menu in menu_list if menu['service']]
    urlpatterns += [path(service['service']._url, service['service'].as_view()) for service in enabled_services]

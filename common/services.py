import collections
import csv
import json
import traceback
from datetime import timedelta, datetime
from functools import reduce
from operator import or_, and_

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist, ValidationError
from django.core.serializers.json import DjangoJSONEncoder
from django.db import transaction
from django.db.models import CharField, Value as Val, Q, F, ProtectedError
from django.db.models.functions import Concat
from django.forms import model_to_dict
from django.http import HttpResponse
from django.shortcuts import render
from django.utils import timezone
from django.views.generic import View

from .helpers import get_default_date_range, compare, get_uuid
from .models import Employee, MenuElement, SupplierMGX, QuickMenu, Error, Tva, Company, UserProfile, CompanyProfile, \
    Document, Wilaya, ArticleCategory


class StandardAdminApi(LoginRequiredMixin, View):
    """
    Standard view to manages database tables Data
    """
    login_url = '/login/'
    _default_actions = ['add', 'change', 'delete', 'export', 'attach', 'confirm', 'send']
    _form_width = '400px'
    _count = 1
    _model = None  # just a default Model
    _parent_model = None
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': False,
        'text_field': ['id'],
        'table_filter': False,
        'date_range_filter': False,
    }
    _model_filter = None
    _fields = {}
    _html_page = ''
    _url = ''
    _parameters = ''
    _short_title = ''
    _title = ''
    _description = ''
    _api_permission_type = '.admin_'

    def get_view_permission(self):
        if self._parent_model:
            return self._parent_model._meta.app_label + self._api_permission_type + self._parent_model._meta.model_name
        else:
            return self._model._meta.app_label + self._api_permission_type + self._model._meta.model_name

    def get_parent_data(self):
        if self._parent_model:
            parent = self._parent_model.objects.filter(id=self.request.GET['parent_id'])
            if parent.exists():
                return parent.values()[0]
        return {'id': '000000/00'}

    def set_authorized_actions(self):
        authorized_actions = []
        for action in self._default_actions:
            if self.request.user.has_perm(
                    self._model._meta.app_label + '.' + action + '_' + self._model._meta.model_name):
                authorized_actions.append(action)
        self._model_data['action_buttons'] = authorized_actions

    def get_fields_list(self, parameter='all'):
        """
        Extract the fields list from _fields
        Returns
        -------
        list of strings
        """
        if parameter == 'all':
            return [*self._fields]
        elif parameter == 'display':
            return [key for key, value in self._fields.items() if value['display']]
        elif parameter == 'edit':
            return [key.split('__')[0] for key, value in self._fields.items() if value['edit']]
        elif parameter == 'input':
            return [key.split('__')[0] for key, value in self._fields.items() if
                    value['input'] and value['input_type'] != 'file']
        elif parameter == 'input_files':
            return [key.split('__')[0] for key, value in self._fields.items() if
                    value['input'] and value['input_type'] == 'file']
        elif parameter == 'generic_instance':
            return ['object_id', 'content_type__model']

    def get_queryset(self):
        """
        Get the model data as a default queryset
        """
        queryset = self._model.objects.values(*self.get_fields_list(parameter='display'))
        return queryset

    def apply_domain_filter(self, queryset, request):
        if self._model.all_domains_common_table:
            return queryset
        else:
            user_profile = UserProfile.objects.get(user=request.user)
            if user_profile.company.group_mode and self._model.reference_table:
                queryset = queryset.filter(company=user_profile.company.master_company)
            else:
                queryset = queryset.filter(company=user_profile.company)
            return queryset

    def filter_queryset(self, request, queryset):
        """
        Apply filters to queryset
        """

        q = request.GET.get('q', None).strip()
        parent_id = request.GET.get('parent_id', None)
        fixed_filter = request.GET.get('fixed_filter', None)
        page = request.GET.get('page', None)
        page_size = request.GET.get('page_size', None)
        field = request.GET.get('field', None)
        from_date = request.GET.get('from_date', None)
        to_date = request.GET.get('to_date', None)

        _fields = self.get_fields_list(parameter='display')
        _fields_filter = ['{}{}'.format(key, value['search_type']) for key, value in self._fields.items() if
                          value['search']]
        _fields.append('pk')
        sort = self._model_data['sort']
        if self._model_filter:
            queryset = queryset.filter(reduce(and_, [Q(**{k: v}) for k, v in self._model_filter.items()]))
        if self._parent_model or (parent_id and parent_id != ''):
            queryset = queryset.filter(parent_id=parent_id)
        if fixed_filter and fixed_filter != '':
            _filter_dict = json.loads(fixed_filter)
            _filter_dict = {k: v for k, v in _filter_dict.items()}
            queryset = queryset.filter(**_filter_dict)
        if from_date:
            queryset = queryset.filter(created_date__gte=from_date)
        if to_date:
            queryset = queryset.filter(created_date__lte=datetime.strptime(to_date, "%Y-%m-%d") + timedelta(days=1))
        if len(self._model_data['text_field']) == 1:
            queryset = queryset.values(*[f for f in _fields]).order_by(*[s for s in sort]).annotate(
                field_text=F(self._model_data['text_field'][0]))
        if len(self._model_data['text_field']) != 1:
            queryset = queryset.values(*[f for f in _fields]).order_by(*[s for s in sort]).annotate(
                field_text=Concat(*self.format_field_text(), output_field=CharField()))

        if self._model_data['reverse_sort']:
            queryset = queryset.reverse()
        if q and field:
            q_list = list(q.split(' '))
            for q_word in q_list:
                if q_word:
                    search_expression = '{}{}'.format(field, self._fields[field]['search_type'])
                    field_list = [Q(**{search_expression: q_word})]
                    queryset = queryset.filter(reduce(or_, field_list))
        if q and not field:
            q_list = list(q.split(' '))
            for q_word in q_list:
                if q_word:
                    field_list = [Q(**{f: q_word}) for f in _fields_filter]
                    queryset = queryset.filter(reduce(or_, field_list))
        self._count = len(queryset)
        if page:
            queryset = queryset[0:(int(page_size) * int(page))]
        return queryset

    def format_field_text(self):
        """
        Create string text of each field in the queryset the to be used to show selected field in the html input area
        """
        output_list = []
        last_field = self._model_data['text_field'][-1]
        for field in self._model_data['text_field']:
            output_list.append(field)
            if not last_field == field:
                output_list.append(Val(' | '))
        return output_list

    def default_field_text(self, pk):
        """
        Create string text of a field as title of this field
        This function is used when getting object to edit , and the output is autocomplete
        """
        data = self._model.objects.filter(pk=pk).values_list(*self._model_data['text_field'])[0]
        if len(data) > 1:
            return [' | '.join(map(lambda x: str(x), data))]
        else:
            return data

    def get_field_title(self, field):
        try:
            return self._fields[field]['title']
        except KeyError:
            return self._model._meta.get_field(field.split('__')[0]).verbose_name

    def format_queryset(self, queryset):
        """
        Create a custom dictionary to be used by my javascript library, and that, to produce
        DataTable and ComboDataTable html
        """
        for key, value in self._fields.items():
            value['text'] = self.get_field_title(key)
            value['format'] = value.get('format', 'text')

        self._fields = collections.OrderedDict(sorted(self._fields.items(), key=lambda item: item[1]['column_order']))
        self._model_data['model_name'] = self._model.__name__.lower()
        self._model_data['parent_data'] = self.get_parent_data()
        response = {'model_data': self._model_data,
                    'fields': list(queryset),
                    'columns_data': self._fields,
                    'count': self._count}
        return response

    def apply_conditional_formatting(self, queryset):
        """
        Formatting data (add css class) depending on rules.
        warning: apply after query limit[] for better performances
        """
        for data in queryset:
            data['conditional_format'] = {}
            data['conditional_format']['all_row'] = []
            for field_name, value in data.items():
                if field_name in ['pk', 'field_text', 'conditional_format']:
                    pass
                else:
                    if self._fields[field_name]['conditional_format']:
                        data['conditional_format'][field_name] = []
                        for condition in self._fields[field_name]['conditional_format']:
                            if compare(condition['value'], condition['operator'], value):
                                data['conditional_format'][field_name].append(condition['css_class'])
                                if condition['all_row']:
                                    data['conditional_format']['all_row'].append(condition['css_class'])

        return queryset

    def add_attached_files_column(self, queryset):
        if 'attach' in self._model_data['action_buttons']:
            for field in queryset:
                field['attachments_exists'] = Document.objects.filter(
                    object_id=field['pk'], content_type=ContentType.objects.get_for_model(self._model)
                ).exists()
        return queryset

    def get_inputs_layout(self):
        """
        Build a dictionary witch represent the input form layout
        :return: dict
        """
        input_groups = [value['input_group'] for key, value in self._fields.items() if value['input']]
        input_dict = {}
        for group in input_groups:
            input_dict[group] = sorted([key for key, value in self._fields.items() if
                                        value['input'] and value['input_group'] == group],
                                       key=lambda k: self._fields[k]['input_order'])

        return input_dict

    def get(self, request):
        user = request.user
        self.set_authorized_actions()
        if request.GET.get('action') == 'export_data':
            if not user.has_perm(self._model._meta.app_label + '.export_' + self._model._meta.model_name):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' GET : export data',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to edit data on this table")

            else:
                response = HttpResponse(content_type='text/csv')
                filename = 'Table ' + self._model._meta.model_name + ' au ' + str(timezone.now())
                response['Content-Disposition'] = 'attachment; filename="' + filename + '.csv"'
                writer = csv.writer(response, delimiter=';')
                _data = self.get_queryset()
                if self._parent_model:
                    _data = _data.filter(parent_id=request.GET.get('parent_id'))
                writer.writerow([key for key in _data[0]])
                for item in _data:
                    row = [item[i] for i in item]
                    writer.writerow(row)
                return response
        elif request.GET.get('action') == 'view_data':
            if not user.has_perm(self.get_view_permission()):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' GET : view_data',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to edit data on this table")
            else:
                queryset = self.get_queryset()
                queryset = self.apply_domain_filter(queryset=queryset, request=request)
                queryset = self.filter_queryset(queryset=queryset, request=request)
                queryset = self.apply_conditional_formatting(queryset=queryset)
                queryset = self.add_attached_files_column(queryset=queryset)
                queryset = self.format_queryset(queryset=queryset)

                response = json.dumps(queryset, cls=DjangoJSONEncoder)
                return HttpResponse(response, content_type='application/json')

        else:
            _uuid = get_uuid()
            return render(request, self._html_page, {'uuid': _uuid})

    def post(self, request):
        user = request.user
        if request.POST.get('action') == 'get_line':
            if not user.has_perm(self.get_view_permission()):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' POST : get_view_parameters',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to view data on this table")
            else:
                try:
                    line_id = request.POST.get('line_id')
                    if line_id == 'undefined' or not line_id:
                        status = 'Error'
                        status_massage = {
                            'Erreur Interne!': "Merci de sélectionner une ligne"}
                        status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                        response = {'status': status, 'status_message': status_massage}
                        Error.objects.create(
                            error_type='Browser Bug',
                            error_name='Line not selected',
                            error='Browser is not inspecting data correctly before sending request',
                            data='model =' + self._model.__name__ + ' POST : get_line ',
                            created_by=user
                        )
                        return HttpResponse(json.dumps(response), content_type='application/json')
                    else:
                        with transaction.atomic():
                            obj = self._model.objects.values(*self.get_fields_list(parameter='edit')).filter(id=line_id)
                            item_data = obj[0]
                            item_data['pk'] = line_id
                            item_data['instance_title'] = self.default_field_text(line_id)
                            status = 'OK'
                            status_massage = 'Ligne chargée'
                            status_data = json.dumps(item_data, cls=DjangoJSONEncoder)
                            response = {'status': status,
                                        'status_message': status_massage,
                                        'status_data': status_data}
                        return HttpResponse(json.dumps(response, cls=DjangoJSONEncoder),
                                            content_type='application/json')
                except ObjectDoesNotExist:
                    status = 'Error'
                    status_massage = {
                        'Integrité des données': "Aucune ligne trouvée !!"}
                    status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                    Error.objects.create(
                        error_type='Bug',
                        error_name='ObjectDoesNotExist',
                        error='Object selected for changing (get_line) does not exist',
                        data=dict(request.POST),
                        created_by=user
                    )
                    response = {'status': status, 'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
                except Exception as e:
                    Error.objects.create(
                        created_by=user,
                        error=traceback.format_exc(),
                        data=dict(request.POST),
                        error_type='Bug',
                        error_name='Exception',
                    )
                    raise e
        elif request.POST.get('action') == 'add_line':
            if not user.has_perm(self._model._meta.app_label + '.add_' + self._model._meta.model_name):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' POST : add_line',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to add data in this table")
            else:
                try:
                    with transaction.atomic():
                        file_params = {k: request.FILES.get(k) for k in self.get_fields_list(parameter='input_files') if
                                       k not in ['id', 'object_id', 'content_type']}
                        post_params = {k: request.POST.get(k).strip() for k in self.get_fields_list(parameter='input')
                                       if
                                       k not in ['object_id', 'content_type']}
                        generic_object = {k: request.POST.get(k) for k in
                                          self.get_fields_list(parameter='generic_instance') if k != 'id'}
                        if generic_object.get('object_id'):
                            generic_object['content_type'] = ContentType.objects.get(
                                model=generic_object['content_type__model'])
                            del generic_object['content_type__model']

                        else:
                            generic_object = {}

                        obj = self._model(**post_params, **file_params, **generic_object, created_by=user)
                        obj.full_clean(exclude=['company'])
                        obj.save()
                        status = 'Done'
                        status_massage = '{} créé(e) sous le numéro {}'.format(self._model._meta.verbose_name.title(),
                                                                               str(obj.pk))
                        response = {'status': status,
                                    'status_message': status_massage
                                    }
                    return HttpResponse(json.dumps(response, cls=DjangoJSONEncoder), content_type='application/json')
                except ValidationError as e:
                    status = 'Error'
                    status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_massage}
                    Error.objects.create(
                        created_by=user,
                        error=str(status_massage),
                        data=dict(request.POST),
                        error_type='User lack training',
                        error_name='ValidationError',
                    )
                    return HttpResponse(json.dumps(response), content_type='application/json')

                except Exception as e:
                    Error.objects.create(
                        created_by=user,
                        error=traceback.format_exc(),
                        data=dict(request.POST),
                        error_type='Bug',
                        error_name='Exception',
                    )
                    raise e
        elif request.POST.get('action') == 'edit_line':
            if not user.has_perm(self._model._meta.app_label + '.change_' + self._model._meta.model_name):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__.lower() + ' POST : edit_line',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to edit data on this table")
            else:
                try:
                    with transaction.atomic():
                        line_id = request.POST.get('line_id')
                        post_params = {k: request.POST.get(k) for k in self.get_fields_list(parameter='edit') if
                                       k != 'id'}
                        obj = self._model.objects.get(id=line_id)
                        for key, value in post_params.items():
                            setattr(obj, key, value)
                        obj.full_clean(exclude=['company'])
                        obj.save(user=user, editing=True)
                        status = 'Done'
                        status_massage = '{} ({} | {}) modifié(e)'.format(self._model._meta.verbose_name.title(),
                                                                          str(obj.pk), str(obj))
                        response = {'status': status,
                                    'status_message': status_massage
                                    }
                        return HttpResponse(json.dumps(response, cls=DjangoJSONEncoder),
                                            content_type='application/json')
                except ValidationError as e:
                    status = 'Error'
                    status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_massage}
                    Error.objects.create(
                        created_by=user,
                        error=str(status_massage),
                        data=dict(request.POST),
                        error_type='User lack training',
                        error_name='ValidationError',
                    )
                    return HttpResponse(json.dumps(response), content_type='application/json')

                except Exception as e:
                    Error.objects.create(
                        created_by=user,
                        error=traceback.format_exc(),
                        data=dict(request.POST),
                        error_type='Bug',
                        error_name='Exception',
                    )
                    raise e
        elif request.POST.get('action') == 'delete_line':
            if not user.has_perm(self._model._meta.app_label + '.delete_' + self._model._meta.model_name):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' POST : delete_line',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to delete data on this table")
            else:
                try:
                    with transaction.atomic():
                        line_id = request.POST.get('line_id')
                        obj = self._model.objects.get(id=line_id)
                        obj.delete(user=user, safe=True)
                        status = 'Done'
                        status_massage = '{} ({} | {}) supprimé(e)'.format(self._model._meta.verbose_name.title(),
                                                                           str(obj.pk), str(obj))
                        response = {'status': status,
                                    'status_message': status_massage
                                    }
                        return HttpResponse(json.dumps(response, cls=DjangoJSONEncoder),
                                            content_type='application/json')
                except ValidationError as e:
                    status = 'Error'
                    status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_massage}
                    Error.objects.create(
                        created_by=user,
                        error=str(status_massage),
                        data=dict(request.POST),
                        error_type='User lack training',
                        error_name='ValidationError',
                    )
                    return HttpResponse(json.dumps(response), content_type='application/json')
                except ProtectedError:
                    status = 'Error'
                    status_massage = json.dumps(
                        ({
                            'Integrité des données':
                                'Supression impossible, cet enregistrement est utilisé dans d\'autres tables'
                        }), cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_massage}
                    Error.objects.create(
                        created_by=user,
                        error_type='User lack training',
                        error_name='ProtectedError',
                        error=str(status_massage),
                        data=dict(request.POST)
                    )
                    return HttpResponse(json.dumps(response), content_type='application/json')
                except Exception as e:
                    Error.objects.create(
                        created_by=user,
                        error=traceback.format_exc(),
                        data=dict(request.POST),
                        error_type='Bug',
                        error_name='Exception',
                    )
                    raise e
        elif request.POST.get('action') == 'get_view_parameters':
            if not user.has_perm(self.get_view_permission()):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' POST : get_view_parameters',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to view data on this table")
            else:
                try:
                    self.set_authorized_actions()
                    input_form_layout = {'input_form_layout': self.get_inputs_layout()}
                    max_file_size_kb = {'max_file_size_kb': Document.max_file_size_kb}
                    table_filter = {'table_filter': self._model_data.get('table_filter')}
                    date_range_filter = {'date_range_filter': self._model_data.get('date_range_filter')}
                    action_buttons = {'action_buttons': self._model_data.get('action_buttons')}
                    filter_fields = {'filter_fields': [
                        {'name': key,
                         'title': self.get_field_title(key)
                         } for key, value in self._fields.items() if value['search']]}
                    form_fields = {'fields': self._fields}
                    form_width = {'form_width': self._form_width}

                    status = ''
                    status_massage = 'Page {} chargés'.format(self._title)
                    status_data = {**action_buttons, **filter_fields, **date_range_filter, **form_fields,
                                   **table_filter, **max_file_size_kb, **input_form_layout, **form_width}
                    response = {'status': status,
                                'status_message': status_massage,
                                'status_data': status_data
                                }
                    return HttpResponse(json.dumps(response, cls=DjangoJSONEncoder), content_type='application/json')
                except Exception as e:
                    Error.objects.create(
                        created_by=user,
                        error=traceback.format_exc(),
                        data=dict(request.POST),
                        error_type='Bug',
                        error_name='Exception',
                    )
                    raise e


class GetMenuElements(View):
    """
    Format menu element from database to match jeasyUI tree format
    """
    query = MenuElement.objects.all().order_by('id', 'order').exclude(state='hidden')

    def format_menu_list(self):
        _tempo_menu = list()
        for obj in self.query:
            new_element = dict()
            new_element['id'] = obj.id
            new_element['text'] = str(obj.text)
            new_element['faIcon'] = str(obj.icon)
            new_element['state'] = str(obj.state)
            new_element['title'] = str(obj.title)
            new_element['subTitle'] = str(obj.sub_title)
            new_element['children'] = list()
            new_element['parent_id'] = obj.parent_id
            new_element['permission'] = obj.permission
            new_element['description'] = obj.description
            new_element['url'] = str(obj.url) + str(obj.parameters)
            _tempo_menu.append(new_element)
        return _tempo_menu

    def bring_children_to_their_parents(self):
        __tempo_menu = self.format_menu_list()
        for element in __tempo_menu:
            if element['parent_id'] is not None:
                for elm in __tempo_menu:
                    if elm['id'] == element['parent_id']:
                        elm['children'].append(element)
        return __tempo_menu

    def remove_element_withou_parent(self):
        __outpu_list = list()
        __tempo_menu = self.bring_children_to_their_parents()
        for element in __tempo_menu:
            if element['parent_id'] is None:
                __outpu_list.append(element)

        return __outpu_list

    def apply_permissions_on_element(self, element, user):
        """
        Remouve an element if it don't have permission or children
        :param element:
        :return:
        """
        if user.has_perm(element['permission']):
            return element
        else:
            if element['children']:
                element['children'] = self.apply_permission_on_element_list(element['children'], user)
                if element['children']:
                    return element
        return None

    def apply_permission_on_element_list(self, elements_list, user):
        """
        remove elements witch dont have permissions
        :return:
        """
        __output_list = []
        if not elements_list:
            return __output_list
        else:
            for element in elements_list:
                __element = self.apply_permissions_on_element(element, user)
                if __element:
                    __output_list.append(__element)
            return __output_list

    def get(self, request, *args, **kwargs):
        user = request.user
        __output_list = self.remove_element_withou_parent()
        __output_list = self.apply_permission_on_element_list(__output_list, user)
        __output_list = json.dumps(__output_list)
        return HttpResponse(__output_list, content_type='application/json')


class GetQuickMenuElement(View):
    """
    Return a Json object of quick menu settings
    """

    def get(self, request, *args, **kwargs):
        _user = request.GET['user']
        response = QuickMenu.objects.filter(user_id=_user).values('menu__id',
                                                                  'menu__title',
                                                                  'menu__sub_title',
                                                                  'menu__icon',
                                                                  'menu__url',
                                                                  'menu__description',
                                                                  'menu__parameters')
        response = json.dumps(list(response), cls=DjangoJSONEncoder)
        return HttpResponse(response, content_type='application/json')

    def post(self, request, *args, **kwargs):
        try:
            _action = request.POST.get('action')
            _user = request.POST.get('user')
            _menu_id = request.POST.get('menu_id')
            if _action == 'add_menu':
                QuickMenu.objects.create(user_id=_user, menu_id=_menu_id)
            elif _action == 'delete_menu':
                QuickMenu.objects.filter(user_id=_user, menu_id=_menu_id).delete()

            return HttpResponse('Done', content_type='application/text')
        except Exception as e:
            Error.objects.create(
                created_by=request.user,
                error=traceback.format_exc(),
                data=dict(request.POST)
            )
            return HttpResponse('Error', content_type='application/text')


class GetRawData(View):
    """
    Get parameters or global data from database
    """

    def get(self, request, *args, **kwargs):
        if request.GET.get('action') == 'get_picking_store_id':
            current_company = UserProfile.objects.get(user=request.user).company
            picking_store = CompanyProfile.objects.get(company=current_company).picking_store
            picking_store_id = picking_store.id
            return HttpResponse(picking_store_id)


class ArchivedDocumentsView(StandardAdminApi):
    _model = Document
    _form_width = '500px'
    _default_actions = ['add', 'delete']
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': True,
        'text_field': ['title'],
        'table_filter': False,
        'date_range_filter': True
    }
    _model_filter = None
    _fields = {
        'file':
            {
                'title': 'Fichier',
                'format': 'file',
                'width': False,
                'column_order': 4,
                'display': True,
                'input': True,
                'input_type': 'file',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': False,
                'edit': True,
                'required': not _model._meta.get_field('file').blank,
                'tooltip': 'l\'identifient de la ligne',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'object_id':
            {
                'title': '',
                'format': 'text',
                'width': False,
                'column_order': 0,
                'display': False,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': False,
                'edit': False,
                'required': not _model._meta.get_field('file').blank,
                'tooltip': 'l\'identifient de la ligne',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'content_type__model':
            {
                'title': '',
                'format': 'text',
                'width': False,
                'column_order': 0,
                'display': False,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': False,
                'edit': False,
                'required': not _model._meta.get_field('file').blank,
                'tooltip': 'l\'identifient de la ligne',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }

    _html_page = 'references/archived_documents_admin.html'
    _url = 'archived_document_admin/'
    _parameters = ''
    _short_title = 'Documents archivés'
    _title = 'Gestion des documents archivé'
    _description = 'La gestion des documets archivés'


class CompaniesList(StandardAdminApi):
    """
    Return Json list of companies
    """
    _model = Company
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _fields = {
        'designation':
            {
                'title': 'Nom de l\'entreprise',
                'format': 'text',
                'width': 6,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Le nom de l\'entreprise ou raison sociale',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'prefix':
            {
                'title': 'Codification',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('prefix').blank,
                'tooltip': 'Le préfixe de codification des documents',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _url = 'companies_list/'
    _api_permission_type = '.view_'


class TvaList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Tva
    _model_data = {
        'enumeration': True,
        'sort': ['tva_rate'],
        'reverse_sort': False,
        'text_field': ['tva_rate'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = []
    _fields = {
        'tva_rate':
            {
                'title': 'Taux de TVA',
                'format': 'decimal',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('tva_rate').blank,
                'tooltip': 'Taux de TVA. Exemple : 19.00',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _url = 'tva_list/'
    _api_permission_type = '.view_'


class WilayaList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Wilaya
    _model_data = {
        'enumeration': True,
        'sort': ['wilaya'],
        'reverse_sort': False,
        'text_field': ['wilaya'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = []
    _fields = {
        'wilaya':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': 7,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('wilaya').blank,
                'tooltip': 'Désignation de la Wilaya',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            }
    }
    _html_page = 'references/wilaya_admin_page.html'
    _url = 'wilaya_list/'


class ArticleCategoryList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = ArticleCategory
    _model_data = {
        'enumeration': True,
        'sort': ['designation'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = []
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation ou famille d\'articles',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('code').blank,
                'tooltip': 'Code de la famille d\'articles',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _url = 'article_category_list/'
    _api_permission_type = '.view_'


enabled_services = [
    {'service': ArchivedDocumentsView},
    {'service': CompaniesList},
    {'service': TvaList},
    {'service': WilayaList},
    {'service': ArticleCategoryList},
]

StandardTable = StandardAdminApi

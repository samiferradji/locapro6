from common import helpers as common_helpers
from common import models as common_models
from common import views

menu_list = [
    {
        'id': 100,
        'text': 'Application',
        'service': False,
        'icon': '<i class="fas fa-chevron-right"></i>',
        'state': '',
        'order': 0,
        'parent_id': None
    },
    {
        'id': 101,
        'text': 'Tables de références',
        'service': False,
        'icon': '<i class="fas fa-chevron-right"></i>',
        'state': '',
        'order': 0,
        'parent_id': 100
    },
    {
        'id': 102,
        'text': '',
        'service': views.EmployeeAdmin,
        'icon': '<i class="fas fa-users"></i>',
        'state': '',
        'order': 0,
        'parent_id': 101
    },
    {
        'id': 103,
        'text': '',
        'service': views.ArticleCategoryAdmin,
        'icon': '<i class="fas fa-layer-group"></i>',
        'state': '',
        'order': 0,
        'parent_id': 101
    },
    {
        'id': 104,
        'text': '',
        'service': views.AccountingCodeAdmin,
        'icon': '<i class="fas fa-code"></i>',
        'state': '',
        'order': 0,
        'parent_id': 101
    },
    {
        'id': 105,
        'text': '',
        'service': views.TvaAdmin,
        'icon': '<i class="fas fa-gavel"></i>',
        'state': '',
        'order': 0,
        'parent_id': 101
    },
    {
        'id': 106,
        'text': '',
        'service': views.WilayaAdmin,
        'icon': '<i class="fas fa-puzzle-piece"></i>',
        'state': '',
        'order': 0,
        'parent_id': 101
    },
    {
        'id': 107,
        'text': '',
        'service': views.CommuneAdmin,
        'icon': '<i class="fas fa-compress"></i>',
        'state': '',
        'order': 0,
        'parent_id': 101
    },
    {
        'id': 108,
        'text': '',
        'service': views.CompanyAdmin,
        'icon': '<i class="fas fa-building"></i>',
        'state': '',
        'order': 0,
        'parent_id': 101
    },
    {
        'id': 109,
        'text': '',
        'service': views.CompanyProfileAdmin,
        'icon': '<i class="fas fa-building"></i>',
        'state': 'hidden',
        'order': 0,
        'parent_id': 101
    }
]
common_helpers.add_new_menus(menu_list)
common_helpers.update_menu(menu_list)

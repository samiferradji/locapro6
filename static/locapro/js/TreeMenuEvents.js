function set_opened_closed_icon() {
    let menu_inputs = document.getElementsByClassName('menuInput');
    for (let i = 0, l = menu_inputs.length; i < l; i++) {
        menu_inputs[i].addEventListener('click', function (ev) {
                if (ev.target.checked === true) {
                    ev.target.parentNode.childNodes[0].childNodes[0].className = 'fas fa-chevron-down';
                    ev.target.parentNode.childNodes[0].childNodes[0].style.paddingRight = '6px';
                                   } else {
                    ev.target.parentNode.childNodes[0].childNodes[0].className = 'fas fa-chevron-right';
                    ev.target.parentNode.childNodes[0].childNodes[0].style.paddingRight = '9px'
                }
            }
        )

    }
}
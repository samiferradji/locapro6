function getRawDataFromServer(action, main_table, uuid, callback, cache='no_cache') {
    if (!main_table.posting) {
        startProcessing(uuid);
        let notification = {};
        main_table.posting = true;
        let _url = '/common/get_raw_data_from_server';
        _url += '?action=' + action;
        if (cache === 'no_cache') {
            _url += '&hash=' + generateUUID()
        }
        let xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                stopProcessing(uuid);
                if (callback) {
                    callback(this.responseText)
                }
                notification.header = '';
                notification.class = 'w3-green';
                notification.text = 'Données obtenues ';
                pop_notification(notification);
                main_table.posting = false;
            }
            else if (this.readyState === 4 && this.status !== 200) {
                stopProcessing(uuid);
                notification.header = 'Bug!!';
                notification.class = 'w3-red';
                notification.text = this.statusText;
                pop_notification(notification);
                main_table.posting = false;
            }
        };
        xmlhttp.open("GET", _url, true);
        xmlhttp.send();
    }
}
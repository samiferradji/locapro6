function BuildForm(container) {
    container.posting = false;
    let autocomplete_inputs = [];
    container.buildInput = function (current_object) {
        if (current_object['type'] === 'autocomplete') {
            autocomplete_inputs.push({
                'id': current_object['id'],
                'url': current_object['url']
            });
            let _autocomplete_div = document.createElement('DIV');
            _autocomplete_div.className = 'autocomplete w3-cell';

            let _autocomplete_span = document.createElement('SPAN');
            _autocomplete_span.title = current_object['tooltip'];

            let input_label = document.createElement('LABEL');
            input_label.for = current_object['id'];
            input_label.innerText = current_object['text'];

            let _info_span = document.createElement('SPAN');
            _info_span.id = 'info_' + current_object['id'];
            _info_span.className = 'label-info';

            let _input = document.createElement('INPUT');
            _input.className = 'w3-input w3-border autocomplete-input';
            _input.type = current_object['type'];
            _input.id = current_object['id'];
            _input.name = current_object['name'];
            _input.required = current_object['required'];
            _input.disabled = current_object['disabled'];
            for (let key in current_object['attributes']) {
                _input.setAttribute(key, current_object['attributes'][key])
            }
            _input.csrf_token = container.settings['csrf'];


            _autocomplete_div.appendChild(_autocomplete_span);
            _autocomplete_span.appendChild(input_label);
            _autocomplete_span.appendChild(_info_span);
            _autocomplete_span.appendChild(_input);
            return _autocomplete_div;
        } else if (current_object['type'] === 'checkbox') {
            let _input_span = document.createElement('SPAN');
            _input_span.title = current_object['tooltip'];
            _input_span.className = 'w3-cell switch-widget';

            let _checkbox_label = document.createElement('SPAN');
            _checkbox_label.className = 'switch-label';

            let _label = document.createTextNode(current_object['text']);


            let input_label = document.createElement('LABEL');
            input_label.for = current_object['id'];
            input_label.className = 'switch';
            //input_label.innerText = current_object['text'];

            let _info_span = document.createElement('SPAN');
            _info_span.id = 'info_' + current_object['id'];
            _info_span.className = 'label-info';

            let _slider_span = document.createElement('SPAN');
            _slider_span.className = 'slider';

            let _input = document.createElement('INPUT');
            _input.className = 'w3-input w3-border';
            _input.type = current_object['type'];
            _input.id = current_object['id'];
            _input.name = current_object['name'];
            _input.required = current_object['required'];
            _input.disabled = current_object['disabled'];
            for (let key in current_object['attributes']) {
                _input.setAttribute(key, current_object['attributes'][key])
            }


            _checkbox_label.appendChild(_label);
            _input_span.appendChild(_checkbox_label);
            input_label.appendChild(_input);
            input_label.appendChild(_slider_span);
            _input_span.appendChild(_info_span);
            _input_span.appendChild(input_label);
            return _input_span;
        } else if (current_object['type'] === 'hidden') {

            let _input = document.createElement('INPUT');
            _input.type = current_object['type'];
            _input.id = current_object['id'];
            _input.name = current_object['name'];
            _input.required = current_object['required'];
            for (let key in current_object['attributes']) {
                _input.setAttribute(key, current_object['attributes'][key])
            }
            return _input;
        } else {

            let _input_span = document.createElement('SPAN');
            _input_span.title = current_object['tooltip'];
            _input_span.className = 'w3-cell';

            let input_label = document.createElement('LABEL');
            input_label.for = current_object['id'];
            input_label.innerText = current_object['text'];

            let _info_span = document.createElement('SPAN');
            _info_span.id = 'info_' + current_object['id'];
            _info_span.className = 'label-info';

            let _input = document.createElement('INPUT');
            _input.className = 'w3-input w3-border';
            _input.type = current_object['type'];
            _input.id = current_object['id'];
            _input.name = current_object['name'];
            _input.required = current_object['required'];
            _input.disabled = current_object['disabled'];
            for (let key in current_object['attributes']) {
                _input.setAttribute(key, current_object['attributes'][key])
            }
            _input_span.appendChild(input_label);
            _input_span.appendChild(_info_span);
            _input_span.appendChild(_input);
            return _input_span;
        }
    };
    for (let group in container.settings.form_layout) {
        let _row = document.createElement('DIV');
        _row.className = 'w3-cell-row';
        container.appendChild(_row);
        for (let i = 0, l = container.settings.form_layout[group].length; i < l; i++) {
            let current_inputs = container.settings['inputs'].filter(function (value) {
                return value['name'] === container.settings.form_layout[group][i]
            })[0];
            _row.appendChild(container.buildInput(current_inputs));
        }
    }
    for (let i = 0, l = container.settings['inputs'].length; i < l; i++) {

        let _events = container.settings['inputs'][i]['events'];
        if (_events) {
            for (let _i = 0, l = _events.length; _i < l; _i++) {
                let _key = Object.keys(_events[_i])[0];
                let current_elm = document.getElementById(container.settings['inputs'][i]['id']);
                current_elm.addEventListener(_key, _events[_i][_key]);
            }
        }
    }
    for (let i = 0; i < autocomplete_inputs.length; i++) {
        let elm = document.getElementById(autocomplete_inputs[i]['id']);
        elm.url = autocomplete_inputs[i]['url'];
        AutocompleteTable(elm)
    }
    if (container.settings['buttons']) {
        let _buttons_div = document.createElement('DIV');
        _buttons_div.className = 'w3-container form-buttons';
        for (let i = 0; i < container.settings['buttons'].length; i++) {
            let _button = document.createElement('BUTTON');
            _button.title = container.settings['buttons'][i]['tooltip'];
            _button.className = 'w3-right w3-button';
            _button.id = container.settings['buttons'][i]['id'];

            let _button_icon = document.createElement('I');
            _button_icon.className = container.settings['buttons'][i]['icon_class'];

            _button.innerText = container.settings['buttons'][i]['text'];
            _button.appendChild(_button_icon);
            _buttons_div.appendChild(_button)
        }
        container.appendChild(_buttons_div);
        for (let i = 0; i < container.settings['buttons'].length; i++) {
            let e = container.settings['buttons'][i]['events'];
            if (e) {
                for (let _i = 0; _i < e.length; _i++) {
                    let _key = Object.keys(e[_i])[0];
                    let current_button = document.getElementById(container.settings['buttons'][i]['id']);
                    current_button.addEventListener(_key, e[_i][_key]);
                }
            }
        }
    }
    container.validateOnBlur = function () {
        let inputs = container.getElementsByTagName('input');
        for (let i = 0, l = inputs.length; i < l; i++) {
            inputs[i].addEventListener('blur', function (ev) {
                if (!ev.target.validity.valid) {
                    let info_icon = document.createElement('i');
                    info_icon.className = 'fas fa-exclamation-triangle icon-warning';
                    let _label_info = document.getElementById('info_' + ev.target.id);
                    info_icon.setAttribute('title', 'Champ obligatoire ou saisie incorrecte');
                    ev.target.setAttribute('data-error', true);
                    _label_info.innerHTML = "";
                    _label_info.appendChild(info_icon);
                } else {
                    ev.target.setAttribute('data-error', false);
                    let _current_input_info = document.getElementById('info_' + ev.target.id);
                    while (_current_input_info.firstChild) {
                        _current_input_info.removeChild(_current_input_info.firstChild);
                    }
                }
            })
        }
    };
    container.validateOnBlur();
    container.validateLocalData = function () {
        let _inputs = container.getElementsByTagName('input');
        for (let i = 0, l = _inputs.length; i < l; i++) {
            _inputs[i].blur();
            if (_inputs[i].getAttribute('data-error') === 'true') {
                container.local_errors = true
            }
        }
    };
    container.post_data = function (callback) {
        if (!container.posting) {
            startProcessing(container.uuid);
            let notification = {};
            container.local_errors = false;
            container.validateLocalData();
            if (container.local_errors) {
                notification.header = 'Erreur!';
                notification.class = 'w3-orange';
                notification.text = 'Merci de corriger les erreurs sur le formulaire';
                pop_notification(notification);
                stopProcessing(container.uuid);
            } else {
                container.posting = true;
                let _url = container.settings['url'];
                let obj = new FormData();
                obj.append('csrfmiddlewaretoken', container.settings['csrf']);
                obj.append('action', container.settings['action']);
                obj.append('parent_id', container.uuid);
                obj.append('line_id', container.line_id);
                let _inputs = this.getElementsByTagName('input');
                for (let i = 0; i < _inputs.length; i++) {
                    let _current_input = _inputs[i];
                    if (_current_input.getAttribute('type') === 'autocomplete') {
                        obj.append(_current_input.getAttribute('name'), _current_input.selectedPk);
                    } else if (_current_input.getAttribute('type') === 'file') {
                        if (_current_input.files[0] && _current_input.files[0].size > window.max_file_size_kb * 1024) {
                            stopProcessing(container.uuid);
                            const notification = {
                                header: 'Erreur',
                                text: 'La taille authorisée du fichier ne doit pas dépasser ' + window.max_file_size_kb + 'kb',
                                class: 'w3-orange'
                            };
                            container.posting = false;
                            pop_notification(notification);
                            return false
                        } else {
                            obj.append(_current_input.getAttribute('name'), _current_input.files[0]);
                        }
                    } else if (_current_input.getAttribute('type') === 'checkbox') {
                        _current_input.getAttribute('type');
                        if (_current_input.checked) {
                            obj.append(_current_input.getAttribute('name'), 'True')
                        } else {
                            obj.append(_current_input.getAttribute('name'), 'False')
                        }
                    } else {
                        obj.append(_current_input.getAttribute('name'), _current_input.value)
                    }
                }
                let xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function () {
                    if (this.readyState === 4 && this.status === 200) {
                        stopProcessing(container.uuid);
                        if (JSON.parse(this.responseText).status === 'Error') {
                            notification.header = 'Erreur!';
                            notification.class = 'w3-orange';
                            let response = JSON.parse(this.responseText).status_message;
                            response = JSON.parse(response);
                            let keys = Object.keys(response);
                            let _ul = document.createElement('UL');
                            for (let _i = 0, _l = keys.length; _i < _l; _i++) {
                                let _li = document.createElement('LI');
                                let _strong = document.createElement('STRONG');
                                let _strong_text = document.createTextNode(keys[_i]);
                                _strong.appendChild(_strong_text);
                                _li.appendChild(_strong);
                                let _return = document.createElement('BR');
                                let _error_msg = document.createTextNode(response[keys[_i]]);
                                _li.appendChild(_return);
                                _li.appendChild(_error_msg);
                                _ul.appendChild(_li);
                            }
                            notification.message_html_element = _ul;
                            pop_notification(notification);
                            container.posting = false;
                        } else {
                            notification.header = 'OK';
                            notification.class = 'w3-green';
                            notification.text = JSON.parse(this.responseText).status_message;
                            pop_notification(notification);
                            let data_posted = new CustomEvent('data_posted_' + container.uuid);
                            document.dispatchEvent(data_posted);
                            container.posting = false;
                            callback()
                        }
                    } else if (this.readyState === 4 && this.status !== 200 && this.status !== 0) {
                        notification.header = 'Bug!!';
                        notification.class = 'w3-red';
                        notification.text = this.statusText;
                        pop_notification(notification);
                        container.posting = false;
                    }
                };
                xmlhttp.open("POST", _url, true);
                xmlhttp.send(obj);
                xmlhttp.onerror = function (ev) {
                    stopProcessing(container.uuid);
                    let notification = {};
                    notification.header = '';
                    notification.class = 'w3-red';
                    notification.text = 'Envoie de données impossible. vérifiez votre connexion avec le serveur ! ';
                    pop_notification(notification);
                    container.posting = false;
                }
            }
        }
    };
    container.disable = function () {
        let _inputs = container.getElementsByTagName('input');
        for (let i = 0, l = _inputs.length; i < l; i++) {
            _inputs[i].disabled = true
        }
    };
    container.clear = function () {
        while (container.firstChild) {
            container.removeChild(container.firstChild);
        }
        BuildForm(container)
    };
    container.enable = function () {
        let _inputs = container.getElementsByTagName('input');
        for (let i = 0; i < _inputs.length; i++) {
            _inputs[i].disabled = false
        }
        _inputs[0].focus()
    };
    container.focusNextInput = function (event, callback) {
        let _inputs = container.getElementsByTagName('input');
        let _current = event.target;
        let _index_length = _inputs.length;
        let _current_index;
        let i;
        for (i = 0, _index_length; i < _index_length; i++) {
            if (_current.id === _inputs[i].id) {
                _current_index = i;
                break
            }
        }

        function nextEnabledInputIndex(c_index, index_len) {
            if (c_index >= index_len - 1) {
                return false
            } else {
                c_index += 1;
                if (_inputs[c_index].disabled === true) {
                    return nextEnabledInputIndex(c_index, index_len)
                } else {
                    return c_index
                }
            }
        }

        if (event.key === 'Enter') {
            let next_enable_input_index = nextEnabledInputIndex(_current_index, _index_length);
            if (!next_enable_input_index) {
                _inputs[_current_index].blur();
                if (callback) {
                    callback()
                }
            } else {
                _inputs[next_enable_input_index].focus();
            }
        }
    };
    container.manage_combo_grid_updated = function () {
        let _inputs = container.getElementsByTagName('input');
        for (let i = 0, l = _inputs.length; i < l; i++) {
            _inputs[i].addEventListener('combo_grid_updated', function (event) {
                let _current = event.target;
                for (let _i = 0, _l = _inputs.length; _i < _l; _i++) {
                    if (_inputs[_i].id === _current.id && _i === _l - 1) {
                        _inputs[_i].focus();
                        break
                    } else if (_inputs[_i].id === _current.id && _i !== _l - 1) {
                        _inputs[_i + 1].focus();
                        break
                    }

                }
            })
        }
    };
    container.manage_combo_grid_updated();
    let inputs = container.getElementsByTagName('input');
    container.setDefaultValues = function () {
        for (let i = 0, l = inputs.length; i < l; i++) {
            let default_value = inputs[i].getAttribute('default_value');
            if (default_value && default_value !== '') {
                if (inputs[i].type === 'autocomplete') {
                    inputs[i].setSelected(default_value);
                } else {
                    inputs[i].value = default_value;
                }
            }
            let get_parent_input = inputs[i].getAttribute('get_parent_input');
            if (get_parent_input && get_parent_input !== '') {
                let current_page = document.getElementById('page_' + container.uuid);
                document.addEventListener('loaded_table_' + container.uuid, function () {
                    inputs[i].value = current_page.main_table.parent_data.id
                })
            }
        }
    };

    container.setDefaultValues();
    // Focus on first enabled input
    for (let i = 0, l = inputs.length; i < l; i++) {
        if (inputs[i].type !== 'hidden' && !inputs[i].disabled) {
            inputs[i].focus();
            break
        }
    }

}

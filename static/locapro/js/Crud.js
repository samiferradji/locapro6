function commit(page_uuid, line_id,url, main_table, csrf_token, callback) {
    if (!main_table.posting) {
        startProcessing(page_uuid);
        let notification = {};
        main_table.posting = true;
        let _url = url;
        let obj = '';
        obj += 'action=commit';
        obj += '&csrfmiddlewaretoken=' + csrf_token;
        obj += '&parent_id=' + line_id;
        let xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let response = JSON.parse(this.responseText);
                if (response.status === 'Error') {
                    stopProcessing(page_uuid);
                    notification.header = 'Erreur!';
                    notification.class = 'w3-orange';
                    let msg_response = JSON.parse(response.status_message);
                    let keys = Object.keys(msg_response);
                    let _ul = document.createElement('UL');
                    for (let _i = 0, _l = keys.length; _i < _l; _i++) {
                        let _li = document.createElement('LI');
                        let _strong = document.createElement('STRONG');
                        let _strong_text = document.createTextNode(keys[_i]);
                        _strong.appendChild(_strong_text);
                        _li.appendChild(_strong);
                        let _return = document.createElement('BR');
                        let _error_msg = document.createTextNode(msg_response[keys[_i]]);
                        _li.appendChild(_return);
                        _li.appendChild(_error_msg);
                        _ul.appendChild(_li);
                    }
                    notification.message_html_element = _ul;
                    pop_notification(notification);
                    main_table.posting = false;
                } else {
                    stopProcessing(page_uuid);
                    notification.header = 'OK';
                    notification.class = 'w3-green';
                    notification.text = response.status_message;
                    pop_notification(notification);
                    main_table.posting = false;
                    if (response.new_id) {
                        callback(response.new_id)
                    } else {
                        callback()
                    }
                }
            } else if (this.readyState === 4 && this.status !== 200) {
                stopProcessing(page_uuid);
                notification.header = 'Bug!!';
                notification.class = 'w3-red';
                notification.text = this.statusText;
                pop_notification(notification);
                main_table.posting = false;
            }
        };
        xmlhttp.open("POST", _url, true);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        xmlhttp.send(obj);
    }
}

function cancel(uuid, url, main_table, csrf_token, callback) {
    if (!main_table.posting) {
        startProcessing(uuid);
        let notification = {};
        main_table.posting = true;
        let _url = url;
        let obj = '';
        obj += 'action=cancel';
        obj += '&csrfmiddlewaretoken=' + csrf_token;
        obj += '&parent_id=' + uuid;
        let xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (JSON.parse(this.responseText).status === 'Error') {
                    stopProcessing(uuid);
                    notification.header = 'Erreur!';
                    notification.class = 'w3-orange';
                    let response = JSON.parse(this.responseText).status_message;
                    response = JSON.parse(response);
                    let keys = Object.keys(response);
                    let _ul = document.createElement('UL');
                    for (let _i = 0, _l = keys.length; _i < _l; _i++) {
                        let _li = document.createElement('LI');
                        let _strong = document.createElement('STRONG');
                        let _strong_text = document.createTextNode(keys[_i]);
                        _strong.appendChild(_strong_text);
                        _li.appendChild(_strong);
                        let _return = document.createElement('BR');
                        let _error_msg = document.createTextNode(response[keys[_i]]);
                        _li.appendChild(_return);
                        _li.appendChild(_error_msg);
                        _ul.appendChild(_li);
                    }
                    notification.message_html_element = _ul;
                    pop_notification(notification);
                    main_table.posting = false;
                } else {
                    stopProcessing(uuid);
                    notification.header = 'OK';
                    notification.class = 'w3-green';
                    notification.text = JSON.parse(this.responseText).status_message;
                    pop_notification(notification);
                    main_table.posting = false;
                    callback()
                }
            } else if (this.readyState === 4 && this.status !== 200) {
                stopProcessing(uuid);
                notification.header = 'Bug!!';
                notification.class = 'w3-red';
                notification.text = this.statusText;
                pop_notification(notification);
                main_table.posting = false;
            }
        };
        xmlhttp.open("POST", _url, true);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        xmlhttp.send(obj);

    }
}

function delete_line(uuid, line_id, url, main_table, csrf_token, callback) {
    if (!line_id) {
        let notification = {};
        notification.header = 'Erreur!';
        notification.class = 'w3-orange';
        notification.text = 'Vous devez choisir une ligne pour la supprimer !';
        pop_notification(notification)
    } else {

        if (!main_table.posting) {
            startProcessing(uuid);
            let notification = {};
            main_table.posting = true;
            let _url = url;
            let obj = '';
            obj += 'action=delete_line';
            obj += '&csrfmiddlewaretoken=' + csrf_token;
            obj += '&parent_id=' + uuid;
            obj += '&line_id=' + line_id;
            let xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
            xmlhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    stopProcessing(uuid);
                    let response;
                    if (JSON.parse(this.responseText).status === 'Error') {
                        notification.header = 'Erreur!';
                        notification.class = 'w3-orange';
                        response = JSON.parse(this.responseText).status_message;
                        response = JSON.parse(response);
                        let keys = Object.keys(response);
                        let _ul = document.createElement('UL');
                        for (let _i = 0, _l = keys.length; _i < _l; _i++) {
                            let _li = document.createElement('LI');
                            let _strong = document.createElement('STRONG');
                            let _strong_text = document.createTextNode(keys[_i]);
                            _strong.appendChild(_strong_text);
                            _li.appendChild(_strong);
                            let _return = document.createElement('BR');
                            let _error_msg = document.createTextNode(response[keys[_i]]);
                            _li.appendChild(_return);
                            _li.appendChild(_error_msg);
                            _ul.appendChild(_li);
                        }
                        notification.message_html_element = _ul;
                        pop_notification(notification);
                        main_table.posting = false;
                    } else {
                        notification.header = 'OK';
                        notification.class = 'w3-green';
                        response = JSON.parse(this.responseText).status_message;
                        notification.text = response;
                        pop_notification(notification);
                        let data_posted = new CustomEvent('data_posted_' + uuid);
                        document.dispatchEvent(data_posted);
                        main_table.posting = false;
                        callback();

                    }
                } else if (this.readyState === 4 && this.status !== 200) {
                    notification.header = 'Bug!!';
                    notification.class = 'w3-red';
                    notification.text = this.statusText;
                    pop_notification(notification);
                    main_table.posting = false;
                }
            };
            xmlhttp.open("POST", _url, true);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            xmlhttp.send(obj);

        }
    }
}

function get_totals(uuid, url, main_table, csrf_token, callback) {

    if (!main_table.posting) {
        startProcessing(uuid);
        let notification = {};
        main_table.posting = true;
        let _url = url;
        let obj = '';
        obj += 'action=get_totals';
        obj += '&csrfmiddlewaretoken=' + csrf_token;
        obj += '&parent_id=' + uuid;
        let xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
        xmlhttp.onreadystatechange = function () {
            let response;
            if (this.readyState === 4 && this.status === 200) {
                if (JSON.parse(this.responseText).status === 'Error') {
                    stopProcessing(uuid);
                    notification.header = 'Erreur!';
                    notification.class = 'w3-orange';
                    response = JSON.parse(this.responseText).status_message;
                    response = JSON.parse(response);
                    let keys = Object.keys(response);
                    let _ul = document.createElement('UL');
                    for (let _i = 0, _l = keys.length; _i < _l; _i++) {
                        let _li = document.createElement('LI');
                        let _strong = document.createElement('STRONG');
                        let _strong_text = document.createTextNode(keys[_i]);
                        _strong.appendChild(_strong_text);
                        _li.appendChild(_strong);
                        let _return = document.createElement('BR');
                        let _error_msg = document.createTextNode(response[keys[_i]]);
                        _li.appendChild(_return);
                        _li.appendChild(_error_msg);
                        _ul.appendChild(_li);
                    }
                    notification.message_html_element = _ul;
                    pop_notification(notification);
                    main_table.posting = false;
                } else {
                    stopProcessing(uuid);
                    notification.header = '';
                    notification.class = 'w3-green';
                    notification.text = 'Totaux actualisés';
                    pop_notification(notification);
                    main_table.totals = JSON.parse(this.responseText);
                    main_table.posting = false;
                    callback()
                }
            } else if (this.readyState === 4 && this.status !== 200) {
                stopProcessing(uuid);
                notification.header = 'Bug!!';
                notification.class = 'w3-red';
                notification.text = this.statusText;
                pop_notification(notification);
                main_table.posting = false;
            }
        };
        xmlhttp.open("POST", _url, true);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        xmlhttp.send(obj);

    }
}

function get_line(uuid, line_id, url, main_table, csrf_token, callback) {
    if (!line_id) {
        let notification = {};
        notification.header = 'Erreur!';
        notification.class = 'w3-orange';
        notification.text = 'Vous devez choisir une ligne';
        pop_notification(notification)
    } else {
        if (!main_table.posting) {
            startProcessing(uuid);
            let notification = {};
            main_table.posting = true;
            let _url = url;
            let obj = '';
            let response;
            obj += 'action=get_line';
            obj += '&csrfmiddlewaretoken=' + csrf_token;
            obj += '&parent_id=' + uuid;
            obj += '&line_id=' + line_id;
            let xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
            xmlhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    if (JSON.parse(this.responseText).status === 'Error') {
                        stopProcessing(uuid);
                        notification.header = 'Erreur!';
                        notification.class = 'w3-orange';
                        response = JSON.parse(this.responseText).status_message;
                        response = JSON.parse(response);
                        let keys = Object.keys(response);
                        let _ul = document.createElement('UL');
                        for (let _i = 0, _l = keys.length; _i < _l; _i++) {
                            let _li = document.createElement('LI');
                            let _strong = document.createElement('STRONG');
                            let _strong_text = document.createTextNode(keys[_i]);
                            _strong.appendChild(_strong_text);
                            _li.appendChild(_strong);
                            let _return = document.createElement('BR');
                            let _error_msg = document.createTextNode(response[keys[_i]]);
                            _li.appendChild(_return);
                            _li.appendChild(_error_msg);
                            _ul.appendChild(_li);
                        }
                        notification.message_html_element = _ul;
                        pop_notification(notification);
                        main_table.posting = false;
                    } else {
                        stopProcessing(uuid);
                        response = JSON.parse(this.responseText);
                        obj = JSON.parse(response.status_data);
                        callback(obj);
                        notification.header = '';
                        notification.class = 'w3-green';
                        notification.text = response.status_message;
                        pop_notification(notification);
                        main_table.posting = false;
                    }
                } else if (this.readyState === 4 && this.status !== 200) {
                    stopProcessing(uuid);
                    notification.header = 'Bug!!';
                    notification.class = 'w3-red';
                    notification.text = this.statusText;
                    pop_notification(notification);
                    main_table.posting = false;
                }
            };
            xmlhttp.open("POST", _url, true);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            xmlhttp.send(obj);

        }
    }
}

function post_custom_data(action, page_uuid, url, main_table, csrf_token, data_dictionary, callback) {
    /*
    * action : the post action
    * uuid : unique global id of the document who is posting data
    * url : the url
    * main_table : table showing the post , if none, replace by current document
    * data_dictionary : data to be post as a javascript dictionary
    * callback : the function to be executed if successful post
    * */

    if (!main_table.posting) {
        startProcessing(page_uuid);
        let notification = {};
        main_table.posting = true;
        let _url = url;
        let obj = '';
        obj += 'action=' + action;
        obj += '&csrfmiddlewaretoken=' + csrf_token;
        for (let _key in data_dictionary) {
            obj += '&' + _key + '=' + data_dictionary[_key];
        }
        let xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                let response = JSON.parse(this.responseText);
                if (response.status === 'Error') {
                    stopProcessing(page_uuid);
                    notification.header = 'Erreur!';
                    notification.class = 'w3-orange';
                    let msg_response = JSON.parse(response.status_message);
                    let keys = Object.keys(msg_response);
                    let _ul = document.createElement('UL');
                    for (let _i = 0, _l = keys.length; _i < _l; _i++) {
                        let _li = document.createElement('LI');
                        let _strong = document.createElement('STRONG');
                        let _strong_text = document.createTextNode(keys[_i]);
                        _strong.appendChild(_strong_text);
                        _li.appendChild(_strong);
                        let _return = document.createElement('BR');
                        let _error_msg = document.createTextNode(msg_response[keys[_i]]);
                        _li.appendChild(_return);
                        _li.appendChild(_error_msg);
                        _ul.appendChild(_li);
                    }
                    notification.message_html_element = _ul;
                    pop_notification(notification);
                    main_table.posting = false;
                } else {
                    stopProcessing(page_uuid);
                    notification.header = 'OK';
                    notification.class = 'w3-green';
                    notification.text = response.status_message;
                    pop_notification(notification);
                    main_table.posting = false;
                    if (response.new_id) {
                        callback(response.new_id)
                    } else {
                        callback()
                    }
                }
            } else if (this.readyState === 4 && this.status !== 200) {
                stopProcessing(page_uuid);
                notification.header = 'Bug!!';
                notification.class = 'w3-red';
                notification.text = this.statusText;
                pop_notification(notification);
                main_table.posting = false;
            }
        };
        xmlhttp.open("POST", _url, true);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        xmlhttp.send(obj);

    }
}

function get_page_view_parameters(action, uuid, url, main_table, csrf_token, data_dictionary, callback) {
    /*
    * action : the post action
    * uuid : unique global id of the document who is posting data
    * url : the url
    * main_table : table showing the post , if none, replace by current document
    * data_dictionary : data to be post as a javascript dictionary
    * callback : the function to be executed if successful post
    * */

    if (!main_table.posting) {
        let notification = {};
        let _html;
        main_table.posting = true;
        let _url = url;
        let obj = '';
        obj += 'action=' + action;
        obj += '&csrfmiddlewaretoken=' + csrf_token;
        obj += '&parent_id=' + uuid;
        for (let _key in data_dictionary) {
            obj += '&' + _key + '=' + data_dictionary[_key];
        }
        let xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                if (JSON.parse(this.responseText).status === 'Error') {
                    notification.header = 'Erreur!';
                    notification.class = 'w3-orange';
                    let response = JSON.parse(this.responseText).status_message;
                    response = JSON.parse(response);
                    let keys = Object.keys(response);
                    let _i, _l;
                    _html = '<ul>';
                    for (_i = 0, _l = keys.length; _i < _l; _i++) {
                        _html += '<li>' + '<strong>' + keys[_i] + '</strong><br>' + response[keys[_i]] + '</li>';

                    }
                    _html += '</ul>';
                    notification.message_html_element = _html;
                    pop_notification(notification);
                    main_table.posting = false;
                } else {
                    notification.header = JSON.parse(this.responseText).status;
                    notification.class = 'w3-green';
                    notification.text = JSON.parse(this.responseText).status_message;
                    pop_notification(notification);
                    main_table.posting = false;
                    if (callback) {
                        callback(this.responseText)
                    }
                }
            } else if (this.readyState === 4 && this.status !== 200) {
                notification.header = 'Bug!!';
                notification.class = 'w3-red';
                notification.text = this.statusText;
                pop_notification(notification);
                main_table.posting = false;
            }
        };
        xmlhttp.open("POST", _url, true);
        xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        xmlhttp.send(obj);

    }
}
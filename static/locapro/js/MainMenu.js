function getMenuData() {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let tree_div = document.getElementById('menu_container');
            tree_div.appendChild(BuildMainMenu(JSON.parse(this.responseText)));
            tree_div.firstChild.classList.add('tree');
            set_opened_closed_icon()
        }
    };
    let _url = '/common/main_menu';
    _url += '?hash=' + generateUUID();
    xmlhttp.open("GET", _url, true);
    xmlhttp.send();
}

function BuildMainMenu(arr) {
    let tree_element = document.createElement('OL');
    let item;
    for (item in arr) {
        if (arr[item].children != '') {
            let tree_li = document.createElement('LI');
            let tree_label = document.createElement('LABEL');
            tree_label.className = 'menuLabel';
            tree_label.setAttribute('for', 'i' + arr[item].id);
            tree_label.id = 'l' + arr[item].id;
            tree_label.innerHTML = arr[item].faIcon + arr[item].text;

            let tree_input = document.createElement('INPUT');
            tree_input.className = 'menuInput';
            tree_input.type = 'checkbox';
            tree_input.id = 'i' + arr[item].id;
            tree_li.appendChild(tree_label);
            tree_li.appendChild(tree_input);
            let tree_ol = BuildMainMenu(arr[item].children);
            tree_li.appendChild(tree_ol);
            tree_element.appendChild(tree_li)
        }
        else {
            let tree_li = document.createElement('LI');
            tree_li.className = 'file';

            let curr_url = arr[item].url;
            let cur_title = arr[item].title;
            let cur_sub_title = arr[item].subTitle;
            let tree_link = document.createElement('A');
            tree_link.setAttribute('title',arr[item].description);

            tree_link.id = arr[item].id;
            tree_link.href = '#';
            tree_link.draggable = true;
            tree_link.ondragstart = function (ev) {
                drag(ev)
            };
            tree_link.onclick = function (ev) {
                getNewPageData(curr_url, cur_title, cur_sub_title)
            };
            tree_link.innerHTML = arr[item].faIcon + arr[item].text;
            tree_li.appendChild(tree_link);
            tree_element.appendChild(tree_li);
        }
    }
    return tree_element
}

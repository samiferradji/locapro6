function BuildConfirmationDialog(page, action) {
    page.confirmation_dialog = document.createElement('DIV');
    page.confirmation_dialog.classList.add('w3-modal');
    page.appendChild(page.confirmation_dialog);

    page.confirmation_dialog_content = document.createElement('DIV');
    page.confirmation_dialog_content.classList.add('w3-modal-content');
    page.confirmation_dialog.appendChild(page.confirmation_dialog_content);

    page.confirmation_dialog_top_panel = document.createElement('div');
    page.confirmation_dialog_top_panel.className = 'w3-bar top-Panel';

    page.confirmation_dialog_title = document.createElement('div');
    page.confirmation_dialog_title.className = 'w3-bar-item';
    page.confirmation_dialog_title.innerText = 'Confirmer des mouvements de stock';

    page.confirmation_dialog_close_button = document.createElement('BUTTON');
    page.confirmation_dialog_close_button.className = 'w3-button w3-bar-item w3-right';
    page.confirmation_dialog_close_button_icon = document.createElement('I');
    page.confirmation_dialog_close_button_icon.className = 'fas fa-times';

    page.confirmation_dialog_close_button.appendChild(page.confirmation_dialog_close_button_icon);
    page.confirmation_dialog_close_button.onclick = function (ev) {
        closeDialog(page.confirmation_dialog)
    };

    page.confirmation_page = document.createElement('DIV');
    page.confirmation_page.classList.add('w3-container');
    page.confirmation_dialog_top_panel.appendChild(page.confirmation_dialog_title);
    page.confirmation_dialog_top_panel.appendChild(page.confirmation_dialog_close_button);
    page.confirmation_dialog_content.appendChild(page.confirmation_dialog_top_panel);
    page.confirmation_dialog_content.appendChild(page.confirmation_page);
    page.confirmation_dialog.appendChild(page.confirmation_dialog_content);
    page.confirmation_page.uuid = generateUUID();
    page.confirmation_page.id = 'page_' + page.confirmation_page.uuid;
    page.confirmation_page.url = '/wms/executor_tempo_admin/';
    page.confirmation_page.csrf_token = page.csrf_token;
    page.confirmation_page.fixed_filter = JSON.stringify({object_id: '0'});
    BuildPage(page.confirmation_page);
    page.confirmation_page.style.height = '400px';
    page.confirmation_dialog_content.style.width = '720px';

    // Events
    let object_id_input = document.createElement('INPUT');
    object_id_input.className = "w3-bar-item filter_input";
    object_id_input.title = "L'identifient du bon à confirmer. (Entrer) pour ajouter des exécutants";
    object_id_input.placeholder = "ID du Bon";

    let employee_code_input = document.createElement('INPUT');
    employee_code_input.className = "w3-bar-item filter_input";
    employee_code_input.title = "Le code de l\'employé à ajouter";
    employee_code_input.placeholder = "Code RH";

    let add_button_icon = document.createElement('i');
    add_button_icon.className = "fas fa-plus";

    let add_button = document.createElement('BUTTON');
    add_button.className = "w3-bar-item w3-button";
    add_button.title = "Ajouter un employé";
    add_button.appendChild(add_button_icon);
    add_button.onclick = function (ev) {
        function callback() {
            employee_code_input.value = '';
            page.confirmation_page.main_table.refresh();
            employee_code_input.focus()
        }

        post_custom_data(
            'add_line_by_employee_code',
            page.confirmation_page.uuid,
            page.confirmation_page.url,
            page.confirmation_page.main_table,
            page.confirmation_page.csrf_token,
            {
                'object_id': object_id_input.value,
                'content_type__model': page.main_table.getAttribute('model_name'),
                'employee_code': employee_code_input.value
            },
            callback
        )
    };

    let confirm_button_icon = document.createElement('i');
    confirm_button_icon.className = "fas fa-check";

    let confirm_button = document.createElement('BUTTON');
    confirm_button.className = "w3-bar-item w3-button";
    confirm_button.title = "Confirmer la réception du bon (CTRL + Entree)";
    confirm_button.appendChild(confirm_button_icon);
    confirm_button.onclick = function (ev) {
        function callback() {
            page.main_table.refresh();
            page.confirmation_page.main_table.refresh();
            object_id_input.value = '';
            object_id_input.focus()
        }

        post_custom_data(
            action,
            page.confirmation_page.uuid,
            page.url,
            page.confirmation_page.main_table,
            page.confirmation_page.csrf_token,
            {'document_id': object_id_input.value},
            callback
        )
    };
    document.addEventListener('page_built_' + page.confirmation_page.uuid, function (ev) {
        page.confirmation_page.main_table_buttons.firstChild.appendChild(object_id_input);
        page.confirmation_page.main_table_buttons.firstChild.appendChild(employee_code_input);
        object_id_input.focus();
        page.confirmation_page.main_table_buttons.firstChild.appendChild(add_button);
        page.confirmation_page.main_table_buttons.firstChild.appendChild(confirm_button);
    });
    object_id_input.onkeydown = function (ev) {
        if (ev.key === 'Enter' && ev.ctrlKey) {
            confirm_button.click()
        } else if (ev.key === 'Enter') {
            function callback() {
                page.confirmation_page.main_table.fixed_filter = JSON.stringify(
                    {
                        object_id: object_id_input.value,
                        content_type__model: page.main_table.getAttribute('model_name')
                    });
                page.confirmation_page.main_table.refresh();
                employee_code_input.focus();
            }

            post_custom_data(
                'check_transaction_status',
                page.confirmation_page.uuid,
                page.url,
                page.confirmation_page.main_table,
                page.confirmation_page.csrf_token,
                {
                    'document_id': object_id_input.value,
                    'for_action': action
                },
                callback
            )
        }
    };
    employee_code_input.onkeydown = function (ev) {
        if (ev.key === 'Enter' && ev.ctrlKey) {
            confirm_button.click()
        } else if (ev.key === '+' || ev.key === 'Enter') {
            add_button.click()
        }
    };


    openDialog(page.confirmation_dialog)
}
function BuildFilterOnTopBar(container) {
    let _uuid = container.uuid;
    let main_table_top_bar = container.firstChild;

    let select_column = document.createElement('SELECT');
    select_column.id = 'filter_field_' + _uuid;
    select_column.className = 'filter_input w3-bar-item w3-right';
    select_column.title = 'Le champ de recherche de la table';
    select_column.style.height = container.offsetHeight - 6 + 'px';
    select_column.style.backgroundColor = 'white';
    select_column.onchange = function () {
        this.classList.add('w3-pale-yellow');
    };
    let q_input = document.createElement('INPUT');
    q_input.id = 'filter_q_' + _uuid;
    q_input.className = 'filter_input w3-bar-item w3-right';
    q_input.placeholder = 'Mots clés';
    q_input.title = 'Taper un ou plusieurs mots clés séparés par des espaces';
    q_input.style.height = container.offsetHeight - 6 + 'px';
    q_input.oninput = function () {
        this.classList.add('w3-pale-yellow');
    };
    q_input.onkeydown = function (ev) {
        if (ev.key === 'Enter') {
            execute_filter.click()
        }
    };
    let from_date_input;
    let to_date_input;
    let execute_filter = document.createElement('BUTTON');
    execute_filter.id = 'apply_filtre_button_' + _uuid;
    execute_filter.className = 'w3-bar-item w3-button w3-right filter_button';
    execute_filter.title = 'Appliquer le filtre';
    let filter_icon = document.createElement('I');
    filter_icon.className = 'fas fa-filter';
    execute_filter.appendChild(filter_icon);
    execute_filter.onclick = function () {
        if (container.date_range_filter) {
            container._table.q = q_input.value;
            container._table.field = select_column.value;
            container._table.from_date = from_date_input.value;
            container._table.to_date = to_date_input.value;
            container._table.refresh();
        } else {
            container._table.q = q_input.value;
            container._table.field = select_column.value;
            container._table.refresh();
        }
        select_column.classList.remove('w3-pale-yellow');
        let _filter_inputs = container.getElementsByTagName('INPUT');
        for (let i = 0, l = _filter_inputs.length; i < l; i++) {
            if (_filter_inputs[i].classList.contains('w3-pale-yellow')) {
                _filter_inputs[i].classList.remove('w3-pale-yellow')
            }
        }
    };
    document.addEventListener('loaded_table_' + container.uuid, function () {
            if (!select_column.value) {
                while (select_column.firstChild) {
                    select_column.removeChild(select_column.firstChild)
                }
                let default_option = document.createElement('OPTION');
                default_option.text = 'Tout';
                default_option.value = '';
                select_column.appendChild(default_option);
                let column_data = container._table.table_data.columns_data;
                for (let column in column_data) {
                   if (!column_data[column].search){delete column_data[column]}
                }
                for (let column in column_data) {
                    let new_option = document.createElement('OPTION');
                    new_option.value = column;
                    new_option.text = column_data[column].text;
                    select_column.appendChild(new_option)
                }

            }
        }
    );
    main_table_top_bar.appendChild(execute_filter);
    if (container.date_range_filter) {
        from_date_input = document.createElement('INPUT');
        from_date_input.id = 'filter_from_date_' + _uuid;
        from_date_input.type = 'date';
        from_date_input.className = 'filter_input w3-bar-item w3-right';
        from_date_input.title = 'Filtrer les données  à partir de cette date';
        from_date_input.style.height = container.offsetHeight - 6 + 'px';
        from_date_input.onchange = function () {
            this.classList.add('w3-pale-yellow');
        };
        to_date_input = document.createElement('INPUT');
        to_date_input.id = 'filter_to_date_' + _uuid;
        to_date_input.type = 'date';
        to_date_input.className = 'filter_input w3-bar-item w3-right';
        to_date_input.title = 'Filtrer les données  jusqu\'à de cette date';
        to_date_input.style.height = container.offsetHeight - 6 + 'px';
        to_date_input.onchange = function () {
            this.classList.add('w3-pale-yellow');
        };
        main_table_top_bar.appendChild(to_date_input);
        main_table_top_bar.appendChild(from_date_input)
    }
    main_table_top_bar.appendChild(q_input);
    main_table_top_bar.appendChild(select_column);
}
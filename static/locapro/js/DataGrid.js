function DataTable(container, callback, cache = 'no_cache') {

    /**
     * This function build an autocomplete table under the input (inp)
     * the input must have an url attribute to use to get the data for the autocomplete
     * example :
     * my_inp = document.getelementByTD("my_input_id")
     * my_inp.url = '/my_data_url/
     * AutocompleteTable(my_inp)
     **/
    container.id = 'datagrid_container_' + container.uuid;
    container.action = container.action || 'view_data';
    container.media_url = container.media_url || '/media/';
    container.parent_id = container.parent_id || '';
    container.parent_data = {'id': '0'};
    container.fixed_filter = container.fixed_filter || ''; // strings only (int raises error) example: JSON.stringify({object_id: 'HP000010/18', content_type__model:'Batch'});
    container.page_size = container.page_size || 50;
    container.page = 1;
    container.loading = false;
    container.table_data = [];
    container.q = '';
    container.field = '';
    container.from_date = container.from_date || '';
    container.to_date = container.to_date || '';
    container.currentFocus = false;
    container.lastFocus = 0;
    container.row_height = 25;   // Pixels
    container._heigh = container._heigh || '45%';
    container.style.height = container._heigh;
    container.style.overflow = 'auto';
    container.refresh = function () {

        container.selectedPk = null;
        container.selectedText = null;
        container.loading = false;
        container.currentFocus = false;
        container.lastFocus = 0;
        container.initialise();
    };
    container.initialise = function () {
        /** Get the data from the url**/
        startProcessing(container.uuid);
        container.loading = true;
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                container.table_data = JSON.parse(this.responseText);
                container.data_count = container.table_data.count;
                container.populateTable(container.table_data);
                container.setAttribute('model_name', container.table_data.model_data.model_name);
                container.parent_data = container.table_data.model_data.parent_data;
                container.loading = false;
                stopProcessing(container.uuid);
                let table_loaded = new CustomEvent('loaded_table_' + container.uuid);
                document.dispatchEvent(table_loaded);
                if (callback) {
                    callback()
                }
            } else if (this.readyState === 4 && this.status !== 200 && this.status !== 0) {
                stopProcessing(container.uuid);
                let notification = {};
                notification.header = 'Bug!!';
                notification.class = 'w3-red';
                notification.text = this.statusText;
                pop_notification(notification);
                container.loading = false;
            }
        };
        let _url = container.url +
            "?action=" + container.action +
            '&q=' + container.q +
            '&parent_id=' + container.parent_id +
            '&fixed_filter=' + container.fixed_filter +
            '&page=' + container.page +
            '&page_size=' + container.page_size +
            '&field=' + container.field +
            '&from_date=' + container.from_date +
            '&to_date=' + container.to_date;
        if (cache === 'no_cache') {
            _url += '&hash=' + generateUUID()
        }
        xmlhttp.open(
            "GET",
            _url,
            true
        );

        xmlhttp.send();
        xmlhttp.onerror = function (ev) {
            stopProcessing(container.uuid);
            let notification = {};
            notification.header = '';
            notification.class = 'w3-red';
            notification.text = 'Actualisation de données impossible. vérifiez votre connexion avec le serveur ! ';
            pop_notification(notification);
            container.loading = false;
        }
    };
    container.populateTable = function (arr) {

        while (container.firstChild) {
            container.removeChild(container.firstChild);
        }


        let enumeration = arr.model_data.enumeration;
        let table_name = arr.model_data.model_name;
        let attachment = false;
        if (arr.model_data.action_buttons.includes('attach')) {
            attachment = true
        }
        let _table = document.createElement('TABLE');
        _table.classList.add('data-table');
        _table.id = table_name + '_' + container.uuid;

        /** 1 - Create the table headers **/

        let _table_header = document.createElement('THEAD');
        let _last_table_header = document.createElement('TH');
        if (enumeration) {
            let _enumeration_header = document.createElement('TH');
            _enumeration_header.textContent = 'N°';
            _table_header.appendChild(_enumeration_header)
        }
        if (attachment) {
            const _attachment_header = document.createElement('TH');
            _attachment_header.className = 'attachment_header';
            const attachment_icon = document.createElement('I');
            attachment_icon.className = 'fas fa-paperclip';
            _attachment_header.appendChild(attachment_icon);
            _table_header.appendChild(_attachment_header)
        }
        let columns = arr.columns_data;
        for (let header in  columns) {
            if (columns[header].display) {
                let column_width = columns[header].width;
                let _new_header = document.createElement('TH');
                if (column_width) {
                    _new_header.style.width = column_width + 'cm';
                } else {
                    _new_header.textContent = columns[header].text;
                }
                _new_header.textContent = columns[header].text;
                _table_header.appendChild(_new_header);
            }
        }
        _table_header.appendChild(_last_table_header);
        _table.appendChild(_table_header);

        /** 2-  populate the datatable **/

        for (let i = 0, l = arr.fields.length; i < l; i++) {
            let row_text = (arr.fields[i].field_text);//.split(' ').join('\xa0');
            let row_pk = (arr.fields[i].pk);
            let row_conditional_format = arr.fields[i].conditional_format;
            let _new_row = document.createElement('TR');
            _new_row.setAttribute('data-pk', row_pk);
            _new_row.setAttribute('data-text', row_text);
            if (enumeration) {
                let _new_table_data = document.createElement('TD');
                _new_table_data.setAttribute('align', 'center');
                _new_table_data.classList.add('table-enumeration');
                _new_table_data.textContent = i + 1;
                _new_row.appendChild(_new_table_data)
            }
            if (attachment) {
                let _new_table_data = document.createElement('TD');
                _new_table_data.classList.add('attachment-table-data');
                _new_table_data.setAttribute('attachment', arr.fields[i]['attachments_exists']);
                if (arr.fields[i]['attachments_exists']) {
                    const attachment_icon = document.createElement('I');
                    attachment_icon.className = 'fas fa-paperclip';
                    attachment_icon.onclick = function (ev) {
                        attachment_icon.parentNode.click();
                        document.getElementById('attach_button__' + container.uuid).click()
                    };
                    _new_table_data.appendChild(attachment_icon);

                }
                _new_row.appendChild(_new_table_data)
            }
            for (let header in  columns) {
                if (columns[header].display) {
                    let _td_text = arr.fields[i][header];
                    let _new_table_data = document.createElement('TD');
                    // apply conditinal format

                    if (row_conditional_format[header]) {
                        let classes = row_conditional_format[header];
                        for (let i = 0, l = classes.length; i < l; i++) {
                            _new_table_data.classList.add(classes[i])
                        }
                    }
                    if (row_conditional_format['all_row']) {
                        let classes = row_conditional_format['all_row'];
                        for (let i = 0, l = classes.length; i < l; i++) {
                            _new_row.classList.add(classes[i])
                        }
                    }


                    if (columns[header].format === 'datetime') {
                        if (_td_text) {
                            _new_table_data.textContent = moment(_td_text).format('DD/MM/YYYY à H:mm');
                        }
                    } else if (columns[header].format === 'date') {
                        if (_td_text) {
                            _new_table_data.textContent = moment(_td_text).format('DD/MM/YYYY');
                        }
                    } else if (columns[header].format === 'money') {
                        if (_td_text) {
                            _new_table_data.setAttribute('align', 'right');
                            _new_table_data.textContent = accounting.formatMoney(_td_text);
                        }
                    } else if (columns[header].format === 'integer') {
                        if (_td_text || _td_text === 0) {
                            _new_table_data.setAttribute('align', 'right');
                            _new_table_data.textContent = accounting.formatNumber(_td_text, 0);
                        }

                    } else if (columns[header].format === 'float') {
                        if (_td_text) {
                            _new_table_data.setAttribute('align', 'right');
                            _new_table_data.textContent = accounting.formatNumber(_td_text, 2);
                        }
                    } else if (columns[header].format === 'percent') {
                        if (_td_text) {
                            _new_table_data.setAttribute('align', 'right');
                            _new_table_data.textContent = accounting.formatMoney(_td_text, '%', 2);
                        }
                    } else if (columns[header].format === 'boolean') {
                        _new_table_data.setAttribute('align', 'center');
                        let _new_icon = document.createElement('I');
                        if (_td_text) {
                            _new_icon.className = 'fas fa-check-square';
                        } else {
                            _new_icon.className = 'fas fa-square';
                        }
                        _new_table_data.appendChild(_new_icon);
                    } else if (columns[header].format === 'media') {
                        if (_td_text) {
                            _new_table_data.setAttribute('align', 'center');
                            let _new_icon = document.createElement('I');
                            _new_icon.className = 'fas fa-file-download';
                            let _new_link = document.createElement('A');
                            _new_link.setAttribute('title', _td_text);
                            _new_link.setAttribute('href', '/media/' + _td_text);
                            _new_link.setAttribute('download', _td_text);
                            _new_link.appendChild(_new_icon);
                            _new_table_data.appendChild(_new_link);
                        }
                    } else if (columns[header].format === 'file') {
                        if (_td_text) {
                            _new_table_data.setAttribute('align', 'left');
                            let _new_icon = document.createElement('I');
                            _new_icon.className = 'fas fa-file-download file-link';
                            let _new_link = document.createElement('A');
                            _new_link.setAttribute('title', _td_text);
                            _new_link.setAttribute('href', '/media/' + _td_text);
                            _new_link.setAttribute('download', _td_text);
                            _new_link.appendChild(_new_icon);
                            let _file_path_array = _td_text.split('/');
                            let file_name = _file_path_array[_file_path_array.length - 1];
                            let _file_name = document.createTextNode(file_name);
                            _new_table_data.appendChild(_new_link);
                            _new_table_data.appendChild(_file_name);

                        }

                    } else {
                        if (_td_text) {
                            _new_table_data.textContent = _td_text
                        }

                    }
                    _new_row.appendChild(_new_table_data)
                }
            }
            let _last_tale_data = document.createElement('TD');
            _new_row.appendChild(_last_tale_data);
            _table.appendChild(_new_row);
        }
        /** 3 - Create the table footer **/
        let columnSpan = Object.keys(columns).length + 1;
        if (enumeration) {
            columnSpan += 1
        }
        if (arr.count > container.page_size * container.page) {
            let _load_next_page_row = document.createElement('TR');
            _load_next_page_row.classList.add('load-next-page-row');
            let _new_table_data = document.createElement('TD');
            _new_table_data.setAttribute('align', 'center');
            _new_table_data.setAttribute('colspan', columnSpan);

            let _new_icon = document.createElement('I');
            _new_icon.className = 'fas fa-angle-double-down';
            _new_table_data.appendChild(_new_icon);
            _load_next_page_row.appendChild(_new_table_data);

            let empty_table_data = document.createElement('TD');
            _load_next_page_row.appendChild(empty_table_data);

            _table.appendChild(_load_next_page_row);
        }
        container.appendChild(_table);
        container.row_height = _table.lastChild.offsetHeight;
        container.addActive()
    };
    document.addEventListener('keydown', function (e) {
        if (currentActive === container) {
            if (e.key === 'ArrowDown') {
                e.preventDefault();
                container.lastFocus = container.currentFocus;
                container.currentFocus++;
                container.addActive();
            } else if (e.key === 'ArrowUp') {
                e.preventDefault();
                container.lastFocus = container.currentFocus;
                container.currentFocus--;
                container.addActive();
            } else if (e.key === 'Enter') {
                e.preventDefault();
                let current_row = container.getElementsByTagName('tr')[container.currentFocus];
                if (current_row) {
                    current_row.click()
                }
            }
        }
    });
    container.addEventListener("click", function (event) {
        currentActive = container;
        let selectedRow;
        let target = event.target.nodeName;
        if (target !== 'TD' && target !== 'TR') {
            return false
        } else {
            if (target === 'TD') {
                selectedRow = event.target.parentNode
            } else if (target === 'TR') {
                selectedRow = event.target
            }
            if (selectedRow.classList.contains('load-next-page-row')) {
                if (!container.loading) {
                    container.page += 1;
                    container.loading = true;
                    container.initialise();
                }
            } else {
                container.lastFocus = container.currentFocus;
                container.currentFocus = selectedRow.rowIndex;
                container.addActive();
            }
            let row_clicked_event = new CustomEvent('row_clicked_in_table_' + container.uuid);
            document.dispatchEvent(row_clicked_event);
        }
    });
    container.scrollToSelected = function (el) {
        let _el = el.getBoundingClientRect();
        let _container = container.getBoundingClientRect();
        let relativePos = _el.top - _container.top;
        if (relativePos > container.clientHeight - container.row_height) {
            container.scrollBy(0, container.row_height)
        } else if (relativePos < container.row_height) {
            container.scrollBy(0, -container.row_height)
        }
    };
    container.addActive = function () {
        let x = container.getElementsByTagName('tr');
        if (!x) return false;
        let loaded_table_length = x.length;
        if (container.currentFocus > loaded_table_length - 2) {
            if (!container.loading && loaded_table_length < container.data_count) {
                container.page += 1;
                container.loading = true;
                container.initialise();
            } else if (container.currentFocus >= container.data_count) {
                container.currentFocus = container.data_count - 1
            }
        } else if (container.currentFocus < 0) {
            container.currentFocus = 0;
        }
        container.removeActive(x);

        let currentRow = x[container.currentFocus];
        if (currentRow !== undefined) {
            currentRow.classList.add("selected");
            //set text of selected row;
            container.selectedText = currentRow.getAttribute('data-text');
            //set Pk of selected row;
            container.selectedPk = currentRow.getAttribute('data-pk');
            container.scrollToSelected(currentRow);
            let row_selected_event = new CustomEvent('row_selected_in_table_' + container.uuid);
            document.dispatchEvent(row_selected_event);
        }
    };
    container.removeActive = function (x) {
        if (x[container.lastFocus]) {
            x[container.lastFocus].classList.remove("selected");
        }
    };
    container.setSelected = function (pk) {
        if (pk !== null || pk !== 0) {
            let _rows = container.getElementsByTagName('TR');
            for (let i = 0, l = _rows.length; i < l; i++) {
                if (_rows[i].getAttribute('data-pk') === pk) {
                    _rows[i].click()
                }
            }
        }

    };
    container.initialise();
}


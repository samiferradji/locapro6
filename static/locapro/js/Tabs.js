let tab_id = 0;  // 0 for the home tab
let tabs_count = 0;  // 0 for the home tab
let maximum_tabs = 8;

function openTab(current_id) {
    let x = document.getElementsByClassName("tab-container");
    for (let i = 0, l = x.length; i < l; i++) {
        x[i].style.display = "none";
    }
    let tablinks = document.getElementsByClassName("tablink");
    for (let i = 0, l = tablinks.length; i < l; i++) {
        tablinks[i].className = tablinks[i].className.replace("selected-link", "");
    }
    document.getElementById('tab' + current_id).style.display = "block";
    document.getElementById('link' + current_id).className += " selected-link";
}

function closeTab(event, current_id) {
    event.stopPropagation();
    let next_tab;
    let _tab = document.getElementById('tab' + current_id);
    let _link = document.getElementById('link' + current_id);
    document.getElementById('tabs-buttons').removeChild(_link);
    document.getElementById('tabs-content').removeChild(_tab);
    let next_tab_id = findNextTab(current_id);
    tabs_count--;
    openTab(next_tab_id);


    function findNextTab(current_tab_id) {
        if (current_tab_id === tab_id) {
            return findPreviousTab(current_tab_id);
        } else {
            current_tab_id++;
            next_tab = document.getElementById('tab' + current_tab_id);
            if (next_tab !== null) {
                return current_tab_id
            } else {
                return findNextTab(current_tab_id)
            }
        }
    }

    function findPreviousTab(current_tab_id) {
        if (current_tab_id === 0) {
            return current_tab_id;
        } else {
            current_tab_id--;
            next_tab = document.getElementById('tab' + current_tab_id);
            if (next_tab !== null) {
                return current_tab_id
            } else {
                return findPreviousTab(current_tab_id)
            }
        }
    }
}

function getNewPageData(url, title, subTitle) {
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            tab_id++;
            createNewTab(tab_id, title, subTitle, this.responseText);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function createNewTab(tab_id, title, subTitle, htmlContent) {
    if (tabs_count > maximum_tabs) {
        let notification = {};
        notification.header = 'Max';
        notification.text = 'Le maximum des fenêtres overtes est atteint';
        notification.class = 'w3-orange';
        pop_notification(notification)
    } else {
        tabs_count++;
        let x = document.getElementsByClassName("tab-container");
        for (let i = 0, l = x.length; i < l; i++) {
            x[i].style.display = "none";
        }
        let tablinks = document.getElementsByClassName("tablink");
        for (let i = 0, l = tablinks.length; i < l; i++) {
            tablinks[i].className = tablinks[i].className.replace("selected-link", "");
        }
        let link_span = document.createElement('SPAN');
        link_span.id = 'link' + tab_id;
        link_span.className = 'w3-bar-item tablink selected-link';
        link_span.onclick = function () {
            openTab(tab_id)
        };

        let link_text_span = document.createElement('SPAN');
        link_text_span.className = 'link-text';

        let link_sub_title = document.createElement('SPAN');
        link_sub_title.className = 'link-sub-title';
        link_sub_title.style.display = 'block';
        link_sub_title.textContent = subTitle;

        let link_title = document.createElement('SPAN');
        link_title.className = 'w3-left';
        link_title.textContent = title;

        let close_link_span = document.createElement('SPAN');
        close_link_span.className = 'close-tab-x';
        close_link_span.setAttribute('title','Fermer');
        const close_icon = document.createElement('I');
        close_icon.className='fas fa-times';
        close_link_span.appendChild(close_icon);
        close_link_span.onclick = function (ev) {
            closeTab(ev, tab_id)
        };
        link_text_span.appendChild(link_sub_title);
        link_text_span.appendChild(link_title);
        link_span.appendChild(link_text_span);
        link_text_span.appendChild(close_link_span);
        document.getElementById('tabs-buttons').appendChild(link_span);

        let new_tab_content = document.createElement("div");
        new_tab_content.id = 'tab' + tab_id;
        new_tab_content.className = 'w3-container tab-container ';
        new_tab_content.style.display = 'block';
        new_tab_content.innerHTML = htmlContent;
        document.getElementById('tabs-content').appendChild(new_tab_content);

        let new_content = document.getElementById('tab' + tab_id);
        let new_script = new_content.getElementsByTagName('script');
        eval(new_script[0].innerHTML);

        let page_inputs = new_content.getElementsByTagName('input');
        if (page_inputs.length !== 0) {
            page_inputs[0].focus()
        }
    }
}
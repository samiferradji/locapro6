window.processing = [];
function startProcessing(uuid) {
    window.processing.push(uuid);
    document.getElementById('processing-first-line_' + uuid).classList.add('processing-first-line');
    document.getElementById('processing-second-line_' + uuid).classList.add('processing-second-line');
    setTimeout(function () {
        if (window.processing.includes(uuid)) {
            let current_page = document.getElementById('page_' + uuid);
            current_page.classList.add('long_processing');
        }
    }, 3000);
}

function stopProcessing(uuid) {
    if (uuid !== undefined) {
        if (window.processing.includes(uuid)) {
            let current_page = document.getElementById('page_' + uuid);
            current_page.classList.remove('long_processing');
            window.processing.splice([window.processing.indexOf(uuid)], 1);
            document.getElementById('processing-first-line_' + uuid).classList.remove('processing-first-line');
            document.getElementById('processing-second-line_' + uuid).classList.remove('processing-second-line');
        }
    }
}
function startLoadingPage() {
    let loading_spinner = document.getElementById('page_loading');
    loading_spinner.style.visibility = 'visible'
}

function stopLoadingPage() {
    let loading_spinner = document.getElementById('page_loading');
    loading_spinner.style.visibility = 'hidden'
}


function refreshQuickMenu(user_id) {
    let _container = document.getElementById('quickMenuContainer');
    _container.innerHTML = '';
    let _url = '/common/quick_menu/';
    _url += '?user='+ user_id;
    _url += '&hash=' + generateUUID();
    let new_li, _html;
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            let menu_data = JSON.parse(this.responseText);
            for (let _i = 0, _l = menu_data.length; _i < _l; _i++) {
                new_li = document.createElement('li');
                new_li.className = 'w3-card-2 w3-padding w3-btn quickMenu';
                new_li.id = menu_data[_i].menu__id;
                new_li.setAttribute('data-url', menu_data[_i].menu__url + menu_data[_i].menu__parameters);
                new_li.setAttribute('data-title', menu_data[_i].menu__title);
                new_li.setAttribute('data-sub-title', menu_data[_i].menu__sub_title);
                new_li.setAttribute('data-sub-title', menu_data[_i].menu__sub_title);
                new_li.setAttribute('title', menu_data[_i].menu__description);
                new_li.setAttribute('draggable', 'true');
                new_li.setAttribute('ondragstart', 'drag(event)');
                _html = '<div class="w3-bar quickMenuHeader">' + menu_data[_i].menu__sub_title + '</div>'
                _html += '<h3 class="quickMenuTextIcon">' + menu_data[_i].menu__icon +
                    '</h3><div class="quickMenuText">' + menu_data[_i].menu__title + '</div>';
                new_li.innerHTML = _html;
                new_li.addEventListener('click', function (ev) {
                    getNewPageData(
                        this.getAttribute('data-url'),
                        this.getAttribute('data-title'),
                        this.getAttribute('data-sub-title'))
                });
                _container.appendChild(new_li)

            }

        }
    };
    xmlhttp.open("GET", _url, true);
    xmlhttp.send();


}

function deleteQuickMenu(ev, user_id, csrf_token) {
    ev.preventDefault();
    ev.stopPropagation();
    let _menu_id = ev.dataTransfer.getData("menu_id");
    let _url = '/common/quick_menu/';
    let obj = '';
    obj += 'action=delete_menu';
    obj += '&csrfmiddlewaretoken=' + csrf_token ;
    obj += '&menu_id=' + _menu_id;
    obj += '&user=' + user_id;
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            refreshQuickMenu(user_id)
        }
    };
    xmlhttp.open('POST', _url, true);
    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    xmlhttp.send(obj);
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("menu_id", ev.target.id);
}

function drop(ev, user_id, csrf_token) {
    ev.preventDefault();
    let _menu_id = ev.dataTransfer.getData("menu_id");
    let _url = '/common/quick_menu/';
    let obj = '';
    obj += 'action=add_menu';
    obj += '&csrfmiddlewaretoken=' + csrf_token;
    obj += '&menu_id=' + _menu_id;
    obj += '&user=' + user_id;
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            refreshQuickMenu(user_id)
        }
    };
    xmlhttp.open('POST', _url, true);
    xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    xmlhttp.send(obj);
}
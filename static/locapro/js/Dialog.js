function openDialog(elm) {
    elm.style.display = 'block';
    elm.classList.add('dialog_opened');
    let inputs = elm.getElementsByTagName('input');
    for (let i = 0, l = inputs.length; i < l; i++) {
        for (let i = 0, l = inputs.length; i < l; i++) {
            if (inputs[i].type !== 'hidden' && !inputs[i].disabled) {
                inputs[i].focus();
                break
            }
        }
    }
}

function closeDialog(elm) {
    elm.style.display = 'none';
    elm.classList.remove('dialog_opened');
}

function confirmationDialog(elm, question, callback) {

    // clear old dialog
    while (elm.firstChild) {
        elm.removeChild(elm.firstChild);
    }

    //  Create dialog elements

    let confirmation_dialog_content = document.createElement('div');
    confirmation_dialog_content.className = 'w3-modal-content';
    confirmation_dialog_content.style.width = '450px';

    let confirmation_dialog_to_panel = document.createElement('div');
    confirmation_dialog_to_panel.className = 'w3-bar top-Panel';


    let confirmation_dialog_to_title = document.createElement('DIV');
    confirmation_dialog_to_title.className = 'w3-bar-item';
    confirmation_dialog_to_title.innerText = 'Confirmation';

    let question_element = document.createElement('DIV');
    let buttons_area = document.createElement('DIV');
    let yes_button = document.createElement('BUTTON');
    let no_button = document.createElement('BUTTON');

    //  Set dialog elements
    question_element.className = 'w3-panel';
    question_element.innerText = question;

    buttons_area.className = 'w3-container form-buttons';
    yes_button.className = 'w3-button w3-right ';
    yes_button.innerText = 'Confirmer';
    no_button.className = 'w3-button w3-right';
    no_button.innerText = 'Annuler';

    // Add Dialog custom events
    no_button.addEventListener('click', function (ev) {
        closeDialog(elm)
    });

    yes_button.addEventListener('click', function (ev) {
        callback();

    });

    // Append dialog elements to contents
    buttons_area.appendChild(no_button);
    buttons_area.appendChild(yes_button);
    question_element.appendChild(buttons_area);

    confirmation_dialog_to_panel.appendChild(confirmation_dialog_to_title);
    confirmation_dialog_content.appendChild(confirmation_dialog_to_panel);
    confirmation_dialog_content.appendChild(question_element);
    elm.appendChild(confirmation_dialog_content);

    elm.style.display = 'block';
    elm.classList.add('dialog_opened');


}

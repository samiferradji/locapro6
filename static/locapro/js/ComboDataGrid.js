function AutocompleteTable(container, cache = 'no_cache') {

    /**
     * This function build an autocomplete table under the input (inp)
     * the input must have an url attribute to use to get the data for the autocomplete
     * example :
     * my_inp = document.getelementByTD("my_input_id")
     * my_inp.url = '/my_data_url/
     * AutocompleteTable(my_inp)
     **/
    container.page_size = 8;
    container.page = 1;
    container.loading = false;
    container.table_data = [];
    container.data_count = 0;
    container.combo_container = "";
    container.parent_id = container.parent_id || '';
    container.fixed_filter = container.fixed_filter || '';
    container.q = '';
    container.currentFocus = -1;
    container.lastFocus = -1;
    container.combo_height = '250px';
    container.timeout = null;
    container.typing_timeout = 250;    // Milliseconds
    container.row_height = null;   // Pixels
    container.selectedPk = '';
    container.column_tiltes = container.column_tiltes || true;  // A completer , prob d'indexes when selecting
    container.setAttribute('autocomplete', 'off');
    container.setAttribute('placeholder', ' ...');
    container.initialise = function () {
        /** Get the data from the url**/
        container.loading = true;
        let xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                container.table_data = JSON.parse(this.responseText);
                container.data_count = container.table_data.count;
                container.populateTable(container.table_data);
                container.setAttribute('model_name', container.table_data.model_data.model_name);
                container.loading = false;
            } else if (this.readyState === 4 && this.status !== 200 && this.status !== 0) {
                let notification = {};
                notification.header = 'Bug!!';
                notification.class = 'w3-red';
                notification.text = this.statusText;
                pop_notification(notification);
                container.loading = false;
            }
        };
        let _url = container.url +
            '?q=' + container.q +
            '&action=view_data' +
            '&parent_id=' + container.parent_id +
            '&fixed_filter=' + container.fixed_filter +
            '&page=' + container.page +
            '&page_size=' + container.page_size;
        if (cache === 'no_cache') {
            _url += '&hash=' + generateUUID()
        }
        xmlhttp.open(
            "GET",
            _url,
            true
        );
        xmlhttp.send();
        xmlhttp.onerror = function (ev) {
            let notification = {};
            notification.header = '';
            notification.class = 'w3-red';
            notification.text = 'Actualisation de données impossible. vérifiez votre connexion avec le serveur ! ';
            pop_notification(notification);
            container.loading = false;
        }

    };
    container.populateTable = function (arr) {
        while (container.combo_container.firstChild) {
            container.combo_container.removeChild(container.combo_container.firstChild);
        }
        let enumeration = container.table_data.model_data.enumeration;
        let table_name = container.table_data.model_data.model_name;
        let _table = document.createElement('TABLE');
        let columns = arr.columns_data;

        _table.classList.add('combo-table');
        _table.id = table_name + '_' + container.uuid;
        /** 1 - Create the table headers **/

        let _table_header = document.createElement('THEAD');
        if (enumeration) {
            let _enumeration_header = document.createElement('TH');
            _enumeration_header.textContent = 'N°';
            _table_header.appendChild(_enumeration_header)
        }
        for (let header in  columns) {
            if (columns[header].display) {
                let column_width = columns[header].width;
                let _new_header = document.createElement('TH');
                if (column_width) {
                    _new_header.style.width = column_width + 'cm';
                } else {
                    _new_header.textContent = columns[header].text;
                }
                _new_header.textContent = columns[header].text;
                _table_header.appendChild(_new_header);
            }
        }
        _table.appendChild(_table_header);
        /** 2-  populate the datatable **/

        for (let i = 0, l = arr.fields.length; i < l; i++) {
            let row_text = (arr.fields[i].field_text);//.split(' ').join('\xa0');
            let row_pk = (arr.fields[i].pk);
            let row_conditional_format = arr.fields[i].conditional_format;
            let _new_row = document.createElement('TR');
            _new_row.setAttribute('data-pk', row_pk);
            _new_row.setAttribute('data-text', row_text);
            if (enumeration) {
                let _new_table_data = document.createElement('TD');
                _new_table_data.setAttribute('align', 'center');
                _new_table_data.classList.add('table-enumeration');
                _new_table_data.textContent = i + 1;
                _new_row.appendChild(_new_table_data)
            }
            for (let header in  columns) {
                if (columns[header].display) {
                    let _td_text = arr.fields[i][header];
                    let _new_table_data = document.createElement('TD');
                    // apply conditinal format

                    if (row_conditional_format[header]) {
                        let classes = row_conditional_format[header];
                        for (let i = 0, l = classes.length; i < l; i++) {
                            _new_table_data.classList.add(classes[i])
                        }
                    }
                    if (row_conditional_format['all_row']) {
                        let classes = row_conditional_format['all_row'];
                        for (let i = 0, l = classes.length; i < l; i++) {
                            _new_row.classList.add(classes[i])
                        }
                    }


                    if (columns[header].format === 'datetime') {
                        if (_td_text) {
                            _new_table_data.textContent = moment(_td_text).format('DD/MM/YYYY à H:mm');
                        }
                    } else if (columns[header].format === 'date') {
                        if (_td_text) {
                            _new_table_data.textContent = moment(_td_text).format('DD/MM/YYYY');
                        }
                    } else if (columns[header].format === 'money') {
                        if (_td_text) {
                            _new_table_data.setAttribute('align', 'right');
                            _new_table_data.textContent = accounting.formatMoney(_td_text);
                        }
                    } else if (columns[header].format === 'integer') {
                        if (_td_text || _td_text === 0) {
                            _new_table_data.setAttribute('align', 'right');
                            _new_table_data.textContent = accounting.formatNumber(_td_text, 0);
                        }

                    } else if (columns[header].format === 'float') {
                        if (_td_text) {
                            _new_table_data.setAttribute('align', 'right');
                            _new_table_data.textContent = accounting.formatNumber(_td_text, 2);
                        }
                    } else if (columns[header].format === 'percent') {
                        if (_td_text) {
                            _new_table_data.setAttribute('align', 'right');
                            _new_table_data.textContent = accounting.formatMoney(_td_text, '%', 2);
                        }
                    } else if (columns[header].format === 'boolean') {
                        _new_table_data.setAttribute('align', 'center');
                        let _new_icon = document.createElement('I');
                        if (_td_text) {
                            _new_icon.className = 'fas fa-check-square';
                        } else {
                            _new_icon.className = 'fas fa-square';
                        }
                        _new_table_data.appendChild(_new_icon);
                    } else if (columns[header].format === 'media') {
                        if (_td_text) {
                            _new_table_data.setAttribute('align', 'center');
                            let _new_icon = document.createElement('I');
                            _new_icon.className = 'fas fa-file-download';
                            let _new_link = document.createElement('A');
                            _new_link.setAttribute('title', _td_text);
                            _new_link.setAttribute('href', '/media/' + _td_text);
                            _new_link.setAttribute('download', _td_text);
                            _new_link.appendChild(_new_icon);
                            _new_table_data.appendChild(_new_link);
                        }
                    } else if (columns[header].format === 'file') {
                        if (_td_text) {
                            _new_table_data.setAttribute('align', 'left');
                            let _new_icon = document.createElement('I');
                            _new_icon.className = 'fas fa-file-download file-link';
                            let _new_link = document.createElement('A');
                            _new_link.setAttribute('title', _td_text);
                            _new_link.setAttribute('href', '/media/' + _td_text);
                            _new_link.setAttribute('download', _td_text);
                            _new_link.appendChild(_new_icon);
                            let _file_path_array = _td_text.split('/');
                            let file_name = _file_path_array[_file_path_array.length - 1];
                            let _file_name = document.createTextNode(file_name);
                            _new_table_data.appendChild(_new_link);
                            _new_table_data.appendChild(_file_name);

                        }

                    } else {
                        if (_td_text) {
                            _new_table_data.textContent = _td_text
                        }

                    }
                    _new_row.appendChild(_new_table_data)
                }
            }
            _table.appendChild(_new_row);
        }
        /** 3 - Create the table footer **/
        let columnSpan = Object.keys(columns).length + 1;
        if (enumeration) {
            columnSpan += 1
        }
        if (container.table_data.count > container.page_size * container.page) {
            let _load_next_page_row = document.createElement('TR');
            let _new_table_data = document.createElement('TD');
            _new_table_data.setAttribute('align', 'center');
            _new_table_data.setAttribute('colspan', columnSpan);

            let _new_icon = document.createElement('I');
            _new_icon.className = 'fas fa-angle-double-down';

            _new_table_data.appendChild(_new_icon);
            _load_next_page_row.appendChild(_new_table_data);
            _table.appendChild(_load_next_page_row)
        }
        container.combo_container.appendChild(_table);
        container.row_height = _table.lastChild.offsetHeight;

        container.addActive();
    };
    container.load_combobox = function () {
        container.q = this.value;
        /** close any already open lists of autocompleted values **/
        container.closeAllLists(container);
        if (container.combo_container === "") {
            container.combo_container = document.createElement("DIV");
            container.combo_container.style.height = container.combo_height;
            container.combo_container.setAttribute("id", this.id + "autocomplete-list");
            container.combo_container.setAttribute("class", "autocomplete-items w3-card");
        }
        container.initialise();
        this.parentNode.appendChild(container.combo_container);
        container.combo_container.addEventListener("mousedown", function (event) {
            event.preventDefault();
            let selectedRow;
            let target = event.target.nodeName;
            if (target === 'TD') {
                selectedRow = event.target.parentNode
            } else if (target === 'TR') {
                selectedRow = event.target
            }
            container.lastFocus = container.currentFocus;
            container.currentFocus = selectedRow.rowIndex;
            container.selectedText = selectedRow.getAttribute('data-text');
            container.value = container.selectedText;
            container.selectedPk = selectedRow.getAttribute('data-pk');
            container.addActive();
            let combo_grid_updated = new Event('combo_grid_updated');
            container.dispatchEvent(combo_grid_updated);
            let row_selected = new CustomEvent('row_selected_in_combo_' + container.id);
            document.dispatchEvent(row_selected);


        });
    };
    container.oninput = function (e) {
        if (container.value === ' ') {
            container.value = ''
        }
        container.value = container.value.replace(/ {2}/g, ' ');
        clearTimeout(container.timeout);
        container.timeout = setTimeout(function () {
            if (!container.loading && container === document.activeElement) {
                container.load_combobox()
            }
        }, container.typing_timeout);
    };
    container.onkeydown = function (e) {
        if (currentActive === container) {
            let x = container.combo_container;
            if (x) {
                if (e.key === 'ArrowDown') {
                    e.preventDefault();
                    container.lastFocus = container.currentFocus;
                    container.currentFocus++;
                    container.addActive();
                } else if (e.key === 'ArrowUp') {
                    e.preventDefault();
                    container.lastFocus = container.currentFocus;
                    container.currentFocus--;
                    container.addActive();
                } else if (e.key === 'Escape') {
                    container.closeAllLists();
                } else if (e.key === 'Enter') {
                    if (container.currentFocus >= 0) {
                        e.preventDefault();
                        let rows = container.combo_container.getElementsByTagName('tr');
                        if (rows) {
                            let currentRow = rows[container.currentFocus];
                            container.selectedPk = currentRow.getAttribute('data-pk');
                            container.selectedText = currentRow.getAttribute('data-text');
                            container.value = container.selectedText;
                            let row_selected = new CustomEvent('row_selected_in_combo_' + container.id);
                            document.dispatchEvent(row_selected);
                        }
                    }
                }
            } else {
                if ((e.keyCode === 40 && e.altKey)) {  //down
                    container.load_combobox()
                }
            }
        }
    };
    container.onblur = function () {
        container.closeAllLists();
        if (!container.value) {
            container.selectedPk = '';
            container.selectedText = ''
        }
        if (container.selectedPk === '') {
            container.value = ''
        } else {
            container.value = container.selectedText
        }
    };
    container.scrollToSelected = function (el) {
        let _el = el.getBoundingClientRect();
        let _container = container.combo_container.getBoundingClientRect();
        let relativePos = _el.top - _container.top;
        if (relativePos > container.combo_container.clientHeight - container.row_height) {
            container.combo_container.scrollBy(0, container.row_height)
        } else if (relativePos < container.row_height) {
            container.combo_container.scrollBy(0, -container.row_height)

        }
    };
    container.addActive = function () {

        let x = container.combo_container.getElementsByTagName('tr');
        if (!x) return false;
        if (container.currentFocus > x.length - 2) {
            if (!container.loading && x.length < container.data_count) {
                container.page += 1;
                container.loading = true;
                container.initialise();
            } else if (container.currentFocus >= container.data_count) {
                container.currentFocus = container.data_count - 1
            }
        } else if (container.currentFocus < 0) {
            container.currentFocus = 0
        }
        container.removeActive(x);

        /**add class "selected"**/
        let currentRow = x[container.currentFocus];
        if (currentRow !== undefined) {
            currentRow.classList.add("selected");
            container.scrollToSelected(currentRow);

        }
    };
    container.removeActive = function (x) {
        if (container.lastFocus !== -1) {
            x[container.lastFocus].classList.remove("selected");
        }
    };
    container.closeAllLists = function (elmnt) {
        container.lastFocus = -1;
        container.currentFocus = -1;
        let x = document.getElementsByClassName("autocomplete-items");
        for (let i = 0; i < x.length; i++) {
            if (elmnt !== x[i] && elmnt !== container) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    };
    container.setSelected = function (pk) {
        if (!pk || pk === 0) {
            container.value = '';
            container.selectedText = '';
            container.selectedPk = '';
        } else {
            container.loading = true;
            let xmlhttp = new XMLHttpRequest();
            let obj = '';
            obj += 'action=get_line';
            obj += '&csrfmiddlewaretoken=' + container.csrf_token;
            obj += '&line_id=' + pk;
            xmlhttp.onreadystatechange = function () {
                if (this.readyState === 4 && this.status === 200) {
                    let response = JSON.parse(this.responseText);
                    let _selected = JSON.parse(response.status_data);
                    console.log(_selected['instance_title']);
                    container.value = _selected['instance_title'];
                    container.selectedText = _selected['instance_title'];
                    container.selectedPk = _selected['pk'];
                    container.loading = false;
                }
            };
            xmlhttp.open("POST", container.url, true);
            xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            xmlhttp.send(obj);
        }
    };
    container.addEventListener('focus', function () {
        currentActive = container;
    });


}

function BuildActionButtons(container) {
    let _bar = document.createElement('DIV');
    _bar.className = 'w3-bar action-buttons';
    container.appendChild(_bar);

    for (let i = 0, l = container.settings.length; i < l; i++) {
        let _button = document.createElement('BUTTON');
        _button.className = 'w3-bar-item w3-button';
        _button.title = container.settings[i]['tooltip'];
        _button.id = container.settings[i]['id'];
        let _button_icon = document.createElement('I');
        _button_icon.className = container.settings[i]['icon_class'];
        let _button_text = container.settings[i]['text'];
        _button.appendChild(_button_icon);
        _button.innerText = _button_text;
        _button.appendChild(_button_icon);
        _bar.appendChild(_button);
    }

    for (let i = 0, l = container.settings.length; i < l; i++) {
        let e = container.settings[i]['events'];
        if (e) {
            for (let _i = 0, _l = e.length; _i < _l; _i++) {
                let _key = Object.keys(e[_i])[0];
                let current_button = document.getElementById(container.settings[i]['id']);
                current_button.addEventListener(_key, e[_i][_key]);
            }
        }
    }

}
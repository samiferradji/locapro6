function show_details_area(page, details_area) {
    details_area.style.display = 'block';
    details_area.style.height = (details_area.parentNode.offsetHeight * 0.7) - 15 + 'px';
    // 15 px because of the details bar
    page.details_page.resizeHigh('100%');
    page.resizeHigh('30%')
}

function hide_details_area(page, details_area) {
    details_area.style.display = 'none';
    page.resizeHigh(page.parentNode.offsetHeight - 15 + 'px');
}

function BuildChildPage(page, details_area, page_built_callback, table_built_callback) {
    document.addEventListener('loaded_table_' + page.uuid, function () {
        while (details_area.firstChild) {
            details_area.removeChild(details_area.firstChild)
        }
        hide_details_area(page, details_area);
        page.details_page = document.createElement('DIV');
        details_area.appendChild(page.details_page);
        page.details_page.url = page.child_url;
        page.details_page.uuid = generateUUID();
        page.details_page.id = 'page_' + page.details_page.uuid;
        page.details_page.csrf_token = page.csrf_token;
        page.details_page.parent_id = page.main_table.selectedPk;
        BuildPage(page.details_page);
        page.details_page.main_table.classList.add('details-data-table');
        document.addEventListener('row_selected_in_table_' + page.uuid, function () {
            page.details_page.main_table.parent_id = page.main_table.selectedPk;
            page.details_page.main_table.refresh();
        });
        document.addEventListener('loaded_table_' + page.details_page.uuid, function () {
            if (table_built_callback) {
                table_built_callback()
            }
        });

        document.addEventListener('page_built_' + page.details_page.uuid, function () {
            if (page_built_callback) {
                page_built_callback()
            }

        });
    })
}
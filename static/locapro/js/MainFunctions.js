let currentActive;  //for current element focus


function notifications_open() {
    document.getElementById("notificationsSidebar").style.display = "block";
}

function notifications_close() {
    document.getElementById("notificationsSidebar").style.display = "none";
}

document.addEventListener("keydown", function (event) {

        if (event.defaultPrevented) {
            return; // Should do nothing if the default action has been cancelled
        }
        let handled = false;
        if (event.key !== undefined) {
            // Handle the event with KeyboardEvent.key and set handled true.
            if (event.key === 'Escape' && event.shiftKey) {
                handled = true;
                let opened_dialogs = document.getElementsByClassName('dialog_opened');
                for (let i = 0, l = opened_dialogs.length; i < l; i++) {
                    closeDialog(opened_dialogs[i]);

                }
            }
        } else if (event.keyCode !== undefined) {
            // Handle the event with KeyboardEvent.keyCode and set handled true.
            if (event.keyCode === 27 && event.shiftKey) {
                handled = true;
                let opened_dialogs = document.getElementsByClassName('dialog_opened');
                for (let i = 0, l = opened_dialogs.length; i < l; i++) {
                    closeDialog(opened_dialogs[i]);
                }
            }
        }

        if (handled) {
            // Suppress "double action" if event handled
            event.preventDefault();
        }
    },
    true
);

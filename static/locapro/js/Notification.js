function pop_notification(obj) {
    let currentdate = moment().format('LLLL');
    let notif_container = document.getElementById('notification');
    let history_notifs_container = document.getElementById('notificationsTable');
    let notification_div = document.createElement('DIV');
    notification_div.className = 'w3-panel w3-display-container ' + obj.class;
    notification_div.style.width = '300px';

    let notification_span = document.createElement('SPAN');
    notification_span.className = 'w3-button w3-large w3-display-topright ' + obj.class;
    notification_span.onclick = function (ev) {
        ev.target.parentElement.style.display = 'none'
    };
    notification_span.textContent = 'x';

    let notification_header = document.createElement('H5');
    notification_header.textContent = obj.header;
    notification_div.appendChild(notification_span);
    notification_div.appendChild(notification_header);
    if (obj.message_html_element) {
        notification_div.appendChild(obj.message_html_element)
    } else {
        let notification_paragraph = document.createElement('P');

        notification_paragraph.textContent = obj.text;
        notification_div.appendChild(notification_paragraph);
    }


    let new_notification = document.createElement('div');
    new_notification.appendChild(notification_div);
    notif_container.appendChild(new_notification);
    setTimeout(function () {
        notif_container.removeChild(new_notification);
        let row = history_notifs_container.insertRow(1);
        let _header_td = document.createElement('TD');
        _header_td.className = obj.class;
        _header_td.textContent = obj.header;
        let _text_td = document.createElement('TD');
        if (obj.message_html_element) {
            _text_td.appendChild(obj.message_html_element)
        } else {
            _text_td.textContent = obj.text
        }

        let _current_date_td = document.createElement('TD');
        _current_date_td.textContent = currentdate;
        row.appendChild(_header_td);
        row.appendChild(_text_td);
        row.appendChild(_current_date_td)


    }, 4000);
}
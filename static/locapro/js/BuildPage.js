// create a standard page

function BuildPage(container) {


//create processing area


    container.processing_area = document.createElement('div');
    container.processing_area.className = 'processing-area';

    container.processing_first_line = document.createElement('div');
    container.processing_first_line.id = 'processing-first-line_' + container.uuid;

    container.processing_second_line = document.createElement('div');
    container.processing_second_line.id = 'processing-second-line_' + container.uuid;

    container.processing_area.appendChild(container.processing_first_line);
    container.processing_area.appendChild(container.processing_second_line);

    container.appendChild(container.processing_area);


// create main table container
    container.main_table = document.createElement('DIV');
    container.main_table.id = 'main_table__' + container.uuid;
    container.main_table.fixed_filter = container.fixed_filter;
    container.main_table.parent_id = container.parent_id;
    container.main_table._heigh = container.offsetHeight - 60 + 'px';

// create main table buttons elements
    container.main_table_buttons = document.createElement('div');
    container.main_table_buttons.id = 'main_table_buttons__' + container.uuid;
    container.main_table_buttons.uuid = container.uuid;
    container.main_table_buttons._table = container.main_table;

// create main table add line dialog element
    container.main_table_add_line_dialog = document.createElement('div');
    container.main_table_add_line_dialog.id = 'main_table_add_line_dialog__' + container.uuid;
    container.main_table_add_line_dialog.className = 'w3-modal';


    container.main_table_add_line_dialog_content = document.createElement('div');
    container.main_table_add_line_dialog_content.id = 'main_table_add_line_dialog_content__' + container.uuid;
    container.main_table_add_line_dialog_content.className = 'w3-modal-content';


    container.main_table_add_line_dialog_top_panel = document.createElement('div');
    container.main_table_add_line_dialog_top_panel.id = 'main_table_add_line_dialog_top_panel__' + container.uuid;
    container.main_table_add_line_dialog_top_panel.className = 'w3-bar top-Panel';


    container.main_table_add_line_dialog_title = document.createElement('div');
    container.main_table_add_line_dialog_title.id = 'main_table_add_line_dialog_title__' + container.uuid;
    container.main_table_add_line_dialog_title.className = 'w3-bar-item';
    container.main_table_add_line_dialog_title.innerText = 'Ajouter un enregistrement';

// create main table add line form element
    container.main_table_add_line_from = document.createElement('div');
    container.main_table_add_line_from.id = 'main_table_add_line_form__' + container.uuid;
    container.main_table_add_line_from.className = 'w3-panel';


// Build add dialog elements
    container.main_table_add_line_dialog_top_panel.appendChild(container.main_table_add_line_dialog_title);
    container.main_table_add_line_dialog_content.appendChild(container.main_table_add_line_dialog_top_panel);
    container.main_table_add_line_dialog_content.appendChild(container.main_table_add_line_from);
    container.main_table_add_line_dialog.appendChild(container.main_table_add_line_dialog_content);

// create main table edit line dialog element
    container.main_table_edit_line_dialog = document.createElement('div');
    container.main_table_edit_line_dialog.id = 'main_table_edit_line_dialog__' + container.uuid;
    container.main_table_edit_line_dialog.className = 'w3-modal';


    container.main_table_edit_line_dialog_content = document.createElement('div');
    container.main_table_edit_line_dialog_content.id = 'main_table_edit_line_dialog_content__' + container.uuid;
    container.main_table_edit_line_dialog_content.className = 'w3-modal-content';
    container.main_table_edit_line_dialog_content.style.width = '450px';


    container.main_table_edit_line_dialog_top_panel = document.createElement('div');
    container.main_table_edit_line_dialog_top_panel.id = 'main_table_edit_line_dialog_top_panel__' + container.uuid;
    container.main_table_edit_line_dialog_top_panel.className = 'w3-bar top-Panel';


    container.main_table_edit_line_dialog_title = document.createElement('div');
    container.main_table_edit_line_dialog_title.id = 'main_table_edit_line_dialog_title__' + container.uuid;
    container.main_table_edit_line_dialog_title.className = 'w3-bar-item';
    container.main_table_edit_line_dialog_title.innerText = 'Modifier un enregistrement';

// create main table edit line form element
    container.main_table_edit_line_from = document.createElement('div');
    container.main_table_edit_line_from.id = 'main_table_edit_line_form__' + container.uuid;
    container.main_table_edit_line_from.className = 'w3-panel';


// Build edit dialog elements
    container.main_table_edit_line_dialog_top_panel.appendChild(container.main_table_edit_line_dialog_title);
    container.main_table_edit_line_dialog_content.appendChild(container.main_table_edit_line_dialog_top_panel);
    container.main_table_edit_line_dialog_content.appendChild(container.main_table_edit_line_from);
    container.main_table_edit_line_dialog.appendChild(container.main_table_edit_line_dialog_content);


// create confirmation dialog element
    container.confirmation_dialog = document.createElement('div');
    container.confirmation_dialog.id = 'confirmation_dialog__' + container.uuid;
    container.confirmation_dialog.className = 'w3-modal';


// Create main table
    container.main_table.url = container.url;
    container.main_table.uuid = container.uuid;


// create attachment dialog
    container.attachment_dialog = document.createElement('div');
    container.attachment_dialog.id = 'attachment_dialog__' + container.uuid;
    container.attachment_dialog.className = 'w3-modal';


    container.attachment_dialog_content = document.createElement('div');
    container.attachment_dialog_content.id = 'attachment_dialog_content__' + container.uuid;
    container.attachment_dialog_content.className = 'w3-modal-content';
    container.attachment_dialog_content.style.width = '450px';


    container.attachment_dialog_top_panel = document.createElement('div');
    container.attachment_dialog_top_panel.id = 'attachment_dialog_top_panel__' + container.uuid;
    container.attachment_dialog_top_panel.className = 'w3-bar top-Panel';


    container.attachment_dialog_title = document.createElement('div');
    container.attachment_dialog_title.id = 'main_table_add_line_dialog_title__' + container.uuid;
    container.attachment_dialog_title.className = 'w3-bar-item';
    container.attachment_dialog_title.textContent = 'Pièce jointes';

// create attachment page
    container.attachment_page = document.createElement('div');
    container.attachment_page.uuid = '_a__' + container.uuid;
    container.attachment_page.id = 'page_' + container.attachment_page.uuid;
    container.attachment_page.className = 'w3-panel';
    container.attachment_page.url = '/common/archived_document_admin/';
    container.attachment_page.csrf_token = container.csrf_token;


    container.attachment_footer_buttons = document.createElement('DIV');
    container.attachment_footer_buttons.className = 'w3-container form-buttons';

    container.attachment_close_button = document.createElement('BUTTON');
    container.attachment_close_button.title = 'Fermer le dialog';
    container.attachment_close_button.className = 'w3-right w3-button';
    container.attachment_close_button.id = 'close_button_' + container.uuid;
    container.attachment_close_button.onclick = function (ev) {
        closeDialog(container.attachment_dialog)
    };
    container.attachment_close_button.textContent = 'Fermer';
    container.attachment_footer_buttons.appendChild(container.attachment_close_button);


// Build add dialog elements
    container.attachment_dialog_top_panel.appendChild(container.attachment_dialog_title);
    container.attachment_dialog_content.appendChild(container.attachment_dialog_top_panel);
    container.attachment_dialog_content.appendChild(container.attachment_page);
    container.attachment_dialog_content.appendChild(container.attachment_footer_buttons);
    container.attachment_dialog.appendChild(container.attachment_dialog_content);

    // Get page parameters
    startLoadingPage();
    get_page_view_parameters(
        action = 'get_view_parameters',
        container.uuid,
        url = container.url,
        container.main_table,
        csrf_token = container.csrf_token,
        data_dictionary = {},
        callback = BuildPageElements
    );

    function set_main_table_buttons(buttons_params) {

        container.main_table_buttons.settings = [
            {
                'id': 'refresh_button__' + container.uuid,
                'text': '',
                'icon_class': 'fas fa-sync-alt',
                'tooltip': 'Actualiser les données',
                'events': [
                    {
                        'click': function () {
                            container.main_table_buttons._table.refresh()
                        }
                    }]
            }];
        for (let i = 0, l = buttons_params.length; i < l; i++) {
            if (buttons_params[i] === 'add') {
                container.main_table_buttons.settings.push(
                    {
                        'id': 'add_line_button__' + container.uuid,
                        'text': '',
                        'icon_class': 'fas fa-plus',
                        'tooltip': 'Nouvel enregistrement',
                        'events': [
                            {
                                'click': function () {
                                    if (container.main_table.parent_id === '0') {
                                        let notification = {};
                                        notification.header = 'Erreur';
                                        notification.class = 'w3-orange';
                                        notification.text = 'Vous devez séléctionner un Bon parent pour ajouter des produits';
                                        pop_notification(notification);
                                    } else {

                                        openDialog(container.main_table_add_line_dialog);
                                        //let d = document.getElementById('main_table_add_line_dialog_content__' + container.uuid);
                                        //dragElement(d)
                                    }

                                }
                            }]
                    }
                )
            } else if (buttons_params[i] === 'change') {
                container.main_table_buttons.settings.push(
                    {
                        'id': 'edit_line_button__' + container.uuid,
                        'text': '',
                        'icon_class': 'fas fa-pen',
                        'tooltip': 'Modifier l\'enregistrement séléctionné',
                        'events': [
                            {
                                'click': function () {
                                    function get_line_data_callback(data) {
                                        openDialog(container.main_table_edit_line_dialog);
                                        let _inputs = container.main_table_edit_line_from.getElementsByTagName('input');

                                        for (let i = 0, l = _inputs.length; i < l; i++) {
                                            if (_inputs[i].getAttribute('type') === 'autocomplete') {
                                                _inputs[i].setSelected(data[_inputs[i].name]);
                                            } else if (_inputs[i].getAttribute('type') === 'date' ||
                                                _inputs[i].getAttribute('type') === 'datetime-local') {
                                                _inputs[i].value = data[_inputs[i].name]
                                            } else if (_inputs[i].getAttribute('type') === 'checkbox') {
                                                _inputs[i].checked = data[_inputs[i].name]
                                            } else {
                                                _inputs[i].value = data[_inputs[i].name];
                                            }

                                        }


                                    }

                                    if (!container.main_table.selectedPk) {
                                        let notification = {};
                                        notification.header = 'Erreur!';
                                        notification.class = 'w3-orange';
                                        notification.text = 'Vous devez choisir une ligne pour la modifier !';
                                        pop_notification(notification)
                                    } else {
                                        get_line(container.uuid,
                                            container.main_table.selectedPk,
                                            container.main_table.url,
                                            container.main_table,
                                            container.csrf_token,
                                            get_line_data_callback
                                        )
                                    }
                                }
                            }]
                    }
                )
            } else if (buttons_params[i] === 'delete') {
                container.main_table_buttons.settings.push(
                    {
                        'id': 'delete_line_button__' + container.uuid,
                        'text': '',
                        'icon_class': 'fas fa-trash',
                        'tooltip': 'Supprimer l\'enregistrement séléctionné',
                        'events': [
                            {
                                'click':
                                    function () {
                                        function posting_callback() {
                                            container.main_table.refresh();
                                            closeDialog(container.confirmation_dialog)
                                        }

                                        function callback() {
                                            delete_line(
                                                container.uuid,
                                                container.main_table.selectedPk,
                                                container.url,
                                                container.main_table,
                                                container.csrf_token,
                                                posting_callback)
                                        }

                                        if (!container.main_table.selectedPk) {
                                            let notification = {};
                                            notification.header = 'Erreur!';
                                            notification.class = 'w3-orange';
                                            notification.text = 'Vous devez choisir une ligne pour la supprimer !';
                                            pop_notification(notification)
                                        } else {

                                            confirmationDialog(
                                                container.confirmation_dialog,
                                                'Voulez-vous supprimer l\'enregistrement sélctionné?',
                                                callback
                                            )
                                        }
                                    }
                            }]
                    }
                )
            } else if (buttons_params[i] === 'send') {
                container.main_table_buttons.settings.push(
                    {
                        'id': 'send_button__' + container.uuid,
                        'text': '',
                        'icon_class': 'fas fa-arrow-right',
                        'tooltip': 'Lancer le bon sélctionné',
                        'events': [
                            {
                                'click': function () {
                                    function callback() {
                                        let print_button = document.getElementById('print_button_' + container.uuid);
                                        print_button.click();
                                        container.main_table.refresh();
                                    }

                                    post_custom_data(
                                        'send_transaction',
                                        container.uuid,
                                        container.url,
                                        container.main_table,
                                        container.csrf_token,
                                        {
                                            'document_id': container.main_table.selectedPk,

                                        },
                                        callback
                                    )

                                }
                            }]
                    }
                )

            } else if (buttons_params[i] === 'return') {
                container.main_table_buttons.settings.push(
                    {
                        'id': 'return_button__' + container.uuid,
                        'text': '',
                        'icon_class': 'fas fa-ban',
                        'tooltip': 'Annuler le bon sélctionné',
                        'events': [
                            {
                                'click': function () {
                                    function return_transaction() {
                                        function callback() {
                                            container.main_table.refresh();
                                            closeDialog(container.confirmation_dialog)
                                        }

                                        post_custom_data(
                                            'return_transaction',
                                            container.uuid,
                                            container.url,
                                            container.main_table,
                                            container.csrf_token,
                                            {
                                                'document_id': container.main_table.selectedPk
                                            },
                                            callback
                                        )
                                    }

                                    confirmationDialog(
                                        container.confirmation_dialog,
                                        'Voulez-vous annuler l\'enregistrement sélctionné?',
                                        return_transaction
                                    )


                                }
                            }]
                    }
                )

            } else if (buttons_params[i] === 'confirm') {
                container.main_table_buttons.settings.push(
                    {
                        'id': 'confirm_button__' + container.uuid,
                        'text': '',
                        'icon_class': 'fas fa-bolt',
                        'tooltip': 'Confirmer le document sélctionné',
                        'events': [
                            {
                                'click': function () {
                                    BuildConfirmationDialog(container, 'confirm_transaction')
                                }
                            }]
                    }
                )
            } else if (buttons_params[i] === 'ship') {
                container.main_table_buttons.settings.push(
                    {
                        'id': 'ship_button__' + container.uuid,
                        'text': '',
                        'icon_class': 'fas fa-truck-loading',
                        'tooltip': 'Exepédier le document sélctionné',
                        'events': [
                            {
                                'click': function () {
                                    BuildConfirmationDialog(container, 'ship_transaction')
                                }
                            }]
                    }
                )
            } else if (buttons_params[i] === 'filter') {
                container.main_table_buttons.settings.push(
                    {
                        'id': 'filter_button__' + container.uuid,
                        'text': '',
                        'icon_class': 'fas fa-filter',
                        'tooltip': 'Afficher / masquer la zone de filtre',
                        'events': null
                    }
                )
            } else if (buttons_params[i] === 'export') {
                container.main_table_buttons.settings.push(
                    {
                        'id': 'export_button__' + container.uuid,
                        'text': '',
                        'icon_class': 'fas fa-cloud-download-alt',
                        'tooltip': 'Exporter les données de la table',
                        'events': [
                            {
                                'click': function () {
                                    let parent_id = container.main_table.parent_data.id || '';
                                    window.open(container.main_table.url + '?action=export_data&parent_id=' + parent_id, "_self");
                                }
                            }]
                    }
                )
            } else if (buttons_params[i] === 'attach') {
                container.main_table_buttons.settings.push(
                    {
                        'id': 'attach_button__' + container.uuid,
                        'text': '',
                        'icon_class': 'fas fa-paperclip',
                        'tooltip': 'Pièces jointes',
                        'events': [
                            {
                                'click': function () {
                                    while (container.attachment_page.firstChild) {
                                        container.attachment_page.removeChild(container.attachment_page.firstChild)
                                    }
                                    if (!container.main_table.selectedPk) {
                                        const notification = {};
                                        notification.header = 'Erreur!';
                                        notification.class = 'w3-orange';
                                        notification.text = 'Vous devez choisir une ligne !';
                                        pop_notification(notification)
                                    } else {
                                        document.addEventListener('loaded_table_' + container.attachment_page.uuid, function () {
                                            openDialog(container.attachment_dialog);
                                            let object_id = document.getElementById('object_id__' + container.attachment_page.uuid);
                                            let model_name = document.getElementById('content_type__model__' + container.attachment_page.uuid);
                                            let close_file_dialog_button = document.getElementById('close_button__' + container.attachment_page.uuid);
                                            object_id.value = container.main_table.selectedPk;
                                            model_name.value = container.main_table.getAttribute('model_name');
                                            object_id.classList.add('w3-hide');
                                            model_name.classList.add('w3-hide');
                                            document.addEventListener('data_posted_' + container.attachment_page.uuid, function () {
                                                close_file_dialog_button.click();
                                                let _selected_row = container.main_table.selectedPk;
                                                container.main_table.refresh();
                                                container.main_table.setSelected(_selected_row)
                                            })


                                        });
                                        container.attachment_page.fixed_filter = JSON.stringify(
                                            {
                                                object_id: container.main_table.selectedPk,
                                                content_type__model: container.main_table.getElementsByTagName('TABLE')[0].id.split('_')[0]
                                            });
                                        BuildPage(container.attachment_page);

                                    }
                                }

                            }

                        ]
                    }
                )
            }
        }

        container.main_table_buttons.uuid = container.uuid;
        if (container.extra_actions) {
            for (let i = 0, l = container.extra_actions.length; i < l; i++) {
                container.main_table_buttons.settings.push(container.extra_actions[i])
            }
        }
        BuildActionButtons(container.main_table_buttons);
    }

// Build add line form

    function build_main_table_add_line_form(fields, form_layout) {


        function last_enabled_input_callback() {
            document.getElementById('add_button__' + container.uuid).click()
        }

        // Build form settings from received parameters
        let _inputs = [];
        for (let field in fields) {
            if (fields[field].input) {
                _inputs.push(
                    {
                        'id': field + '__' + container.uuid,
                        'name': field,
                        'text': fields[field].title,
                        'type': fields[field].input_type,
                        'url': fields[field].input_autocomplete_url,
                        'tooltip': fields[field].tooltip,
                        'required': fields[field].required,
                        'disabled': false,
                        'attributes': fields[field].input_attributes,
                        'events': [
                            {
                                'keyup': function (e) {
                                    container.main_table_add_line_from.focusNextInput(e, last_enabled_input_callback)
                                }
                            }]
                    }
                )
            }
        }
        let _buttons = [
            {
                'id': 'close_button__' + container.uuid,
                'text': 'Fermer',
                'icon_class': '',
                'tooltip': 'Fermer le dialogue',
                'events': [
                    {
                        'click': function () {
                            closeDialog(container.main_table_add_line_dialog)
                        }
                    }]
            },
            {
                'id': 'add_button__' + container.uuid,
                'text': 'Ajouter',
                'icon_class': '',
                'tooltip': 'Enregistrer les informations saisies',
                'events': [
                    {
                        'click': function () {
                            function callback() {
                                container.main_table_add_line_from.clear();
                                container.main_table.refresh()
                            }

                            container.main_table_add_line_from.post_data(callback);
                        }
                    }]
            }
        ];
        container.main_table_add_line_from.settings = {
            'url': container.url,
            'action': 'add_line',
            'csrf': container.csrf_token,
            'form_layout': form_layout,
            'inputs': _inputs,
            'buttons': _buttons
        };
        container.main_table_add_line_from.uuid = container.uuid;
        BuildForm(container.main_table_add_line_from)

    }

    function build_main_table_edit_line_form(fields, form_layout) {


        function last_enabled_input_callback() {
            document.getElementById('edit_button__' + container.uuid).click()
        }

        // Build form settings from received parameters
        let _inputs = [];
        for (let field in fields) {
            if (fields[field].input) {
                _inputs.push(
                    {
                        'id': field + '__edit__' + container.uuid,
                        'name': field,
                        'text': fields[field].title,
                        'type': fields[field].input_type,
                        'url': fields[field].input_autocomplete_url,
                        'tooltip': fields[field].tooltip,
                        'required': fields[field].required,
                        'disabled': !fields[field].edit,
                        'attributes': fields[field].input_attributes,
                        'events': [
                            {
                                'keyup': function (e) {
                                    container.main_table_edit_line_from.focusNextInput(e, last_enabled_input_callback)
                                }
                            }]
                    }
                )
            }

        }
        let _buttons = [
            {
                'id': 'close_button__edit__' + container.uuid,
                'text': 'Fermer',
                'icon_class': '',
                'tooltip': 'Fermer le dialogue',
                'events': [
                    {
                        'click': function () {
                            closeDialog(container.main_table_edit_line_dialog)
                        }
                    }]
            },
            {
                'id': 'edit_button__' + container.uuid,
                'text': 'Modifier',
                'icon_class': '',
                'tooltip': 'Enregistrer les nouvelle informations saisies',
                'events': [
                    {
                        'click': function () {
                            function callback() {
                                closeDialog(container.main_table_edit_line_dialog);
                                container.main_table_edit_line_from.clear();
                                container.main_table.refresh()
                            }

                            container.main_table_edit_line_from.line_id = container.main_table.selectedPk;
                            container.main_table_edit_line_from.post_data(callback);
                        }
                    }]
            }
        ];
        let _settings = {
            'url': container.url,
            'action': 'edit_line',
            'csrf': container.csrf_token,
            'form_layout': form_layout,
            'inputs': _inputs,
            'buttons': _buttons
        };

        container.main_table_edit_line_from.uuid = container.uuid;
        container.main_table_edit_line_from.settings = _settings;

        BuildForm(container.main_table_edit_line_from)

    }


// Build the page
    function BuildPageElements(data) {
        stopLoadingPage();
        data = JSON.parse(data);
        window.max_file_size_kb = data.status_data.max_file_size_kb;
        set_main_table_buttons(data.status_data.action_buttons);
        container.main_table_add_line_dialog_content.style.width = data.status_data.form_width;
        container.main_table_edit_line_dialog_content.style.width = data.status_data.form_width;
        build_main_table_add_line_form(data.status_data.fields, data.status_data.input_form_layout);
        build_main_table_edit_line_form(data.status_data.fields, data.status_data.input_form_layout);
        container.main_table_buttons.date_range_filter = data.status_data.date_range_filter;
        if (data.status_data.table_filter) {
            BuildFilterOnTopBar(container.main_table_buttons)
        }
        DataTable(container.main_table);
        let page_built = new CustomEvent('page_built_' + container.uuid);
        document.dispatchEvent(page_built);
    }


    container.appendChild(container.main_table_buttons);
    container.appendChild(container.main_table);
    container.appendChild(container.main_table_add_line_dialog);
    container.appendChild(container.main_table_edit_line_dialog);
    container.appendChild(container.confirmation_dialog);
    container.appendChild(container.attachment_dialog);
    // Page custom methods

    container.resizeHigh = function (_value) {
        container.style.height = _value;
        container.main_table.style.height = container.offsetHeight - 60 + 'px';
    }
}
"""locapro61 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from common import views as common_views

urlpatterns = ([
    url(r'^$', common_views.home),
    url(r'^admin/', admin.site.urls),
    url(r'^login/', auth_views.login),
    url(r'^common/', include('common.urls')),
    url(r'^immobilisations/', include('immobilisation.urls')),
    url(r'^wms/', include('wms.urls')),
    url(r'^interfacing/', include('interfacing.urls'))
]) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

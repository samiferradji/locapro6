def get_default_product_status_id(user):
    """
    This function returns the default product's status when creating a new product reception
    """
    from wms.models import ProductStatus
    from common.models import UserProfile
    current_company = UserProfile.objects.get(user=user).company
    if current_company.group_mode:
        return ProductStatus.objects.filter(company=current_company.master_company).order_by('created_date').first().id
    else:
        return ProductStatus.objects.filter(company=current_company).order_by('created_date').first().id


def get_default_document_status_id():
    """
    This function returns the default document's status when creating a new document
    """
    from wms.models import DocumentStatus
    return DocumentStatus.objects.order_by('created_date').first().id


def get_confirmed_document_status_id():
    """
    This function returns the confirmed document's status
    """
    from wms.models import DocumentStatus
    return DocumentStatus.objects.order_by('created_date')[1].id


def get_shipped_document_status_id():
    """
    This function returns the shipped document's status
    """
    from wms.models import DocumentStatus
    return DocumentStatus.objects.order_by('created_date')[2].id


def get_received_document_status_id():
    """
    This function returns the received document's status
    """
    from wms.models import DocumentStatus
    return DocumentStatus.objects.order_by('created_date')[4].id


def get_default_reception_emplacement_id_for_pharmaceutical_purchase(user):
    """
    This function returns the default emplacement when creating a new pharmaceutical reception
    """
    from common.models import CompanyProfile
    from common.models import UserProfile
    current_company = UserProfile.objects.get(user=user).company
    return CompanyProfile.objects.get(company=current_company).pharmaceutical_purchase_reception_emplacement_id


def get_default_reception_emplacement_id_for_external_transfers(user):
    """
    This function returns the default emplacement when creating a new reception between grouped companies
    """
    from common.models import CompanyProfile
    from common.models import UserProfile
    current_company = UserProfile.objects.get(user=user).company
    return CompanyProfile.objects.get(company=current_company).pharmaceutical_purchase_reception_emplacement_id


def get_default_product_picking_emplacement_id(user, product_instance):
    """
    This function returns the default emplacement for product picking
    """
    from wms.models import Emplacement
    from common.models import UserProfile
    from common.models import CompanyProfile
    current_company = UserProfile.objects.get(user=user).company
    picking_store = CompanyProfile.objects.get(company=current_company).picking_store
    if product_instance.picking_emplacement:
        return product_instance.picking_emplacement_id
    else:
        return Emplacement.objects.filter(store=picking_store).order_by('created_date').first().id


DEFAULT_PURCHASE_PAYMENT_TERM_IN_DAYS = 90

PURCHASE_LOGISTIC_FLEW_ID = 7
SHIP_LOGISTIC_FLEW_ID = 8

EXTERNAL_TRANSFER_LOGISTIC_FLEW_ID = 6
INVENTORY_COUNTING_LOGISTIC_FLEW_ID = 10

NEGATIVE_STOCK_PER_EMPLACEMENT_ALLOWED = False
NEGATIVE_STOCK_PER_BATCH_ALLOWED = False
NEGATIVE_STOCK_PER_PRODUCT_ALLOWED = False

ALLOW_COMING_QUANTITY_TRANSFER_TRANSACTION = False
UNPACKED_UNITS_STORAGE_ID = '1'
COMPLETE_PACKAGES_STORAGE_ID = '2'
PALLETIZED_PACKAGES_STORAGE_ID = '3'

# Document status
DOCUMENT_CREATED_ID = 1
DOCUMENT_CONFIRMED_ID = 2
DOCUMENT_SHIPPED_ID = 3
DOCUMENT_DELIVERED_ID = 4
DOCUMENT_RECEIVED_ID = 5
DOCUMENT_SENT_ID = 6

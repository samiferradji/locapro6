from django.core.exceptions import ValidationError

from wms import parameters as wms_parameters


def quantity_is_not_zero(quantity):
    """Return ValidationError if the quantity equal to Zero

    Parameters
    ----------
    quantity:int

    Returns
    -------
    True or raise ValidationError

    """
    if quantity == 0:
        raise ValidationError('La quantité saisie ne pas être égale à zéro')
    else:
        return True


def transaction_can_be_confirmed(transaction_instance, user):
    """
    Check if a transaction can be confirmed
    Returns
    -------
    True if ok

    """
    if transaction_instance.document_status_id == wms_parameters.DOCUMENT_CREATED_ID:
        return True
    else:
        raise ValidationError({'N° Document': 'Ce document est déja confirmé'})


def transaction_can_be_shipped(transaction_instance):
    """
    Check if a transaction is confirmed
    Returns
    -------
    True if ok

    """

    if transaction_instance.document_status_id == wms_parameters.DOCUMENT_CONFIRMED_ID:
        return True
    else:
        if transaction_instance.document_status_id == wms_parameters.DOCUMENT_CREATED_ID:
            raise ValidationError({'N° Document': 'Ce document n\'est pas encore lancé'})
        else:
            raise ValidationError({'N° Document': 'Ce document est déja expédié'})


def transaction_can_be_received(transaction_instance):
    """
    Check if a transaction is received
    Returns
    -------
    True if ok

    """
    if transaction_instance.document_status_id == wms_parameters.get_received_document_status_id():
        raise ValidationError({'N° Document': 'Ce document est déja reçu'})
    else:
        return True


#  to do optimise this validators

def internal_movement_can_be_confirmed(transaction_instance):  # and external too
    """
    Check if a transaction can be confirmed
    Returns
    -------
    True if ok

    """

    if transaction_instance.document_status_id == wms_parameters.DOCUMENT_SENT_ID:
        return True

    else:
        if transaction_instance.document_status_id == wms_parameters.DOCUMENT_CREATED_ID:
            raise ValidationError({'N° Document': 'Ce document n\'est pas encore lancé'})
        else:
            raise ValidationError({'N° Document': 'Ce document est déja confirmé'})


def internal_movement_can_be_send(transaction_instance):
    """
    Check if a transaction can be send
    Returns
    -------
    True if ok

    """
    if transaction_instance.document_status_id == wms_parameters.DOCUMENT_CREATED_ID:
        return True
    else:
        raise ValidationError({'N° Document': 'Ce document ne peut pas être lancé. Il est déja lancé ou confirmé'})

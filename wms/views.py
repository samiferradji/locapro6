import csv
import json
import traceback
from datetime import timedelta
from decimal import Decimal

from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError, ObjectDoesNotExist, PermissionDenied
from django.core.serializers.json import DjangoJSONEncoder
from django.db import transaction
from django.db.models import F, When, Q, Case, IntegerField, Max, Sum
from django.http import HttpResponse
from django.utils import timezone

from common import helpers as common_helpers
from common.models import Error, Employee
from common.services import StandardAdminApi
from wms import helpers as wms_helpers
from wms import parameters as wms_parameters
from wms import validators as wms_validators
from .models import PharmaceuticalInvoice, Purchase, Transfer, TransferTempo, \
    TransferDetails, WarehouseReservationDetail, Emplacement, Inventory, ExecutorTempo, ExternalTransfer, \
    Dci, PharmaceuticalForm, ProductStatus, Store, Area, Laboratory, \
    LogisticFlowType, PharmaceuticalSupplier, Product, Batch, InventoryCount, InventoryCountDetail, \
    ExternalTransferDetails, PurchaseDetail


#  Reference Tables


class DciAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Dci
    _model_data = {
        'enumeration': True,
        'sort': ['designation'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation commune internationale',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('code').blank,
                'tooltip': 'Code unique de la dci',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _html_page = 'wms/admin_templates/dci_admin_page.html'
    _url = 'dci_admin/'
    _parameters = ''
    _short_title = 'DCIs'
    _title = 'DCIs'
    _description = 'La gestion de la table des dénominations communes internationale des médicaments'


class PharmaceuticalFormAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = PharmaceuticalForm
    _model_data = {
        'enumeration': True,
        'sort': ['designation'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation de la forme galénique',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'short_designation':
            {
                'title': 'Abréviation',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('short_designation').blank,
                'tooltip': 'Abréviation de la désignation de la forme',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _html_page = 'wms/admin_templates/pharmaceutical_form_admin_page.html'
    _url = 'pharmaceutical_form_admin/'
    _parameters = ''
    _short_title = 'Fomes Galéniques'
    _title = 'Fomes Galéniques'
    _description = 'La gestion de la table des fomes Galéniques'


class ProductStatusAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = ProductStatus
    _model_data = {
        'enumeration': True,
        'sort': ['designation'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation du statut. Exemple : Conforme, périmé...',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _html_page = 'wms/admin_templates/product_status_admin_page.html'
    _url = 'product_status_admin/'
    _parameters = ''
    _short_title = 'Statuts Produits'
    _title = 'Statuts Produits'
    _description = 'La gestion de la table des Statuts-Produits'


class StoreAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Store
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation du magasin',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'storage_type_id':
            {
                'title': 'Types d\'entrepôsage',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/storage_type_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('storage_type').blank,
                'tooltip': 'Exemple : Palette, colis, vrac ...',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'storage_type__designation':
            {
                'title': 'Types d\'entrepôsage',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('storage_type').blank,
                'tooltip': 'Exemple : Palette, colis, vrac ...',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _html_page = 'wms/admin_templates/store_admin_page.html'
    _url = 'store_admin/'
    _parameters = ''
    _short_title = 'Magasins'
    _title = 'Magasins'
    _description = 'La gestion de la table des magasins'


class AreaAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Area
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation de la zone de stockage',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _html_page = 'wms/admin_templates/area_admin_page.html'
    _url = 'area_admin/'
    _parameters = ''
    _short_title = 'Zones'
    _title = 'Zone de stockage'
    _description = 'La gestion de la table des zones de stockage'


class LaboratoryAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Laboratory
    _model_data = {
        'enumeration': True,
        'sort': ['designation'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Nom du laboratoire',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('code').blank,
                'tooltip': 'Code unique du laboratoire',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _html_page = 'wms/admin_templates/laboratory_admin_page.html'
    _url = 'laboratory_admin/'
    _parameters = ''
    _short_title = 'Laboratoires'
    _title = 'Laboratoires Pharmaceutiques'
    _description = 'La gestion de la table des laboratoires pharmaceutiques'


class LogisticFlowTypeAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = LogisticFlowType
    _model_data = {
        'enumeration': False,
        'sort': ['id'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['change', 'export']
    _fields = {
        'id':
            {
                'title': 'ID',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('id').blank,
                'tooltip': 'identifiant du flux',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation du flux',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'description':
            {
                'title': 'Description',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('description').blank,
                'tooltip': 'Description du flux',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'point_line_execution': {
            'title': 'Pts Line Exec',
            'format': 'integer',
            'width': False,
            'column_order': 2,
            'display': True,
            'input': True,
            'input_type': 'text',
            'input_autocomplete_url': False,
            'input_validation_url': False, 'input_attributes': {},
            'input_group': 3,
            'input_order': 1,
            'edit': True,
            'required': not _model._meta.get_field('point_line_execution').blank,
            'tooltip': 'Description du flux',
            'search': True,
            'search_type': '__icontains',
            'conditional_format': None
        },
        'point_line_typing': {
            'title': 'Pts Line Typ',
            'format': 'integer',
            'width': False,
            'column_order': 2,
            'display': True,
            'input': True,
            'input_type': 'text',
            'input_autocomplete_url': False,
            'input_validation_url': False, 'input_attributes': {},
            'input_group': 3,
            'input_order': 1,
            'edit': True,
            'required': not _model._meta.get_field('point_line_typing').blank,
            'tooltip': 'Description du flux',
            'search': True,
            'search_type': '__icontains',
            'conditional_format': None
        },
        'point_line_control': {
            'title': 'Pts Line Ctrl',
            'format': 'integer',
            'width': False,
            'column_order': 2,
            'display': True,
            'input': True,
            'input_type': 'text',
            'input_autocomplete_url': False,
            'input_validation_url': False, 'input_attributes': {},
            'input_group': 3,
            'input_order': 1,
            'edit': True,
            'required': not _model._meta.get_field('point_line_control').blank,
            'tooltip': 'Description du flux',
            'search': True,
            'search_type': '__icontains',
            'conditional_format': None
        },
        'point_sku_execution': {
            'title': 'Pts Boite Exec',
            'format': 'integer',
            'width': False,
            'column_order': 2,
            'display': True,
            'input': True,
            'input_type': 'text',
            'input_autocomplete_url': False,
            'input_validation_url': False, 'input_attributes': {},
            'input_group': 4,
            'input_order': 1,
            'edit': True,
            'required': not _model._meta.get_field('point_sku_execution').blank,
            'tooltip': 'Description du flux',
            'search': True,
            'search_type': '__icontains',
            'conditional_format': None
        },
        'point_sku_control': {
            'title': 'Pts Boite Ctrl',
            'format': 'integer',
            'width': False,
            'column_order': 2,
            'display': True,
            'input': True,
            'input_type': 'text',
            'input_autocomplete_url': False,
            'input_validation_url': False, 'input_attributes': {},
            'input_group': 4,
            'input_order': 1,
            'edit': True,
            'required': not _model._meta.get_field('point_sku_control').blank,
            'tooltip': 'Description du flux',
            'search': True,
            'search_type': '__icontains',
            'conditional_format': None
        },
        'point_package_execution': {
            'title': 'Pts Colis Exec',
            'format': 'integer',
            'width': False,
            'column_order': 2,
            'display': True,
            'input': True,
            'input_type': 'text',
            'input_autocomplete_url': False,
            'input_validation_url': False, 'input_attributes': {},
            'input_group': 5,
            'input_order': 1,
            'edit': True,
            'required': not _model._meta.get_field('point_package_execution').blank,
            'tooltip': 'Description du flux',
            'search': True,
            'search_type': '__icontains',
            'conditional_format': None
        },
        'point_package_validation': {
            'title': 'Pts Colis Ctrl',
            'format': 'integer',
            'width': False,
            'column_order': 2,
            'display': True,
            'input': True,
            'input_type': 'text',
            'input_autocomplete_url': False,
            'input_validation_url': False, 'input_attributes': {},
            'input_group': 5,
            'input_order': 1,
            'edit': True,
            'required': not _model._meta.get_field('point_package_validation').blank,
            'tooltip': 'Description du flux',
            'search': True,
            'search_type': '__icontains',
            'conditional_format': None
        },
        'point_palette_execution': {
            'title': 'Pts Palette Exec',
            'format': 'integer',
            'width': False,
            'column_order': 2,
            'display': True,
            'input': True,
            'input_type': 'text',
            'input_autocomplete_url': False,
            'input_validation_url': False, 'input_attributes': {},
            'input_group': 6,
            'input_order': 1,
            'edit': True,
            'required': not _model._meta.get_field('point_palette_execution').blank,
            'tooltip': 'Description du flux',
            'search': True,
            'search_type': '__icontains',
            'conditional_format': None
        },
        'point_palette_control': {
            'title': 'Pts Palette Ctrl',
            'format': 'integer',
            'width': False,
            'column_order': 2,
            'display': True,
            'input': True,
            'input_type': 'text',
            'input_autocomplete_url': False,
            'input_validation_url': False, 'input_attributes': {},
            'input_group': 6,
            'input_order': 1,
            'edit': True,
            'required': not _model._meta.get_field('point_palette_control').blank,
            'tooltip': 'Description du flux',
            'search': True,
            'search_type': '__icontains',
            'conditional_format': None
        }
    }
    _html_page = 'wms/admin_templates/logistic_flew_type_admin_page.html'
    _url = 'logistic_flew_type_admin/'
    _parameters = ''
    _short_title = 'Flux Logistiques'
    _title = 'Types des flux logistiques'
    _description = 'La gestion de la table des types des flux logistiques'


class EmplacementAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Emplacement
    _model_data = {
        'enumeration': True,
        'sort': ['emplacement', 'store'],
        'reverse_sort': False,
        'text_field': ['emplacement', 'store'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'emplacement':
            {
                'title': 'Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Désignation du flux',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'store__designation':
            {
                'title': 'Magasin',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('store').blank,
                'tooltip': '',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'store_id':
            {
                'title': 'Magasin',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/store_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('store_id').blank,
                'tooltip': 'Le magasin dans lequel se trouve l\'emplacement',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'area__designation':
            {
                'title': 'Zone',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('area').blank,
                'tooltip': '',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'area_id':
            {
                'title': 'Zone',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/area_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('area_id').blank,
                'tooltip': 'Le zone dans lequel se trouve l\'emplacement',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'storage_type__designation':
            {
                'title': 'Type de stockage',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('storage_type').blank,
                'tooltip': 'Description du flux',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'storage_type_id':
            {
                'title': 'Type de stockage',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/storage_type_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('storage_type_id').blank,
                'tooltip': 'Le type de stockage principale de cet emplacement',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'volume': {
            'title': 'Max. volume',
            'format': 'float',
            'width': False,
            'column_order': 2,
            'display': True,
            'input': True,
            'input_type': 'text',
            'input_autocomplete_url': False,
            'input_validation_url': False, 'input_attributes': {},
            'input_group': 5,
            'input_order': 1,
            'edit': True,
            'required': not _model._meta.get_field('volume').blank,
            'tooltip': 'Description du flux',
            'search': True,
            'search_type': '__icontains',
            'conditional_format': None
        },
        'weight': {
            'title': 'Max. poids',
            'format': 'float',
            'width': False,
            'column_order': 2,
            'display': True,
            'input': True,
            'input_type': 'text',
            'input_autocomplete_url': False,
            'input_validation_url': False, 'input_attributes': {},
            'input_group': 5,
            'input_order': 1,
            'edit': True,
            'required': not _model._meta.get_field('weight').blank,
            'tooltip': 'Description du flux',
            'search': True,
            'search_type': '__icontains',
            'conditional_format': None
        },

    }
    _html_page = 'wms/admin_templates/emplacement_admin_page.html'
    _url = 'emplacement_admin/'
    _parameters = ''
    _short_title = 'Emplacements'
    _title = 'Emplacements'
    _description = 'La gestion de la table des emplacements'


class PharmaceuticalSupplierAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = PharmaceuticalSupplier
    _model_data = {
        'enumeration': True,
        'sort': ['supplier'],
        'reverse_sort': False,
        'text_field': ['code', 'supplier'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export', 'attach']
    _fields = {
        'code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('code').blank,
                'tooltip': 'Code unique du fournisseur',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'supplier':
            {
                'title': 'Nom du fournisseur',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('supplier').blank,
                'tooltip': 'Le nom du fournisseur',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'RC':
            {
                'title': 'N° du RC',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('RC').blank,
                'tooltip': 'Le numéro du registre de commerce',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'NIF':
            {
                'title': 'NIF',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('NIF').blank,
                'tooltip': 'Le numéro d\'identification fiscale',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'contact':
            {
                'title': 'Contact',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 5,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('contact').blank,
                'tooltip': 'Le nom du personne à contacter chez ce fourniseur',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'telephone':
            {
                'title': 'Telephone',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 6,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('telephone').blank,
                'tooltip': 'Le numéro de téléphone du fournisseur',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'email':
            {
                'title': 'Email',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 7,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('email').blank,
                'tooltip': 'l\'e-mail du fournisseur',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'active':
            {
                'title': 'Actif ?',
                'format': 'boolean',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'checkbox',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 8,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('active').blank,
                'tooltip': 'Uniquement les fournisseur actif peuvent êtres utilisés pour enregistrer des transactions',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },

    }
    _html_page = 'wms/admin_templates/pharmaceutical_supplier_admin_page.html'
    _url = 'pharmaceutical_supplier_admin/'
    _parameters = ''
    _short_title = 'Fournisseurs Pharma'
    _title = 'Fournisseurs Pharmaceutiques'
    _description = 'La gestion de la table des fournisseurs des produits pharmaceutiques'


class ProductAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Product
    _model_data = {
        'enumeration': True,
        'sort': ['product'],
        'reverse_sort': False,
        'text_field': ['product'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _form_width = '500px'
    _default_actions = ['add', 'change', 'delete', 'export', 'attach']
    _fields = {
        'code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('code').blank,
                'tooltip': 'Code unique du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'product':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('product').blank,
                'tooltip': 'Le nom commercial du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'dci__code':
            {
                'title': 'Code DCI',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('dci').blank,
                'tooltip': 'La designation commune internationale du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'dci_id':
            {
                'title': 'DCI',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/dci_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('dci').blank,
                'tooltip': 'La designation commune internationale du produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'category__designation':
            {
                'title': 'Famille',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 2,
                'edit': False,
                'required': not _model._meta.get_field('category').blank,
                'tooltip': 'La famille ou la catégorie du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'category_id':
            {
                'title': 'Famille',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'common/article_category_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('category').blank,
                'tooltip': 'La famille ou la catégorie du produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'pharmaceutical_form__short_designation':
            {
                'title': 'Forme',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('pharmaceutical_form').blank,
                'tooltip': 'La forme galénique du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'pharmaceutical_form_id':
            {
                'title': 'Forme',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/pharmaceutical_form_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 5,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('pharmaceutical_form').blank,
                'tooltip': 'La forme galénique du produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'dosage':
            {
                'title': 'Dosage',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('dosage').blank,
                'tooltip': 'Le dosage du produit. Exemple: 500mg ou 2mg/ml',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'conditioning':
            {
                'title': 'Cond.',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 2,
                'edit': True,
                'required': not _model._meta.get_field('conditioning').blank,
                'tooltip': 'Le coditionnement du produit. Exemple: Boite de 20 ou B/20',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'laboratory__designation':
            {
                'title': 'Laboratoire',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('laboratory').blank,
                'tooltip': 'Le laboratoire producteur du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'laboratory_id':
            {
                'title': 'Laboratoire',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/laboratory_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 6,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('laboratory').blank,
                'tooltip': 'Le laboratoire producteur du produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'weight':
            {
                'title': 'Pds Boite',
                'format': 'float',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'float',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 7,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('weight').blank,
                'tooltip': 'Le poids d\'une boite',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'volume':
            {
                'title': 'Vlm Boite',
                'format': 'float',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'float',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 7,
                'input_order': 2,
                'edit': True,
                'required': not _model._meta.get_field('volume').blank,
                'tooltip': 'Le volume d\'une boite',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'package_weight':
            {
                'title': 'Pds Colis',
                'format': 'float',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'float',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 7,
                'input_order': 3,
                'edit': True,
                'required': not _model._meta.get_field('package_weight').blank,
                'tooltip': 'Le poids d\'un colis',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'picking_stock_min':
            {
                'title': 'Min Picking',
                'format': 'number',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 9,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('picking_stock_min').blank,
                'tooltip': 'Stock minimum dans le magasin de picking',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'picking_stock_max':
            {
                'title': 'Min Picking',
                'format': 'number',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 9,
                'input_order': 2,
                'edit': True,
                'required': not _model._meta.get_field('picking_stock_max').blank,
                'tooltip': 'Stock maximum dans le magasin de picking',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'principal_stock_min':
            {
                'title': 'Min Principal',
                'format': 'number',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 10,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('principal_stock_min').blank,
                'tooltip': 'Stock minimum dans le magasin principal',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'principal_stock_max':
            {
                'title': 'Max Principal',
                'format': 'number',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 10,
                'input_order': 2,
                'edit': True,
                'required': not _model._meta.get_field('principal_stock_max').blank,
                'tooltip': 'Le poids d\'un colis',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'thermal_sensibility':
            {
                'title': 'Thermosensible',
                'format': 'boolean',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'checkbox',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 8,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('thermal_sensibility').blank,
                'tooltip': 'Le produit est sensible à une température supérieure à 8°C',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None},
        'psychotropic':
            {
                'title': 'Psychotrope',
                'format': 'boolean',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'checkbox',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 8,
                'input_order': 2,
                'edit': True,
                'required': not _model._meta.get_field('psychotropic').blank,
                'tooltip': 'Le produit est un psychotrope',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None},
        'tva__tva_rate':
            {
                'title': 'TVA',
                'format': 'percent',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'decimal',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('tva').blank,
                'tooltip': 'La TVA appliquée sur ce produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'tva_id':
            {
                'title': 'TVA',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'common/tva_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 5,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('tva').blank,
                'tooltip': 'La TVA appliquée sur ce produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'active':
            {
                'title': 'Actif ?',
                'format': 'boolean',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'checkbox',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 11,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('active').blank,
                'tooltip': 'Uniquement les fournisseur actif peuvent êtres utilisés pour enregistrer des transactions',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },

    }
    _html_page = 'wms/admin_templates/product_admin_page.html'
    _url = 'product_admin/'
    _parameters = ''
    _short_title = 'Produit'
    _title = 'Produits'
    _description = 'La gestion de la table des produits'


class BatchAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Batch
    _model_data = {
        'enumeration': True,
        'sort': ['created_date'],
        'reverse_sort': True,
        'text_field': ['batch_number'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _form_width = '500px'
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'product_id':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/product_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('product').blank,
                'tooltip': 'Le nom produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'product__code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('product').blank,
                'tooltip': 'Le code du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'product__product':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('product').blank,
                'tooltip': 'Le nom du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch_number':
            {
                'title': 'N° Lot',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch_number').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'expiration_date':
            {
                'title': 'DDP',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'date',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 5,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('expiration_date').blank,
                'tooltip': 'La date de péremption du produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': [
                    {'value': timezone.now().date(), 'operator': '>', 'all_row': False, 'css_class': 'w3-text-red'},
                    {'value': timezone.now().date() + timedelta(days=90), 'operator': '>', 'all_row': False,
                     'css_class': 'w3-text-orange'}
                ]
            },
        'supplier_id':
            {
                'title': 'Fournisseur',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/pharmaceutical_supplier_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 6,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('supplier').blank,
                'tooltip': 'Le fournisseur du Lot',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'supplier__supplier':
            {
                'title': 'Fournisseur',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 7,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('supplier').blank,
                'tooltip': 'Le fournisseur du Lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'packaging':
            {
                'title': 'Colisage',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 7,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('packaging').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'purchase_pu_ht_net':
            {
                'title': 'PU Achat Net',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 8,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('purchase_pu_ht_net').blank,
                'tooltip': 'Le prix d\'achat net',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'tva_id':
            {
                'title': 'TVA',
                'format': 'percent',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': '/common/tva_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 9,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('tva').blank,
                'tooltip': 'Taux de TVA',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'tva__tva_rate':
            {
                'title': 'TVA',
                'format': 'percent',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 10,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('tva').blank,
                'tooltip': 'Taux de TVA',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'selling_pu_ht':
            {
                'title': 'PU Vente HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 11,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('selling_pu_ht').blank,
                'tooltip': 'Le prix de vente du produit en hors taxes',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'shp':
            {
                'title': 'SHP',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 12,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('shp').blank,
                'tooltip': 'SHP du produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'ppa_ht':
            {
                'title': 'PPA HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 13,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('ppa_ht').blank,
                'tooltip': 'Le Prix Public Algérien du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'object_id':
            {
                'title': 'Origin id',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 14,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('object_id').blank,
                'tooltip': '',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'content_type__model':
            {
                'title': 'Origin',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 15,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('content_type').blank,
                'tooltip': '',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },

    }
    _html_page = 'wms/admin_templates/batch_admin_page.html'
    _url = 'batch_admin/'
    _parameters = ''
    _short_title = 'Lots'
    _title = 'Lots'
    _description = 'La gestion de la table des lots'


#  Purchase Transactions


class PharmaceuticalInvoicesAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = PharmaceuticalInvoice
    _model_data = {
        'enumeration': True,
        'sort': ['created_date'],
        'reverse_sort': True,
        'text_field': ['invoice_number', 'supplier__supplier'],
        'table_filter': True,
        'date_range_filter': True,
    }
    _default_actions = ['add', 'change', 'delete', 'export', 'attach']
    _fields = {
        'supplier__supplier':
            {
                'title': 'Nom du fournisseur',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('supplier').blank,
                'tooltip': 'Le nom du fournisseur',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'supplier_id':
            {
                'title': 'Fournisseur',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/pharmaceutical_supplier_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('supplier').blank,
                'tooltip': 'Le nom du fournisseur',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'invoice_number':
            {
                'title': 'N° Facture',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('invoice_number').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'invoice_date':
            {
                'title': 'Date Facture',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'date',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('invoice_date').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'payment_date':
            {
                'title': 'Echéance',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'date',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 2,
                'edit': True,
                'required': not _model._meta.get_field('payment_date').blank,
                'tooltip': 'La de payement de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },

        'value_ht':
            {
                'title': 'Montant HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'money',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('value_ht').blank,
                'tooltip': 'Le montant total de la facture en hors taxes avant remises',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'discount_on_invoice':
            {
                'title': 'Remise',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'money',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 5,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('discount_on_invoice').blank,
                'tooltip': 'Le montant de la remise totale sur la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'value_ht_net':
            {
                'title': 'Montant HT Net',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'money',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 6,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('value_ht_net').blank,
                'tooltip': 'Le montant total de la facture en hors taxes aprrès remises',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'value_tva':
            {
                'title': 'TVA (Valeur)',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'money',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 7,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('value_tva').blank,
                'tooltip': 'Le montant total de la TVA appliquée sur cette facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'value_ttc':
            {
                'title': 'Montant TTC',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'money',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 8,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('value_ttc').blank,
                'tooltip': 'Le montant total de la facture avec toutes taxes comprises',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'document_status__status':
            {
                'title': 'Statut du Document',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'date',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 9,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('document_status').blank,
                'tooltip': 'Les status actuel du document',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'created_date':
            {
                'title': 'Créer le ',
                'format': 'datetime',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'datetime',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 10,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('created_date').blank,
                'tooltip': 'Date de création de\'enregistrement',
                'search': False,
                'search_type': '',
                'conditional_format': None
            },
        'created_by__username':
            {
                'title': 'Créer par ',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 11,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('created_by').blank,
                'tooltip': 'L\'utilisateur qui a crée cet enregistrement',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },

    }
    _html_page = 'wms/transactions_templates/pharmaceutical_invoice_admin_page.html'
    _url = 'pharmaceutical_invoice_admin/'
    _parameters = ''
    _short_title = 'Factures'
    _title = 'Factures des achats Pharmaceutiques'
    _description = 'La gestion de la table des factures des achats pharmaceutiques'


class PurchaseAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Purchase
    _form_width = '500px'
    _model_data = {
        'enumeration': True,
        'sort': ['created_date'],
        'reverse_sort': True,
        'text_field': ['receipt_number', 'invoice__supplier__supplier'],
        'table_filter': True,
        'date_range_filter': True,
    }
    _default_actions = ['add', 'change', 'export', 'attach', 'send', 'confirm', 'return']
    _fields = {
        'id':
            {
                'title': 'ID',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('id').blank,
                'tooltip': 'ID du bon de réception',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'receipt_number':
            {
                'title': 'N° de Réception',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('receipt_number').blank,
                'tooltip': 'Le code unique de la récéption',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'receipt_date':
            {
                'title': 'Date Réception',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'date',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {'default_value': timezone.now().date()},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('receipt_date').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'invoice__supplier__supplier':
            {
                'title': 'Fournisseur',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('invoice').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'invoice__invoice_number':
            {
                'title': 'N° Facture',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('invoice').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'invoice__invoice_date':
            {
                'title': 'Date Facture',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('invoice').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'invoice_id':
            {
                'title': 'Facture',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/pharmaceutical_invoice_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('invoice_id').blank,
                'tooltip': 'La facture d\'achat',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'observation':
            {
                'title': 'Observation',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('observation').blank,
                'tooltip': 'Observation ou commentaire sur la réception',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'document_status__status':
            {
                'title': 'Statut du Document',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'date',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 9,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('document_status').blank,
                'tooltip': 'Les status actuel du document',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'created_date':
            {
                'title': 'Créer le ',
                'format': 'datetime',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'datetime',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 10,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('created_date').blank,
                'tooltip': 'Date de création de\'enregistrement',
                'search': False,
                'search_type': '',
                'conditional_format': None
            },
        'created_by__username':
            {
                'title': 'Créer par ',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 11,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('created_by').blank,
                'tooltip': 'L\'utilisateur qui a crée cet enregistrement',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            }
    }
    _html_page = 'wms/transactions_templates/purchase_admin_page.html'
    _url = 'purchase_admin/'
    _parameters = ''
    _short_title = 'Liste des Réceptions'
    _title = 'Réception des achats Pharmaceutiques'
    _description = 'La gestion de la table des réception des achats pharmaceutiques'

    def post(self, request):
        if request.POST['action'] == 'check_transaction_status':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    wms_validators.internal_movement_can_be_confirmed(transaction_instance=obj_instance)
                    status = 'Done'
                    status_massage = 'Validation autorisée'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le Numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'confirm_transaction':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    wms_validators.internal_movement_can_be_confirmed(transaction_instance=obj_instance)
                    obj_instance.commit_transaction()
                    wms_helpers.confirm_transaction(obj_instance)
                    new_workload = wms_helpers.create_workload(obj_instance, request.user)
                    wms_helpers.dispatch_workload(new_workload)
                    status = 'Done'
                    status_massage = 'Mouvement confirmé'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'send_transaction':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    obj_instance.send_transaction(user=request.user)
                    status = 'Done'
                    status_massage = 'Réception lancé'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'return_transaction':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    obj_instance.return_transaction(user=request.user)
                    status = 'Done'
                    status_massage = '{} {} annulé'.format(obj_instance.flew_type.designation, obj_instance.id)
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e


        else:
            return super(PurchaseAdmin, self).post(request)


class PurchaseDetailsAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = PurchaseDetail
    _parent_model = Purchase
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': True,
        'text_field': ['id'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'parent_id':
            {
                'title': '',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'hidden',
                'input_autocomplete_url': False,
                'input_validation_url': False,
                'input_attributes': {'get_parent_input': 'true'},
                'input_group': 0,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le produit',
                'search': False,
                'search_type': '',
                'conditional_format': None
            },
        'batch__product_id':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': '/wms/product_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le produit',
                'search': False,
                'search_type': '',
                'conditional_format': None
            },
        'batch__product__code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le code du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__product__product':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le nom commercial du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__batch_number':
            {
                'title': 'N° Lot',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot fournisseur',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__expiration_date':
            {
                'title': 'DDP',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'date',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 2,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'La date de péremption du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__packaging':
            {
                'title': 'Colisage',
                'format': 'number',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 3,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le nombre de boites par colis',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__purchase_pu_ht':
            {
                'title': 'PU Achat HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'money',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le prix d\'achat en hors taxes',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__unit_discount_percentage':
            {
                'title': 'Remise (%)',
                'format': 'percent',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 2,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Remise appliquée sur ce produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__purchase_pu_ht_net':
            {
                'title': 'PU Achat Net',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'money',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le prix d\'achat Net',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__tva__tva_rate':
            {
                'title': 'TVA',
                'format': 'percent',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 5,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Taux de TVA appliqué sur ce produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__tva_id':
            {
                'title': 'TVA (%)',
                'format': 'percent',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'common/tva_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 5,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Taux de TVA appliqué sur ce produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__selling_pu_ht':
            {
                'title': 'PU Vente HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'money',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 3,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Prix de vente en hors taxes',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__selling_pu_ttc':
            {
                'title': 'PU Vente TTC',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'money',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 5,
                'input_order': 3,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Prix de vente avec toutes taxes comprises',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__shp':
            {
                'title': 'SHP',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'money',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 5,
                'input_order': 2,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le SHP du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__ppa_ht':
            {
                'title': 'PPA HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'money',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 5,
                'input_order': 3,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le PPA en hors taxes',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__unit_weight':
            {
                'title': 'Pds Boite',
                'format': 'float',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {'default_value': 0},
                'input_group': 6,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le poids de la boite',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__unit_volume':
            {
                'title': 'Vlm Boite',
                'format': 'float',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {'default_value': 0},
                'input_group': 6,
                'input_order': 2,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le volume de la boite',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__packaged_weight':
            {
                'title': 'Pds Colis',
                'format': 'float',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {'default_value': 0},
                'input_group': 6,
                'input_order': 3,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le poids du colis',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'quantity':
            {
                'title': 'Quantité',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 7,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('quantity').blank,
                'tooltip': 'Quantité réçue',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': [
                    {'value': 0, 'operator': '<', 'all_row': False, 'css_class': 'w3-text-red'}
                ]
            },
        'emplacement__emplacement':
            {
                'title': 'Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Emplacement de réception du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            }
    }
    _html_page = 'wms/transactions_templates/purchase_detail_admin_page.html'
    _url = 'purchase_detail_admin/'
    _parameters = ''
    _short_title = 'Produits reçus'
    _title = 'Journal des produits reçus'
    _description = 'Journal des Réception des achats Pharmaceutiques'

    def post(self, request):
        user = request.user
        if self._parent_model and (not request.POST.get('parent_id') or request.POST.get('parent_id') == '000000/00'):
            status = 'Error'
            status_massage = json.dumps({'Bon parent': 'Vous devez choisir d\'abord un bon puis ajouter les détails'},
                                        cls=DjangoJSONEncoder)
            response = {'status': status, 'status_message': status_massage}
            return HttpResponse(json.dumps(response), content_type='application/json')

        if request.POST['action'] == 'add_line':
            if not user.has_perm(self._model._meta.app_label + '.add_' + self._model._meta.model_name):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' POST : add_line',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to add data in this table")
            else:
                try:
                    with transaction.atomic():
                        parent_id = request.POST.get('parent_id')
                        product_id = request.POST.get('batch__product_id')
                        batch_number = request.POST.get('batch__batch_number').strip()
                        expiration_date = request.POST.get('batch__expiration_date')
                        supplier_id = self._parent_model.objects.get(id=parent_id).invoice.supplier_id
                        packaging = request.POST.get('batch__packaging').strip()
                        purchase_pu_ht = (
                            Decimal(
                                request.POST.get(
                                    'batch__purchase_pu_ht').replace(' ', '').replace('DZD', '')) if request.POST.get(
                                'batch__purchase_pu_ht') != '' else None)
                        unit_discount_percentage = (
                            Decimal(
                                request.POST.get('batch__unit_discount_percentage', '0.00').replace(' ',
                                                                                                    '')) if request.POST.get(
                                'batch__unit_discount_percentage') != '' else None)
                        tva_id = request.POST.get('batch__tva_id')

                        selling_pu_ht = (
                            Decimal(
                                request.POST.get(
                                    'batch__selling_pu_ht', '0.00').replace(' ', '').replace('DZD',
                                                                                             '')) if request.POST.get(
                                'batch__selling_pu_ht') != '' else None)
                        ppa_ht = (
                            Decimal(
                                request.POST.get('batch__ppa_ht', '0.00').replace(' ', '').replace('DZD',
                                                                                                   '')) if request.POST.get(
                                'batch__ppa_ht') != '' else None)
                        shp = (
                            Decimal(
                                request.POST.get('batch__shp', '0.00').replace(' ', '').replace('DZD',
                                                                                                '')) if request.POST.get(
                                'batch__shp') != '' else None)
                        unit_weight = (
                            Decimal(
                                request.POST.get('unit_weight', '0.00').replace(' ', '')) if request.POST.get(
                                'batch__unit_weight') != '' else None)
                        unit_volume = (
                            Decimal(
                                request.POST.get('unit_volume', '0.00').replace(' ', '')) if request.POST.get(
                                'batch__unit_volume') != '' else None)
                        packaged_weight = (
                            Decimal(
                                request.POST.get('packaged_weight', '0.00').replace(' ', '')) if request.POST.get(
                                'batch__packaged_weight') != '' else None)
                        quantity = int(request.POST.get('quantity').strip())

                        new_batch = Batch(
                            created_by=user,
                            content_type=ContentType.objects.get_for_model(self._model),
                            object_id='0',
                            product_id=product_id,
                            batch_number=batch_number,
                            expiration_date=expiration_date,
                            supplier_id=supplier_id,
                            packaging=packaging,
                            purchase_pu_ht=purchase_pu_ht,
                            unit_discount_percentage=unit_discount_percentage,
                            selling_pu_ht=selling_pu_ht,
                            tva_id=tva_id,
                            shp=shp,
                            ppa_ht=ppa_ht,
                            unit_weight=unit_weight,
                            unit_volume=unit_volume,
                            packaged_weight=packaged_weight
                        )
                        new_batch.full_clean(exclude=['company'])
                        new_batch.save()
                        curr_obj = self._model(
                            created_by=user,
                            batch=new_batch,
                            parent_id=parent_id,
                            quantity=quantity,
                            emplacement_id=wms_parameters.get_default_reception_emplacement_id_for_pharmaceutical_purchase(
                                user)
                        )
                        curr_obj.full_clean(
                            exclude=['company'])
                        curr_obj.save()
                        new_batch.object_id = curr_obj.id
                        new_batch.save(editing=True, safe=False)
                        status = 'Done'
                        status_massage = 'Ligne ajoutée'
                        response = {'status': status, 'status_message': status_massage}
                        return HttpResponse(json.dumps(response), content_type='application/json')
                except ValidationError as e:
                    status = 'Error'
                    status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_massage}
                    Error.objects.create(
                        created_by=user,
                        error='ValidationError : ' + str(status_massage),
                        data=dict(request.POST)
                    )
                    return HttpResponse(json.dumps(response), content_type='application/json')

                except Exception as e:
                    Error.objects.create(
                        created_by=user,
                        error=traceback.format_exc(),
                        data=dict(request.POST)
                    )
                    raise e
        elif request.POST['action'] == 'edit_line':
            if not user.has_perm(self._model._meta.app_label + '.change_' + self._model._meta.model_name):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' POST : edit_line',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to edit data on this table")
            else:
                try:
                    with transaction.atomic():
                        parent_id = request.POST.get('parent_id')
                        line_id = request.POST.get('line_id')
                        product_id = request.POST.get('batch__product_id')
                        batch_number = request.POST.get('batch__batch_number').strip()
                        expiration_date = request.POST.get('batch__expiration_date')
                        supplier_id = self._parent_model.objects.get(id=parent_id).invoice.supplier_id
                        packaging = request.POST.get('batch__packaging').strip()
                        purchase_pu_ht = (
                            Decimal(
                                request.POST.get(
                                    'batch__purchase_pu_ht').replace(' ', '').replace('DZD', '')) if request.POST.get(
                                'batch__purchase_pu_ht') != '' else None)
                        unit_discount_percentage = (
                            Decimal(
                                request.POST.get('batch__unit_discount_percentage', '0.00').replace(' ',
                                                                                                    '')) if request.POST.get(
                                'batch__unit_discount_percentage') != '' else None)
                        tva_id = request.POST.get('batch__tva_id')

                        selling_pu_ht = (
                            Decimal(
                                request.POST.get(
                                    'batch__selling_pu_ht', '0.00').replace(' ', '').replace('DZD',
                                                                                             '')) if request.POST.get(
                                'batch__selling_pu_ht') != '' else None)
                        ppa_ht = (
                            Decimal(
                                request.POST.get('batch__ppa_ht', '0.00').replace(' ', '').replace('DZD',
                                                                                                   '')) if request.POST.get(
                                'batch__ppa_ht') != '' else None)
                        shp = (
                            Decimal(
                                request.POST.get('batch__shp', '0.00').replace(' ', '').replace('DZD',
                                                                                                '')) if request.POST.get(
                                'batch__shp') != '' else None)
                        unit_weight = (
                            Decimal(
                                request.POST.get('unit_weight', '0.00').replace(' ', '')) if request.POST.get(
                                'batch__unit_weight') != '' else None)
                        unit_volume = (
                            Decimal(
                                request.POST.get('unit_volume', '0.00').replace(' ', '')) if request.POST.get(
                                'batch__unit_volume') != '' else None)
                        packaged_weight = (
                            Decimal(
                                request.POST.get('packaged_weight', '0.00').replace(' ', '')) if request.POST.get(
                                'batch__packaged_weight') != '' else None)
                        quantity = int(request.POST.get('quantity').strip())

                        new_batch = Batch.objects.get(
                            content_type=ContentType.objects.get_for_model(self._model),
                            object_id=line_id
                        )

                        new_batch.created_by_id = user.id
                        new_batch.product_id = product_id
                        new_batch.batch_number = batch_number
                        new_batch.expiration_date = expiration_date
                        new_batch.supplier_id = supplier_id
                        new_batch.packaging = packaging
                        new_batch.purchase_pu_ht = purchase_pu_ht
                        new_batch.unit_discount_percentage = unit_discount_percentage
                        new_batch.selling_pu_ht = selling_pu_ht
                        new_batch.tva_id = tva_id
                        new_batch.shp = shp
                        new_batch.ppa_ht = ppa_ht
                        new_batch.unit_weight = unit_weight
                        new_batch.unit_volume = unit_volume
                        new_batch.packaged_weight = packaged_weight

                        new_batch.full_clean(exclude=['company'])
                        new_batch.save(editing=True, safe=False)
                        curr_obj = self._model.objects.get(id=line_id)
                        curr_obj.created_by = user
                        curr_obj.batch = new_batch
                        curr_obj.parent_id = parent_id
                        curr_obj.quantity = quantity

                        curr_obj.full_clean(
                            exclude=['company'])
                        curr_obj.save(editing=True, user=user)
                        new_batch.object_id = curr_obj.id
                        new_batch.save(editing=True, safe=False)
                        status = 'Done'
                        status_massage = 'Ligne modifiée'
                        response = {'status': status, 'status_message': status_massage}
                        return HttpResponse(json.dumps(response), content_type='application/json')
                except ValidationError as e:

                    status = 'Error'
                    status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_massage}
                    Error.objects.create(
                        created_by=user,
                        error='ValidationError : ' + str(status_massage),
                        data=dict(request.POST)
                    )
                    return HttpResponse(json.dumps(response), content_type='application/json')
                except Exception as e:
                    Error.objects.create(
                        created_by=user,
                        error=traceback.format_exc(),
                        data=dict(request.POST)
                    )
                    raise e
        if request.POST.get('action') == 'get_line':
            if not user.has_perm(self.get_view_permission()):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' POST : get_view_parameters',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to view data on this table")
            else:
                try:
                    line_id = request.POST.get('line_id')
                    if line_id == 'undefined' or not line_id:
                        status = 'Error'
                        status_massage = {
                            'Erreur Interne!': "Merci de sélectionner une ligne"}
                        status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                        response = {'status': status, 'status_message': status_massage}
                        Error.objects.create(
                            error_type='Browser Bug',
                            error_name='Line not selected',
                            error='Browser is not inspecting data correctly before sending request',
                            data='model =' + self._model.__name__ + ' POST : get_line ',
                            created_by=user
                        )
                        return HttpResponse(json.dumps(response), content_type='application/json')
                    else:
                        with transaction.atomic():
                            field_to_edit = ['parent_id', 'batch__product_id', 'batch__batch_number',
                                             'batch__expiration_date', 'batch__packaging', 'batch__purchase_pu_ht',
                                             'batch__unit_discount_percentage', 'batch__purchase_pu_ht_net',
                                             'batch__tva_id', 'batch__selling_pu_ht', 'batch__selling_pu_ttc',
                                             'batch__shp', 'batch__ppa_ht', 'batch__unit_weight', 'batch__unit_volume',
                                             'batch__packaged_weight', 'quantity', 'emplacement_id']

                            obj = self._model.objects.values(*field_to_edit).filter(id=line_id)
                            item_data = obj[0]
                            item_data['pk'] = line_id
                            item_data['instance_title'] = self.default_field_text(line_id)
                            status = 'OK'
                            status_massage = 'Ligne chargée'
                            status_data = json.dumps(item_data, cls=DjangoJSONEncoder)
                            response = {'status': status,
                                        'status_message': status_massage,
                                        'status_data': status_data}
                        return HttpResponse(json.dumps(response, cls=DjangoJSONEncoder),
                                            content_type='application/json')
                except ObjectDoesNotExist:
                    status = 'Error'
                    status_massage = {
                        'Integrité des données': "Aucune ligne trouvée !!"}
                    status_massage = json.dumps(status_massage, cls=DjangoJSONEncoder)
                    Error.objects.create(
                        error_type='Bug',
                        error_name='ObjectDoesNotExist',
                        error='Object selected for changing (get_line) does not exist',
                        data=dict(request.POST),
                        created_by=user
                    )
                    response = {'status': status, 'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
                except Exception as e:
                    Error.objects.create(
                        created_by=user,
                        error=traceback.format_exc(),
                        data=dict(request.POST),
                        error_type='Bug',
                        error_name='Exception',
                    )
                    raise e
        else:
            return super(PurchaseDetailsAdmin, self).post(request)


class InventoryAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Inventory
    _model_data = {
        'enumeration': False,
        'sort': ['batch__product', 'emplacement__emplacement'],
        'reverse_sort': True,
        'text_field': ['batch__product'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['export']
    _fields = {
        'batch__product__code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le code du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__product__product':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le nom du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__batch_number':
            {
                'title': 'N° Lot',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__expiration_date':
            {
                'title': 'DDP',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'La date de péremption du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': [
                    {'value': timezone.now().date(), 'operator': '>', 'all_row': False, 'css_class': 'w3-text-red'},
                    {'value': timezone.now().date() + timedelta(days=90), 'operator': '>', 'all_row': False,
                     'css_class': 'w3-text-orange'}
                ]
            },
        'batch__supplier__supplier':
            {
                'title': 'Fournisseur',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le fournisseur du Lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__purchase_pu_ht_net':
            {
                'title': 'PU Achat Net',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le prix d\'achat net',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__tva__tva_rate':
            {
                'title': 'TVA',
                'format': 'percent',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Taux de TVA',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__selling_pu_ht':
            {
                'title': 'PU Vente HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le prix de vente du produit en hors taxes',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__shp':
            {
                'title': 'SHP',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'SHP du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__ppa_ht':
            {
                'title': 'PPA HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le Prix Public Algérien du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'emplacement__store__designation':
            {
                'title': 'Magasin',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Le magasin de stockage',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'emplacement__emplacement':
            {
                'title': 'Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__packaging':
            {
                'title': 'Colisage',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Boites par colis',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'quantity_sum':
            {
                'title': 'Quantité',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('quantity').blank,
                'tooltip': 'La quantité totale',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': [
                    {'value': 0, 'operator': '>', 'all_row': False, 'css_class': 'w3-text-red'}
                ]
            },
        'complete_package':
            {
                'title': 'Colis',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Nombre de colis complets',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'unpacked_units':
            {
                'title': 'VRAC',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Nombre de boite en VRAC',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'product_status__designation':
            {
                'title': 'Status',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Le status du produit. Exemple: Périmé, Conforme',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },

    }
    _html_page = 'wms/transactions_templates/inventory_admin_page.html'
    _url = 'inventory_admin/'
    _parameters = ''
    _short_title = 'Stock par Emplacement'
    _title = 'Stock par Emplacement'
    _description = 'Stock détailé par Emplacement'

    def get_queryset(self):
        queryset = self._model.objects.values(
            'emplacement',
            'product_status'
        ).filter(received=True).annotate(
            pk=Max('id'),
            quantity_sum=Sum('quantity')
        ).annotate(
            complete_package=Case(
                When(~Q(batch__packaging=0), then=(F('quantity_sum') / F('batch__packaging'))),
                default=0,
                output_field=IntegerField()
            ),
            unpacked_units=Case(
                When(~Q(batch__packaging=0), then=F('quantity_sum') % F('batch__packaging')), default=F('quantity_sum'),
                output_field=IntegerField())
        ).exclude(quantity_sum=0)
        return queryset

    def get(self, request):
        if request.GET.get('action') == 'export_data':
            _data = Inventory.objects.filter(received=1).values(
                'emplacement',
                'product_status'
            ).annotate(
                quantity_sum=Sum('quantity')
            ).annotate(
                complete_package=Case(
                    When(~Q(batch__packaging=0), then=(F('quantity_sum') / F('batch__packaging'))),
                    default=0,
                    output_field=IntegerField()
                ),
                unpacked_units=Case(
                    When(~Q(batch__packaging=0), then=F('quantity_sum') % F('batch__packaging')),
                    default=F('quantity_sum'),
                    output_field=IntegerField())).values(
                'batch__product__code',
                'batch__product__product',
                'batch__batch_number',
                'batch__expiration_date',
                'batch__supplier__supplier',
                'batch__packaging',
                'batch__purchase_pu_ht_net',
                'batch__selling_pu_ht',
                'batch__ppa_ht',
                'batch__tva__tva_rate',
                'batch__shp',
                'emplacement__emplacement',
                'emplacement__store__designation',
                'product_status__designation',
                'quantity_sum',
                'complete_package',
                'unpacked_units',
            )  # .exclude(quantity_sum=0)
            response = HttpResponse(content_type='text/csv')
            filename = 'Stock du ' + str(timezone.now())
            response['Content-Disposition'] = 'attachment; filename="' + filename + '.csv"'
            writer = csv.writer(response, delimiter=';')
            writer.writerow([
                'Code',
                'Produit',
                'Lot',
                'DDP',
                'Fournisseur',
                'PU ACH HT NET',
                'PU VEN HT NET',
                'PPA HT',
                'TVA',
                'SHP',
                'Magasin',
                'Emplacement',
                'Colisage',
                'Quantité',
                'Colis',
                'Vrac',
                'Statut'
            ])
            for item in _data:
                row = [
                    item['batch__product__code'],
                    item['batch__product__product'],
                    item['batch__batch_number'],
                    item['batch__expiration_date'],
                    item['batch__supplier__supplier'],
                    item['batch__purchase_pu_ht_net'],
                    item['batch__selling_pu_ht'],
                    item['batch__ppa_ht'],
                    item['batch__tva__tva_rate'],
                    item['batch__shp'],
                    item['emplacement__store__designation'],
                    item['emplacement__emplacement'],
                    item['batch__packaging'],
                    item['quantity_sum'],
                    item['complete_package'],
                    item['unpacked_units'],
                    item['product_status__designation'],
                ]
                writer.writerow(row)
            return response
        else:
            return super(InventoryAdmin, self).get(request)


class ExecutorTempoAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = ExecutorTempo
    _form_width = '400px'
    _model_data = {
        'enumeration': False,
        'sort': ['id'],
        'reverse_sort': True,
        'text_field': ['id'],
        'table_filter': False,
        'date_range_filter': False,
    }
    _default_actions = ['delete']
    _fields = {
        'employee__name':
            {
                'title': 'Employee',
                'format': 'text',
                'width': False,
                'column_order': 4,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': False,
                'edit': False,
                'required': not _model._meta.get_field('employee').blank,
                'tooltip': 'Le nom de l\'employee',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'employee__code':
            {
                'title': 'Code RH',
                'format': 'text',
                'width': False,
                'column_order': 4,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': False,
                'edit': False,
                'required': not _model._meta.get_field('employee').blank,
                'tooltip': 'Le nom de l\'employee',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            }
    }
    _html_page = 'wms/transactions_templates/executor_tempo_admin_page.html'
    _url = 'executor_tempo_admin/'
    _parameters = ''
    _short_title = 'Tempo Executors'
    _title = 'Tempo Executors'
    _description = 'Tempo Executors'

    def post(self, request):
        user = request.user
        if request.POST.get('action') == 'add_line_by_employee_code':
            if not user.has_perm(self._model._meta.app_label + '.add_' + self._model._meta.model_name):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' POST : add_line',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to add data in this table")
            else:
                try:
                    with transaction.atomic():
                        object_id = request.POST.get('object_id')
                        employee = Employee.objects.get(code=request.POST.get('employee_code'))
                        content_type = ContentType.objects.get(model=request.POST.get('content_type__model'))
                        obj = self._model(
                            object_id=object_id,
                            employee=employee,
                            content_type=content_type,
                            created_by=user
                        )
                        obj.full_clean(exclude=['company'])
                        obj.save()
                        status = 'Done'
                        status_massage = '{} créé(e) sous le numéro {}'.format(self._model._meta.verbose_name.title(),
                                                                               str(obj.pk))
                        response = {'status': status,
                                    'status_message': status_massage
                                    }
                    return HttpResponse(json.dumps(response, cls=DjangoJSONEncoder), content_type='application/json')
                except Employee.DoesNotExist:
                    status = 'Error'
                    status_massage = json.dumps({'Code RH': 'Ce code RH n\'existe pas!'},
                                                cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_massage}
                    Error.objects.create(
                        created_by=user,
                        error=str(status_massage),
                        data=dict(request.POST),
                        error_type='User lack training',
                        error_name='ValidationError',
                    )
                    return HttpResponse(json.dumps(response), content_type='application/json')

                except ValidationError as e:
                    status = 'Error'
                    status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_massage}
                    Error.objects.create(
                        created_by=user,
                        error=str(status_massage),
                        data=dict(request.POST),
                        error_type='User lack training',
                        error_name='ValidationError',
                    )
                    return HttpResponse(json.dumps(response), content_type='application/json')

                except Exception as e:
                    Error.objects.create(
                        created_by=user,
                        error=traceback.format_exc(),
                        data=dict(request.POST),
                        error_type='Bug',
                        error_name='Exception',
                    )
                    raise e
        else:
            return super(ExecutorTempoAdmin, self).post(request)


class TransferAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Transfer
    _model_data = {
        'enumeration': False,
        'sort': ['created_date'],
        'reverse_sort': True,
        'text_field': ['id'],
        'table_filter': True,
        'date_range_filter': True,
    }
    _default_actions = ['add', 'export', 'send', 'return', 'commit', 'confirm']
    _fields = {
        'id':
            {
                'title': 'N°',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('id').blank,
                'tooltip': 'ID du journal de comptage',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'from_store__designation':
            {
                'title': 'Depuis Magasin',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('from_store').blank,
                'tooltip': 'Le magasin à partir duquel les produits sont mouvemenentés',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'from_store_id':
            {
                'title': 'Depuis Magasin',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/store_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('from_store_id').blank,
                'tooltip': 'Le magasin à partir duquel les produits sont mouvemenentés',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'to_store__designation':
            {
                'title': 'Vers Magasin',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('from_store').blank,
                'tooltip': 'Le magasin vers  lequel les produits sont mouvemenentés',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'to_store_id':
            {
                'title': 'Vers Magasin',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/store_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('from_store_id').blank,
                'tooltip': 'Le magasin vers  lequel les produits sont mouvemenentés',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'flew_type__designation':
            {
                'title': 'Transaction',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('from_store').blank,
                'tooltip': 'Le type du mouvement',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'flew_type_id':
            {
                'title': 'Transaction',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/internal_movement_logistic_flew_type_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('from_store_id').blank,
                'tooltip': 'Le type du mouvement à effectuer',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'observation':
            {
                'title': 'Observation',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('observation').blank,
                'tooltip': 'Observation ou commentaire sur ce mouvement',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'document_status__status':
            {
                'title': 'Statut du Document',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'date',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 9,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('document_status').blank,
                'tooltip': 'Les status actuel du document',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'created_date':
            {
                'title': 'Créer le ',
                'format': 'datetime',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'datetime',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 10,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('created_date').blank,
                'tooltip': 'Date de création de\'enregistrement',
                'search': False,
                'search_type': '',
                'conditional_format': None
            },
        'created_by__username':
            {
                'title': 'Créer par ',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 11,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('created_by').blank,
                'tooltip': 'L\'utilisateur qui a crée cet enregistrement',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },

    }
    _html_page = 'wms/transactions_templates/transfer_admin_page.html'
    _url = 'transfer_admin/'
    _parameters = ''
    _short_title = 'Liste des Mouvements'
    _title = 'Mouvements internes des stocks'
    _description = 'La gestion de la table des mouvements internes des stocks'

    def get_queryset(self):
        user = self.request.user
        if user.has_perm(self._model._meta.app_label + '.view_all_' + self._model._meta.model_name):
            return self._model.objects.values(*self.get_fields_list(parameter='display'))
        else:
            return self._model.objects.values(*self.get_fields_list(parameter='display')).filter(created_by=user)

    def post(self, request):
        if request.POST['action'] == 'check_transaction_status':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    wms_validators.internal_movement_can_be_confirmed(transaction_instance=obj_instance)
                    status = 'Done'
                    status_massage = 'Validation autorisée'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le Numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'confirm_transaction':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    wms_validators.internal_movement_can_be_confirmed(transaction_instance=obj_instance)
                    obj_instance.commit_transaction()
                    wms_helpers.confirm_transaction(obj_instance)
                    new_workload = wms_helpers.create_workload(obj_instance, request.user)
                    wms_helpers.dispatch_workload(new_workload)
                    status = 'Done'
                    status_massage = 'Mouvement confirmé'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'send_transaction':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    obj_instance.send_transaction(user=request.user)
                    status = 'Done'
                    status_massage = 'Mouvement lancé'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'return_transaction':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    obj_instance.return_transaction(user=request.user)
                    status = 'Done'
                    status_massage = '{} {} annulé'.format(obj_instance.flew_type.designation, obj_instance.id)
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        else:
            return super(TransferAdmin, self).post(request)


class TransferDetailsAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = TransferDetails
    _parent_model = Transfer
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': True,
        'text_field': ['id'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'delete', 'export']
    _fields = {
        'parent_id':
            {
                'title': '',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'hidden',
                'input_autocomplete_url': False,
                'input_validation_url': False,
                'input_attributes': {'get_parent_input': 'true'},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('parent_id').blank,
                'tooltip': 'parent_id',
                'search': False,
                'search_type': '',
                'conditional_format': None
            },
        'product_id':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/product_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': False,
                'tooltip': '',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch_id':
            {
                'title': 'Lot / Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': '/wms/inventory_for_internal_transfer/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot et l\'emplacement',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__product__code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__product__product':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__batch_number':
            {
                'title': 'N° Lot',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__expiration_date':
            {
                'title': 'DDP',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__packaging':
            {
                'title': 'Colisage',
                'format': 'number',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__tva__tva_rate':
            {
                'title': 'TVA',
                'format': 'percent',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__shp':
            {
                'title': 'SHP',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__ppa_ht':
            {
                'title': 'PPA HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'quantity':
            {
                'title': 'Quantité',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'integer',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('quantity').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': [
                    {'value': 0, 'operator': '<', 'all_row': False, 'css_class': 'w3-text-red'}
                ]
            },
        'from_emplacement__emplacement':
            {
                'title': 'Depuis Empl',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('from_emplacement').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'to_emplacement__emplacement':
            {
                'title': 'Vers Empl',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('to_emplacement').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'to_emplacement_id':
            {
                'title': 'Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': '/wms/emplacement_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('to_emplacement').blank,
                'tooltip': 'Emplacement de destination',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'to_status_id':
            {
                'title': 'Status',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': '/wms/product_status_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('to_status').blank,
                'tooltip': 'Nouveau statut',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'from_status__designation':
            {
                'title': 'Depuis Statut',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('from_status').blank,
                'tooltip': '',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'to_status__designation':
            {
                'title': 'Vers Statut',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('to_status').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            }
    }
    _html_page = 'wms/transactions_templates/transfer_detail_admin_page.html'
    _url = 'transfer_detail_admin/'
    _parameters = ''
    _short_title = 'Journal mouvement internes'
    _title = 'Journal mouvement internes'
    _description = 'Journal mouvement internes'

    def post(self, request):
        user = request.user
        if request.POST.get('action') == 'add_line':
            if not user.has_perm(self._model._meta.app_label + '.add_' + self._model._meta.model_name):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' POST : add_line',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to add data in this table")
            else:
                try:
                    with transaction.atomic():
                        parent_id = request.POST.get('parent_id')
                        inventory_id = request.POST.get('batch_id')
                        current_inventory = Inventory.objects.get(id=inventory_id)
                        batch_id = current_inventory.batch_id
                        quantity = request.POST.get('quantity').strip()
                        from_emplacement_id = current_inventory.emplacement_id
                        to_emplacement_id = request.POST.get('to_emplacement_id')
                        if not to_emplacement_id or to_emplacement_id == '':
                            to_emplacement_id = Emplacement.objects.filter(
                                store=Transfer.objects.get(id=parent_id).to_store).order_by(
                                'created_date')[0].id
                        if Transfer.objects.get(id=parent_id).flew_type_id == 5:
                            to_emplacement_id = wms_parameters.get_default_product_picking_emplacement_id(
                                user, current_inventory.batch.product
                            )
                        from_status_id = current_inventory.product_status_id
                        to_status_id = request.POST.get('to_status_id')
                        if not to_status_id or to_status_id == '':
                            to_status_id = from_status_id
                        curr_obj = TransferDetails(
                            created_by=user,
                            parent_id=parent_id,
                            batch_id=batch_id,
                            quantity=quantity,
                            from_emplacement_id=from_emplacement_id,
                            to_emplacement_id=to_emplacement_id,
                            from_status_id=from_status_id,
                            to_status_id=to_status_id
                        )
                        curr_obj.full_clean(
                            exclude=['company'])
                        allowed_quantity = wms_helpers.quantity_is_allowed(inventory_object=current_inventory,
                                                                           quantity=quantity,
                                                                           warehouse_transaction='transfer')
                        if allowed_quantity['allowed']:
                            curr_obj.save()
                            status = 'Done'
                            status_massage = 'Ligne ajoutée'
                            response = {'status': status, 'status_message': status_massage}
                        else:
                            status = 'Error'
                            status_massage = json.dumps(
                                {'Quantité': 'Disponible uniquement {}'.format(allowed_quantity['valid_quantity'])})
                            response = {'status': status, 'status_message': status_massage}
                        return HttpResponse(json.dumps(response), content_type='application/json')
                except ValidationError as e:
                    status = 'Error'
                    status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_massage}
                    Error.objects.create(
                        created_by=user,
                        error=str(status_massage),
                        data=dict(request.POST),
                        error_type='User lack training',
                        error_name='ValidationError',
                    )
                    return HttpResponse(json.dumps(response), content_type='application/json')

                except Exception as e:
                    Error.objects.create(
                        created_by=user,
                        error=traceback.format_exc(),
                        data=dict(request.POST),
                        error_type='Bug',
                        error_name='Exception',
                    )
                    raise e
        else:
            return super(TransferDetailsAdmin, self).post(request)


class ExternalTransferAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = ExternalTransfer
    _model_data = {
        'enumeration': False,
        'sort': ['created_date'],
        'reverse_sort': True,
        'text_field': ['id'],
        'table_filter': True,
        'date_range_filter': True,
    }
    _default_actions = ['add', 'export', 'send', 'return', 'commit', 'confirm', 'ship', 'deliver']
    _fields = {
        'id':
            {
                'title': 'N°',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('id').blank,
                'tooltip': 'ID du journal de comptage',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'from_company__designation':
            {
                'title': 'Depuis Entreprise',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('from_company').blank,
                'tooltip': 'L\'entreprise à partir de laquelle les produits sont mouvemenentés',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'to_company__designation':
            {
                'title': 'Vers Entreprise',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('from_company').blank,
                'tooltip': 'L\'entreprise vers laquelle les produits sont mouvemenentés',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'to_company_id':
            {
                'title': 'Vers Entreprise',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'common/companies_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('to_company').blank,
                'tooltip': 'L\'entreprise vers laquelle les produits sont mouvemenentés',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'observation':
            {
                'title': 'Observation',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('observation').blank,
                'tooltip': 'Observation ou commentaire sur ce mouvement',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'document_status__status':
            {
                'title': 'Statut du Document',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'date',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 9,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('document_status').blank,
                'tooltip': 'Les status actuel du document',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'created_date':
            {
                'title': 'Créer le ',
                'format': 'datetime',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'datetime',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 10,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('created_date').blank,
                'tooltip': 'Date de création de\'enregistrement',
                'search': False,
                'search_type': '',
                'conditional_format': None
            },
        'created_by__username':
            {
                'title': 'Créer par ',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 11,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('created_by').blank,
                'tooltip': 'L\'utilisateur qui a crée cet enregistrement',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },

    }
    _html_page = 'wms/transactions_templates/external_transfer_admin_page.html'
    _url = 'external_transfer_admin/'
    _parameters = ''
    _short_title = 'Liste des Mouvements externes'
    _title = 'Mouvements Entre filiales'
    _description = 'La gestion de la table des mouvements entre filiales'

    def get_queryset(self):
        user = self.request.user
        if user.has_perm(self._model._meta.app_label + '.view_all_' + self._model._meta.model_name):
            return self._model.objects.values(*self.get_fields_list(parameter='display'))
        else:
            return self._model.objects.values(*self.get_fields_list(parameter='display')).filter(created_by=user)

    def post(self, request):
        if request.POST['action'] == 'check_transaction_status':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    for_action = request.POST.get('for_action')
                    obj_instance = self._model.objects.get(id=document_id)
                    if for_action == 'confirm_transaction':
                        wms_validators.internal_movement_can_be_confirmed(transaction_instance=obj_instance)
                        status_massage = 'Validation autorisée'
                    elif for_action == 'ship_transaction':
                        wms_validators.transaction_can_be_shipped(transaction_instance=obj_instance)
                        status_massage = 'Expédition autorisée'
                    status = 'Done'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le Numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'confirm_transaction':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    wms_validators.internal_movement_can_be_confirmed(transaction_instance=obj_instance)
                    obj_instance.commit_transaction()
                    wms_helpers.confirm_transaction(obj_instance)
                    new_workload = wms_helpers.create_workload(obj_instance, request.user)
                    wms_helpers.dispatch_workload(new_workload)
                    status = 'Done'
                    status_massage = 'Mouvement confirmé'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'send_transaction':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    obj_instance.send_transaction(user=request.user)
                    status = 'Done'
                    status_massage = 'Mouvement lancé'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'return_transaction':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    obj_instance.return_transaction(user=request.user)
                    status = 'Done'
                    status_massage = '{} {} annulé'.format(obj_instance.flew_type.designation, obj_instance.id)
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        if request.POST['action'] == 'check_transaction_status':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    wms_validators.internal_movement_can_be_confirmed(transaction_instance=obj_instance)
                    status = 'Done'
                    status_massage = 'Validation autorisée'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le Numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'ship_transaction':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    obj_instance.ship_transaction(user=request.user)
                    new_workload = wms_helpers.create_workload(
                        transaction_instance=obj_instance,
                        user=request.user,
                        flew_type_id=wms_parameters.SHIP_LOGISTIC_FLEW_ID
                    )
                    wms_helpers.dispatch_workload(new_workload)
                    status = 'Done'
                    status_massage = '{} {} expédié'.format(obj_instance.flew_type.designation, obj_instance.id)
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST.get('action') == 'add_line':
            if not request.user.has_perm(self._model._meta.app_label + '.add_' + self._model._meta.model_name):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' POST : add_line',
                    created_by=request.user
                )
                raise PermissionDenied("You do not have permission to add data in this table")
            else:
                try:
                    with transaction.atomic():
                        file_params = {k: request.FILES.get(k) for k in self.get_fields_list(parameter='input_files') if
                                       k not in ['id', 'object_id', 'content_type']}
                        post_params = {k: request.POST.get(k).strip() for k in self.get_fields_list(parameter='input')
                                       if
                                       k not in ['id', 'object_id', 'content_type']}
                        company_data = {'from_company_id': common_helpers.get_user_company(request.user).id}

                        obj = self._model(**post_params, **file_params, **company_data, created_by=request.user)
                        obj.full_clean(exclude=['company'])
                        obj.save()
                        status = 'Done'
                        status_massage = '{} créé(e) sous le numéro {}'.format(self._model._meta.verbose_name.title(),
                                                                               str(obj.pk))
                        response = {'status': status,
                                    'status_message': status_massage
                                    }
                    return HttpResponse(json.dumps(response, cls=DjangoJSONEncoder), content_type='application/json')
                except ValidationError as e:
                    status = 'Error'
                    status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_massage}
                    Error.objects.create(
                        created_by=request.user,
                        error=str(status_massage),
                        data=dict(request.POST),
                        error_type='User lack training',
                        error_name='ValidationError',
                    )
                    return HttpResponse(json.dumps(response), content_type='application/json')

                except Exception as e:
                    Error.objects.create(
                        created_by=request.user,
                        error=traceback.format_exc(),
                        data=dict(request.POST),
                        error_type='Bug',
                        error_name='Exception',
                    )
                    raise e
        else:
            return super(ExternalTransferAdmin, self).post(request)


class ExternalTransferDetailsAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = ExternalTransferDetails
    _parent_model = ExternalTransfer
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': True,
        'text_field': ['id'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'delete', 'export']
    _fields = {
        'parent_id':
            {
                'title': '',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'hidden',
                'input_autocomplete_url': False,
                'input_validation_url': False,
                'input_attributes': {'get_parent_input': 'true'},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('parent_id').blank,
                'tooltip': 'parent_id',
                'search': False,
                'search_type': '',
                'conditional_format': None
            },
        'product_id':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/product_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': False,
                'tooltip': '',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch_id':
            {
                'title': 'Lot / Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': '/wms/inventory_for_internal_transfer/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot et l\'emplacement',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__product__code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__product__product':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__batch_number':
            {
                'title': 'N° Lot',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__expiration_date':
            {
                'title': 'DDP',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__packaging':
            {
                'title': 'Colisage',
                'format': 'number',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__tva__tva_rate':
            {
                'title': 'TVA',
                'format': 'percent',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__shp':
            {
                'title': 'SHP',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__ppa_ht':
            {
                'title': 'PPA HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'quantity':
            {
                'title': 'Quantité',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'integer',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('quantity').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': [
                    {'value': 0, 'operator': '<', 'all_row': False, 'css_class': 'w3-text-red'}
                ]
            },
        'emplacement__store__designation':
            {
                'title': 'Magasin',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': '',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'emplacement__emplacement':
            {
                'title': 'Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': '',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'status__designation':
            {
                'title': 'Vers Statut',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('status').blank,
                'tooltip': '',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            }
    }
    # _html_page = 'wms/transactions_templates/external_transfer_detail_admin_page.html'
    _url = 'external_transfer_detail_admin/'
    _parameters = ''

    # _short_title = 'Journal mouvement external'
    # _title = 'Journal mouvement external'
    # _description = 'Journal mouvement external'

    def post(self, request):
        user = request.user
        if request.POST.get('action') == 'add_line':
            if not user.has_perm(self._model._meta.app_label + '.add_' + self._model._meta.model_name):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' POST : add_line',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to add data in this table")
            else:
                try:
                    with transaction.atomic():
                        parent_id = request.POST.get('parent_id')
                        inventory_id = request.POST.get('batch_id')
                        current_inventory = Inventory.objects.get(id=inventory_id)
                        batch_id = current_inventory.batch_id
                        quantity = int(request.POST.get('quantity').strip())
                        emplacement_id = current_inventory.emplacement_id
                        status_id = current_inventory.product_status_id
                        curr_obj = ExternalTransferDetails(
                            created_by=user,
                            parent_id=parent_id,
                            batch_id=batch_id,
                            quantity=quantity,
                            emplacement_id=emplacement_id,
                            status_id=status_id,
                        )
                        curr_obj.full_clean(
                            exclude=['company'])
                        allowed_quantity = wms_helpers.quantity_is_allowed(inventory_object=current_inventory,
                                                                           quantity=quantity,
                                                                           warehouse_transaction='transfer')
                        if allowed_quantity['allowed']:
                            curr_obj.save()
                            status = 'Done'
                            status_massage = 'Ligne ajoutée'
                            response = {'status': status, 'status_message': status_massage}
                        else:
                            status = 'Error'
                            status_massage = json.dumps(
                                {'Quantité': 'Disponible uniquement {}'.format(allowed_quantity['valid_quantity'])})
                            response = {'status': status, 'status_message': status_massage}
                        return HttpResponse(json.dumps(response), content_type='application/json')
                except ValidationError as e:
                    status = 'Error'
                    status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_massage}
                    Error.objects.create(
                        created_by=user,
                        error=str(status_massage),
                        data=dict(request.POST),
                        error_type='User lack training',
                        error_name='ValidationError',
                    )
                    return HttpResponse(json.dumps(response), content_type='application/json')

                except Exception as e:
                    Error.objects.create(
                        created_by=user,
                        error=traceback.format_exc(),
                        data=dict(request.POST),
                        error_type='Bug',
                        error_name='Exception',
                    )
                    raise e
        else:
            return super(ExternalTransferDetailsAdmin, self).post(request)


class WarehouseReservationDetailAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = WarehouseReservationDetail
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': True,
        'text_field': ['id'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete']
    _fields = {
        'parent_id':
            {
                'title': '',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'hidden',
                'input_autocomplete_url': False,
                'input_validation_url': False,
                'input_attributes': {'get_parent_input': 'true'},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('parent_id').blank,
                'tooltip': 'parent_id',
                'search': False,
                'search_type': '',
                'conditional_format': None
            },
        'product_id':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/product_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': False,
                'tooltip': '',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },

        'batch_id':
            {
                'title': 'Lot / Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': '/wms/inventory_for_internal_transfer/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot et l\'emplacement',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__product__code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__product__product':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__batch_number':
            {
                'title': 'N° Lot',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__expiration_date':
            {
                'title': 'DDP',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__packaging':
            {
                'title': 'Colisage',
                'format': 'number',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__tva__tva_rate':
            {
                'title': 'TVA',
                'format': 'percent',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__shp':
            {
                'title': 'SHP',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__ppa_ht':
            {
                'title': 'PPA HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'quantity':
            {
                'title': 'Quantité',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'integer',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('quantity').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': [
                    {'value': 0, 'operator': '<', 'all_row': False, 'css_class': 'w3-text-red'}
                ]
            },
        'from_emplacement__emplacement':
            {
                'title': 'Depuis Empl',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('from_emplacement').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'to_emplacement__emplacement':
            {
                'title': 'Vers Empl',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('to_emplacement').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'to_emplacement_id':
            {
                'title': 'Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': '/wms/emplacement_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('to_emplacement').blank,
                'tooltip': 'Emplacement de destination',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'to_status_id':
            {
                'title': 'Status',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': '/wms/product_status_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('to_status').blank,
                'tooltip': 'Nouveau statut',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'from_status__designation':
            {
                'title': 'Depuis Statut',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('from_status').blank,
                'tooltip': '',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'to_status__designation':
            {
                'title': 'Vers Statut',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('to_status').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            }
    }
    _html_page = 'wms/transactions_templates/warehouse_reservation_admin_page.html'
    _url = 'warehouse_reservation_admin/'
    _parameters = ''
    _short_title = 'Journal des reservation'
    _title = 'Journal des reservation'
    _description = 'Journal des reservation'

    def post(self, request):
        user = request.user
        if request.POST.get('action') == 'add_line':
            if not user.has_perm(self._model._meta.app_label + '.add_' + self._model._meta.model_name):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' POST : add_line',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to add data in this table")
            else:
                try:
                    with transaction.atomic():
                        parent_id = request.POST.get('parent_id')
                        inventory_id = request.POST.get('batch_id')
                        current_inventory = Inventory.objects.get(id=inventory_id)
                        batch_id = current_inventory.batch_id
                        quantity = int(request.POST.get('quantity').strip())
                        from_emplacement_id = current_inventory.emplacement_id
                        to_emplacement_id = request.POST.get(
                            'to_emplacement_id',
                            Emplacement.objects.filter(
                                store=TransferTempo.objects.get(id=parent_id).to_store).order_by(
                                'created_date').first().id
                        )
                        if TransferTempo.objects.get(id=parent_id).flew_type_id == 5:
                            to_emplacement_id = wms_parameters.get_default_product_picking_emplacement_id(
                                user, current_inventory.batch.product
                            )
                        from_status_id = current_inventory.product_status_id
                        to_status_id = request.POST.get('to_status_id', from_status_id)
                        curr_obj = WarehouseReservationDetail(
                            created_by=user,
                            parent_id=parent_id,
                            batch_id=batch_id,
                            quantity=quantity,
                            from_emplacement_id=from_emplacement_id,
                            to_emplacement_id=to_emplacement_id,
                            from_status_id=from_status_id,
                            to_status_id=to_status_id
                        )
                        curr_obj.full_clean(
                            exclude=['company'])
                        allowed_quantity = wms_helpers.quantity_is_allowed(inventory_object=current_inventory,
                                                                           quantity=quantity,
                                                                           warehouse_transaction='transfer')
                        if allowed_quantity['allowed']:
                            curr_obj.save()
                            status = 'Done'
                            status_massage = 'Ligne ajoutée'
                            response = {'status': status, 'status_message': status_massage}
                        else:
                            status = 'Error'
                            status_massage = json.dumps(
                                {'Quantité': 'Disponible uniquement {}'.format(allowed_quantity['valid_quantity'])})
                            response = {'status': status, 'status_message': status_massage}
                        return HttpResponse(json.dumps(response), content_type='application/json')
                except ValidationError as e:
                    status = 'Error'
                    status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_massage}
                    Error.objects.create(
                        created_by=user,
                        error=str(status_massage),
                        data=dict(request.POST),
                        error_type='User lack training',
                        error_name='ValidationError',
                    )
                    return HttpResponse(json.dumps(response), content_type='application/json')

                except Exception as e:
                    Error.objects.create(
                        created_by=user,
                        error=traceback.format_exc(),
                        data=dict(request.POST),
                        error_type='Bug',
                        error_name='Exception',
                    )
                    raise e
        else:
            return super(WarehouseReservationDetailAdmin, self).post(request)


class InventoryCountAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = InventoryCount
    _model_data = {
        'enumeration': False,
        'sort': ['created_date'],
        'reverse_sort': True,
        'text_field': ['id'],
        'table_filter': True,
        'date_range_filter': True,
    }
    _default_actions = ['add', 'change', 'delete', 'export', 'confirm']
    _fields = {
        'id':
            {
                'title': 'N°',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('id').blank,
                'tooltip': 'ID du journal de comptage',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'counting_date':
            {
                'title': 'Date Comptage',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'date',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {'default_value': timezone.now().date()},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('counting_date').blank,
                'tooltip': 'La date du comptage',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'store__designation':
            {
                'title': 'Magasin',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('store').blank,
                'tooltip': 'Le magasin de comptage',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'store_id':
            {
                'title': 'Magasin',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/store_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('store_id').blank,
                'tooltip': 'Le magasin de comptage',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'observation':
            {
                'title': 'Observation',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('observation').blank,
                'tooltip': 'Observation ou détail sur le journal de comptage',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'document_status__status':
            {
                'title': 'Statut du Document',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'date',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 9,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('document_status').blank,
                'tooltip': 'Les status actuel du document',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'created_date':
            {
                'title': 'Créer le ',
                'format': 'datetime',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'datetime',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 10,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('created_date').blank,
                'tooltip': 'Date de création de\'enregistrement',
                'search': False,
                'search_type': '',
                'conditional_format': None
            },
        'created_by__username':
            {
                'title': 'Créer par ',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 11,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('created_by').blank,
                'tooltip': 'L\'utilisateur qui a crée cet enregistrement',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            }
    }
    _html_page = 'wms/transactions_templates/inventory_count_admin_page.html'
    _url = 'inventory_count_admin/'
    _parameters = ''
    _short_title = 'Journaux de comptage'
    _title = 'Journaux de comptage des stocks'
    _description = 'La gestion de la table des journaux de comptage des stocks'

    def get_queryset(self):
        user = self.request.user
        if user.has_perm(self._model._meta.app_label + '.view_all_' + self._model._meta.model_name):
            return self._model.objects.values(*self.get_fields_list(parameter='display'))
        else:
            return self._model.objects.values(*self.get_fields_list(parameter='display')).filter(created_by=user)

    def post(self, request):
        if request.POST['action'] == 'check_transaction_status':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    wms_validators.transaction_can_be_confirmed(transaction_instance=obj_instance, user=request.user)
                    status = 'Done'
                    status_massage = 'Validation autorisée'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le Numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        elif request.POST['action'] == 'confirm_transaction':
            try:
                with transaction.atomic():
                    document_id = request.POST.get('document_id').strip()
                    obj_instance = self._model.objects.get(id=document_id)
                    wms_validators.transaction_can_be_confirmed(transaction_instance=obj_instance, user=request.user)
                    wms_helpers.confirm_transaction(obj_instance)
                    obj_instance.commit_transaction()
                    new_workload = wms_helpers.create_workload(obj_instance, request.user)
                    wms_helpers.dispatch_workload(new_workload)
                    status = 'Done'
                    status_massage = 'Mouvement confirmé'
                    response = {'status': status,
                                'status_message': status_massage}
                    return HttpResponse(json.dumps(response), content_type='application/json')
            except self._model.DoesNotExist:
                status = 'Error'
                status_massage = json.dumps({'N° Document': 'Le numéro du document est incorrect'})
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except ValidationError as e:
                status = 'Error'
                status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                response = {'status': status, 'status_message': status_massage}
                Error.objects.create(
                    created_by=request.user,
                    error='ValidationError : ' + str(status_massage),
                    data=dict(request.POST)
                )
                return HttpResponse(json.dumps(response), content_type='application/json')
            except Exception as e:
                Error.objects.create(
                    created_by=request.user,
                    error=traceback.format_exc(),
                    data=dict(request.POST)
                )
                raise e
        else:
            return super(InventoryCountAdmin, self).post(request)


class InventoryCountDetailAdmin(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = InventoryCountDetail
    _parent_model = InventoryCount
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': True,
        'text_field': ['id'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'parent_id':
            {
                'title': '',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'hidden',
                'input_autocomplete_url': False,
                'input_validation_url': False,
                'input_attributes': {'get_parent_input': 'true'},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('parent_id').blank,
                'tooltip': 'parent_id',
                'search': False,
                'search_type': '',
                'conditional_format': None
            },
        'product_id':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/product_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': True,
                'required': False,
                'tooltip': 'Le produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch_id':
            {
                'title': 'Lot',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': '/wms/batch_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot et l\'emplacement',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__product__id':
            {
                'title': 'ID Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__product__code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__product__product':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__batch_number':
            {
                'title': 'N° Lot',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__expiration_date':
            {
                'title': 'DDP',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__packaging':
            {
                'title': 'Colisage',
                'format': 'number',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__purchase_pu_ht_net':
            {
                'title': 'PU Achat',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__tva__tva_rate':
            {
                'title': 'TVA',
                'format': 'percent',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__shp':
            {
                'title': 'SHP',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__ppa_ht':
            {
                'title': 'PPA HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'quantity':
            {
                'title': 'Quantité',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'integer',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('quantity').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__iexact',
                'conditional_format': [
                    {'value': 0, 'operator': '<', 'all_row': False, 'css_class': 'w3-text-red'}
                ]
            },
        'emplacement__emplacement':
            {
                'title': 'Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'emplacement_id':
            {
                'title': 'Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': '/wms/emplacement_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Emplacement de destination',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'status_id':
            {
                'title': 'Status',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': '/wms/product_status_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('status').blank,
                'tooltip': 'Nouveau statut',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'status__designation':
            {
                'title': 'Statut',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('status').blank,
                'tooltip': '',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            }, }
    _url = 'inventory_count_details_admin/'

    def post(self, request):
        user = request.user
        if request.POST.get('action') == 'add_line':
            if not user.has_perm(self._model._meta.app_label + '.add_' + self._model._meta.model_name):
                Error.objects.create(
                    error_type='Bug',
                    error_name='PermissionDenied',
                    error='',
                    data='model =' + self._model.__name__ + ' POST : add_line',
                    created_by=user
                )
                raise PermissionDenied("You do not have permission to add data in this table")
            else:
                try:
                    with transaction.atomic():

                        post_params = {k: request.POST.get(k).strip() for k in self.get_fields_list(parameter='input')
                                       if
                                       k not in ['id', 'object_id', 'content_type', 'product_id']}

                        obj = self._model(**post_params, created_by=user)
                        obj.full_clean(exclude=['company'])
                        obj.save()
                        status = 'Done'
                        status_massage = '{} créé(e) sous le numéro {}'.format(self._model._meta.verbose_name.title(),
                                                                               str(obj.pk))
                        response = {'status': status,
                                    'status_message': status_massage
                                    }
                    return HttpResponse(json.dumps(response, cls=DjangoJSONEncoder), content_type='application/json')
                except ValidationError as e:
                    status = 'Error'
                    status_massage = json.dumps(dict(e), cls=DjangoJSONEncoder)
                    response = {'status': status, 'status_message': status_massage}
                    Error.objects.create(
                        created_by=user,
                        error=str(status_massage),
                        data=dict(request.POST),
                        error_type='User lack training',
                        error_name='ValidationError',
                    )
                    return HttpResponse(json.dumps(response), content_type='application/json')

                except Exception as e:
                    Error.objects.create(
                        created_by=user,
                        error=traceback.format_exc(),
                        data=dict(request.POST),
                        error_type='Bug',
                        error_name='Exception',
                    )
                    raise e
        else:
            return super(InventoryCountDetailAdmin, self).post(request)


class InventoryDetailJournal(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = InventoryCountDetail
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': True,
        'text_field': ['id'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['export']
    _fields = {
        'product_id':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/product_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': False,
                'tooltip': '',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch_id':
            {
                'title': 'Lot',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/batch_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot et l\'emplacement',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__product_id':
            {
                'title': 'ID Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__product__code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__product__product':
            {
                'title': 'Produit',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__batch_number':
            {
                'title': 'N° Lot',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__expiration_date':
            {
                'title': 'DDP',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__packaging':
            {
                'title': 'Colisage',
                'format': 'number',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__purchase_pu_ht_net':
            {
                'title': 'PU Achat',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__tva__tva_rate':
            {
                'title': 'TVA',
                'format': 'percent',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__shp':
            {
                'title': 'SHP',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__ppa_ht':
            {
                'title': 'PPA HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'quantity':
            {
                'title': 'Quantité',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'integer',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('quantity').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': [
                    {'value': 0, 'operator': '<', 'all_row': False, 'css_class': 'w3-text-red'}
                ]
            },
        'emplacement__emplacement':
            {
                'title': 'Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'emplacement_id':
            {
                'title': 'Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/emplacement_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Emplacement de destination',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'status_id':
            {
                'title': 'Status',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/product_status_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 3,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('status').blank,
                'tooltip': 'Nouveau statut',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'status__designation':
            {
                'title': 'Statut',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('status').blank,
                'tooltip': '',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'parent__id':
            {
                'title': 'N° Journal',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'hidden',
                'input_autocomplete_url': False,
                'input_validation_url': False,
                'input_attributes': {'get_parent_input': 'true'},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('parent_id').blank,
                'tooltip': 'parent_id',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'parent__observation':
            {
                'title': 'Observation',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'hidden',
                'input_autocomplete_url': False,
                'input_validation_url': False,
                'input_attributes': {'get_parent_input': 'true'},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('parent_id').blank,
                'tooltip': 'parent_id',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }

    _html_page = 'wms/transactions_templates/inventory_count_detail_admin_page.html'
    _url = 'inventory_detail_journal/'

    _short_title = 'Journal global'
    _title = 'Journal global d\'inventaire'
    _description = 'Etat global des produits inventoriés'

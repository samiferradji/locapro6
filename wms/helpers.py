from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.db.models import Sum, Count, When, IntegerField, Case, Q, F
from django.utils import timezone
from django.core.exceptions import ValidationError

from wms import parameters as wms_parameters
from wms.models import Inventory, WarehouseReservationDetail, Workload, TransferDetails, WorkloadExecution, \
    ExecutorTempo, ExternalTransferDetails, Transfer, ExternalTransfer, Purchase, Batch, InventoryCount, \
    InventoryCountDetail, PurchaseDetail


def quantity_is_allowed(inventory_object: object, quantity: int, warehouse_transaction: str):
    """Check if a quantity is allowed to be moved in the inventory

    This function protect inventory from negative values when moving a quantity in the inventory(transfer, selling)

    Parameters
    ----------
    inventory_object
        object instance of the inventory selected to be moved
    quantity
        Quantity to be moved
    warehouse_transaction
        The current transaction example: transfer, selling ...

    Returns
    -------
    bool
        True if the quantity is allowed, False if not

    """
    try:
        quantity = int(quantity)
    except ValueError:
        raise ValidationError(
            {'Quantité': 'La quantité doit être un chiffre entier. Exemple: 124, 66 ... '}
        )
    if warehouse_transaction == 'transfer':
        affected_quantity = 0
        allowed_quantity = 0
        if not wms_parameters.NEGATIVE_STOCK_PER_EMPLACEMENT_ALLOWED:
            inventory_similar_objects = Inventory.objects.filter(
                batch=inventory_object.batch,
                emplacement=inventory_object.emplacement,
                product_status=inventory_object.product_status
            )
            if not wms_parameters.ALLOW_COMING_QUANTITY_TRANSFER_TRANSACTION:
                inventory_similar_objects = inventory_similar_objects.filter(
                    Q(received=True) | (Q(received=False) & Q(quantity__lt=0)))

            elif wms_parameters.ALLOW_COMING_QUANTITY_TRANSFER_TRANSACTION:
                inventory_similar_objects = inventory_similar_objects.exclude(
                    (Q(received=False) & Q(quantity__lt=0)))

            if inventory_similar_objects.exists():
                allowed_quantity = inventory_similar_objects.aggregate(Sum('quantity'))['quantity__sum']

            valid_quantity = allowed_quantity - affected_quantity
            if valid_quantity >= quantity:
                return {'allowed': True}
            else:
                return {'allowed': False, 'valid_quantity': valid_quantity}
        elif not wms_parameters.NEGATIVE_STOCK_PER_BATCH_ALLOWED:
            inventory_similar_objects = Inventory.objects.filter(batch=inventory_object.batch)
            if not wms_parameters.ALLOW_COMING_QUANTITY_TRANSFER_TRANSACTION:
                inventory_similar_objects = inventory_similar_objects.filter(
                    Q(received=True) | (Q(received=False) & Q(quantity__lt=0)))

            elif wms_parameters.ALLOW_COMING_QUANTITY_TRANSFER_TRANSACTION:
                inventory_similar_objects = inventory_similar_objects.exclude(
                    (Q(received=False) & Q(quantity__lt=0)))

            if inventory_similar_objects.exists():
                allowed_quantity = inventory_similar_objects.aggregate(Sum('quantity'))['quantity__sum']

            valid_quantity = allowed_quantity - affected_quantity
            if valid_quantity >= quantity:
                return {'allowed': True}
            else:
                return {'allowed': False, 'valid_quantity': valid_quantity}
        elif not wms_parameters.NEGATIVE_STOCK_PER_PRODUCT_ALLOWED:
            inventory_similar_objects = Inventory.objects.filter(batch__product=inventory_object.batch.product)

            if not wms_parameters.ALLOW_COMING_QUANTITY_TRANSFER_TRANSACTION:
                inventory_similar_objects = inventory_similar_objects.filter(
                    Q(received=True) | (Q(received=False) & Q(quantity__lt=0)))

            elif wms_parameters.ALLOW_COMING_QUANTITY_TRANSFER_TRANSACTION:
                inventory_similar_objects = inventory_similar_objects.exclude(
                    (Q(received=False) & Q(quantity__lt=0)))

            if inventory_similar_objects.exists():
                allowed_quantity = inventory_similar_objects.aggregate(Sum('quantity'))['quantity__sum']

            valid_quantity = allowed_quantity - affected_quantity
            if valid_quantity >= quantity:
                return {'allowed': True}
            else:
                return {'allowed': False, 'valid_quantity': valid_quantity}
        else:
            return {'allowed': True}


def confirm_physical_movement(transaction_instance):
    """Set the inventory attribute 'received' to True to confirm the physical movement of a product

    This concernes transactions with inpact on the inventory.
    When inventory movements are created, they have the a 'False' received status. Then when received or confirmed
    their 'received' status is set to True

    Returns
    -------

    """
    Inventory.objects.filter(
        content_type=ContentType.objects.get_for_model(transaction_instance),
        object_id=transaction_instance.id
    ).update(received=True)


def confirm_transaction(transaction_instance):
    """Confirm a flew transaction by setting his document_status to 'Confirmed'

    Returns
    -------
    True if successful , Exception otherwise

    """
    with transaction.atomic():
        transaction_instance.document_status_id = wms_parameters.get_confirmed_document_status_id()
        transaction_instance.save()


def cancel_transaction(transaction_instance, transaction_id):
    """ Cancel a warehouse transaction

    Returns
    -------
    True if successful , Exception otherwise

    """
    with transaction.atomic():
        transaction_instance.document_status = 3  # TODO add the cancel status
        transaction_instance.save()
        Inventory.objects.filter(
            content_type=ContentType.objects.get_for_model(transaction_instance),
            object_id=transaction_id
        ).delete()
        Workload.objects.filter(
            content_type=ContentType.objects.get_for_model(transaction_instance),
            object_id=transaction_id
        ).delete()


def ship_transaction(transaction_instance):
    """Ship a flew transaction by setting his document_status to 'Shipped'

        Returns
        -------
        True if successful , Exception otherwise

        """
    transaction_instance.document_status_id = wms_parameters.DOCUMENT_SHIPPED_ID
    transaction_instance.save()


def receive_transaction(transaction_instance):
    """Receive a flew transaction by setting his document_status to 'Received'

            Returns
            -------
            True if successful , Exception otherwise

            """
    transaction_instance.document_status_id = wms_parameters.get_received_document_status_id()
    transaction_instance.save()


def receive_products_from_external_transfer(external_transfer_instance, user):
    """
    insert products received from external transfer into inventory
    Returns
    -------
    True if successful , Exception otherwise

    """
    object_details = ExternalTransferDetails.objects.filter(parent=external_transfer_instance)
    for obj in object_details:
        new_batch = obj.batch
        new_batch.pk = None
        new_batch.created_by = user
        new_batch.created_date = None
        new_batch.modified_date = None
        new_batch.save()
        new_inventory = Inventory(
            created_by=user,
            content_type=ContentType.objects.get_for_model(external_transfer_instance),
            object_id=external_transfer_instance.id,
            batch=new_batch,
            quantity=obj.quantity,
            product_status=obj.status,
            transaction='External Transfer in',
            emplacement_id=wms_parameters.get_default_reception_emplacement_id_for_external_transfers(user),
            received=True
        )
        new_inventory.save()


def workload_creation_rules(transaction_instance):
    """ returns parameters for workload creation and aggregation

    Parameters
    ----------
    transaction_instance:
        the current transaction instance
    Returns
    -------
        dict

    """
    if transaction_instance.flew_type_id in [1, 3, 4]:  # Transfer , complete_package_selling, status_changing
        return {
            'model': Transfer,
            'details_model': TransferDetails,
            'unpacked_units': {'from_emplacement__storage_type_id': wms_parameters.UNPACKED_UNITS_STORAGE_ID},
            'complete_package': {'from_emplacement__storage_type_id': wms_parameters.COMPLETE_PACKAGES_STORAGE_ID},
            'palletized_package': {'from_emplacement__storage_type_id': wms_parameters.PALLETIZED_PACKAGES_STORAGE_ID}
        }
    elif transaction_instance.flew_type_id in [2, 5]:  # Storing ,
        return {
            'model': Transfer,
            'details_model': TransferDetails,
            'unpacked_units': {'to_emplacement__storage_type_id': wms_parameters.UNPACKED_UNITS_STORAGE_ID},
            'complete_package': {'to_emplacement__storage_type_id': wms_parameters.COMPLETE_PACKAGES_STORAGE_ID},
            'palletized_package': {'to_emplacement__storage_type_id': wms_parameters.PALLETIZED_PACKAGES_STORAGE_ID}
        }
    elif transaction_instance.flew_type_id == 6:  # external transfer
        return {
            'model': ExternalTransfer,
            'details_model': ExternalTransferDetails,
            'unpacked_units': {'emplacement__storage_type_id': wms_parameters.UNPACKED_UNITS_STORAGE_ID},
            'complete_package': {'emplacement__storage_type_id': wms_parameters.COMPLETE_PACKAGES_STORAGE_ID},
            'palletized_package': {'emplacement__storage_type_id': wms_parameters.PALLETIZED_PACKAGES_STORAGE_ID}
        }
    elif transaction_instance.flew_type_id == 7:  # Purchase reception
        return {
            'model': Purchase,
            'details_model': PurchaseDetail,
            'unpacked_units': {'emplacement__storage_type_id': wms_parameters.UNPACKED_UNITS_STORAGE_ID},
            'complete_package': {'emplacement__storage_type_id': wms_parameters.COMPLETE_PACKAGES_STORAGE_ID},
            'palletized_package': {'emplacement__storage_type_id': wms_parameters.PALLETIZED_PACKAGES_STORAGE_ID}
        }
    elif transaction_instance.flew_type_id == 10:  # Inventory Count
        return {
            'model': InventoryCount,
            'details_model': InventoryCountDetail,
            'unpacked_units': {'emplacement__storage_type_id': wms_parameters.UNPACKED_UNITS_STORAGE_ID},
            'complete_package': {'emplacement__storage_type_id': wms_parameters.COMPLETE_PACKAGES_STORAGE_ID},
            'palletized_package': {'emplacement__storage_type_id': wms_parameters.PALLETIZED_PACKAGES_STORAGE_ID}
        }


def create_workload(transaction_instance=None, user=None, flew_type_id=None):
    creation_parameters = workload_creation_rules(transaction_instance)

    transaction_details = creation_parameters['details_model'].objects.filter(parent_id=transaction_instance).annotate(
        complete_package=Case(
            When(~Q(batch__packaging=0) & Q(**creation_parameters['complete_package']),
                 then=(F('quantity') / F('batch__packaging'))),
            default=0,
            output_field=IntegerField()
        ),
        palletized_package=Case(
            When(~Q(batch__packaging=0) & Q(**creation_parameters['palletized_package']),
                 then=(F('quantity') / F('batch__packaging'))),
            default=0,
            output_field=IntegerField()
        ),
        unpacked_units=Case(
            When(
                Q(batch__packaging=0), then=F('quantity')),
            When(
                ~Q(batch__packaging=0) & Q(**creation_parameters['unpacked_units']), then=F('quantity')),
            When(
                ~Q(batch__packaging=0) & ~Q(**creation_parameters['unpacked_units']),
                then=F('quantity') % F('batch__packaging')),
            default=0,
            output_field=IntegerField())
    )
    transaction_details = transaction_details.aggregate(
        quantity__sum=Sum('quantity'),
        unpacked_units__sum=Sum('unpacked_units'),
        complete_package__sum=Sum('complete_package', output_field=IntegerField()),
        palletized_package__sum=Sum('palletized_package', output_field=IntegerField()),
        lines__count=Count('id')
    )
    flew_type_id = transaction_instance.flew_type_id if not flew_type_id else flew_type_id
    new_obj = Workload(
        object_id=transaction_instance.id,
        content_type=ContentType.objects.get_for_model(transaction_instance),
        company=transaction_instance.company,
        created_by=transaction_instance.created_by,
        flew_type_id=flew_type_id,
        confirmed_by=user,
        confirmed_date=timezone.now(),
        total_units=transaction_details['quantity__sum'],
        lines=transaction_details['lines__count'],
        unpacked_units=transaction_details['unpacked_units__sum'],
        complete_packages=transaction_details['complete_package__sum'],
        palletized_packages=transaction_details['palletized_package__sum'],
    )
    new_obj.save()
    return new_obj


def update_workload_calculus(transaction_instance=None):
    creation_parameters = workload_creation_rules(transaction_instance)

    transaction_details = creation_parameters['details_model'].objects.filter(parent_id=transaction_instance).annotate(
        complete_package=Case(
            When(~Q(batch__packaging=0) & Q(**creation_parameters['complete_package']),
                 then=(F('quantity') / F('batch__packaging'))),
            default=0,
            output_field=IntegerField()
        ),
        palletized_package=Case(
            When(~Q(batch__packaging=0) & Q(**creation_parameters['palletized_package']),
                 then=(F('quantity') / F('batch__packaging'))),
            default=0,
            output_field=IntegerField()
        ),
        unpacked_units=Case(
            When(
                Q(batch__packaging=0), then=F('quantity')),
            When(
                ~Q(batch__packaging=0) & Q(**creation_parameters['unpacked_units']), then=F('quantity')),
            When(
                ~Q(batch__packaging=0) & ~Q(**creation_parameters['unpacked_units']),
                then=F('quantity') % F('batch__packaging')),
            default=0,
            output_field=IntegerField())
    )
    transaction_details = transaction_details.aggregate(
        quantity__sum=Sum('quantity'),
        unpacked_units__sum=Sum('unpacked_units'),
        complete_package__sum=Sum('complete_package', output_field=IntegerField()),
        palletized_package__sum=Sum('palletized_package', output_field=IntegerField()),
        lines__count=Count('id'))
    try:
        current_workload = Workload.objects.get(
            object_id=transaction_instance.id,
            content_type=ContentType.objects.get_for_model(transaction_instance),
            flew_type_id=transaction_instance.flew_type_id
        )
    except Workload.DoesNotExist:
        print(transaction_instance.id)
        print(ContentType.objects.get_for_model(transaction_instance))
        raise Workload.DoesNotExist

    current_workload.flew_type_id = transaction_instance.flew_type_id
    current_workload.total_units = transaction_details['quantity__sum']
    current_workload.lines = transaction_details['lines__count']
    current_workload.unpacked_units = transaction_details['unpacked_units__sum']
    current_workload.complete_packages = transaction_details['complete_package__sum']
    current_workload.palletized_packages = transaction_details['palletized_package__sum']
    current_workload.save()
    return True


def dispatch_workload(workload_instance):
    with transaction.atomic():
        executors_queryset = ExecutorTempo.objects.filter(
            content_type=workload_instance.content_type,
            object_id=workload_instance.object_id
        ).all()
        if executors_queryset.count() > 3:
            raise ValidationError(
                {'Liste des exécutants': 'Le nombre des exécutants ne doit pas dépassé trois(3) personnes '}
            )
        elif executors_queryset.count() < 1:
            raise ValidationError(
                {'Liste des exécutants': 'Vous devez insérer au moins un (1) exécutant '}
            )
        else:

            for executor in executors_queryset:
                new_obj = WorkloadExecution(
                    workload=workload_instance,
                    employee=executor.employee,
                    group_workforce=len(executors_queryset),
                    company=workload_instance.company,
                    created_by=workload_instance.created_by
                )
                new_obj.save()
            executors_queryset.delete()


def get_product_emplacement_in_picking_store(product_instance):
    """
    Get the default emplacement for a products picking. Used when re-feeding picking shelves
    Returns
    -------
        emplacement: object
            The emplacement object

    """
    return product_instance.picking_emplacement if product_instance.picking_emplacement else wms_parameters.get

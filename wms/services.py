import csv
import json
import traceback
from datetime import timedelta, datetime
from functools import reduce
from operator import and_, or_

from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied, ValidationError
from django.core.serializers.json import DjangoJSONEncoder
from django.db import transaction
from django.db.models import F, When, Q, Case, IntegerField, Max, Sum, OuterRef, Subquery, CharField, \
    ExpressionWrapper
from django.db.models.functions import Concat, Coalesce
from django.http import HttpResponse
from django.utils import timezone
from django.views.generic.base import View

from common.models import UserProfile, Employee, Error
from common.services import StandardTable, StandardAdminApi
from .models import Purchase, PurchaseDetailTempo, PharmaceuticalInvoice, PharmaceuticalSupplier, \
    Product, Inventory, Transfer, WarehouseReservationDetail, TransferDetails, LogisticFlowType, Store, Emplacement, \
    ProductStatus, ExecutorTempo, StorageType, \
    ExternalTransferDetails, ExternalTransfer, Workload, WorkloadExecution, InventoryCountDetail, Batch, InventoryCount, \
    Area, Dci, Laboratory, PharmaceuticalForm
from .helpers import update_workload_calculus


def update_workload(request):
    with transaction.atomic():
        for instance in Transfer.objects.all():
            if instance.document_status_id not in [1, 6]:
                update_workload_calculus(instance)
        for instance in Purchase.objects.filter(created_date__gte='2019-01-01'):
            if instance.document_status_id not in [1, 6]:
                update_workload_calculus(instance)
        for instance in ExternalTransfer.objects.all():
            if instance.document_status_id not in [1, 6]:
                update_workload_calculus(instance)

    return HttpResponse('Workload calculus updated')


class DciList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Dci
    _model_data = {
        'enumeration': True,
        'sort': ['designation'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = []
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation commune internationale',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('code').blank,
                'tooltip': 'Code unique de la dci',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _url = 'dci_list/'
    _api_permission_type = '.view_'


class AreaList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Area
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = []
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 7,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation de la zone de stockage',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _url = 'area_list/'
    _api_permission_type = '.view_'


class StoreList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Store
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = []
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation du magasin',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'storage_type_id':
            {
                'title': 'Types d\'entrepôsage',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/storage_type_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('storage_type').blank,
                'tooltip': 'Exemple : Palette, colis, vrac ...',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'storage_type__designation':
            {
                'title': 'Types d\'entrepôsage',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('storage_type').blank,
                'tooltip': 'Exemple : Palette, colis, vrac ...',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _url = 'store_list/'
    _api_permission_type = '.view_'


class EmplacementList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Emplacement
    _model_data = {
        'enumeration': True,
        'sort': ['emplacement', 'store'],
        'reverse_sort': False,
        'text_field': ['emplacement', 'store__designation'],
        'table_filter': False,
        'date_range_filter': False,
    }
    _default_actions = []
    _fields = {
        'emplacement':
            {
                'title': 'Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Désignation du flux',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'store__designation':
            {
                'title': 'Magasin',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('store').blank,
                'tooltip': '',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'storage_type__designation':
            {
                'title': 'Type de stockage',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('storage_type').blank,
                'tooltip': 'Description du flux',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            }
    }
    _url = 'emplacement_list/'
    _api_permission_type = '.view_'


class PharmaceuticalFormList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = PharmaceuticalForm
    _model_data = {
        'enumeration': True,
        'sort': ['designation'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = []
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation de la forme galénique',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'short_designation':
            {
                'title': 'Abréviation',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('short_designation').blank,
                'tooltip': 'Abréviation de la désignation de la forme',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _url = 'pharmaceutical_form_list/'
    _api_permission_type = '.view_'


class StorageTypeList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = StorageType
    _model_data = {
        'enumeration': True,
        'sort': ['id'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': 7,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation du type d\'entrepôsage. Exemple : Palette, Colis, Vrac ...',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _url = 'storage_type_list/'
    _api_permission_type = '.view_'


class LaboratoryList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Laboratory
    _model_data = {
        'enumeration': True,
        'sort': ['designation'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = []
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Nom du laboratoire',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('code').blank,
                'tooltip': 'Code unique du laboratoire',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _url = 'laboratory_list/'
    _api_permission_type = '.view_'


class PharmaceuticalSupplierList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = PharmaceuticalSupplier
    _model_data = {
        'enumeration': True,
        'sort': ['supplier'],
        'reverse_sort': False,
        'text_field': ['code', 'supplier'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = []
    _fields = {
        'code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('code').blank,
                'tooltip': 'Code unique du fournisseur',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'supplier':
            {
                'title': 'Nom du fournisseur',
                'format': 'text',
                'width': 10,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('supplier').blank,
                'tooltip': 'Le nom du fournisseur',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            }
    }
    _url = 'pharmaceutical_supplier_list/'
    _api_permission_type = '.view_'

    def get_queryset(self):
        return super(PharmaceuticalSupplierList, self).get_queryset().filter(active=True)


class PharmaceuticalInvoicesList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = PharmaceuticalInvoice
    _model_data = {
        'enumeration': True,
        'sort': ['created_date'],
        'reverse_sort': True,
        'text_field': ['invoice_number', 'invoice_date', 'supplier__supplier'],
        'table_filter': True,
        'date_range_filter': True,
    }
    _default_actions = []
    _fields = {
        'supplier__supplier':
            {
                'title': 'Nom du fournisseur',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('supplier').blank,
                'tooltip': 'Le nom du fournisseur',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'invoice_number':
            {
                'title': 'N° Facture',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('invoice_number').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'invoice_date':
            {
                'title': 'Date Facture',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'date',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('invoice_date').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'value_ttc':
            {
                'title': 'Montant TTC',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'money',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 8,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('value_ttc').blank,
                'tooltip': 'Le montant total de la facture avec toutes taxes comprises',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'document_status__status':
            {
                'title': 'Statut',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'date',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 9,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('document_status').blank,
                'tooltip': 'Les status actuel du document',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _url = 'pharmaceutical_invoice_list/'


class ProductStatusList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = ProductStatus
    _model_data = {
        'enumeration': True,
        'sort': ['designation'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = []
    _fields = {
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': False,
                'column_order': 2,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation du statut. Exemple : Conforme, périmé...',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _url = 'product_status_list/'
    _api_permission_type = '.view_'


class ProductList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Product
    _model_data = {
        'enumeration': False,
        'sort': ['product'],
        'reverse_sort': False,
        'text_field': ['product'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = []
    _fields = {
        'code':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('code').blank,
                'tooltip': 'Code unique du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'product':
            {
                'title': 'Produit',
                'format': 'text',
                'width': 7,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('product').blank,
                'tooltip': 'Le nom commercial du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            }
    }
    _html_page = ''
    _url = 'product_list/'
    _api_permission_type = '.view_'


class InternalMovementLogisticFlowTypeList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = LogisticFlowType
    _model_data = {
        'enumeration': False,
        'sort': ['id'],
        'reverse_sort': False,
        'text_field': ['designation'],
        'table_filter': False,
        'date_range_filter': False,
    }
    _default_actions = []
    _fields = {
        'id':
            {
                'title': 'ID',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('id').blank,
                'tooltip': 'ID du flux',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'designation':
            {
                'title': 'Désignation',
                'format': 'text',
                'width': 7,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('designation').blank,
                'tooltip': 'Désignation du flux',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            }
    }
    _url = 'internal_movement_logistic_flew_type_list/'
    _api_permission_type = '.view_'

    def get_queryset(self):
        queryset = self._model.objects.all().filter(id__in=[1, 2, 3, 4, 5])
        return queryset


class InventoryForInternalTransfer(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Inventory
    _model_data = {
        'enumeration': False,
        'sort': ['batch__expiration_date', 'emplacement__emplacement'],
        'reverse_sort': False,
        'text_field': ['batch__batch_number', 'batch__expiration_date', 'batch__ppa_ht'],
        'table_filter': True,
        'date_range_filter': False,
    }
    _default_actions = []
    _fields = {
        'batch__batch_number':
            {
                'title': 'N° Lot',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__expiration_date':
            {
                'title': 'DDP',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'La date de péremption du produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': [
                    {'value': timezone.now().date(), 'operator': '>', 'all_row': False, 'css_class': 'w3-text-red'},
                    {'value': timezone.now().date() + timedelta(days=90), 'operator': '>', 'all_row': False,
                     'css_class': 'w3-text-orange'}
                ]
            },
        'batch__shp':
            {
                'title': 'SHP',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'SHP du produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__ppa_ht':
            {
                'title': 'PPA',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Le Prix Public Algérien du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'emplacement__emplacement':
            {
                'title': 'Emplacement',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Le numéro de la facture',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch__packaging':
            {
                'title': 'Colisage',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('batch').blank,
                'tooltip': 'Boites par colis',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },

        'quantity_sum':
            {
                'title': 'Quantité',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('quantity').blank,
                'tooltip': 'La quantité totale',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': [
                    {'value': 0, 'operator': '>', 'all_row': False, 'css_class': 'w3-text-red'}
                ]
            },

        'in_progress_in':
            {
                'title': 'En E',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('quantity').blank,
                'tooltip': 'Entrés en cours',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': False
            },
        'in_progress_out':
            {
                'title': 'En S',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('quantity').blank,
                'tooltip': 'Entrés en cours',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': False
            },
        'complete_package':
            {
                'title': 'Colis',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Nombre de colis complets',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'unpacked_units':
            {
                'title': 'Vrac',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Nombre de boite en VRAC',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'affected':
            {
                'title': 'Affecté',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 2,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('quantity').blank,
                'tooltip': 'Entrés en cours',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': False
            },
        'product_status__designation':
            {
                'title': 'Statut',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 0,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('emplacement').blank,
                'tooltip': 'Le status du produit. Exemple: Périmé, Conforme',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },

    }
    _url = 'inventory_for_internal_transfer/'
    _api_permission_type = '.view_'

    def get_queryset(self):
        queryset = self._model.objects.values(
            'emplacement',
            'product_status',
            'batch_id'
        ).annotate(
            pk=Max('id'),
            quantity_sum=Sum(Case(When(received=True, then='quantity'), default=0))
        ).annotate(
            in_progress_in=Sum(Case(
                When(Q(quantity__gt=0) & Q(received=False) & Q(sent=True), then=F('quantity')), default=0
            ))
        ).annotate(
            in_progress_out=Sum(Case(
                When(Q(quantity__lt=0) & Q(received=False) & Q(sent=True), then=F('quantity')), default=0
            ))
        ).annotate(
            affected=Sum(Case(
                When(Q(quantity__lt=0) & Q(received=False) & Q(sent=False), then=F('quantity')), default=0
            ))
        ).annotate(
            complete_package=Case(
                When(~Q(batch__packaging=0), then=(F('quantity_sum') / F('batch__packaging'))),
                default=0,
                output_field=IntegerField()
            ),
            unpacked_units=Case(
                When(~Q(batch__packaging=0), then=F('quantity_sum') % F('batch__packaging')), default=F('quantity_sum')
            )
        ).filter(Q(quantity_sum__gt=0) | Q(in_progress_in__gt=0))
        return queryset


class BatchList(StandardAdminApi):
    """
    Standard view to manages database tables Data
    """
    _model = Batch
    _model_data = {
        'enumeration': True,
        'sort': ['created_date'],
        'reverse_sort': True,
        'text_field': ['batch_number'],
        'table_filter': False,
        'date_range_filter': False,
    }
    _form_width = '500px'
    _default_actions = ['add', 'change', 'delete', 'export']
    _fields = {
        'product_id':
            {
                'title': 'Code',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': False,
                'input': True,
                'input_type': 'autocomplete',
                'input_autocomplete_url': 'wms/product_list/',
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 1,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('product').blank,
                'tooltip': 'Le nom produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'batch_number':
            {
                'title': 'N° Lot',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 4,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('batch_number').blank,
                'tooltip': 'Le numéro du lot',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'expiration_date':
            {
                'title': 'DDP',
                'format': 'date',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'date',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 5,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('expiration_date').blank,
                'tooltip': 'La date de péremption du produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': [
                    {'value': timezone.now().date(), 'operator': '>', 'all_row': False, 'css_class': 'w3-text-red'},
                    {'value': timezone.now().date() + timedelta(days=90), 'operator': '>', 'all_row': False,
                     'css_class': 'w3-text-orange'}
                ]
            },
        'supplier__supplier':
            {
                'title': 'Fournisseur',
                'format': 'text',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 7,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('supplier').blank,
                'tooltip': 'Le fournisseur du Lot',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'packaging':
            {
                'title': 'Colisage',
                'format': 'integer',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'number',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 7,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('packaging').blank,
                'tooltip': 'Le numéro du lot',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'purchase_pu_ht_net':
            {
                'title': 'PU Achat Net',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 8,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('purchase_pu_ht_net').blank,
                'tooltip': 'Le prix d\'achat net',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'tva__tva_rate':
            {
                'title': 'TVA',
                'format': 'percent',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': False,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 10,
                'input_order': 1,
                'edit': False,
                'required': not _model._meta.get_field('tva').blank,
                'tooltip': 'Taux de TVA',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'selling_pu_ht':
            {
                'title': 'PU Vente HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 11,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('selling_pu_ht').blank,
                'tooltip': 'Le prix de vente du produit en hors taxes',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'shp':
            {
                'title': 'SHP',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 12,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('shp').blank,
                'tooltip': 'SHP du produit',
                'search': False,
                'search_type': '__icontains',
                'conditional_format': None
            },
        'ppa_ht':
            {
                'title': 'PPA HT',
                'format': 'money',
                'width': False,
                'column_order': 1,
                'display': True,
                'input': True,
                'input_type': 'text',
                'input_autocomplete_url': False,
                'input_validation_url': False, 'input_attributes': {},
                'input_group': 13,
                'input_order': 1,
                'edit': True,
                'required': not _model._meta.get_field('ppa_ht').blank,
                'tooltip': 'Le Prix Public Algérien du produit',
                'search': True,
                'search_type': '__icontains',
                'conditional_format': None
            },
    }
    _url = 'batch_list/'
    _api_permission_type = '.view_'


enabled_services = [
    {'service': AreaList},
    {'service': DciList},
    {'service': StoreList},
    {'service': StorageTypeList},
    {'service': ProductList},
    {'service': ProductStatusList},
    {'service': PharmaceuticalFormList},
    {'service': InternalMovementLogisticFlowTypeList},
    {'service': InventoryForInternalTransfer},
    {'service': EmplacementList},
    {'service': LaboratoryList},
    {'service': PharmaceuticalSupplierList},
    {'service': BatchList},
    {'service': PharmaceuticalInvoicesList},
]


class WorkloadStatistics(StandardAdminApi):

    def get_queryset(self):
        #from_date = "2019-10-16 00:00:00"
        #to_date = "2019-11-15 23:59:59"
        
        from_date = self.request.GET['from_date']
        to_date = self.request.GET['to_date']

        _creation_points = Workload.objects.filter(
            created_by_id=OuterRef('userprofile__user__id')
        ).exclude(
            created_date__gt=to_date
        ).exclude(
            created_date__lt=from_date
        ).values(
            'created_by_id').annotate(
            lines_created=Sum('lines'),
            total_points_for_lines_created=Sum(ExpressionWrapper(
                (F('lines') * F('flew_type__point_line_typing')) / 10000,
                output_field=IntegerField())),
        ).values(
            'total_points_for_lines_created',
        )
        _confimation_points = Workload.objects.filter(
            confirmed_by_id=OuterRef('userprofile__user__id')
        ).exclude(
            created_date__gt=to_date
        ).exclude(
            created_date__lt=from_date
        ).values(
            'confirmed_by_id').annotate(
            complete_packages_confirmed=Sum('complete_packages'),
            unpacked_units_confirmed=Sum('unpacked_units'),
            palletized_packages_confirmed=Sum('palletized_packages'),
            lines_confirmed=Sum('lines'),
        ).annotate(
            points_for_lines_controled=Sum(ExpressionWrapper(
                F('lines') * F('flew_type__point_line_control'),
                output_field=IntegerField())),
            points_unpacked_units_confirmed=Sum(ExpressionWrapper(
                F('unpacked_units') * F('flew_type__point_sku_control'),
                output_field=IntegerField())),
            points_for_complete_package_controled=Sum(ExpressionWrapper(
                F('complete_packages') * F('flew_type__point_package_validation'),
                output_field=IntegerField())),
            points_for_palette_controled=Sum(ExpressionWrapper(
                F('palletized_packages') * F('flew_type__point_palette_control'),
                output_field=IntegerField()))

        ).annotate(
            total_confirmation_points=ExpressionWrapper(
                (F('points_for_lines_controled') +
                 F('points_unpacked_units_confirmed') +
                 F('points_for_complete_package_controled') +
                 F('points_for_palette_controled')) / 10000,
                output_field=IntegerField())
        ).values(
            'total_confirmation_points',

        )

        _execution_points = WorkloadExecution.objects.filter(
            employee_id=OuterRef('id')
        ).exclude(
            created_date__gt=to_date
        ).exclude(
            created_date__lt=from_date
        ).values('employee_id').annotate(

            # Lines

            points_for_lines_executed=Sum(ExpressionWrapper(
                F('workload__lines') * F('workload__flew_type__point_line_execution') / F('group_workforce'),
                output_field=IntegerField())),
            # Unpacked units

            points_for_sku_executed=Sum(ExpressionWrapper(
                F('workload__unpacked_units') * F('workload__flew_type__point_sku_execution') / F('group_workforce'),
                output_field=IntegerField())),

            # Complete packages

            points_for_complete_package_executed=Sum(ExpressionWrapper(
                F('workload__complete_packages') * F('workload__flew_type__point_package_execution') / F(
                    'group_workforce'),
                output_field=IntegerField())),

            # Palettes

            points_for_palette_executed=Sum(ExpressionWrapper(
                F('workload__palletized_packages') * F('workload__flew_type__point_palette_execution'),
                output_field=IntegerField())),
        ).annotate(
            total_execution_points=Sum(ExpressionWrapper(
                (F('workload__lines') * F('workload__flew_type__point_line_execution') +
                 F('workload__unpacked_units') * F('workload__flew_type__point_sku_execution') +
                 F('workload__complete_packages') * F('workload__flew_type__point_package_execution') +
                 F('workload__palletized_packages') * F('workload__flew_type__point_palette_execution')) / 10000
                ,
                output_field=IntegerField()))

        ).values(
            'total_execution_points',
        )

        query = Employee.objects.values(
            'code'
        ).annotate(
            total_typing_points=Coalesce(Subquery(_creation_points), 0),
            total_execution_points=Coalesce(Subquery(_execution_points), 0),
            total_confirmation_points=Coalesce(Subquery(_confimation_points), 0)
        ).annotate(
            total_points=ExpressionWrapper(
                F('total_typing_points') + F('total_execution_points') + F('total_confirmation_points'),
                output_field=IntegerField())
        ).values(
            'code',
            'name',
            'function',
            'total_typing_points',
            'total_execution_points',
            'total_confirmation_points',
            'total_points',
        ).filter(
            total_points__gt=0
        )
        return list(query)

    def get(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        response = json.dumps((queryset), cls=DjangoJSONEncoder)
        return HttpResponse(response, content_type='application/json')

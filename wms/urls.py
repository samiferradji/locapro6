from django.urls import path

from locapro6 import settings
from wms import views, services, prints

if not settings.MIGRATION_MODE:
    from .menus import menu_list
    from .services import enabled_services

urlpatterns = []

# Common
urlpatterns += [
    #     path('list_of_pharmaceutical_suppliers/', services.ListOfPharmaceuticalSuppliers.as_view()),
    #     path('list_of_product_combo/', services.ListOfProducts.as_view()),
    #     path('list_of_logistic_flew_combo/', services.ListOfLogisticFlewTypes.as_view()),
    #     path('list_of_from_store_authorized_combo/', services.ListOfFromStoreAuthorized.as_view()),
    #     path('list_of_to_store_authorized_combo/', services.ListOfFromStoreAuthorized.as_view()),
    #     path('list_of_emplacement_combo/', services.ListOfEmplacements.as_view()),
    #     path('list_of_product_status_combo/', services.ListOfProductStatus.as_view()),
    path('workload_statistics/', services.WorkloadStatistics.as_view()),
    path('update_workload_calculus/', services.update_workload),
    #     path('upload_invoice_scan/', views.upload_invoice_scan),
    #
]
#
# # Purchase
urlpatterns += [path('print_pharmaceutical_purchase_receipt/', prints.print_pharmaceutical_purchase_receipt)
                #     path('list_of_pharmaceutical_purchases_details/', services.ListOfPurchaseDetails.as_view()),
                #     path('list_of_pharmaceutical_purchases_details_tempo/', services.ListOfPurchaseDetailsTempo.as_view()),
                #     path('list_of_pharmaceutical_purchases/', services.ListOfPurchase.as_view()),
                #     path('pharmaceutical_purchases_transactions/', views.pharmaceutical_purchase_transactions),
                #     path('pharmaceutical_purchases_history/', views.pharmaceutical_purchase_history),

                #     path('upload_purchase_receipt_scan/', views.upload_purchase_receipt_scan),
                ]
#
# # Purchase confirmation
# urlpatterns += [
#     path('pharmaceutical_purchase_confirmation/', views.pharmaceutical_purchase_confirmation),
#     path('list_of_temporary_executors_for_pharmaceutical_purchase/',
#          services.ListOfTemporaryExecutorsForPharmaceuticalPurchase.as_view()),
# ]
#
# # Invoices management
# urlpatterns += [
#     path('pharmaceutical_invoices_view/', views.pharmaceutical_invoices),
#     path('pharmaceutical_invoices_history/', services.HistoryOfPharmaceuticalInvoices.as_view()),
#     path('list_of_pharmaceutical_invoices_combo/', services.ListOfPharmaceuticalInvoices.as_view()),
#
# ]
#
# # Inventory reports
# urlpatterns += [
#     path('inventory_by_emplacement_view', views.inventory_by_emplacement),
#     path('inventory_by_emplacement', services.InventoryByEmplacement.as_view()),
#     path('export_inventory_by_emplacement', services.ExportInventoryByEmplacement.as_view()),
# ]
#
# # Transfer
urlpatterns += [
    #     path('list_of_transfer_details/', services.ListOfTransferDetails.as_view()),
    #     path('list_of_transfer_details_tempo/', services.ListOfTransferDetailsTempo.as_view()),
    #     path('list_of_transfer/', services.ListOfTransfer.as_view()),
    #     path('transfer_transactions/', views.transfer_transactions),
    #     path('inventory_for_transfer/', services.ListOfAvailableProductsForTransfer.as_view()),
    #     path('transfer_history/', views.transfer_history),
    path('print_transfer_order/', prints.print_transfer_order),
    path('print_pharmaceutical_inventory_count/', prints.print_pharmaceutical_inventory_count_receipt),
    path('print_external_transfer_order/', prints.print_external_transfer_order)
]
#
# # Transfer confirmation
# urlpatterns += [
#     path('warehouse_transfer_confirmation/', views.warehouse_transfer_confirmation),
#     path('list_of_temporary_executors/', services.ListOfTemporaryExecutorsForTransfer.as_view()),
#
# ]
#
# # External Transfer
# urlpatterns += [
#     path('list_of_external_transfer_details/', services.ListOfExternalTransferDetails.as_view()),
#     path('list_of_external_transfer_details_tempo/', services.ListOfExternalTransferDetailsTempo.as_view()),
#     path('list_of_external_transfer/', services.ListOfExternalTransfer.as_view()),
#     path('external_transfer_transactions/', views.external_transfer_transactions),
#     path('external_transfer_history/', views.external_transfer_history),
#     path('print_external_transfer_order/', prints.print_external_transfer_order)
# ]
#
# # External Transfer confirmation, shipping and reception
# urlpatterns += [
#     path('external_transfer_confirmation/', views.external_transfer_confirmation),
#     path('external_transfer_shipping/', views.external_transfer_shipping),
#     path('external_transfer_reception/', views.external_transfer_reception),
#     path('list_of_temporary_executors_for_external_transfer/',
#          services.ListOfTemporaryExecutorsForExternalTransfer.as_view()),
#     path('export_external_transfer_data/', services.ExportExternalTransferData.as_view()),
#     path('upload_external_transfer/', interface_view.upload_external_transfer),
# ]

if not settings.MIGRATION_MODE:
    urlpatterns += [path(menu['service']._url, menu['service'].as_view()) for menu in menu_list if menu['service']]
    urlpatterns += [path(service['service']._url, service['service'].as_view()) for service in enabled_services]

from common import helpers as common_helpers
from wms import views as wms_views

menu_list = [
    # Pharmaceutical purchases
    {
        'id': 300,
        'text': 'Achats Pharmaceutiques',
        'service': False,
        'icon': '<i class="fas fa-chevron-right"></i>',
        'state': '',
        'order': 0,
        'parent_id': None
    },
    {
        'id': 301,
        'text': 'Tables de référence',
        'service': False,
        'icon': '<i class="fas fa-chevron-right"></i>',
        'state': '',
        'order': 0,
        'parent_id': 300
    },
    {
        'id': 302,
        'text': '',
        'service': wms_views.DciAdmin,
        'icon': '<i class="fas fa-pills"></i>',
        'state': '',
        'order': 0,
        'parent_id': 301
    },
    {
        'id': 303,
        'text': '',
        'service': wms_views.PharmaceuticalFormAdmin,
        'icon': '<i class="fas fa-prescription-bottle"></i>',
        'state': '',
        'order': 0,
        'parent_id': 301
    },
    {
        'id': 304,
        'text': '',
        'service': wms_views.ProductStatusAdmin,
        'icon': '<i class="fas fa-info-circle"></i>',
        'state': '',
        'order': 0,
        'parent_id': 301
    },
    {
        'id': 305,
        'text': '',
        'service': wms_views.LaboratoryAdmin,
        'icon': '<i class="fas fa-vials"></i>',
        'state': '',
        'order': 0,
        'parent_id': 301
    },
    {
        'id': 306,
        'text': '',
        'service': wms_views.PharmaceuticalSupplierAdmin,
        'icon': '<i class="fas fa-industry"></i>',
        'state': '',
        'order': 0,
        'parent_id': 301
    },
    {
        'id': 307,
        'text': 'Transactions',
        'service': False,
        'icon': '<i class="fas fa-chevron-right"></i>',
        'state': '',
        'order': 0,
        'parent_id': 300
    },
    {
        'id': 308,
        'text': '',
        'service': wms_views.PharmaceuticalInvoicesAdmin,
        'icon': '<i class="fas fa-file"></i>',
        'state': '',
        'order': 0,
        'parent_id': 307
    },
    {
        'id': 309,
        'text': '',
        'service': wms_views.PurchaseAdmin,
        'icon': '<i class="fas fa-box-open"></i>',
        'state': '',
        'order': 0,
        'parent_id': 307
    },
    {
        'id': 310,
        'text': '',
        'service': wms_views.PurchaseDetailsAdmin,
        'icon': '<i class="fas fa-bars"></i>',
        'state': 'hidden',
        'order': 0,
        'parent_id': 307
    },
    {
        'id': 311,
        'text': '',
        'service': '',
        'icon': '<i class="fas fa-truck-loading"></i>',
        'state': 'hidden',
        'order': 0,
        'parent_id': 307
    },
    {
        'id': 312,
        'text': '',
        'service': '',
        'icon': '<i class="fas fa-bars"></i>',
        'state': 'hidden',
        'order': 0,
        'parent_id': 307
    },
    {
        'id': 314,
        'text': '',
        'service': wms_views.ProductAdmin,
        'icon': '<i class="fas fa-prescription-bottle"></i>',
        'state': '',
        'order': 0,
        'parent_id': 301
    },
    {
        'id': 315,
        'text': '',
        'service': wms_views.BatchAdmin,
        'icon': '<i class="fas fa-circle-notch"></i>',
        'state': '',
        'order': 0,
        'parent_id': 301
    },

    #  Warehouse

    {
        'id': 400,
        'text': 'Entrepôt',
        'service': False,
        'icon': '<i class="fas fa-chevron-right"></i>',
        'state': '',
        'order': 0,
        'parent_id': None
    },
    {
        'id': 401,
        'text': 'Tables de référence',
        'service': False,
        'icon': '<i class="fas fa-chevron-right"></i>',
        'state': '',
        'order': 0,
        'parent_id': 400
    },
    {
        'id': 402,
        'text': '',
        'service': wms_views.StoreAdmin,
        'icon': '<i class="fas fa-archway"></i>',
        'state': '',
        'order': 0,
        'parent_id': 401
    },
    {
        'id': 403,
        'text': '',
        'service': wms_views.AreaAdmin,
        'icon': '<i class="fas fa-map"></i>',
        'state': '',
        'order': 0,
        'parent_id': 401
    },
    {
        'id': 404,
        'text': '',
        'service': wms_views.LogisticFlowTypeAdmin,
        'icon': '<i class="fas fa-location-arrow"></i>',
        'state': '',
        'order': 0,
        'parent_id': 401
    },
    {
        'id': 405,
        'text': '',
        'service': wms_views.EmplacementAdmin,
        'icon': '<i class="fas fa-map-marker-alt"></i>',
        'state': '',
        'order': 0,
        'parent_id': 401
    },
    {
        'id': 406,
        'text': 'Stock',
        'service': None,
        'icon': '<i class="fas fa-chevron-right"></i>',
        'state': '',
        'order': 0,
        'parent_id': 400
    },
    {
        'id': 407,
        'text': '',
        'service': wms_views.InventoryAdmin,
        'icon': '<i class="fas fa-boxes"></i>',
        'state': '',
        'order': 0,
        'parent_id': 406
    },
    {
        'id': 408,
        'text': '',
        'service': wms_views.ExecutorTempoAdmin,
        'icon': '<i class="fas fa-boxes"></i>',
        'state': 'hidden',
        'order': 0,
        'parent_id': 406
    },

    #  Internal mouvment
    {
        'id': 409,
        'text': 'Mouvements internes',
        'service': False,
        'icon': '<i class="fas fa-chevron-right"></i>',
        'state': '',
        'order': 0,
        'parent_id': 400
    },
    {
        'id': 410,
        'text': '',
        'service': wms_views.TransferAdmin,
        'icon': '<i class="fas fa-dolly"></i>',
        'state': '',
        'order': 0,
        'parent_id': 409
    },
    {
        'id': 411,
        'text': '',
        'service': wms_views.TransferDetailsAdmin,
        'icon': '<i class="fas fa-boxes"></i>',
        'state': 'hidden',
        'order': 0,
        'parent_id': 409
    },
    {
        'id': 412,
        'text': '',
        'service': '',
        'icon': '<i class="fas fa-plus"></i>',
        'state': 'hidden',
        'order': 0,
        'parent_id': 409
    },
    {
        'id': 413,
        'text': '',
        'service': wms_views.WarehouseReservationDetailAdmin,
        'icon': '<i class="fas fa-boxes"></i>',
        'state': 'hidden',
        'order': 0,
        'parent_id': 409
    },
    #  Inventory count
    {
        'id': 414,
        'text': 'Inventaires',
        'service': False,
        'icon': '<i class="fas fa-chevron-right"></i>',
        'state': '',
        'order': 0,
        'parent_id': 400
    },
    {
        'id': 415,
        'text': '',
        'service': wms_views.InventoryCountAdmin,
        'icon': '<i class="fas fa-box-open"></i>',
        'state': '',
        'order': 0,
        'parent_id': 414
    },
    {
        'id': 416,
        'text': '',
        'service': wms_views.InventoryCountDetailAdmin,
        'icon': '<i class="fas fa-dolly"></i>',
        'state': 'hidden',
        'order': 0,
        'parent_id': 414
    },
    {
        'id': 417,
        'text': '',
        'service': wms_views.InventoryDetailJournal,
        'icon': '<i class="fas fa-bars"></i>',
        'state': '',
        'order': 0,
        'parent_id': 414
    },
    #  External movement
    {
        'id': 418,
        'text': 'Transferts entre filiales',
        'service': False,
        'icon': '<i class="fas fa-chevron-right"></i>',
        'state': '',
        'order': 0,
        'parent_id': 400
    },
    {
        'id': 419,
        'text': '',
        'service': wms_views.ExternalTransferAdmin,
        'icon': '<i class="fas fa-exchange-alt"></i>',
        'state': '',
        'order': 0,
        'parent_id': 418
    },
    {
        'id': 420,
        'text': '',
        'service': wms_views.ExternalTransferDetailsAdmin,
        'icon': '<i class="fas fa-arrow-right"></i>',
        'state': 'hidden',
        'order': 0,
        'parent_id': 418
    },
]
common_helpers.add_new_menus(menu_list)
common_helpers.update_menu(menu_list)

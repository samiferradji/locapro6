from django.db.models import F, Q, When, IntegerField, Case
from django.http import HttpResponse
from django.shortcuts import render

from .models import Purchase, Transfer, TransferDetails, ExternalTransfer, ExternalTransferDetails, \
    InventoryCount, InventoryCountDetail, PurchaseDetail
from common import parameters as common_params
from wms import parameters as wms_params


def print_pharmaceutical_purchase_receipt(request):
    """
    Produice printable document
    """
    id_parent = request.GET.get('parent_id', None)
    try:
        header = Purchase.objects.select_related().get(id=id_parent)
        if header.document_status_id == wms_params.DOCUMENT_CREATED_ID:
            return HttpResponse('Le Bon à imprimer n\'est pas encore lancé !')
        details = PurchaseDetail.objects.filter(parent=header).annotate(
            number_of_packages=Case(
                When(~Q(batch__packaging=0), then=(F('quantity') / F('batch__packaging'))),
                default=0,
                output_field=IntegerField()
            ),
            number_of_units=Case(
                When(~Q(batch__packaging=0), then=(F('quantity') % F('batch__packaging'))),
                default=F('quantity'),
                output_field=IntegerField()
            )
        )
        return render(request, 'wms/prints/purchase_receipt.html',
                      {'header': header, 'details': details})
    except Purchase.DoesNotExist:
        return HttpResponse('Aucune transaction séléctionnée pour l\'impression')


def print_transfer_order(request):
    """
    Produice printable document
    """
    id_parent = request.GET.get('parent_id', None)
    try:
        header = Transfer.objects.select_related().get(id=id_parent)

        if header.document_status_id == wms_params.DOCUMENT_CREATED_ID:
            return HttpResponse('Le Bon à imprimer n\'est pas encore lancé !')
        details = TransferDetails.objects.select_related(
            'batch__product', 'from_emplacement', 'to_emplacement', 'from_status', 'to_status'
        ).filter(
            parent=header).annotate(
            number_of_packages=Case(
                When(~Q(batch__packaging=0), then=(F('quantity') / F('batch__packaging'))),
                default=0,
                output_field=IntegerField()
            ),
            number_of_units=Case(
                When(~Q(batch__packaging=0), then=(F('quantity') % F('batch__packaging'))),
                default=F('quantity'),
                output_field=IntegerField()
            )
        ).order_by('from_emplacement')
        return render(request, 'wms/prints/transfer_receipt.html',
                      {'header': header, 'details': details})
    except Transfer.DoesNotExist:
        return HttpResponse('Aucune transaction séléctionnée pour l\'impression')


def print_external_transfer_order(request):
    """
    Produice printable document
    """
    id_parent = request.GET.get('parent_id', None)
    try:
        header = ExternalTransfer.objects.select_related().get(id=id_parent)

        if header.document_status_id == wms_params.DOCUMENT_CREATED_ID:
            return HttpResponse('Le Bon à imprimer n\'est pas encore lancé !')
        details = ExternalTransferDetails.objects.select_related(
            'batch__product', 'emplacement', 'status').filter(
            parent=header).annotate(
            number_of_packages=Case(
                When(~Q(batch__packaging=0), then=(F('quantity') / F('batch__packaging'))),
                default=0,
                output_field=IntegerField()
            ),
            number_of_units=Case(
                When(~Q(batch__packaging=0), then=(F('quantity') % F('batch__packaging'))),
                default=F('quantity'),
                output_field=IntegerField()
            )
        ).order_by('emplacement')
        return render(request, 'wms/prints/external_transfer_receipt.html',
                      {'header': header, 'details': details})
    except ExternalTransfer.DoesNotExist:
        return HttpResponse('Aucune transaction séléctionnée pour l\'impression')


def print_pharmaceutical_inventory_count_receipt(request):
    """
    Produice printable document
    """
    id_parent = request.GET.get('parent_id', None)
    try:
        header = InventoryCount.objects.select_related().get(id=id_parent)
        details = InventoryCountDetail.objects.filter(parent=header).annotate(
            number_of_packages=Case(
                When(~Q(batch__packaging=0), then=(F('quantity') / F('batch__packaging'))),
                default=0,
                output_field=IntegerField()
            ),
            number_of_units=Case(
                When(~Q(batch__packaging=0), then=(F('quantity') % F('batch__packaging'))),
                default=F('quantity'),
                output_field=IntegerField()
            )
        )
        return render(request, 'wms/prints/inventory_count_receipt.html',
                      {'header': header, 'details': details})
    except InventoryCount.DoesNotExist:
        return HttpResponse('Aucune Journal séléctionnée pour l\'impression')

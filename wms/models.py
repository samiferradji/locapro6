from decimal import Decimal

from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.db.models import Sum, F, DecimalField

from common import helpers as common_helpers
from common.models import Company, Tva, ArticleCategory, DocumentStatus, DomainBaseModel, BaseModel, UserProfile, \
    Employee
from wms import parameters as wms_params
from wms import validators as wms_validators
from .validators import quantity_is_not_zero


#  Reference tables

class Dci(DomainBaseModel):
    reference_table = True
    code = models.CharField(max_length=30, verbose_name='Code DCI')
    designation = models.CharField(max_length=50, verbose_name='DCI')

    def __str__(self):
        return self.code

    class Meta:
        verbose_name = 'DCI'
        verbose_name_plural = 'DCIs'
        unique_together = ('code', 'company', 'deleted_at')
        permissions = (
            ('admin_dci', 'Can Admin DCIs'),
            ('view_dci', 'Can view DCIs'),
            ('export_dci', 'Can export DCIs'),
        )


class PharmaceuticalForm(DomainBaseModel):
    reference_table = True
    designation = models.CharField(max_length=30, verbose_name='Forme Pharmaceutique')
    short_designation = models.CharField(max_length=6, verbose_name='Abréviation')

    def __str__(self):
        return self.designation

    class Meta:
        verbose_name = 'Forme Pharmaceutique'
        verbose_name_plural = 'Formes Pharmaceutiques'
        unique_together = ('designation', 'company', 'deleted_at')
        permissions = (
            ('admin_pharmaceuticalform', 'Can Admin Pharmaceutical Forms'),
            ('view_pharmaceuticalform', 'Can view Pharmaceutical Forms'),
            ('export_pharmaceuticalform', 'Can export Pharmaceutical Forms'),
        )


class ProductStatus(DomainBaseModel):
    reference_table = True
    designation = models.CharField(max_length=30, verbose_name='Statut produit')

    def __str__(self):
        return self.designation

    class Meta:
        verbose_name = 'Statut Produit'
        verbose_name_plural = 'Statuts Produit'
        unique_together = ('designation', 'company', 'deleted_at')
        permissions = (
            ('admin_productstatus', 'Can Admin Product Status'),
            ('view_productstatus', 'Can view Product Status'),
            ('export_productstatus', 'Can export Product Status'),
        )


class StorageType(BaseModel):
    """
    Types of storage, example : palette , pack ...
    Id's must be input
    """
    reference_table = True
    custom_id_generator = False
    all_domains_common_table = True
    id = models.IntegerField(primary_key=True, editable=True)
    designation = models.CharField(max_length=50, verbose_name="Type d'entreposage")

    def __str__(self):
        return self.designation

    class Meta:
        verbose_name = 'Type de stockage'
        verbose_name_plural = 'Types de stockage'
        unique_together = ('designation', 'deleted_at')
        permissions = (
            ('admin_storagetype', 'Can Admin Storage Types'),
            ('view_storagetype', 'Can view Storage Types'),
            ('export_storagetype', 'Can export Storage Types'),
        )


class Store(DomainBaseModel):
    designation = models.CharField(max_length=30, verbose_name='Désignation Magasin')
    storage_type = models.ForeignKey(StorageType, default=1, verbose_name="Type d'entreposage principal",
                                     on_delete=models.PROTECT)

    def __str__(self):
        return self.designation

    class Meta:
        verbose_name = 'Magasin'
        verbose_name_plural = 'Magasins'
        unique_together = ('designation', 'company', 'deleted_at')
        permissions = (
            ('admin_store', 'Can Admin Stores'),
            ('view_store', 'Can view Stores'),
            ('export_store', 'Can export Stores'),
        )

    def save(self, *args, **kwargs):
        if not self.id:
            with transaction.atomic():
                super(Store, self).save()
                current_company = UserProfile.objects.get(user=self.created_by).company
                stores_count = Store.default_manager.filter(company=current_company).count()
                new_emplacement = Emplacement(store=self, storage_type=self.storage_type,
                                              emplacement='inst-{}'.format(stores_count), created_by=self.created_by)
            new_emplacement.save()
        else:
            super(Store, self).save()


class Area(DomainBaseModel):
    designation = models.CharField(max_length=20, verbose_name='Zone')
    description = models.TextField(max_length=200, verbose_name='description', blank=True)

    def __str__(self):
        return self.designation

    class Meta:
        verbose_name = 'Zone'
        verbose_name_plural = 'Zones'
        unique_together = ('designation', 'company', 'deleted_at')
        permissions = (
            ('admin_area', 'Can Admin Areas'),
            ('view_area', 'Can view Areas'),
            ('export_area', 'Can export Areas'),
        )


class Emplacement(DomainBaseModel):
    emplacement = models.CharField(max_length=10, verbose_name='Emplacement')
    store = models.ForeignKey(Store, verbose_name='Magasin', on_delete=models.PROTECT)
    area = models.ForeignKey(Area, on_delete=models.PROTECT, verbose_name='Zone', null=True, blank=True)
    volume = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Capacité en volume', null=True,
                                 blank=True)
    weight = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Charge Maximale', null=True, blank=True)
    storage_type = models.ForeignKey(StorageType, default=1, verbose_name="Type d'entreposage",
                                     on_delete=models.PROTECT)

    def __str__(self):
        return '{} - {} - {} '.format(self.emplacement, self.store.designation, self.company)

    class Meta:
        verbose_name = 'Emplacement'
        verbose_name_plural = 'Emplacements'
        unique_together = ('emplacement', 'company', 'deleted_at')
        permissions = (
            ('admin_emplacement', 'Can Admin Emplacements'),
            ('view_emplacement', 'Can view Emplacements'),
            ('export_emplacement', 'Can export Emplacements'),
        )


class PharmaceuticalSupplier(DomainBaseModel):
    reference_table = True
    code = models.CharField(max_length=10, verbose_name='Code fournisseur')
    supplier = models.CharField(max_length=50, verbose_name='Fournisseur')
    RC = models.CharField(max_length=30, verbose_name='RC', blank=True)
    NIF = models.CharField(max_length=30, verbose_name='NIF', blank=True)
    contact = models.CharField(max_length=100, verbose_name='Contact', blank=True)
    telephone = models.CharField(max_length=20, verbose_name='Telephone', blank=True)
    address = models.CharField(max_length=150, verbose_name='Adresse', blank=True)
    email = models.EmailField(verbose_name='Email', blank=True)
    active = models.BooleanField(verbose_name='Actif ?', default=True)

    def __str__(self):
        return ' '.join((self.code, self.supplier))

    class Meta:
        verbose_name = 'Fournisseur Pharma'
        verbose_name_plural = 'Fournisseurs Pharma'
        unique_together = (('code', 'company', 'deleted_at'), ('supplier', 'company', 'deleted_at'))
        permissions = (
            ('admin_pharmaceuticalsupplier', 'Can Admin Pharmaceutical Suppliers'),
            ('view_pharmaceuticalsupplier', 'Can view Pharmaceutical Suppliers'),
            ('export_pharmaceuticalsupplier', 'Can export Pharmaceutical Suppliers'),
        )


class Laboratory(DomainBaseModel):
    reference_table = True
    code = models.CharField(max_length=10, verbose_name='Code laboratoire', blank=True)
    designation = models.CharField(max_length=50, verbose_name='Laboratoire')

    def __str__(self):
        return ' '.join((self.code, self.designation))

    class Meta:
        verbose_name = 'Laboratoire'
        verbose_name_plural = 'Laboratoires'
        unique_together = (('code', 'company', 'deleted_at'), ('designation', 'company', 'deleted_at'))
        permissions = (
            ('admin_laboratory', 'Can Admin Laboratories'),
            ('view_laboratory', 'Can view Laboratories'),
            ('export_laboratory', 'Can export Laboratories'),
        )


class Product(DomainBaseModel):
    reference_table = True
    code = models.CharField(max_length=10, verbose_name='Code')
    category = models.ForeignKey(ArticleCategory, verbose_name='Famille', on_delete=models.CASCADE, null=True,
                                 blank=True)
    product = models.CharField(max_length=50, verbose_name='Désignation')
    dci = models.ForeignKey(Dci, verbose_name='DCI', null=True, on_delete=models.PROTECT, blank=True)
    pharmaceutical_form = models.ForeignKey(PharmaceuticalForm, verbose_name='Forme', null=True,
                                            blank=True, on_delete=models.PROTECT)
    dosage = models.CharField(max_length=20, verbose_name='Dosage', blank=True)
    conditioning = models.CharField(max_length=10, verbose_name='Cond', blank=True)
    laboratory = models.ForeignKey(Laboratory, verbose_name='Laboratoire', on_delete=models.PROTECT, null=True,
                                   blank=True)
    weight = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Poids', default=0, blank=True)
    volume = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Volume', default=0, blank=True)
    package_weight = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Poids colis', default=0,
                                         blank=True)
    thermal_sensibility = models.BooleanField('Thermo?', default=False)
    psychotropic = models.BooleanField(verbose_name='Psy?', default=False)
    picking_emplacement = models.ForeignKey(Emplacement, verbose_name='Prélèvement', null=True, blank=True,
                                            on_delete=models.PROTECT)
    picking_stock_min = models.IntegerField(verbose_name='Min', default=50)
    picking_stock_max = models.IntegerField(verbose_name='Max', default=200)
    principal_stock_min = models.IntegerField(verbose_name='Stock Min', default=500)
    principal_stock_max = models.IntegerField(verbose_name='Stock Max', default=2000)
    tva = models.ForeignKey(Tva, on_delete=models.PROTECT, verbose_name='TVA', default=1)
    active = models.BooleanField(verbose_name='Actif ?', default=True)
    description = models.TextField(max_length=500, verbose_name='Description', blank=True)

    def __str__(self):
        return ' '.join(
            (self.code, self.product))

    class Meta:
        verbose_name = 'Produit'
        verbose_name_plural = 'Produits'
        unique_together = (('code', 'company', 'deleted_at'), ('product', 'company', 'deleted_at'))
        permissions = (
            ('admin_product', 'Can Admin Products'),
            ('view_product', 'Can view Products'),
            ('export_product', 'Can export Products'),
        )


class LogisticFlowType(BaseModel):
    """
    Example : entreposage , transfer ...
    Id's must be input
    """
    reference_table = True
    custom_id_generator = True
    all_domains_common_table = True
    id = models.IntegerField(primary_key=True)
    designation = models.CharField(max_length=50, verbose_name='Désignation du mouvement', unique=True)
    difficulty_level = models.SmallIntegerField(verbose_name='Niveau de difficulté', null=True, blank=True)
    description = models.TextField(verbose_name='Description du mouvement', blank=True)
    point_line_execution = models.IntegerField(verbose_name='Points par ligne executée', null=True, blank=True)
    point_line_typing = models.IntegerField(verbose_name='Points par ligne saisie', null=True, blank=True)
    point_line_control = models.IntegerField(verbose_name='Points par ligne controlée', null=True, blank=True)
    point_sku_execution = models.IntegerField(verbose_name='Points par boites executée', null=True, blank=True)
    point_sku_control = models.IntegerField(verbose_name='Points par boite controlée', null=True, blank=True)
    point_package_execution = models.IntegerField(verbose_name='Points par colis executé', null=True, blank=True)
    point_package_validation = models.IntegerField(verbose_name='Points par colis controlée', null=True,
                                                   blank=True)
    point_palette_execution = models.IntegerField(verbose_name='Point par colis palettisés executée',
                                                  null=True, blank=True)
    point_palette_control = models.IntegerField(verbose_name='Point par colis palettisé controlée',
                                                null=True, blank=True)

    def __str__(self):
        return self.designation

    class Meta:
        verbose_name = 'Type de flux'
        verbose_name_plural = 'Types de flux'
        permissions = (
            ('admin_logisticflowtype', 'Can Admin Logistic Flow Types'),
            ('view_logisticflowtype', 'Can view Logistic Flow Types'),
            ('export_logisticflowtype', 'Can export Logistic Flow Types'),
        )


class FromStoreAuthorised(DomainBaseModel):
    user = models.ForeignKey(User, verbose_name='Utilisateur', on_delete=models.CASCADE, related_name='from_store_user')
    store = models.ForeignKey(Store, verbose_name='Depuis : Magasins Autorisés', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Depuis Magasin Autorisé'
        permissions = (
            ('admin_fromstoreauthorized', 'Can Adnim From Stores Authorized'),
            ('view_fromstoreauthorized', 'Can view From Stores Authorized'),
            ('export_fromstoreauthorized', 'Can export From Stores Authorized'),
        )


class ToStoreAuthorised(DomainBaseModel):
    user = models.ForeignKey(User, verbose_name='Utilisateur', on_delete=models.CASCADE, related_name='to_store_user')
    store = models.ForeignKey(Store, verbose_name='Vers : Magasins Autorisés', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Vers Magasin Autorisé'
        permissions = (
            ('admin_tostoreauthorized', 'Can Admin To Stores Authorized'),
            ('view_tostoreauthorized', 'Can view To Stores Authorized'),
            ('export_tostoreauthorized', 'Can export To Stores Authorized'),
        )


class ProductStatusAuthorised(DomainBaseModel):
    user = models.ForeignKey(User, verbose_name='Utilisateur', on_delete=models.CASCADE,
                             related_name='product_status_user')
    status = models.ForeignKey(ProductStatus, verbose_name='Statuts Produits Autorisés', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Statut Autorisé'
        verbose_name_plural = 'Statuts Autorisés'
        permissions = (
            ('admin_ProductStatusAuthorised', 'Can Admin Product Status Authorized'),
            ('view_ProductStatusAuthorised', 'Can view Product Status Authorized'),
            ('export_ProductStatusAuthorised', 'Can export Product Status Authorized'),
        )


#  Product Batch tables

class BatchBase(DomainBaseModel):
    product = models.ForeignKey(Product, verbose_name='Produit', on_delete=models.PROTECT)
    batch_number = models.CharField(max_length=20, verbose_name='Lot', db_index=True)
    expiration_date = models.DateField(verbose_name='DDP', db_index=True)
    supplier = models.ForeignKey(PharmaceuticalSupplier, on_delete=models.PROTECT, verbose_name='Fournisseur')
    packaging = models.IntegerField(verbose_name='Colis', db_index=True)
    purchase_pu_ht = models.DecimalField(max_digits=9, decimal_places=2, verbose_name="PU achat HT")
    unit_discount_percentage = models.DecimalField(max_digits=5, decimal_places=2, verbose_name="Remise (%)", default=0)
    unit_discount_value = models.DecimalField(max_digits=9, decimal_places=2, verbose_name="Remise (Valeur)", default=0)
    purchase_pu_ht_net = models.DecimalField(max_digits=9, decimal_places=2, verbose_name="PU achat Net HT")
    tva = models.ForeignKey(Tva, verbose_name='Taux TVA', on_delete=models.PROTECT)
    selling_pu_ht = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='PU vente HT')
    selling_pu_ttc = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='PU vente TTC')  # +TVA
    shp = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='SHP')
    ppa_ht = models.DecimalField(max_digits=9, decimal_places=2, verbose_name='PPA')
    unit_weight = models.DecimalField(decimal_places=2, max_digits=9, verbose_name='Pds Boite', default=0)
    unit_volume = models.DecimalField(decimal_places=2, max_digits=9, verbose_name='Vlm Boite', default=0)
    packaged_weight = models.DecimalField(decimal_places=2, max_digits=9, verbose_name='Pds Colis', default=0)

    class Meta:
        abstract = True

    def __str__(self):
        return ' '.join([
            self.product.__str__(),
            ' | Lot: ',
            self.batch_number.__str__(),
            ' | DDP: ',
            self.expiration_date.__str__(),
            ' | Fourn: ',
            self.supplier.__str__(),
            ' | PU Achat :',
            self.purchase_pu_ht.__str__(),
            ' | PPA : ',
            self.ppa_ht.__str__()
        ])


class Batch(BatchBase):
    content_type = models.ForeignKey(ContentType, on_delete=models.PROTECT)
    object_id = models.CharField(max_length=15)
    origin = GenericForeignKey('content_type', 'object_id')

    def set_default_origin(self):
        if not self.object_id:
            self.object_id = 1
        if not self.content_type_id:
            self.content_type_id = 1

    def calculate_unit_discount_value(self):
        self.unit_discount_value = round((self.purchase_pu_ht * self.unit_discount_percentage) / 100, 2)

    def calculate_purchase_pu_ht_net(self):
        self.purchase_pu_ht_net = round(self.purchase_pu_ht - self.unit_discount_value, 2)

    def calculate_purchase_pu_ht(self):
        if not self.purchase_pu_ht:
            self.purchase_pu_ht = round(Decimal(self.purchase_pu_ht_net) + Decimal(self.unit_discount_value), 2)

    def calculate_selling_pu_ttc(self):
        if not self.selling_pu_ttc:
            self.selling_pu_ttc = round(
                Decimal(self.selling_pu_ht) + (self.tva.tva_rate * Decimal(self.selling_pu_ht) / 100), 2)

    def full_clean(self, *args, **kwargs):
        """
        Order is important !!!
        """
        self.set_default_origin()
        self.calculate_purchase_pu_ht()
        self.calculate_unit_discount_value()
        self.calculate_purchase_pu_ht_net()
        self.calculate_selling_pu_ttc()
        super(Batch, self).full_clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        with transaction.atomic():

            if self.id:
                try:
                    obj = ArchivedBatch.objects.get(batch=self)
                    obj.product = self.product
                    obj.batch_number = self.batch_number
                    obj.expiration_date = self.expiration_date
                    obj.supplier = self.supplier
                    obj.packaging = self.packaging
                    obj.purchase_pu_ht = self.purchase_pu_ht
                    obj.unit_discount_percentage = self.unit_discount_percentage
                    obj.unit_discount_value = self.unit_discount_value
                    obj.purchase_pu_ht_net = self.purchase_pu_ht_net
                    obj.selling_pu_ht = self.selling_pu_ht
                    obj.tva = self.tva
                    obj.selling_pu_ttc = self.selling_pu_ttc
                    obj.shp = self.shp
                    obj.ppa_ht = self.ppa_ht
                    obj.unit_weight = self.unit_weight
                    obj.unit_volume = self.unit_volume
                    obj.packaged_weight = self.packaged_weight
                    obj.save()
                    super(Batch, self).save()
                except ArchivedBatch.DoesNotExist:
                    super(Batch, self).save()
            else:
                super(Batch, self).save()
                obj = ArchivedBatch()
                obj.batch = self
                obj.product = self.product
                obj.batch_number = self.batch_number
                obj.expiration_date = self.expiration_date
                obj.supplier = self.supplier
                obj.packaging = self.packaging
                obj.purchase_pu_ht = self.purchase_pu_ht
                obj.unit_discount_percentage = self.unit_discount_percentage
                obj.unit_discount_value = self.unit_discount_value
                obj.purchase_pu_ht_net = self.purchase_pu_ht_net
                obj.selling_pu_ht = self.selling_pu_ht
                obj.tva = self.tva
                obj.selling_pu_ttc = self.selling_pu_ttc
                obj.shp = self.shp
                obj.ppa_ht = self.ppa_ht
                obj.unit_weight = self.unit_weight
                obj.unit_volume = self.unit_volume
                obj.packaged_weight = self.packaged_weight
                obj.created_by = self.created_by
                obj.save()

    class Meta:
        verbose_name = 'Lot'
        permissions = (
            ('admin_batch', 'Can Admin Batch'),
            ('view_batch', 'Can view Batch'),
            ('export_batch', 'Can Export Batch'),
        )


class ArchivedBatch(BatchBase):
    batch = models.OneToOneField(Batch, verbose_name='Lot ID', on_delete=models.PROTECT)

    class Meta:
        verbose_name = 'Lot Archivé'
        verbose_name_plural = 'Lots Archivés'
        permissions = (
            ('admin_archivedbatch', 'Can Admin Archived Batch'),
            ('view_archivedbatch', 'Can view Archived Batch'),
            ('export_archivedbatch', 'Can export Archived Batch'),
        )


#  purchase tables


class PharmaceuticalInvoice(DomainBaseModel):
    supplier = models.ForeignKey(PharmaceuticalSupplier, on_delete=models.PROTECT, verbose_name='Fournisseur')
    invoice_number = models.CharField(max_length=50, verbose_name='N° Facture', db_index=True)
    invoice_date = models.DateField(verbose_name='Date facture', db_index=True)
    value_ht = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Montant HT')
    discount_on_invoice = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Remise total')
    value_ht_net = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Montant HT Net')
    value_tva = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Montant TVA')
    value_ttc = models.DecimalField(decimal_places=2, max_digits=15, verbose_name='Montant TTC')
    payment_date = models.DateField(verbose_name='Echeance')
    document_status = models.ForeignKey(DocumentStatus, verbose_name='Statut du document', on_delete=models.PROTECT,
                                        default=wms_params.get_default_document_status_id)

    class Meta:
        verbose_name = 'Facture d\'achat pharmaceutque'
        verbose_name_plural = 'Factures des achats pharmaceutiques'
        unique_together = ('supplier', 'invoice_number', 'invoice_date', 'deleted_at')
        permissions = (
            ('admin_pharmaceuticalinvoice', 'Can Admin Pharmaceutical Invoices'),
            ('view_pharmaceuticalinvoice', 'Can view Pharmaceutical Invoices'),
            ('export_pharmaceuticalinvoice', 'Can export Pharmaceutical Invoices'),
            ('attach_pharmaceuticalinvoice', 'Can attach Pharmaceutical Invoices Document'),
        )

    def __str__(self):
        return ' | '.join((str(self.invoice_number), str(self.invoice_date), str(self.supplier), str(self.value_ttc)))


class PurchaseBase(DomainBaseModel):
    invoice = models.ForeignKey(PharmaceuticalInvoice, verbose_name='Facture d\'achatFournissur',
                                on_delete=models.PROTECT)
    receipt_number = models.CharField(max_length=15, verbose_name='Numéro de Bon de réception',
                                      db_index=True)
    receipt_date = models.DateField(verbose_name='Date de Réception')
    document_status = models.ForeignKey(DocumentStatus, verbose_name='Statut du document', on_delete=models.PROTECT,
                                        default=wms_params.get_default_document_status_id)
    observation = models.TextField(max_length=200, blank=True)
    flew_type = models.ForeignKey(LogisticFlowType, verbose_name='Type de flux', on_delete=models.PROTECT,
                                  default=wms_params.PURCHASE_LOGISTIC_FLEW_ID)

    def __str__(self):
        return ' | '.join([self.receipt_number, str(self.receipt_date)])

    class Meta:
        abstract = True
        unique_together = ('receipt_number', 'company', 'deleted_at')


class PurchaseDetailBase(DomainBaseModel):
    batch = models.ForeignKey(Batch, on_delete=models.PROTECT)
    quantity = models.IntegerField(verbose_name='Quantité')
    emplacement = models.ForeignKey(Emplacement, verbose_name='Emplacement', on_delete=models.PROTECT)

    class Meta:
        abstract = True


class PurchaseTempo(PurchaseBase):
    id = models.CharField(max_length=36, primary_key=True, default=common_helpers.get_uuid)
    receipt_number = models.CharField(max_length=36, verbose_name='Numéro de Bon de réception',
                                      default=common_helpers.get_uuid)

    def commit_transaction(self):
        """
        Transferts the inputed data from temporary tables to final tables

        """
        try:
            with transaction.atomic():
                new_obj = Purchase.objects.create(
                    created_by=self.created_by,
                    invoice=self.invoice,
                    receipt_number=common_helpers.create_new_custom_id(Purchase, self.created_by),
                    receipt_date=self.receipt_date,
                    document_status=self.document_status,
                    observation=self.observation,
                    flew_type=self.flew_type
                )
                purchase_details = PurchaseDetailTempo.objects.filter(
                    parent=self
                )
                if purchase_details.count() == 0:
                    raise ValidationError(
                        {'Document': 'Pour enregistrer, le document doit contenir au moins une ligne'})
                for obj in purchase_details:
                    new_batch = Batch(
                        created_by=obj.created_by,
                        content_type=ContentType.objects.get_for_model(new_obj),
                        object_id=new_obj.id,
                        product=obj.product,
                        batch_number=obj.batch_number,
                        expiration_date=obj.expiration_date,
                        supplier=obj.supplier,
                        packaging=obj.packaging,
                        purchase_pu_ht=obj.purchase_pu_ht,
                        unit_discount_percentage=obj.unit_discount_percentage,
                        unit_discount_value=obj.unit_discount_value,
                        purchase_pu_ht_net=obj.purchase_pu_ht_net,
                        selling_pu_ht=obj.selling_pu_ht,
                        tva=obj.tva,
                        selling_pu_ttc=obj.selling_pu_ttc,
                        shp=obj.shp,
                        ppa_ht=obj.ppa_ht,
                        unit_weight=obj.unit_weight,
                        unit_volume=obj.unit_volume,
                        packaged_weight=obj.packaged_weight
                    )
                    new_batch.save()
                    PurchaseDetail.objects.create(
                        created_by=obj.created_by,
                        batch=new_batch,
                        parent=new_obj,
                        quantity=obj.quantity,
                        emplacement=obj.emplacement
                    )
                    Inventory.objects.create(
                        created_by=obj.created_by,
                        content_type=ContentType.objects.get_for_model(new_obj),
                        object_id=new_obj.id,
                        batch=new_batch,
                        transaction='Achat Reçu',
                        quantity=obj.quantity,
                        product_status_id=wms_params.get_default_product_status_id(obj.created_by),
                        emplacement=obj.emplacement
                    )
                    obj.delete()
                self.delete()

            return new_obj.receipt_number

        except Exception as e:
            raise e

    def cancel_transaction(self):

        """
        Cancel the temporary transaction
        """
        try:
            with transaction.atomic():
                self.temporary_details_objs = PurchaseDetailTempo.objects.filter(
                    parent=self
                )
                self.temporary_details_objs.delete()
                self.delete()
                return self.id
        except Exception as e:
            raise e

    def dispatch_global_discount(self, global_discount: int = 0):
        obj_details = PurchaseDetailTempo.objects.filter(parent=self)
        obj_details_sum = PurchaseDetailTempo.objects.filter(parent=self).aggregate(
            line_sum=Sum(F('purchase_pu_ht') * F('quantity'), output_field=DecimalField()))
        if obj_details.exists():
            for obj in obj_details:
                new_unit_discount_value = global_discount * obj.purchase_pu_ht * obj.quantity / obj_details_sum[
                    'line_sum']
                new_unit_discount_percentage = new_unit_discount_value / obj.purchase_pu_ht / obj.quantity * 100
                obj.unit_discount_percentage += new_unit_discount_percentage
                obj.save()

    def delete(self, *args, **kwargs):
        kwargs['safe'] = False
        super(PurchaseTempo, self).delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        kwargs['safe'] = False
        super(PurchaseTempo, self).save(*args, **kwargs)


class PurchaseDetailTempo(BatchBase):
    custom_id_generator = False
    id = models.AutoField(primary_key=True)
    parent = models.ForeignKey(PurchaseTempo, on_delete=models.CASCADE, verbose_name='Bon de réception')
    quantity = models.PositiveIntegerField(verbose_name='Quantité')
    emplacement = models.ForeignKey(Emplacement, verbose_name='Emplacement', on_delete=models.CASCADE)

    def calculate_unit_discount_value(self):
        self.unit_discount_value = (self.purchase_pu_ht * self.unit_discount_percentage) / 100

    def calculate_purchase_pu_ht_net(self):
        self.purchase_pu_ht_net = self.purchase_pu_ht - self.unit_discount_value

    def calculate_selline_pu_ttc(self):
        self.selling_pu_ttc = self.selling_pu_ht + ((self.tva.tva_rate * self.selling_pu_ht) / 100)

    def save(self, *args, **kwargs):
        with transaction.atomic():
            self.calculate_unit_discount_value()
            self.calculate_purchase_pu_ht_net()
            self.calculate_selline_pu_ttc()
            super(PurchaseDetailTempo, self).save()

    def delete(self, *args, **kwargs):
        kwargs['safe'] = False
        super(PurchaseDetailTempo, self).delete(*args, **kwargs)

    class Meta:
        permissions = (
            ('admin_purchasedetailtempo', 'Can Admin Purchase Details Tempo'),
            ('view_purchasedetailtempo', 'Can view Purchase Details Tempo'),
            ('export_purchasedetailtempo', 'Can export Purchase Details Tempo'),
        )


class Purchase(PurchaseBase):
    receipt_number = models.CharField(max_length=15, verbose_name='Numéro du Bon de réception',
                                      db_index=True)

    def send_transaction(self, user):
        """
        Transfers the document to execution
        This change the document status from 'Créted' to 'Sent'
        """
        try:
            with transaction.atomic():
                wms_validators.internal_movement_can_be_send(transaction_instance=self)
                obj_details = PurchaseDetail.objects.filter(parent_id=self.id)
                if obj_details.count() == 0:
                    raise ValidationError(
                        {'Document': 'Pour lancer un mouvement, le document doit contenir au moins une ligne'})
                self.document_status_id = wms_params.DOCUMENT_SENT_ID
                self.save(user=user)
                for obj in obj_details:
                    related_movement = Inventory.objects.filter(
                        object_id=obj.id,
                        content_type=ContentType.objects.get_for_model(obj)
                    )
                    for movement in related_movement:
                        movement.sent = True
                        movement.save(editing=True, safe=False)

            return self.id

        except Exception as e:
            raise e

    def return_transaction(self, user):
        """
        Return the document from execution.
        This change the document status from 'Sent' to 'Created'
        """
        try:
            if self.document_status_id == wms_params.DOCUMENT_CONFIRMED_ID:
                raise ValidationError(
                    {'Document Confirmé': 'Pour annuler ce mouvement vous pouver créer un nouveau  mouvement dans '
                                          'le sense inverse'})
            if self.document_status_id == wms_params.DOCUMENT_CREATED_ID:
                raise ValidationError(
                    {'Document non lancé': 'Vous puvez modifier ce bon au lieu de l\'annuler'})
            else:
                with transaction.atomic():
                    self.document_status_id = wms_params.DOCUMENT_CREATED_ID
                    self.save(user=user)
                    obj_details = PurchaseDetail.objects.filter(parent_id=self.id)
                    for obj in obj_details:
                        related_movement = Inventory.objects.filter(
                            object_id=obj.id,
                            content_type=ContentType.objects.get_for_model(obj)
                        )
                        for movement in related_movement:
                            movement.sent = False
                            movement.save(editing=True, safe=False)

                return self.id

        except Exception as e:
            raise e

    def commit_transaction(self):
        """
        Transfers the imputed data from temporary tables to final tables
        """
        try:
            with transaction.atomic():
                obj_details = PurchaseDetail.objects.filter(parent_id=self.id)
                if obj_details.count() == 0:
                    raise ValidationError(
                        {'Document': 'Pour confirmer un mouvement, le document doit contenir au moins une ligne'})
                for obj in obj_details:
                    Inventory.objects.filter(
                        object_id=obj.id,
                        content_type=ContentType.objects.get_for_model(obj)
                    ).update(received=True)

            return self.id

        except Exception as e:
            raise e

    def full_clean(self, *args, **kwargs):
        if not self.receipt_number:
            self.receipt_number = common_helpers.create_new_custom_id(Purchase, self.created_by)
            super(Purchase, self).full_clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        if kwargs.get('editing') and self.document_status_id == wms_params.DOCUMENT_CREATED_ID:
            obj_details = PurchaseDetail.objects.filter(parent_id=self.id)
            if obj_details.count() != 0:
                raise ValidationError(
                    {'Modification impossible': 'Le bon doit être vide pour pouvoire le modifier '})
            else:
                super(Purchase, self).save(*args, **kwargs)
        elif kwargs.get('editing') and self.document_status_id != wms_params.DOCUMENT_CREATED_ID:
            raise ValidationError(
                {'Modification impossible': 'Les bons avec le statut "Créé" seulement peuvent être modifié'})
        else:
            super(Purchase, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        raise ValidationError({'Suppression impossible': 'Ce bon ne peut pas être supprimé'})

    class Meta:
        permissions = (
            ('admin_purchase', 'Can Admin Purchases'),
            ('confirm_purchase', 'Can confirm Purchases'),
            ('view_purchase', 'Can view Purchases'),
            ('import_purchase_data', 'Can Import Purchases'),
            ('export_purchase_data', 'Can export Purchases'),
            ('attach_purchase_document', 'Can Attach Purchases Document'),
        )
        unique_together = ('receipt_number', 'company', 'deleted_at')


class PurchaseDetail(PurchaseDetailBase):
    custom_id_generator = False
    id = models.AutoField(primary_key=True)
    parent = models.ForeignKey(Purchase, on_delete=models.PROTECT, verbose_name='Bon de réception')

    def save(self, *args, **kwargs):
        if self.parent.document_status_id == wms_params.DOCUMENT_SENT_ID:
            raise ValidationError({
                'Bon Lancé': 'Ce bon ne peut pas être modifier. Merci de créer un nouveau \
                                      bon pour des nouveaux mouvements'})
        elif self.parent.document_status_id != wms_params.DOCUMENT_CREATED_ID:
            raise ValidationError({
                'Bon Confirmé': 'Ce bon ne peut plus être modifier.'})
        if kwargs.get('editing'):
            with transaction.atomic():
                super(PurchaseDetail, self).save(*args, **kwargs)
                related_inventory = Inventory.objects.get(object_id=self.id,
                                                          content_type=ContentType.objects.get_for_model(self))
                related_inventory.created_by = self.created_by
                related_inventory.content_type = ContentType.objects.get_for_model(self)
                related_inventory.object_id = self.id
                related_inventory.batch = self.batch
                related_inventory.quantity = self.quantity
                related_inventory.emplacement = self.emplacement
                related_inventory.save(editing=True, safe=False)
        else:
            with transaction.atomic():
                super(PurchaseDetail, self).save(*args, **kwargs)
                Inventory.objects.create(
                    created_by=self.created_by,
                    content_type=ContentType.objects.get_for_model(self),
                    object_id=self.id,
                    batch=self.batch,
                    quantity=self.quantity,
                    product_status_id=wms_params.get_default_product_status_id(self.created_by),
                    transaction='Purchase in',
                    emplacement=self.emplacement,
                    sent=False
                )

    def delete(self, *args, **kwargs):
        if self.parent.document_status_id == wms_params.DOCUMENT_SENT_ID:
            raise ValidationError({
                'Bon Lancé': 'Ce bon ne peut pas être modifier. Merci de créer un nouveau \
                                      bon pour des nouveaux mouvements'})
        elif self.parent.document_status_id != wms_params.DOCUMENT_CREATED_ID:
            raise ValidationError({
                'Bon Confirmé': 'Ce bon ne peut plus être modifier.'})
        else:
            with transaction.atomic():
                related_mouvement = Inventory.objects.filter(
                    object_id=self.id,
                    content_type=ContentType.objects.get_for_model(self)
                )
                for obj in related_mouvement:
                    obj.delete(*args, **kwargs)
                kwargs['safe'] = False  # Very important do not change !!
                super(PurchaseDetail, self).delete(*args, **kwargs)

    class Meta:
        permissions = (
            ('admin_purchasedetail', 'Can Admin Purchase Details'),
            ('view_purchasedetail', 'Can view Purchase Details'),
            ('export_purchasedetail', 'Can export Purchase Details'),
        )


#  Inventory tables

class Inventory(DomainBaseModel):
    content_type = models.ForeignKey(ContentType, on_delete=models.PROTECT)
    object_id = models.CharField(max_length=15)
    origin = GenericForeignKey('content_type', 'object_id')
    batch = models.ForeignKey(Batch, verbose_name='Lot', on_delete=models.PROTECT)
    quantity = models.IntegerField(verbose_name='Quantité', db_index=True)
    emplacement = models.ForeignKey(Emplacement, verbose_name='Emplacement', on_delete=models.PROTECT)
    product_status = models.ForeignKey(ProductStatus, verbose_name='Statut', on_delete=models.PROTECT)
    transaction = models.CharField(max_length=20, verbose_name='Transaction')
    received = models.BooleanField(default=False, verbose_name='Reçu?', db_index=True)
    sent = models.BooleanField(default=False, verbose_name='Lancé?', db_index=True)

    class Meta:
        permissions = (
            ('admin_inventory', 'Can Admin Inventory'),
            ('view_inventory', 'Can view Inventory'),
            ('export_inventory', 'Can Export Inventory'),
        )


# Internal transfer tables

class TransferBase(DomainBaseModel):
    from_store = models.ForeignKey(Store, verbose_name='Depuis magasin', related_name='from_store',
                                   on_delete=models.PROTECT)
    to_store = models.ForeignKey(Store, verbose_name='Vers magasin', on_delete=models.PROTECT, related_name='to_store')
    document_status = models.ForeignKey(DocumentStatus, verbose_name='Statut du document', on_delete=models.PROTECT,
                                        default=wms_params.get_default_document_status_id)
    flew_type = models.ForeignKey(LogisticFlowType, verbose_name='Type de flux', on_delete=models.PROTECT)
    observation = models.TextField(max_length=400, verbose_name='Observation', blank=True)

    def __str__(self):
        return "{}{}".format(str(self.from_store), str(self.to_store))

    class Meta:
        abstract = True


class TransferDetailsBase(DomainBaseModel):
    custom_id_generator = False
    id = models.AutoField(primary_key=True, verbose_name='id', editable=False)
    parent = models.CharField(max_length=36)
    batch = models.ForeignKey(Batch, on_delete=models.PROTECT, verbose_name='Lot')
    from_emplacement = models.ForeignKey(Emplacement, verbose_name='Depuis Empl', on_delete=models.PROTECT,
                                         related_name='from_empl')
    to_emplacement = models.ForeignKey(Emplacement, verbose_name='Vers Empl', on_delete=models.PROTECT,
                                       related_name='to_empl')
    from_status = models.ForeignKey(ProductStatus, verbose_name='Depuis Statut', on_delete=models.PROTECT,
                                    related_name='from_status')
    to_status = models.ForeignKey(ProductStatus, verbose_name='Vers Statut', on_delete=models.PROTECT,
                                  related_name='to_status')
    quantity = models.PositiveIntegerField(verbose_name='Quantité', validators=[quantity_is_not_zero])

    class Meta:
        abstract = True


class TransferTempo(TransferBase):
    class Meta:
        permissions = (
            ('admin_transfertempo', 'Can Admin Internal Transfer Tempo'),
            ('confirm_transfertempo', 'Can confirm Internal Transfer Tempo'),
            ('view_transfertempo', 'Can view Internal Transfers Tempo'),
            ('export_transfertempo', 'Can export Internal Transfers Tempo'),
        )

    custom_id_generator = False
    id = models.CharField(max_length=36, primary_key=True, default=common_helpers.get_uuid)
    from_store = models.ForeignKey(Store, verbose_name='Depuis magasin', related_name='from_store_tempo',
                                   on_delete=models.PROTECT)
    to_store = models.ForeignKey(Store, verbose_name='Vers magasin', on_delete=models.PROTECT,
                                 related_name='to_store_tempo')

    def commit_transaction(self):
        """
        Transfers the imputed data from temporary tables to final tables
        """
        try:
            with transaction.atomic():
                new_obj = Transfer.objects.create(
                    created_by=self.created_by,
                    flew_type=self.flew_type,
                    from_store=self.from_store,
                    to_store=self.to_store,
                    document_status=self.document_status,
                    observation=self.observation

                )
                obj_details = WarehouseReservationDetail.objects.filter(parent_id=self.id)
                if obj_details.count() == 0:
                    raise ValidationError(
                        {'Document': 'Pour enregistrer, le document doit contenir au moins une ligne'})
                for obj in obj_details:
                    TransferDetails.objects.create(
                        created_by=obj.created_by,
                        parent=new_obj,
                        batch=obj.batch,
                        from_emplacement=obj.from_emplacement,
                        to_emplacement=obj.to_emplacement,
                        from_status=obj.from_status,
                        to_status=obj.to_status,
                        quantity=obj.quantity
                    )
                    Inventory.objects.create(
                        created_by=obj.created_by,
                        content_type=ContentType.objects.get_for_model(new_obj),
                        object_id=new_obj.id,
                        batch=obj.batch,
                        quantity=obj.quantity,
                        product_status=obj.to_status,
                        transaction='Transfer in',
                        emplacement=obj.to_emplacement
                    )
                    Inventory.objects.create(
                        created_by=obj.created_by,
                        content_type=ContentType.objects.get_for_model(new_obj),
                        object_id=new_obj.id,
                        batch=obj.batch,
                        quantity=obj.quantity * -1,
                        product_status=obj.from_status,
                        transaction='Transfer out',
                        emplacement=obj.from_emplacement
                    )
                obj_details.delete()
                self.delete()

            return new_obj.id

        except Exception as e:
            raise e

    def cancel_transaction(self):
        """
        Cancel the temporary transaction
        """
        try:
            with transaction.atomic():
                self.temporary_details_objs = WarehouseReservationDetail.objects.filter(
                    parent_id=self.id
                )
                self.temporary_details_objs.delete()
                self.delete()
                return self.id
        except Exception as e:
            raise e

    def delete(self, *args, **kwargs):
        kwargs['safe'] = False
        super(TransferTempo, self).delete(*args, **kwargs)

    def full_clean(self, *args, **kwargs):
        if self.flew_type_id in ['2', '4']:  # stacking or status changing
            self.to_store = self.from_store
            kwargs['exclude'].append('to_store')
        super(TransferTempo, self).full_clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        kwargs['safe'] = False
        super(TransferTempo, self).save(*args, **kwargs)


class WarehouseReservationDetail(TransferDetailsBase):
    parent = None
    custom_id_generator = False
    id = models.AutoField(primary_key=True)
    parent_id = models.CharField(max_length=36)

    from_emplacement = models.ForeignKey(Emplacement, verbose_name='Depuis Empl', on_delete=models.PROTECT,
                                         related_name='from_empl_tempo')
    to_emplacement = models.ForeignKey(Emplacement, verbose_name='Vers Empl', on_delete=models.PROTECT,
                                       related_name='to_empl_tempo')
    from_status = models.ForeignKey(ProductStatus, verbose_name='Depuis Statut', on_delete=models.PROTECT,
                                    related_name='from_status_tempo')
    to_status = models.ForeignKey(ProductStatus, verbose_name='Vers Statut', on_delete=models.PROTECT,
                                  related_name='to_status_tempo')

    def delete(self, *args, **kwargs):
        kwargs['safe'] = False
        super(WarehouseReservationDetail, self).delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        kwargs['safe'] = False
        super(WarehouseReservationDetail, self).save(*args, **kwargs)

    class Meta:
        permissions = (
            ('admin_warehousereservationdetail', 'Can Admin Warehouse Reservation'),
            ('view_warehousereservationdetail', 'Can view Warehouse Reservation'),
            ('export_warehousereservationdetail', 'Can export Warehouse Reservation'),
        )


class Transfer(TransferBase):

    def send_transaction(self, user):
        """
        Transfers the document to execution
        This change the document status from 'Créted' to 'Sent'
        """
        try:
            with transaction.atomic():
                wms_validators.internal_movement_can_be_send(transaction_instance=self)
                obj_details = TransferDetails.objects.filter(parent_id=self.id)
                if obj_details.count() == 0:
                    raise ValidationError(
                        {'Document': 'Pour lancer un mouvement, le document doit contenir au moins une ligne'})
                self.document_status_id = wms_params.DOCUMENT_SENT_ID
                self.save(user=user)
                for obj in obj_details:
                    related_movement = Inventory.objects.filter(
                        object_id=obj.id,
                        content_type=ContentType.objects.get_for_model(obj)
                    )
                    for movement in related_movement:
                        movement.sent = True
                        movement.save(editing=True, safe=False)

            return self.id

        except Exception as e:
            raise e

    def return_transaction(self, user):
        """
        Return the document from execution.
        This change the document status from 'Sent' to 'Created'
        """
        try:
            if self.document_status_id == wms_params.DOCUMENT_CONFIRMED_ID:
                raise ValidationError(
                    {'Document Confirmé': 'Pour annuler ce mouvement vous pouver créer un nouveau  mouvement dans '
                                          'le sense inverse'})
            if self.document_status_id == wms_params.DOCUMENT_CREATED_ID:
                raise ValidationError(
                    {'Document non lancé': 'Vous puvez modifier ce bon au lieu de l\'annuler'})
            else:
                with transaction.atomic():
                    self.document_status_id = wms_params.DOCUMENT_CREATED_ID
                    self.save(user=user)
                    obj_details = TransferDetails.objects.filter(parent_id=self.id)
                    for obj in obj_details:
                        related_movement = Inventory.objects.filter(
                            object_id=obj.id,
                            content_type=ContentType.objects.get_for_model(obj)
                        )
                        for movement in related_movement:
                            movement.sent = False
                            movement.save(editing=True, safe=False)

                return self.id

        except Exception as e:
            raise e

    def commit_transaction(self):
        """
        Transfers the imputed data from temporary tables to final tables
        """
        try:
            with transaction.atomic():
                obj_details = TransferDetails.objects.filter(parent_id=self.id)
                if obj_details.count() == 0:
                    raise ValidationError(
                        {'Document': 'Pour confirmer un mouvement, le document doit contenir au moins une ligne'})
                for obj in obj_details:
                    Inventory.objects.filter(
                        object_id=obj.id,
                        content_type=ContentType.objects.get_for_model(obj)
                    ).update(received=True)

            return self.id

        except Exception as e:
            raise e

    def full_clean(self, *args, **kwargs):
        if self.flew_type_id in ['2', '4']:  # stacking or status changing
            self.to_store = self.from_store
            kwargs['exclude'].append('to_store')
        elif self.from_store == self.to_store:
            raise ValidationError({
                'Magasin': 'Vers Magasin ne peut pas être identique à Depuis Magasin'})
        super(Transfer, self).full_clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        if kwargs.get('editing'):
            raise ValidationError({
                'Bon Confirmé': 'Ce bon ne peut plus être modifier.'})
        else:
            super(Transfer, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        raise ValidationError({'Suppression impossible': 'Ce bon ne peut pas être supprimé'})

    class Meta:
        permissions = (
            ('admin_transfer', 'Can Admin Internal Transfer'),
            ('confirm_transfer', 'Can confirm Internal Transfer'),
            ('send_transfer', 'Can Send Internal Transfer'),
            ('return_transfer', 'Can Return Internal Transfer'),
            ('view_transfer', 'Can view Internal Transfers'),
            ('view_all_transfer', 'Can view AllInternal Transfers'),
            ('export_transfer', 'Can export Internal Transfers'),
        )


class TransferDetails(TransferDetailsBase):
    parent = models.ForeignKey(Transfer, on_delete=models.PROTECT)

    def full_clean(self, *args, **kwargs):
        if self.parent.flew_type_id == 2 and self.to_emplacement.store != self.parent.to_store:
            raise ValidationError({'Emplacement': 'L\'emplacement choisi n\'appartient pas au magasin d`entreposage'})
        else:
            super(TransferDetails, self).full_clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        if self.parent.document_status_id == wms_params.DOCUMENT_SENT_ID:
            raise ValidationError({
                'Bon Lancé': 'Ce bon ne peut pas être modifier. Merci de créer un nouveau \
                                      bon pour des nouveaux mouvements'})
        if self.parent.document_status_id != wms_params.get_default_document_status_id():
            raise ValidationError({
                'Bon Confirmé': 'Ce bon ne peut plus être modifier. Merci de créer un nouveau \
                                      bon pour des nouveaux mouvements'})
        elif kwargs.get('editing'):
            raise ValidationError({
                'Interdiction': 'Ce bon ne peut plus être modifier. Merci de créer un nouveau \
                                                  bon pour des nouveaux mouvements'})
        else:
            with transaction.atomic():
                super(TransferDetails, self).save(*args, **kwargs)
                Inventory.objects.create(
                    created_by=self.created_by,
                    content_type=ContentType.objects.get_for_model(self),
                    object_id=self.id,
                    batch=self.batch,
                    quantity=self.quantity,
                    product_status=self.to_status,
                    transaction='Transfer in',
                    emplacement=self.to_emplacement,
                    sent=False
                )
                Inventory.objects.create(
                    created_by=self.created_by,
                    content_type=ContentType.objects.get_for_model(self),
                    object_id=self.id,
                    batch=self.batch,
                    quantity=self.quantity * -1,
                    product_status=self.from_status,
                    transaction='Transfer out',
                    emplacement=self.from_emplacement,
                    sent=False
                )

    def delete(self, *args, **kwargs):
        if self.parent.document_status_id == wms_params.DOCUMENT_SENT_ID:
            raise ValidationError({
                'Bon Lancé': 'Ce bon ne peut pas être modifier. Merci de créer un nouveau \
                                      bon pour des nouveaux mouvements'})
        if self.parent.document_status_id != wms_params.get_default_document_status_id():
            raise ValidationError({
                'Bon Confirmé': 'Ce bon ne peut plus être modifier.'})
        else:
            with transaction.atomic():
                related_mouvement = Inventory.objects.filter(
                    object_id=self.id,
                    content_type=ContentType.objects.get_for_model(self)
                )
                for obj in related_mouvement:
                    obj.delete(*args, **kwargs)
                kwargs['safe'] = False  # Very important do not change !!
                super(TransferDetails, self).delete(*args, **kwargs)

    class Meta:
        permissions = (
            ('admin_transferdetails', 'Can Admin Transfer Details'),
            ('view_transferdetails', 'Can view Transfer Details'),
            ('export_transferdetails', 'Can export Transfer Details'),
        )


# External transfer tables

class ExternalTransferBase(DomainBaseModel):
    from_company = models.ForeignKey(Company, verbose_name='Depuis Entreprise', related_name='from_company',
                                     on_delete=models.PROTECT)
    to_company = models.ForeignKey(Company, verbose_name='Vers Entreprise', on_delete=models.PROTECT,
                                   related_name='to_company')
    document_status = models.ForeignKey(DocumentStatus, verbose_name='Statut du document', on_delete=models.PROTECT,
                                        default=wms_params.get_default_document_status_id)
    flew_type = models.ForeignKey(LogisticFlowType, verbose_name='Type de flux', on_delete=models.PROTECT,
                                  default=wms_params.EXTERNAL_TRANSFER_LOGISTIC_FLEW_ID)
    observation = models.TextField(max_length=400, verbose_name='Observation', blank=True)

    def __str__(self):
        return "{}{}".format(str(self.from_company), str(self.to_company))

    class Meta:
        abstract = True


class ExternalTransferTempo(ExternalTransferBase):
    custom_id_generator = False
    id = models.CharField(max_length=36, primary_key=True)
    from_company = models.ForeignKey(Company, verbose_name='Depuis Entreprise', related_name='from_company_tempo',
                                     on_delete=models.PROTECT)
    to_company = models.ForeignKey(Company, verbose_name='Vers Entreprise', on_delete=models.PROTECT,
                                   related_name='to_company_tempo')

    def commit_transaction(self):
        """
        Transfers the imputed data from temporary tables to final tables
        """
        try:
            with transaction.atomic():
                new_obj = ExternalTransfer.objects.create(
                    created_by=self.created_by,
                    flew_type=self.flew_type,
                    from_company=self.from_company,
                    to_company=self.to_company,
                    document_status=self.document_status,
                    observation=self.observation

                )
                obj_details = WarehouseReservationDetail.objects.filter(parent_id=self.id)
                if obj_details.count() == 0:
                    raise ValidationError(
                        {'Document': 'Pour enregistrer, le document doit contenir au moins une ligne'})
                for obj in obj_details:
                    ExternalTransferDetails.objects.create(
                        created_by=obj.created_by,
                        parent=new_obj,
                        batch=obj.batch,
                        emplacement=obj.from_emplacement,
                        status=obj.from_status,
                        quantity=obj.quantity
                    )
                    Inventory.objects.create(
                        created_by=obj.created_by,
                        content_type=ContentType.objects.get_for_model(new_obj),
                        object_id=new_obj.id,
                        batch=obj.batch,
                        quantity=obj.quantity * -1,
                        product_status=obj.from_status,
                        transaction='Ex. Transfer out',
                        emplacement=obj.from_emplacement
                    )
                obj_details.delete()
                self.delete()

            return new_obj.id

        except Exception as e:
            raise e

    def cancel_transaction(self):
        """
        Cancel the temporary transaction
        """
        try:
            with transaction.atomic():
                self.temporary_details_objects = WarehouseReservationDetail.objects.filter(
                    parent_id=self.id
                )
                self.temporary_details_objects.delete()
                self.delete()
                return self.id
        except Exception as e:
            raise e


class ExternalTransfer(ExternalTransferBase):

    def send_transaction(self, user):
        """
        Transfers the document to execution
        This change the document status from 'Créted' to 'Sent'
        """
        try:
            with transaction.atomic():
                wms_validators.internal_movement_can_be_send(transaction_instance=self)
                obj_details = ExternalTransferDetails.objects.filter(parent_id=self.id)
                if obj_details.count() == 0:
                    raise ValidationError(
                        {'Document': 'Pour lancer un mouvement, le document doit contenir au moins une ligne'})
                self.document_status_id = wms_params.DOCUMENT_SENT_ID
                self.save(user=user)
                for obj in obj_details:
                    related_movement = Inventory.objects.filter(
                        object_id=obj.id,
                        content_type=ContentType.objects.get_for_model(obj)
                    )
                    for movement in related_movement:
                        movement.sent = True
                        movement.save(editing=True, safe=False)

            return self.id

        except Exception as e:
            raise e

    def return_transaction(self, user):
        """
        Return the document from execution.
        This change the document status from 'Sent' to 'Created'
        """
        try:
            if self.document_status_id == wms_params.DOCUMENT_CREATED_ID:
                raise ValidationError(
                    {'Document non lancé': 'Vous puvez modifier ce bon au lieu de l\'annuler'})
            elif self.document_status_id != wms_params.DOCUMENT_SENT_ID:
                raise ValidationError(
                    {'Document Confirmé': 'Pour annuler ce mouvement vous pouver créer un nouveau  mouvement dans le '
                                          'sense inverse'})
            else:
                with transaction.atomic():
                    self.document_status_id = wms_params.DOCUMENT_CREATED_ID
                    self.save(user=user)
                    obj_details = ExternalTransferDetails.objects.filter(parent_id=self.id)
                    for obj in obj_details:
                        related_movement = Inventory.objects.filter(
                            object_id=obj.id,
                            content_type=ContentType.objects.get_for_model(obj)
                        )
                        for movement in related_movement:
                            movement.sent = False
                            movement.save(editing=True, safe=False)

                return self.id

        except Exception as e:
            raise e

    def ship_transaction(self, user):
        """
       Transfers the document to execution
       This change the document status from 'Créted' to 'Sent'
       """
        try:
            with transaction.atomic():
                wms_validators.transaction_can_be_shipped(transaction_instance=self)
                obj_details = ExternalTransferDetails.objects.filter(parent_id=self.id)
                if obj_details.count() == 0:
                    raise ValidationError(
                        {'Document': 'Pour lancer un mouvement, le document doit contenir au moins une ligne'})
                self.document_status_id = wms_params.DOCUMENT_SHIPPED_ID
                self.save(user=user)
            return self.id
        except Exception as e:
            raise e

    def commit_transaction(self):
        """
        Transfers the imputed data from temporary tables to final tables
        """
        try:
            with transaction.atomic():
                obj_details = ExternalTransferDetails.objects.filter(parent_id=self.id)
                if obj_details.count() == 0:
                    raise ValidationError(
                        {'Document': 'Pour confirmer un mouvement, le document doit contenir au moins une ligne'})
                for obj in obj_details:
                    Inventory.objects.filter(
                        object_id=obj.id,
                        content_type=ContentType.objects.get_for_model(obj)
                    ).update(received=True)

            return self.id

        except Exception as e:
            raise e

    def full_clean(self, *args, **kwargs):

        if self.from_company == self.to_company:
            raise ValidationError({
                'Entreprise': 'Vers Entreprise ne peut pas être identique à Depuis Entreprise'})
        super(ExternalTransfer, self).full_clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        if kwargs.get('editing'):
            raise ValidationError({
                'Bon Confirmé': 'Ce bon ne peut plus être modifier.'})
        else:
            super(ExternalTransfer, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.document_status_id != wms_params.get_default_document_status_id():
            raise ValidationError({
                'Bon Confirmé': 'Ce bon ne peut plus être supprimé. Merci de créer un nouveau \
                                      bon pour le un eventuel réadjustement'})
        else:
            with transaction.atomic():
                for obj in ExternalTransfer.objects.filter(parent=self).all():
                    obj.delete(*args, **kwargs)
                super(ExternalTransfer, self).delete(*args, **kwargs)

    class Meta:
        permissions = (
            ('admin_externaltransfer', 'Can Admin Internal Transfer'),
            ('confirm_externaltransfer', 'Can confirm Internal Transfer'),
            ('send_externaltransfer', 'Can Send Internal Transfer'),
            ('return_externaltransfer', 'Can Return Internal Transfer'),
            ('view_externaltransfer', 'Can view Internal Transfers'),
            ('view_all_externaltransfer', 'Can view AllInternal Transfers'),
            ('ship_externaltransfer', 'Can Ship External Transfers'),
            ('receive_externaltransfer', 'Can Receive External Transfers'),
            ('export_externaltransfer', 'Can Export External Transfers'),
            ('import_externaltransfer', 'Can Import External Transfers'),
        )


class ExternalTransferDetails(DomainBaseModel):
    custom_id_generator = False
    id = models.AutoField(primary_key=True, verbose_name='id', editable=False)
    parent = models.ForeignKey(ExternalTransfer, on_delete=models.PROTECT)
    batch = models.ForeignKey(Batch, on_delete=models.PROTECT, verbose_name='Lot')
    emplacement = models.ForeignKey(Emplacement, verbose_name='Emplacement', on_delete=models.PROTECT)
    status = models.ForeignKey(ProductStatus, verbose_name='Statut', on_delete=models.PROTECT)
    quantity = models.PositiveIntegerField(verbose_name='Quantité', validators=[quantity_is_not_zero])

    def save(self, *args, **kwargs):
        if self.parent.document_status_id == wms_params.DOCUMENT_SENT_ID:
            raise ValidationError({
                'Bon Lancé': 'Ce bon ne peut pas être modifier. Merci de créer un nouveau \
                                          bon pour des nouveaux mouvements'})
        if self.parent.document_status_id != wms_params.get_default_document_status_id():
            raise ValidationError({
                'Bon Confirmé': 'Ce bon ne peut plus être modifier. Merci de créer un nouveau \
                                          bon pour des nouveaux mouvements'})
        elif kwargs.get('editing'):
            raise ValidationError({
                'Interdiction': 'Ce bon ne peut plus être modifier. Merci de créer un nouveau \
                                                      bon pour des nouveaux mouvements'})
        else:
            with transaction.atomic():
                super(ExternalTransferDetails, self).save(*args, **kwargs)

                Inventory.objects.create(
                    created_by=self.created_by,
                    content_type=ContentType.objects.get_for_model(self),
                    object_id=self.id,
                    batch=self.batch,
                    quantity=self.quantity * -1,
                    product_status=self.status,
                    transaction='Ext. Transfer out',
                    emplacement=self.emplacement,
                    sent=False
                )

    def delete(self, *args, **kwargs):
        if self.parent.document_status_id == wms_params.DOCUMENT_SENT_ID:
            raise ValidationError({
                'Bon Lancé': 'Ce bon ne peut pas être modifier. Merci de créer un nouveau \
                                          bon pour des nouveaux mouvements'})
        if self.parent.document_status_id != wms_params.get_default_document_status_id():
            raise ValidationError({
                'Bon Confirmé': 'Ce bon ne peut plus être modifier.'})
        else:
            with transaction.atomic():
                related_mouvement = Inventory.objects.filter(
                    object_id=self.id,
                    content_type=ContentType.objects.get_for_model(self)
                )
                for obj in related_mouvement:
                    obj.delete(*args, **kwargs)
                kwargs['safe'] = False  # Very important do not change !!
                super(ExternalTransferDetails, self).delete(*args, **kwargs)

    class Meta:
        permissions = (
            ('admin_externaltransferdetails', 'Can Admin External Transfer Details'),
            ('view_externaltransferdetails', 'Can view External Transfer Details'),
            ('export_externaltransferdetails', 'Can export External Transfer Details'),
        )


#  Workload Management

class Workload(DomainBaseModel):
    """
    Workload calculated from warehouse transactions
    """
    custom_id_generator = False
    reference_table = False
    id = models.AutoField(primary_key=True)
    confirmed_by = models.ForeignKey(
        User, on_delete=models.PROTECT, verbose_name='Confirmé par', editable=False, related_name='confirmed_by_user')
    confirmed_date = models.DateTimeField(auto_now_add=True, verbose_name='Confirmé le')
    total_units = models.PositiveIntegerField(verbose_name='Quantité totale')
    unpacked_units = models.PositiveIntegerField(verbose_name='Quantité en VRAC')
    lines = models.PositiveIntegerField(verbose_name='Lignes')
    complete_packages = models.PositiveIntegerField(verbose_name='Colis complets')
    palletized_packages = models.PositiveIntegerField(verbose_name='Colis palettisé')
    flew_type = models.ForeignKey(LogisticFlowType, verbose_name='Type de flux', on_delete=models.PROTECT)
    content_type = models.ForeignKey(ContentType, on_delete=models.PROTECT)
    object_id = models.CharField(max_length=15)
    origin = GenericForeignKey('content_type', 'object_id')

    class Meta:
        unique_together = ('content_type', 'object_id', 'flew_type', 'deleted_at')
        permissions = (
            ('admin_workload', 'Can Admin Workload'),
            ('view_workload', 'Can view Workload'),
            ('export_workload', 'Can export Workload'),
        )


class WorkloadExecution(DomainBaseModel):
    """
    Relay executors with workload
    """
    custom_id_generator = False
    reference_table = False
    id = models.AutoField(primary_key=True)
    workload = models.ForeignKey(Workload, on_delete=models.CASCADE)
    employee = models.ForeignKey(Employee, on_delete=models.PROTECT)
    group_workforce = models.PositiveSmallIntegerField(verbose_name='Effectif du groupe')

    class Meta:
        permissions = (
            ('admin_workloadexecution', 'Can Admin Workload Execution'),
            ('view_workloadexecution', 'Can view Workload Execution'),
            ('export_workloadexecution', 'Can export Workload Workload Execution'),
        )


class ExecutorTempo(DomainBaseModel):
    custom_id_generator = False
    reference_table = False
    id = models.AutoField(primary_key=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.PROTECT)
    object_id = models.CharField(max_length=15)
    document = GenericForeignKey('content_type', 'object_id')
    employee = models.ForeignKey(Employee, on_delete=models.CASCADE, verbose_name='Employé')

    class Meta:
        unique_together = ('content_type', 'object_id', 'employee', 'deleted_at')
        permissions = (
            ('admin_executortempo', 'Can Admin Workload Execution Tempo'),
            ('view_executortempo', 'Can view Workload Execution Tempo'),
            ('export_executortempo', 'Can export Workload Workload Execution Tempo'),
        )

    def clean(self):
        executors_queryset = ExecutorTempo.objects.filter(content_type=self.content_type,
                                                          object_id=self.object_id)
        if executors_queryset.filter(employee=self.employee).exists():
            raise ValidationError(
                {'Code RH': 'Un employé avec ce code RH est déja ajouté'}
            )
        elif executors_queryset.count() >= 3:
            raise ValidationError(
                {'Exécutants': 'Le nombre d\'exécutants est limité à trois (3) personnes'})

        super(ExecutorTempo, self).clean()

    def delete(self, *args, **kwargs):
        kwargs['safe'] = False
        super(ExecutorTempo, self).delete(*args, **kwargs)

    def save(self, *args, **kwargs):
        kwargs['safe'] = False
        super(ExecutorTempo, self).save(*args, **kwargs)


#  Inventory counting tables

class InventoryCount(DomainBaseModel):
    store = models.ForeignKey(Store, verbose_name='Magasin', on_delete=models.PROTECT)
    counting_date = models.DateField(verbose_name='Date de Comptage')
    document_status = models.ForeignKey(DocumentStatus, verbose_name='Statut du document', on_delete=models.PROTECT,
                                        default=wms_params.get_default_document_status_id)
    observation = models.TextField(max_length=300, blank=True)
    flew_type = models.ForeignKey(LogisticFlowType, verbose_name='Type de flux', on_delete=models.PROTECT,
                                  default=wms_params.INVENTORY_COUNTING_LOGISTIC_FLEW_ID)

    def __str__(self):
        return '{}|{}'.format(self.store, self.counting_date)

    class Meta:
        permissions = (
            ('admin_inventorycount', 'Can Admin inventory counting'),
            ('confirm_inventorycount', 'Can confirm inventory counting'),
            ('view_inventorycount', 'Can view inventory counting'),
            ('view_all_inventorycount', 'Can view all inventory counting'),
            ('export_inventorycount', 'Can export inventory counting'),
        )

    def commit_transaction(self):
        """
        Transfers the imputed data from temporary tables to final tables
        """
        try:
            with transaction.atomic():
                obj_details = InventoryCountDetail.objects.filter(parent=self)
                if obj_details.count() == 0:
                    raise ValidationError({
                        'Zéro Lignes !': 'Vous ne pouvez pas valider un bon avec zéro lignes saisies'})
                for obj in obj_details:
                    Inventory.objects.create(
                        created_by=obj.created_by,
                        content_type=ContentType.objects.get_for_model(self),
                        object_id=obj.id,
                        batch=obj.batch,
                        quantity=obj.quantity,
                        product_status=obj.status,
                        transaction='Inv. count in',
                        emplacement=obj.emplacement,
                        received=True
                    )
            return self.id

        except Exception as e:
            raise e

    def save(self, *args, **kwargs):
        if kwargs.get('editing'):
            if self.document_status_id != wms_params.DOCUMENT_CREATED_ID:
                raise ValidationError({
                    'Bon Confirmé': 'Ce bon ne peut plus être modifier. Merci de créer un nouveau \
                                          bon pour le un eventuel réadjustement'})

            elif self.store_id != InventoryCount.objects.get(id=self.id).store_id:
                raise ValidationError({
                    'Magasin': 'Le magasin ne peux pas être modifier'})

            else:
                super(InventoryCount, self).save(*args, **kwargs)
        else:
            super(InventoryCount, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.document_status_id != wms_params.DOCUMENT_CREATED_ID:
            raise ValidationError({
                'Bon Confirmé': 'Ce bon ne peut plus être supprimé. Merci de créer un nouveau \
                                      bon pour le un eventuel réadjustement'})
        else:
            with transaction.atomic():
                for obj in InventoryCountDetail.objects.filter(parent=self).all():
                    obj.delete(*args, **kwargs)
                super(InventoryCount, self).delete(*args, **kwargs)


class InventoryCountDetail(DomainBaseModel):
    custom_id_generator = False
    id = models.AutoField(primary_key=True)
    parent = models.ForeignKey(InventoryCount, on_delete=models.CASCADE)
    batch = models.ForeignKey(Batch, on_delete=models.PROTECT, verbose_name='Lot')
    emplacement = models.ForeignKey(Emplacement, verbose_name='Emplacement', on_delete=models.PROTECT)
    status = models.ForeignKey(ProductStatus, verbose_name='Vers Statut', on_delete=models.PROTECT)
    quantity = models.PositiveIntegerField(verbose_name='Quantité', validators=[quantity_is_not_zero])

    def full_clean(self, *args, **kwargs):
        if self.emplacement.store != self.parent.store:
            raise ValidationError({'Emplacement': 'L\'emplacement choisi n\'appartient pas au magasin de comptage'})
        else:
            super(InventoryCountDetail, self).full_clean(*args, **kwargs)

    def save(self, *args, **kwargs):
        if self.parent.document_status_id != wms_params.get_default_document_status_id():
            raise ValidationError({
                'Bon Confirmé': 'Ce bon ne peut plus être modifier. Merci de créer un nouveau \
                                      bon pour le un eventuel réadjustement'})
        else:
            super(InventoryCountDetail, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.parent.document_status_id != wms_params.get_default_document_status_id():
            raise ValidationError({
                'Bon Confirmé': 'Ce bon ne peut plus être modifier. Merci de créer un nouveau \
                                      bon pour le un eventuel réadjustement'})
        else:
            super(InventoryCountDetail, self).delete(*args, **kwargs)

    class Meta:
        permissions = (
            ('admin_inventorycountdetail', 'Can Admin Inventory Count Detail'),
            ('view_inventorycountdetail', 'Can view Inventory Count Detail'),
            ('export_inventorycountdetail', 'Can export Inventory Count Detail'),
        )

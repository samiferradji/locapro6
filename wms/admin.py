from django.contrib import admin
from .models import *
from common.admin import AdminBase


class DciAdmin(AdminBase):
    list_display = ['id', 'code', 'designation', 'created_by', 'created_date', 'modified_date']
    search_fields = ['id', 'code', 'designation']


class ProductStatusAdmin(AdminBase):
    list_display = ['id', 'designation', 'created_by', 'created_date', 'modified_date']
    search_fields = ['id', 'designation']


class PharmaceuticalFormAdmin(AdminBase):
    list_display = ['id', 'designation', 'short_designation', 'created_by', 'created_date', 'modified_date']
    search_fields = ['id', 'designation', 'short_designation']


class StorageTypeAdmin(AdminBase):
    list_display = ['id', 'designation', 'created_by', 'created_date', 'modified_date']
    search_fields = ['id', 'designation', 'short_designation']


class StoreAdmin(AdminBase):
    list_display = ['id', 'designation', 'storage_type', 'created_by', 'created_date', 'modified_date']
    search_fields = ['id', 'designation', 'storage_type__designation']


class EmplacementAdmin(AdminBase):
    list_display = ['id', 'emplacement', 'store', 'volume', 'weight', 'storage_type', 'created_by', 'created_date',
                    'modified_date']
    search_fields = ['id', 'emplacement', 'store__designation', 'weight', 'storage_type__designation']


class PharmaceuticalSupplierAdmin(AdminBase):
    list_display = ['id', 'code', 'supplier', 'RC', 'NIF', 'contact', 'telephone', 'address', 'email', 'active',
                    'created_by', 'created_date', 'modified_date']
    search_fields = ['id', 'code', 'supplier', 'RC', 'NIF', 'contact', 'telephone', 'address', 'email', 'active']


class LaboratoryAdmin(AdminBase):
    list_display = ['id', 'designation', 'created_by', 'created_date', 'modified_date']
    search_fields = ['id', 'designation']


class LogisticFlowTypeAdmin(admin.ModelAdmin):
    list_display = ['id', 'designation', 'difficulty_level', 'description', 'point_line_execution', 'point_line_typing',
                    'point_line_control', 'point_sku_execution', 'point_sku_control', 'point_package_execution',
                    'point_package_validation', 'point_palette_execution', 'point_palette_control']
    search_fields = ['id', 'designation', 'difficulty_level', 'description']

    exclude = ['created_by']

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        obj.save()


class FromStoreAuthorisedAdmin(AdminBase):
    list_display = ['id', 'user', 'store', 'created_by', 'created_date', 'modified_date']
    search_fields = ['id', 'user__username', 'store__designation']
    autocomplete_fields = ['user']


class ToStoreAuthorisedAdmin(AdminBase):
    list_display = ['id', 'user', 'store', 'created_by', 'created_date', 'modified_date']
    search_fields = ['id', 'user__username', 'store__designation']
    autocomplete_fields = ['user']


class ProductStatusAuthorisedAdmin(AdminBase):
    list_display = ['id', 'user', 'status', 'created_by', 'created_date', 'modified_date']
    search_fields = ['id', 'user__username', 'status__designation']
    autocomplete_fields = ['user']


class ProductAdmin(AdminBase):
    list_display = [
        'code', 'product', 'dci', 'pharmaceutical_form', 'dosage', 'laboratory', 'conditioning',
        'weight', 'volume', 'package_weight', 'thermal_sensibility', 'psychotropic', 'picking_emplacement',
        'picking_stock_min', 'picking_stock_max', 'principal_stock_min', 'principal_stock_max', 'tva', 'created_by',
        'created_date', 'modified_date', 'active'
    ]
    search_fields = [
        'code', 'product', 'pharmaceutical_form__designation', 'dosage', 'conditioning', 'dci__designation',
        'laboratory__designation', 'weight', 'volume', 'package_weight', 'thermal_sensibility', 'psychotropic',
        'picking_emplacement__emplacement', 'picking_stock_min', 'picking_stock_max', 'principal_stock_min',
        'principal_stock_max', 'tva__tva_rate', 'description',
    ]
    autocomplete_fields = [
        'pharmaceutical_form',
        'dci',
        'laboratory',
        'picking_emplacement'
    ]


class PharmaceuticalInvoiceAdmin(AdminBase):
    list_display = ['id',
                    'supplier',
                    'invoice_number',
                    'invoice_date',
                    'value_ht',
                    'discount_on_invoice',
                    'value_tva',
                    'value_ttc',
                    'created_by',
                    'created_date',
                    ]
    search_fields = ['id',
                     'supplier__supplier',
                     'invoice_number',
                     'invoice_date',
                     'value_ht',
                     'discount_on_invoice',
                     'value_tva',
                     'value_ttc',
                     ]
    autocomplete_fields = ['supplier']


class PurchaseAdmin(AdminBase):
    list_display = ['id',
                    'invoice',
                    'receipt_number',
                    'receipt_date',
                    'document_status',
                    'observation',
                    'created_by',
                    'created_date',
                    ]
    search_fields = ['id',
                     'invoice__invoice_number',
                     'invoice__supplier__supplier',
                     'receipt_number',
                     'receipt_date',
                     'document_status',
                     'observation',
                     ]
    autocomplete_fields = ['invoice']


class PurchaseDetailAdmin(AdminBase):
    list_display = ['parent', 'batch', 'quantity', 'emplacement']
    autocomplete_fields = ['parent', 'emplacement', 'batch']
    list_select_related = ['parent', 'batch']
    list_per_page = 15


class BatchAdmin(AdminBase):
    search_fields = ['batch_number']


admin.site.register(Dci, DciAdmin)
admin.site.register(ProductStatus, ProductStatusAdmin)
admin.site.register(PharmaceuticalForm, PharmaceuticalFormAdmin)
admin.site.register(StorageType, StorageTypeAdmin)
admin.site.register(Store, StoreAdmin)
admin.site.register(Emplacement, EmplacementAdmin)
admin.site.register(PharmaceuticalSupplier, PharmaceuticalSupplierAdmin)
admin.site.register(Laboratory, LaboratoryAdmin)
admin.site.register(LogisticFlowType, LogisticFlowTypeAdmin)
admin.site.register(FromStoreAuthorised, FromStoreAuthorisedAdmin)
admin.site.register(ToStoreAuthorised, ToStoreAuthorisedAdmin)
admin.site.register(ProductStatusAuthorised, ProductStatusAuthorisedAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(PharmaceuticalInvoice, PharmaceuticalInvoiceAdmin)
admin.site.register(Purchase, PurchaseAdmin)
admin.site.register(Batch, BatchAdmin)
admin.site.register(ArchivedBatch)
admin.site.register(Transfer)
admin.site.register(TransferDetails)
